var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

import PropTypes from 'prop-types';
//
import { flexRender, applyHooks, applyPropHooks, mergeProps } from '../utils';

import { useTableState } from './useTableState';

var renderErr = 'You must specify a render "type". This could be "Header", "Filter", or any other custom renderers you have set on your column.';

var propTypes = {
  // General
  data: PropTypes.any,
  columns: PropTypes.arrayOf(PropTypes.shape({
    aggregate: PropTypes.func,
    filterFn: PropTypes.func,
    filterAll: PropTypes.bool,
    sortByFn: PropTypes.func,
    resolvedDefaultSortDesc: PropTypes.bool,
    canSortBy: PropTypes.bool,
    canGroupBy: PropTypes.bool,
    Cell: PropTypes.any,
    Header: PropTypes.any,
    Filter: PropTypes.any
  })),

  filterFn: PropTypes.func,
  sortByFn: PropTypes.func,
  orderByFn: PropTypes.func,
  groupByFn: PropTypes.func,

  manualGrouping: PropTypes.bool,
  manualFilters: PropTypes.bool,
  manualSorting: PropTypes.bool,

  defaultSortDesc: PropTypes.bool,
  disableMultiSort: PropTypes.bool,
  subRowsKey: PropTypes.string,
  expandedKey: PropTypes.string,
  userAggregations: PropTypes.object,

  debug: PropTypes.bool
};

export var useTable = function useTable(props) {
  for (var _len = arguments.length, plugins = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    plugins[_key - 1] = arguments[_key];
  }

  // Validate props
  PropTypes.checkPropTypes(propTypes, props, 'property', 'useTable');

  // Destructure props
  var _props$data = props.data,
      data = _props$data === undefined ? [] : _props$data,
      userState = props.state,
      debug = props.debug;

  // Always provide a default state

  var defaultState = useTableState();

  // But use the users state if provided
  var state = userState || defaultState;

  // These are hooks that plugins can use right before render
  var hooks = {
    beforeRender: [],
    columns: [],
    headers: [],
    headerGroups: [],
    rows: [],
    row: [],
    renderableRows: [],
    getTableProps: [],
    getRowProps: [],
    getHeaderRowProps: [],
    getHeaderProps: [],
    getCellProps: []

    // The initial api
  };var api = _extends({}, props, {
    data: data,
    state: state,
    hooks: hooks
  });

  if (debug) console.time('hooks');
  // Loop through plugins to build the api out
  api = plugins.filter(Boolean).reduce(function (prev, next) {
    return next(prev);
  }, api);
  if (debug) console.timeEnd('hooks');

  // Run the beforeRender hook
  if (debug) console.time('hooks.beforeRender');
  applyHooks(api.hooks.beforeRender, undefined, api);
  if (debug) console.timeEnd('hooks.beforeRender');

  if (debug) console.time('hooks.columns');
  api.columns = applyHooks(api.hooks.columns, api.columns, api);
  if (debug) console.timeEnd('hooks.columns');

  if (debug) console.time('hooks.headers');
  api.headers = applyHooks(api.hooks.headers, api.headers, api);
  if (debug) console.timeEnd('hooks.headers');
  [].concat(_toConsumableArray(api.columns), _toConsumableArray(api.headers)).forEach(function (column) {
    // Give columns/headers rendering power
    column.render = function (type) {
      var userProps = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      if (!type) {
        throw new Error(renderErr);
      }
      return flexRender(column[type], _extends({}, api, column, userProps));
    };

    // Give columns/headers getHeaderProps
    column.getHeaderProps = function (props) {
      return mergeProps({
        key: ['header', column.id].join('_')
      }, applyPropHooks(api.hooks.getHeaderProps, column, api), props);
    };
  });

  if (debug) console.time('hooks.headerGroups');
  api.headerGroups = applyHooks(api.hooks.headerGroups, api.headerGroups, api).filter(function (headerGroup, i) {
    // Filter out any headers and headerGroups that don't have visible columns
    headerGroup.headers = headerGroup.headers.filter(function (header) {
      var recurse = function recurse(columns) {
        return columns.filter(function (column) {
          if (column.columns) {
            return recurse(column.columns);
          }
          return column.visible;
        }).length;
      };
      if (header.columns) {
        return recurse(header.columns);
      }
      return header.visible;
    });

    // Give headerGroups getRowProps
    if (headerGroup.headers.length) {
      headerGroup.getRowProps = function () {
        var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        return mergeProps({
          key: ['header' + i].join('_')
        }, applyPropHooks(api.hooks.getHeaderRowProps, headerGroup, api), props);
      };
      return true;
    }

    return false;
  });
  if (debug) console.timeEnd('hooks.headerGroups');

  // Run the rows (this could be a dangerous hook with a ton of data)
  if (debug) console.time('hooks.rows');
  api.rows = applyHooks(api.hooks.rows, api.rows, api);
  if (debug) console.timeEnd('hooks.rows');

  // This function is absolutely necessary and MUST be called on
  // any rows the user wishes to be displayed.
  api.prepareRow = function (row) {
    var index = row.index;

    row.getRowProps = function (props) {
      return mergeProps({ key: ['row', index].join('_') }, applyHooks(api.hooks.getRowProps, row, api), props);
    };

    row.cells = row.cells.filter(function (cell) {
      return cell.column.visible;
    });

    row.cells.forEach(function (cell) {
      if (!cell) {
        return;
      }

      var column = cell.column;


      cell.getCellProps = function (props) {
        var columnPathStr = [index, column.id].join('_');
        return mergeProps({
          key: ['cell', columnPathStr].join('_')
        }, applyPropHooks(api.hooks.getCellProps, cell, api), props);
      };

      cell.render = function (type) {
        var userProps = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        if (!type) {
          throw new Error('You must specify a render "type". This could be "Cell", "Header", "Filter", "Aggregated" or any other custom renderers you have set on your column.');
        }
        return flexRender(column[type], _extends({}, api, cell, userProps));
      };
    });
  };

  api.getTableProps = function (userProps) {
    return mergeProps(applyPropHooks(api.hooks.getTableProps, api), userProps);
  };

  api.getRowProps = function (userProps) {
    return mergeProps(applyPropHooks(api.hooks.getRowProps, api), userProps);
  };

  return api;
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VUYWJsZS5qcyJdLCJuYW1lcyI6WyJQcm9wVHlwZXMiLCJmbGV4UmVuZGVyIiwiYXBwbHlIb29rcyIsImFwcGx5UHJvcEhvb2tzIiwibWVyZ2VQcm9wcyIsInVzZVRhYmxlU3RhdGUiLCJyZW5kZXJFcnIiLCJwcm9wVHlwZXMiLCJkYXRhIiwiYW55IiwiY29sdW1ucyIsImFycmF5T2YiLCJzaGFwZSIsImFnZ3JlZ2F0ZSIsImZ1bmMiLCJmaWx0ZXJGbiIsImZpbHRlckFsbCIsImJvb2wiLCJzb3J0QnlGbiIsInJlc29sdmVkRGVmYXVsdFNvcnREZXNjIiwiY2FuU29ydEJ5IiwiY2FuR3JvdXBCeSIsIkNlbGwiLCJIZWFkZXIiLCJGaWx0ZXIiLCJvcmRlckJ5Rm4iLCJncm91cEJ5Rm4iLCJtYW51YWxHcm91cGluZyIsIm1hbnVhbEZpbHRlcnMiLCJtYW51YWxTb3J0aW5nIiwiZGVmYXVsdFNvcnREZXNjIiwiZGlzYWJsZU11bHRpU29ydCIsInN1YlJvd3NLZXkiLCJzdHJpbmciLCJleHBhbmRlZEtleSIsInVzZXJBZ2dyZWdhdGlvbnMiLCJvYmplY3QiLCJkZWJ1ZyIsInVzZVRhYmxlIiwicHJvcHMiLCJwbHVnaW5zIiwiY2hlY2tQcm9wVHlwZXMiLCJ1c2VyU3RhdGUiLCJzdGF0ZSIsImRlZmF1bHRTdGF0ZSIsImhvb2tzIiwiYmVmb3JlUmVuZGVyIiwiaGVhZGVycyIsImhlYWRlckdyb3VwcyIsInJvd3MiLCJyb3ciLCJyZW5kZXJhYmxlUm93cyIsImdldFRhYmxlUHJvcHMiLCJnZXRSb3dQcm9wcyIsImdldEhlYWRlclJvd1Byb3BzIiwiZ2V0SGVhZGVyUHJvcHMiLCJnZXRDZWxsUHJvcHMiLCJhcGkiLCJjb25zb2xlIiwidGltZSIsImZpbHRlciIsIkJvb2xlYW4iLCJyZWR1Y2UiLCJwcmV2IiwibmV4dCIsInRpbWVFbmQiLCJ1bmRlZmluZWQiLCJmb3JFYWNoIiwiY29sdW1uIiwicmVuZGVyIiwidHlwZSIsInVzZXJQcm9wcyIsIkVycm9yIiwia2V5IiwiaWQiLCJqb2luIiwiaGVhZGVyR3JvdXAiLCJpIiwicmVjdXJzZSIsInZpc2libGUiLCJsZW5ndGgiLCJoZWFkZXIiLCJwcmVwYXJlUm93IiwiaW5kZXgiLCJjZWxscyIsImNlbGwiLCJjb2x1bW5QYXRoU3RyIl0sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBT0EsU0FBUCxNQUFzQixZQUF0QjtBQUNBO0FBQ0EsU0FBU0MsVUFBVCxFQUFxQkMsVUFBckIsRUFBaUNDLGNBQWpDLEVBQWlEQyxVQUFqRCxRQUFtRSxVQUFuRTs7QUFFQSxTQUFTQyxhQUFULFFBQThCLGlCQUE5Qjs7QUFFQSxJQUFNQyxZQUNKLGdJQURGOztBQUdBLElBQU1DLFlBQVk7QUFDaEI7QUFDQUMsUUFBTVIsVUFBVVMsR0FGQTtBQUdoQkMsV0FBU1YsVUFBVVcsT0FBVixDQUNQWCxVQUFVWSxLQUFWLENBQWdCO0FBQ2RDLGVBQVdiLFVBQVVjLElBRFA7QUFFZEMsY0FBVWYsVUFBVWMsSUFGTjtBQUdkRSxlQUFXaEIsVUFBVWlCLElBSFA7QUFJZEMsY0FBVWxCLFVBQVVjLElBSk47QUFLZEssNkJBQXlCbkIsVUFBVWlCLElBTHJCO0FBTWRHLGVBQVdwQixVQUFVaUIsSUFOUDtBQU9kSSxnQkFBWXJCLFVBQVVpQixJQVBSO0FBUWRLLFVBQU10QixVQUFVUyxHQVJGO0FBU2RjLFlBQVF2QixVQUFVUyxHQVRKO0FBVWRlLFlBQVF4QixVQUFVUztBQVZKLEdBQWhCLENBRE8sQ0FITzs7QUFrQmhCTSxZQUFVZixVQUFVYyxJQWxCSjtBQW1CaEJJLFlBQVVsQixVQUFVYyxJQW5CSjtBQW9CaEJXLGFBQVd6QixVQUFVYyxJQXBCTDtBQXFCaEJZLGFBQVcxQixVQUFVYyxJQXJCTDs7QUF1QmhCYSxrQkFBZ0IzQixVQUFVaUIsSUF2QlY7QUF3QmhCVyxpQkFBZTVCLFVBQVVpQixJQXhCVDtBQXlCaEJZLGlCQUFlN0IsVUFBVWlCLElBekJUOztBQTJCaEJhLG1CQUFpQjlCLFVBQVVpQixJQTNCWDtBQTRCaEJjLG9CQUFrQi9CLFVBQVVpQixJQTVCWjtBQTZCaEJlLGNBQVloQyxVQUFVaUMsTUE3Qk47QUE4QmhCQyxlQUFhbEMsVUFBVWlDLE1BOUJQO0FBK0JoQkUsb0JBQWtCbkMsVUFBVW9DLE1BL0JaOztBQWlDaEJDLFNBQU9yQyxVQUFVaUI7QUFqQ0QsQ0FBbEI7O0FBb0NBLE9BQU8sSUFBTXFCLFdBQVcsU0FBWEEsUUFBVyxDQUFDQyxLQUFELEVBQXVCO0FBQUEsb0NBQVpDLE9BQVk7QUFBWkEsV0FBWTtBQUFBOztBQUM3QztBQUNBeEMsWUFBVXlDLGNBQVYsQ0FBeUJsQyxTQUF6QixFQUFvQ2dDLEtBQXBDLEVBQTJDLFVBQTNDLEVBQXVELFVBQXZEOztBQUVBO0FBSjZDLG9CQUtFQSxLQUxGLENBS3JDL0IsSUFMcUM7QUFBQSxNQUtyQ0EsSUFMcUMsK0JBSzlCLEVBTDhCO0FBQUEsTUFLbkJrQyxTQUxtQixHQUtFSCxLQUxGLENBSzFCSSxLQUwwQjtBQUFBLE1BS1JOLEtBTFEsR0FLRUUsS0FMRixDQUtSRixLQUxROztBQU83Qzs7QUFDQSxNQUFNTyxlQUFldkMsZUFBckI7O0FBRUE7QUFDQSxNQUFNc0MsUUFBUUQsYUFBYUUsWUFBM0I7O0FBRUE7QUFDQSxNQUFNQyxRQUFRO0FBQ1pDLGtCQUFjLEVBREY7QUFFWnBDLGFBQVMsRUFGRztBQUdacUMsYUFBUyxFQUhHO0FBSVpDLGtCQUFjLEVBSkY7QUFLWkMsVUFBTSxFQUxNO0FBTVpDLFNBQUssRUFOTztBQU9aQyxvQkFBZ0IsRUFQSjtBQVFaQyxtQkFBZSxFQVJIO0FBU1pDLGlCQUFhLEVBVEQ7QUFVWkMsdUJBQW1CLEVBVlA7QUFXWkMsb0JBQWdCLEVBWEo7QUFZWkMsa0JBQWM7O0FBR2hCO0FBZmMsR0FBZCxDQWdCQSxJQUFJQyxtQkFDQ2xCLEtBREQ7QUFFRi9CLGNBRkU7QUFHRm1DLGdCQUhFO0FBSUZFO0FBSkUsSUFBSjs7QUFPQSxNQUFJUixLQUFKLEVBQVdxQixRQUFRQyxJQUFSLENBQWEsT0FBYjtBQUNYO0FBQ0FGLFFBQU1qQixRQUFRb0IsTUFBUixDQUFlQyxPQUFmLEVBQXdCQyxNQUF4QixDQUErQixVQUFDQyxJQUFELEVBQU9DLElBQVA7QUFBQSxXQUFnQkEsS0FBS0QsSUFBTCxDQUFoQjtBQUFBLEdBQS9CLEVBQTJETixHQUEzRCxDQUFOO0FBQ0EsTUFBSXBCLEtBQUosRUFBV3FCLFFBQVFPLE9BQVIsQ0FBZ0IsT0FBaEI7O0FBRVg7QUFDQSxNQUFJNUIsS0FBSixFQUFXcUIsUUFBUUMsSUFBUixDQUFhLG9CQUFiO0FBQ1h6RCxhQUFXdUQsSUFBSVosS0FBSixDQUFVQyxZQUFyQixFQUFtQ29CLFNBQW5DLEVBQThDVCxHQUE5QztBQUNBLE1BQUlwQixLQUFKLEVBQVdxQixRQUFRTyxPQUFSLENBQWdCLG9CQUFoQjs7QUFFWCxNQUFJNUIsS0FBSixFQUFXcUIsUUFBUUMsSUFBUixDQUFhLGVBQWI7QUFDWEYsTUFBSS9DLE9BQUosR0FBY1IsV0FBV3VELElBQUlaLEtBQUosQ0FBVW5DLE9BQXJCLEVBQThCK0MsSUFBSS9DLE9BQWxDLEVBQTJDK0MsR0FBM0MsQ0FBZDtBQUNBLE1BQUlwQixLQUFKLEVBQVdxQixRQUFRTyxPQUFSLENBQWdCLGVBQWhCOztBQUVYLE1BQUk1QixLQUFKLEVBQVdxQixRQUFRQyxJQUFSLENBQWEsZUFBYjtBQUNYRixNQUFJVixPQUFKLEdBQWM3QyxXQUFXdUQsSUFBSVosS0FBSixDQUFVRSxPQUFyQixFQUE4QlUsSUFBSVYsT0FBbEMsRUFBMkNVLEdBQTNDLENBQWQ7QUFDQSxNQUFJcEIsS0FBSixFQUFXcUIsUUFBUU8sT0FBUixDQUFnQixlQUFoQjtBQUNYLCtCQUFJUixJQUFJL0MsT0FBUixzQkFBb0IrQyxJQUFJVixPQUF4QixHQUFpQ29CLE9BQWpDLENBQXlDLGtCQUFVO0FBQ2pEO0FBQ0FDLFdBQU9DLE1BQVAsR0FBZ0IsVUFBQ0MsSUFBRCxFQUEwQjtBQUFBLFVBQW5CQyxTQUFtQix1RUFBUCxFQUFPOztBQUN4QyxVQUFJLENBQUNELElBQUwsRUFBVztBQUNULGNBQU0sSUFBSUUsS0FBSixDQUFVbEUsU0FBVixDQUFOO0FBQ0Q7QUFDRCxhQUFPTCxXQUFXbUUsT0FBT0UsSUFBUCxDQUFYLGVBQ0ZiLEdBREUsRUFFRlcsTUFGRSxFQUdGRyxTQUhFLEVBQVA7QUFLRCxLQVREOztBQVdBO0FBQ0FILFdBQU9iLGNBQVAsR0FBd0I7QUFBQSxhQUN0Qm5ELFdBQ0U7QUFDRXFFLGFBQUssQ0FBQyxRQUFELEVBQVdMLE9BQU9NLEVBQWxCLEVBQXNCQyxJQUF0QixDQUEyQixHQUEzQjtBQURQLE9BREYsRUFJRXhFLGVBQWVzRCxJQUFJWixLQUFKLENBQVVVLGNBQXpCLEVBQXlDYSxNQUF6QyxFQUFpRFgsR0FBakQsQ0FKRixFQUtFbEIsS0FMRixDQURzQjtBQUFBLEtBQXhCO0FBUUQsR0F0QkQ7O0FBd0JBLE1BQUlGLEtBQUosRUFBV3FCLFFBQVFDLElBQVIsQ0FBYSxvQkFBYjtBQUNYRixNQUFJVCxZQUFKLEdBQW1COUMsV0FBV3VELElBQUlaLEtBQUosQ0FBVUcsWUFBckIsRUFBbUNTLElBQUlULFlBQXZDLEVBQXFEUyxHQUFyRCxFQUEwREcsTUFBMUQsQ0FDakIsVUFBQ2dCLFdBQUQsRUFBY0MsQ0FBZCxFQUFvQjtBQUNsQjtBQUNBRCxnQkFBWTdCLE9BQVosR0FBc0I2QixZQUFZN0IsT0FBWixDQUFvQmEsTUFBcEIsQ0FBMkIsa0JBQVU7QUFDekQsVUFBTWtCLFVBQVUsU0FBVkEsT0FBVTtBQUFBLGVBQ2RwRSxRQUFRa0QsTUFBUixDQUFlLGtCQUFVO0FBQ3ZCLGNBQUlRLE9BQU8xRCxPQUFYLEVBQW9CO0FBQ2xCLG1CQUFPb0UsUUFBUVYsT0FBTzFELE9BQWYsQ0FBUDtBQUNEO0FBQ0QsaUJBQU8wRCxPQUFPVyxPQUFkO0FBQ0QsU0FMRCxFQUtHQyxNQU5XO0FBQUEsT0FBaEI7QUFPQSxVQUFJQyxPQUFPdkUsT0FBWCxFQUFvQjtBQUNsQixlQUFPb0UsUUFBUUcsT0FBT3ZFLE9BQWYsQ0FBUDtBQUNEO0FBQ0QsYUFBT3VFLE9BQU9GLE9BQWQ7QUFDRCxLQVpxQixDQUF0Qjs7QUFjQTtBQUNBLFFBQUlILFlBQVk3QixPQUFaLENBQW9CaUMsTUFBeEIsRUFBZ0M7QUFDOUJKLGtCQUFZdkIsV0FBWixHQUEwQjtBQUFBLFlBQUNkLEtBQUQsdUVBQVMsRUFBVDtBQUFBLGVBQ3hCbkMsV0FDRTtBQUNFcUUsZUFBSyxZQUFVSSxDQUFWLEVBQWVGLElBQWYsQ0FBb0IsR0FBcEI7QUFEUCxTQURGLEVBSUV4RSxlQUFlc0QsSUFBSVosS0FBSixDQUFVUyxpQkFBekIsRUFBNENzQixXQUE1QyxFQUF5RG5CLEdBQXpELENBSkYsRUFLRWxCLEtBTEYsQ0FEd0I7QUFBQSxPQUExQjtBQVFBLGFBQU8sSUFBUDtBQUNEOztBQUVELFdBQU8sS0FBUDtBQUNELEdBL0JnQixDQUFuQjtBQWlDQSxNQUFJRixLQUFKLEVBQVdxQixRQUFRTyxPQUFSLENBQWdCLG9CQUFoQjs7QUFFWDtBQUNBLE1BQUk1QixLQUFKLEVBQVdxQixRQUFRQyxJQUFSLENBQWEsWUFBYjtBQUNYRixNQUFJUixJQUFKLEdBQVcvQyxXQUFXdUQsSUFBSVosS0FBSixDQUFVSSxJQUFyQixFQUEyQlEsSUFBSVIsSUFBL0IsRUFBcUNRLEdBQXJDLENBQVg7QUFDQSxNQUFJcEIsS0FBSixFQUFXcUIsUUFBUU8sT0FBUixDQUFnQixZQUFoQjs7QUFFWDtBQUNBO0FBQ0FSLE1BQUl5QixVQUFKLEdBQWlCLGVBQU87QUFBQSxRQUNkQyxLQURjLEdBQ0pqQyxHQURJLENBQ2RpQyxLQURjOztBQUV0QmpDLFFBQUlHLFdBQUosR0FBa0I7QUFBQSxhQUNoQmpELFdBQ0UsRUFBRXFFLEtBQUssQ0FBQyxLQUFELEVBQVFVLEtBQVIsRUFBZVIsSUFBZixDQUFvQixHQUFwQixDQUFQLEVBREYsRUFFRXpFLFdBQVd1RCxJQUFJWixLQUFKLENBQVVRLFdBQXJCLEVBQWtDSCxHQUFsQyxFQUF1Q08sR0FBdkMsQ0FGRixFQUdFbEIsS0FIRixDQURnQjtBQUFBLEtBQWxCOztBQU9BVyxRQUFJa0MsS0FBSixHQUFZbEMsSUFBSWtDLEtBQUosQ0FBVXhCLE1BQVYsQ0FBaUI7QUFBQSxhQUFReUIsS0FBS2pCLE1BQUwsQ0FBWVcsT0FBcEI7QUFBQSxLQUFqQixDQUFaOztBQUVBN0IsUUFBSWtDLEtBQUosQ0FBVWpCLE9BQVYsQ0FBa0IsZ0JBQVE7QUFDeEIsVUFBSSxDQUFDa0IsSUFBTCxFQUFXO0FBQ1Q7QUFDRDs7QUFIdUIsVUFLaEJqQixNQUxnQixHQUtMaUIsSUFMSyxDQUtoQmpCLE1BTGdCOzs7QUFPeEJpQixXQUFLN0IsWUFBTCxHQUFvQixpQkFBUztBQUMzQixZQUFNOEIsZ0JBQWdCLENBQUNILEtBQUQsRUFBUWYsT0FBT00sRUFBZixFQUFtQkMsSUFBbkIsQ0FBd0IsR0FBeEIsQ0FBdEI7QUFDQSxlQUFPdkUsV0FDTDtBQUNFcUUsZUFBSyxDQUFDLE1BQUQsRUFBU2EsYUFBVCxFQUF3QlgsSUFBeEIsQ0FBNkIsR0FBN0I7QUFEUCxTQURLLEVBSUx4RSxlQUFlc0QsSUFBSVosS0FBSixDQUFVVyxZQUF6QixFQUF1QzZCLElBQXZDLEVBQTZDNUIsR0FBN0MsQ0FKSyxFQUtMbEIsS0FMSyxDQUFQO0FBT0QsT0FURDs7QUFXQThDLFdBQUtoQixNQUFMLEdBQWMsVUFBQ0MsSUFBRCxFQUEwQjtBQUFBLFlBQW5CQyxTQUFtQix1RUFBUCxFQUFPOztBQUN0QyxZQUFJLENBQUNELElBQUwsRUFBVztBQUNULGdCQUFNLElBQUlFLEtBQUosQ0FDSixxSkFESSxDQUFOO0FBR0Q7QUFDRCxlQUFPdkUsV0FBV21FLE9BQU9FLElBQVAsQ0FBWCxlQUNGYixHQURFLEVBRUY0QixJQUZFLEVBR0ZkLFNBSEUsRUFBUDtBQUtELE9BWEQ7QUFZRCxLQTlCRDtBQStCRCxHQTFDRDs7QUE0Q0FkLE1BQUlMLGFBQUosR0FBb0I7QUFBQSxXQUNsQmhELFdBQVdELGVBQWVzRCxJQUFJWixLQUFKLENBQVVPLGFBQXpCLEVBQXdDSyxHQUF4QyxDQUFYLEVBQXlEYyxTQUF6RCxDQURrQjtBQUFBLEdBQXBCOztBQUdBZCxNQUFJSixXQUFKLEdBQWtCO0FBQUEsV0FBYWpELFdBQVdELGVBQWVzRCxJQUFJWixLQUFKLENBQVVRLFdBQXpCLEVBQXNDSSxHQUF0QyxDQUFYLEVBQXVEYyxTQUF2RCxDQUFiO0FBQUEsR0FBbEI7O0FBRUEsU0FBT2QsR0FBUDtBQUNELENBM0tNIiwiZmlsZSI6InVzZVRhYmxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFByb3BUeXBlcyBmcm9tICdwcm9wLXR5cGVzJ1xuLy9cbmltcG9ydCB7IGZsZXhSZW5kZXIsIGFwcGx5SG9va3MsIGFwcGx5UHJvcEhvb2tzLCBtZXJnZVByb3BzIH0gZnJvbSAnLi4vdXRpbHMnXG5cbmltcG9ydCB7IHVzZVRhYmxlU3RhdGUgfSBmcm9tICcuL3VzZVRhYmxlU3RhdGUnXG5cbmNvbnN0IHJlbmRlckVyciA9XG4gICdZb3UgbXVzdCBzcGVjaWZ5IGEgcmVuZGVyIFwidHlwZVwiLiBUaGlzIGNvdWxkIGJlIFwiSGVhZGVyXCIsIFwiRmlsdGVyXCIsIG9yIGFueSBvdGhlciBjdXN0b20gcmVuZGVyZXJzIHlvdSBoYXZlIHNldCBvbiB5b3VyIGNvbHVtbi4nXG5cbmNvbnN0IHByb3BUeXBlcyA9IHtcbiAgLy8gR2VuZXJhbFxuICBkYXRhOiBQcm9wVHlwZXMuYW55LFxuICBjb2x1bW5zOiBQcm9wVHlwZXMuYXJyYXlPZihcbiAgICBQcm9wVHlwZXMuc2hhcGUoe1xuICAgICAgYWdncmVnYXRlOiBQcm9wVHlwZXMuZnVuYyxcbiAgICAgIGZpbHRlckZuOiBQcm9wVHlwZXMuZnVuYyxcbiAgICAgIGZpbHRlckFsbDogUHJvcFR5cGVzLmJvb2wsXG4gICAgICBzb3J0QnlGbjogUHJvcFR5cGVzLmZ1bmMsXG4gICAgICByZXNvbHZlZERlZmF1bHRTb3J0RGVzYzogUHJvcFR5cGVzLmJvb2wsXG4gICAgICBjYW5Tb3J0Qnk6IFByb3BUeXBlcy5ib29sLFxuICAgICAgY2FuR3JvdXBCeTogUHJvcFR5cGVzLmJvb2wsXG4gICAgICBDZWxsOiBQcm9wVHlwZXMuYW55LFxuICAgICAgSGVhZGVyOiBQcm9wVHlwZXMuYW55LFxuICAgICAgRmlsdGVyOiBQcm9wVHlwZXMuYW55LFxuICAgIH0pXG4gICksXG5cbiAgZmlsdGVyRm46IFByb3BUeXBlcy5mdW5jLFxuICBzb3J0QnlGbjogUHJvcFR5cGVzLmZ1bmMsXG4gIG9yZGVyQnlGbjogUHJvcFR5cGVzLmZ1bmMsXG4gIGdyb3VwQnlGbjogUHJvcFR5cGVzLmZ1bmMsXG5cbiAgbWFudWFsR3JvdXBpbmc6IFByb3BUeXBlcy5ib29sLFxuICBtYW51YWxGaWx0ZXJzOiBQcm9wVHlwZXMuYm9vbCxcbiAgbWFudWFsU29ydGluZzogUHJvcFR5cGVzLmJvb2wsXG5cbiAgZGVmYXVsdFNvcnREZXNjOiBQcm9wVHlwZXMuYm9vbCxcbiAgZGlzYWJsZU11bHRpU29ydDogUHJvcFR5cGVzLmJvb2wsXG4gIHN1YlJvd3NLZXk6IFByb3BUeXBlcy5zdHJpbmcsXG4gIGV4cGFuZGVkS2V5OiBQcm9wVHlwZXMuc3RyaW5nLFxuICB1c2VyQWdncmVnYXRpb25zOiBQcm9wVHlwZXMub2JqZWN0LFxuXG4gIGRlYnVnOiBQcm9wVHlwZXMuYm9vbCxcbn1cblxuZXhwb3J0IGNvbnN0IHVzZVRhYmxlID0gKHByb3BzLCAuLi5wbHVnaW5zKSA9PiB7XG4gIC8vIFZhbGlkYXRlIHByb3BzXG4gIFByb3BUeXBlcy5jaGVja1Byb3BUeXBlcyhwcm9wVHlwZXMsIHByb3BzLCAncHJvcGVydHknLCAndXNlVGFibGUnKVxuXG4gIC8vIERlc3RydWN0dXJlIHByb3BzXG4gIGNvbnN0IHsgZGF0YSA9IFtdLCBzdGF0ZTogdXNlclN0YXRlLCBkZWJ1ZyB9ID0gcHJvcHNcblxuICAvLyBBbHdheXMgcHJvdmlkZSBhIGRlZmF1bHQgc3RhdGVcbiAgY29uc3QgZGVmYXVsdFN0YXRlID0gdXNlVGFibGVTdGF0ZSgpXG5cbiAgLy8gQnV0IHVzZSB0aGUgdXNlcnMgc3RhdGUgaWYgcHJvdmlkZWRcbiAgY29uc3Qgc3RhdGUgPSB1c2VyU3RhdGUgfHwgZGVmYXVsdFN0YXRlXG5cbiAgLy8gVGhlc2UgYXJlIGhvb2tzIHRoYXQgcGx1Z2lucyBjYW4gdXNlIHJpZ2h0IGJlZm9yZSByZW5kZXJcbiAgY29uc3QgaG9va3MgPSB7XG4gICAgYmVmb3JlUmVuZGVyOiBbXSxcbiAgICBjb2x1bW5zOiBbXSxcbiAgICBoZWFkZXJzOiBbXSxcbiAgICBoZWFkZXJHcm91cHM6IFtdLFxuICAgIHJvd3M6IFtdLFxuICAgIHJvdzogW10sXG4gICAgcmVuZGVyYWJsZVJvd3M6IFtdLFxuICAgIGdldFRhYmxlUHJvcHM6IFtdLFxuICAgIGdldFJvd1Byb3BzOiBbXSxcbiAgICBnZXRIZWFkZXJSb3dQcm9wczogW10sXG4gICAgZ2V0SGVhZGVyUHJvcHM6IFtdLFxuICAgIGdldENlbGxQcm9wczogW10sXG4gIH1cblxuICAvLyBUaGUgaW5pdGlhbCBhcGlcbiAgbGV0IGFwaSA9IHtcbiAgICAuLi5wcm9wcyxcbiAgICBkYXRhLFxuICAgIHN0YXRlLFxuICAgIGhvb2tzLFxuICB9XG5cbiAgaWYgKGRlYnVnKSBjb25zb2xlLnRpbWUoJ2hvb2tzJylcbiAgLy8gTG9vcCB0aHJvdWdoIHBsdWdpbnMgdG8gYnVpbGQgdGhlIGFwaSBvdXRcbiAgYXBpID0gcGx1Z2lucy5maWx0ZXIoQm9vbGVhbikucmVkdWNlKChwcmV2LCBuZXh0KSA9PiBuZXh0KHByZXYpLCBhcGkpXG4gIGlmIChkZWJ1ZykgY29uc29sZS50aW1lRW5kKCdob29rcycpXG5cbiAgLy8gUnVuIHRoZSBiZWZvcmVSZW5kZXIgaG9va1xuICBpZiAoZGVidWcpIGNvbnNvbGUudGltZSgnaG9va3MuYmVmb3JlUmVuZGVyJylcbiAgYXBwbHlIb29rcyhhcGkuaG9va3MuYmVmb3JlUmVuZGVyLCB1bmRlZmluZWQsIGFwaSlcbiAgaWYgKGRlYnVnKSBjb25zb2xlLnRpbWVFbmQoJ2hvb2tzLmJlZm9yZVJlbmRlcicpXG5cbiAgaWYgKGRlYnVnKSBjb25zb2xlLnRpbWUoJ2hvb2tzLmNvbHVtbnMnKVxuICBhcGkuY29sdW1ucyA9IGFwcGx5SG9va3MoYXBpLmhvb2tzLmNvbHVtbnMsIGFwaS5jb2x1bW5zLCBhcGkpXG4gIGlmIChkZWJ1ZykgY29uc29sZS50aW1lRW5kKCdob29rcy5jb2x1bW5zJylcblxuICBpZiAoZGVidWcpIGNvbnNvbGUudGltZSgnaG9va3MuaGVhZGVycycpXG4gIGFwaS5oZWFkZXJzID0gYXBwbHlIb29rcyhhcGkuaG9va3MuaGVhZGVycywgYXBpLmhlYWRlcnMsIGFwaSlcbiAgaWYgKGRlYnVnKSBjb25zb2xlLnRpbWVFbmQoJ2hvb2tzLmhlYWRlcnMnKTtcbiAgWy4uLmFwaS5jb2x1bW5zLCAuLi5hcGkuaGVhZGVyc10uZm9yRWFjaChjb2x1bW4gPT4ge1xuICAgIC8vIEdpdmUgY29sdW1ucy9oZWFkZXJzIHJlbmRlcmluZyBwb3dlclxuICAgIGNvbHVtbi5yZW5kZXIgPSAodHlwZSwgdXNlclByb3BzID0ge30pID0+IHtcbiAgICAgIGlmICghdHlwZSkge1xuICAgICAgICB0aHJvdyBuZXcgRXJyb3IocmVuZGVyRXJyKVxuICAgICAgfVxuICAgICAgcmV0dXJuIGZsZXhSZW5kZXIoY29sdW1uW3R5cGVdLCB7XG4gICAgICAgIC4uLmFwaSxcbiAgICAgICAgLi4uY29sdW1uLFxuICAgICAgICAuLi51c2VyUHJvcHMsXG4gICAgICB9KVxuICAgIH1cblxuICAgIC8vIEdpdmUgY29sdW1ucy9oZWFkZXJzIGdldEhlYWRlclByb3BzXG4gICAgY29sdW1uLmdldEhlYWRlclByb3BzID0gcHJvcHMgPT5cbiAgICAgIG1lcmdlUHJvcHMoXG4gICAgICAgIHtcbiAgICAgICAgICBrZXk6IFsnaGVhZGVyJywgY29sdW1uLmlkXS5qb2luKCdfJyksXG4gICAgICAgIH0sXG4gICAgICAgIGFwcGx5UHJvcEhvb2tzKGFwaS5ob29rcy5nZXRIZWFkZXJQcm9wcywgY29sdW1uLCBhcGkpLFxuICAgICAgICBwcm9wc1xuICAgICAgKVxuICB9KVxuXG4gIGlmIChkZWJ1ZykgY29uc29sZS50aW1lKCdob29rcy5oZWFkZXJHcm91cHMnKVxuICBhcGkuaGVhZGVyR3JvdXBzID0gYXBwbHlIb29rcyhhcGkuaG9va3MuaGVhZGVyR3JvdXBzLCBhcGkuaGVhZGVyR3JvdXBzLCBhcGkpLmZpbHRlcihcbiAgICAoaGVhZGVyR3JvdXAsIGkpID0+IHtcbiAgICAgIC8vIEZpbHRlciBvdXQgYW55IGhlYWRlcnMgYW5kIGhlYWRlckdyb3VwcyB0aGF0IGRvbid0IGhhdmUgdmlzaWJsZSBjb2x1bW5zXG4gICAgICBoZWFkZXJHcm91cC5oZWFkZXJzID0gaGVhZGVyR3JvdXAuaGVhZGVycy5maWx0ZXIoaGVhZGVyID0+IHtcbiAgICAgICAgY29uc3QgcmVjdXJzZSA9IGNvbHVtbnMgPT5cbiAgICAgICAgICBjb2x1bW5zLmZpbHRlcihjb2x1bW4gPT4ge1xuICAgICAgICAgICAgaWYgKGNvbHVtbi5jb2x1bW5zKSB7XG4gICAgICAgICAgICAgIHJldHVybiByZWN1cnNlKGNvbHVtbi5jb2x1bW5zKVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGNvbHVtbi52aXNpYmxlXG4gICAgICAgICAgfSkubGVuZ3RoXG4gICAgICAgIGlmIChoZWFkZXIuY29sdW1ucykge1xuICAgICAgICAgIHJldHVybiByZWN1cnNlKGhlYWRlci5jb2x1bW5zKVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBoZWFkZXIudmlzaWJsZVxuICAgICAgfSlcblxuICAgICAgLy8gR2l2ZSBoZWFkZXJHcm91cHMgZ2V0Um93UHJvcHNcbiAgICAgIGlmIChoZWFkZXJHcm91cC5oZWFkZXJzLmxlbmd0aCkge1xuICAgICAgICBoZWFkZXJHcm91cC5nZXRSb3dQcm9wcyA9IChwcm9wcyA9IHt9KSA9PlxuICAgICAgICAgIG1lcmdlUHJvcHMoXG4gICAgICAgICAgICB7XG4gICAgICAgICAgICAgIGtleTogW2BoZWFkZXIke2l9YF0uam9pbignXycpLFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGFwcGx5UHJvcEhvb2tzKGFwaS5ob29rcy5nZXRIZWFkZXJSb3dQcm9wcywgaGVhZGVyR3JvdXAsIGFwaSksXG4gICAgICAgICAgICBwcm9wc1xuICAgICAgICAgIClcbiAgICAgICAgcmV0dXJuIHRydWVcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIGZhbHNlXG4gICAgfVxuICApXG4gIGlmIChkZWJ1ZykgY29uc29sZS50aW1lRW5kKCdob29rcy5oZWFkZXJHcm91cHMnKVxuXG4gIC8vIFJ1biB0aGUgcm93cyAodGhpcyBjb3VsZCBiZSBhIGRhbmdlcm91cyBob29rIHdpdGggYSB0b24gb2YgZGF0YSlcbiAgaWYgKGRlYnVnKSBjb25zb2xlLnRpbWUoJ2hvb2tzLnJvd3MnKVxuICBhcGkucm93cyA9IGFwcGx5SG9va3MoYXBpLmhvb2tzLnJvd3MsIGFwaS5yb3dzLCBhcGkpXG4gIGlmIChkZWJ1ZykgY29uc29sZS50aW1lRW5kKCdob29rcy5yb3dzJylcblxuICAvLyBUaGlzIGZ1bmN0aW9uIGlzIGFic29sdXRlbHkgbmVjZXNzYXJ5IGFuZCBNVVNUIGJlIGNhbGxlZCBvblxuICAvLyBhbnkgcm93cyB0aGUgdXNlciB3aXNoZXMgdG8gYmUgZGlzcGxheWVkLlxuICBhcGkucHJlcGFyZVJvdyA9IHJvdyA9PiB7XG4gICAgY29uc3QgeyBpbmRleCB9ID0gcm93XG4gICAgcm93LmdldFJvd1Byb3BzID0gcHJvcHMgPT5cbiAgICAgIG1lcmdlUHJvcHMoXG4gICAgICAgIHsga2V5OiBbJ3JvdycsIGluZGV4XS5qb2luKCdfJykgfSxcbiAgICAgICAgYXBwbHlIb29rcyhhcGkuaG9va3MuZ2V0Um93UHJvcHMsIHJvdywgYXBpKSxcbiAgICAgICAgcHJvcHNcbiAgICAgIClcblxuICAgIHJvdy5jZWxscyA9IHJvdy5jZWxscy5maWx0ZXIoY2VsbCA9PiBjZWxsLmNvbHVtbi52aXNpYmxlKVxuXG4gICAgcm93LmNlbGxzLmZvckVhY2goY2VsbCA9PiB7XG4gICAgICBpZiAoIWNlbGwpIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHsgY29sdW1uIH0gPSBjZWxsXG5cbiAgICAgIGNlbGwuZ2V0Q2VsbFByb3BzID0gcHJvcHMgPT4ge1xuICAgICAgICBjb25zdCBjb2x1bW5QYXRoU3RyID0gW2luZGV4LCBjb2x1bW4uaWRdLmpvaW4oJ18nKVxuICAgICAgICByZXR1cm4gbWVyZ2VQcm9wcyhcbiAgICAgICAgICB7XG4gICAgICAgICAgICBrZXk6IFsnY2VsbCcsIGNvbHVtblBhdGhTdHJdLmpvaW4oJ18nKSxcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFwcGx5UHJvcEhvb2tzKGFwaS5ob29rcy5nZXRDZWxsUHJvcHMsIGNlbGwsIGFwaSksXG4gICAgICAgICAgcHJvcHNcbiAgICAgICAgKVxuICAgICAgfVxuXG4gICAgICBjZWxsLnJlbmRlciA9ICh0eXBlLCB1c2VyUHJvcHMgPSB7fSkgPT4ge1xuICAgICAgICBpZiAoIXR5cGUpIHtcbiAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgICAgICAgICAnWW91IG11c3Qgc3BlY2lmeSBhIHJlbmRlciBcInR5cGVcIi4gVGhpcyBjb3VsZCBiZSBcIkNlbGxcIiwgXCJIZWFkZXJcIiwgXCJGaWx0ZXJcIiwgXCJBZ2dyZWdhdGVkXCIgb3IgYW55IG90aGVyIGN1c3RvbSByZW5kZXJlcnMgeW91IGhhdmUgc2V0IG9uIHlvdXIgY29sdW1uLidcbiAgICAgICAgICApXG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZsZXhSZW5kZXIoY29sdW1uW3R5cGVdLCB7XG4gICAgICAgICAgLi4uYXBpLFxuICAgICAgICAgIC4uLmNlbGwsXG4gICAgICAgICAgLi4udXNlclByb3BzLFxuICAgICAgICB9KVxuICAgICAgfVxuICAgIH0pXG4gIH1cblxuICBhcGkuZ2V0VGFibGVQcm9wcyA9IHVzZXJQcm9wcyA9PlxuICAgIG1lcmdlUHJvcHMoYXBwbHlQcm9wSG9va3MoYXBpLmhvb2tzLmdldFRhYmxlUHJvcHMsIGFwaSksIHVzZXJQcm9wcylcblxuICBhcGkuZ2V0Um93UHJvcHMgPSB1c2VyUHJvcHMgPT4gbWVyZ2VQcm9wcyhhcHBseVByb3BIb29rcyhhcGkuaG9va3MuZ2V0Um93UHJvcHMsIGFwaSksIHVzZXJQcm9wcylcblxuICByZXR1cm4gYXBpXG59XG4iXX0=