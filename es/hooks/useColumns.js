var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

import { useMemo } from "react";

import { getFirstDefined, getBy } from "../utils";

export var useColumns = function useColumns(props) {
  var debug = props.debug,
      userColumns = props.columns,
      _props$state = _slicedToArray(props.state, 1),
      groupBy = _props$state[0].groupBy;

  var _useMemo = useMemo(function () {
    if (debug) console.info("getColumns");

    // Decorate All the columns
    var columnTree = decorateColumnTree(userColumns);

    // Get the flat list of all columns
    var columns = flattenBy(columnTree, "columns");

    columns = [].concat(_toConsumableArray(groupBy.map(function (g) {
      return columns.find(function (col) {
        return col.id === g;
      });
    })), _toConsumableArray(columns.filter(function (col) {
      return !groupBy.includes(col.id);
    })));

    // Get headerGroups
    var headerGroups = makeHeaderGroups(columns, findMaxDepth(columnTree));
    var headers = flattenBy(headerGroups, "headers");

    return {
      columns: columns,
      headerGroups: headerGroups,
      headers: headers
    };
  }, [groupBy, userColumns]),
      columns = _useMemo.columns,
      headerGroups = _useMemo.headerGroups,
      headers = _useMemo.headers;

  return _extends({}, props, {
    columns: columns,
    headerGroups: headerGroups,
    headers: headers
  });

  // Find the depth of the columns
  function findMaxDepth(columns) {
    var depth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

    return columns.reduce(function (prev, curr) {
      if (curr.columns) {
        return Math.max(prev, findMaxDepth(curr.columns, depth + 1));
      }
      return depth;
    }, 0);
  }

  function decorateColumn(column, parent) {
    // First check for string accessor
    var _column = column,
        id = _column.id,
        accessor = _column.accessor,
        Header = _column.Header;


    if (typeof accessor === "string") {
      id = id || accessor;
      var accessorString = accessor;
      accessor = function accessor(row) {
        return getBy(row, accessorString);
      };
    }

    if (!id && typeof Header === "string") {
      id = Header;
    }

    if (!id) {
      // Accessor, but no column id? This is bad.
      console.error(column);
      throw new Error("A column id is required!");
    }

    column = _extends({
      Header: "",
      Cell: function Cell(cell) {
        return cell.value;
      },
      show: true
    }, column, {
      id: id,
      accessor: accessor,
      parent: parent
    });

    return column;
  }

  // Build the visible columns, headers and flat column list
  function decorateColumnTree(columns, parent) {
    var depth = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

    return columns.map(function (column) {
      column = decorateColumn(column, parent);
      if (column.columns) {
        column.columns = decorateColumnTree(column.columns, column, depth + 1);
      }
      return column;
    });
  }

  function flattenBy(columns, childKey) {
    var flatColumns = [];

    var recurse = function recurse(columns) {
      columns.forEach(function (d) {
        if (!d[childKey]) {
          flatColumns.push(d);
        } else {
          recurse(d[childKey]);
        }
      });
    };

    recurse(columns);

    return flatColumns;
  }

  // Build the header groups from the bottom up
  function makeHeaderGroups(columns, maxDepth) {
    var headerGroups = [];

    var removeChildColumns = function removeChildColumns(column) {
      delete column.columns;
      if (column.parent) {
        removeChildColumns(column.parent);
      }
    };
    columns.forEach(removeChildColumns);

    var buildGroup = function buildGroup(columns) {
      var depth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

      var headerGroup = {
        headers: []
      };

      var parentColumns = [];

      var hasParents = columns.some(function (col) {
        return col.parent;
      });

      columns.forEach(function (column) {
        var isFirst = !parentColumns.length;
        var latestParentColumn = [].concat(parentColumns).reverse()[0];

        // If the column has a parent, add it if necessary
        if (column.parent) {
          if (isFirst || latestParentColumn.originalID !== column.parent.id) {
            parentColumns.push(_extends({}, column.parent, {
              originalID: column.parent.id,
              id: [column.parent.id, parentColumns.length].join("_")
            }));
          }
        } else if (hasParents) {
          // If other columns have parents, add a place holder if necessary
          var placeholderColumn = decorateColumn({
            originalID: [column.id, "placeholder", maxDepth - depth].join("_"),
            id: [column.id, "placeholder", maxDepth - depth, parentColumns.length].join("_")
          });
          if (isFirst || latestParentColumn.originalID !== placeholderColumn.originalID) {
            parentColumns.push(placeholderColumn);
          }
        }

        // Establish the new columns[] relationship on the parent
        if (column.parent || hasParents) {
          latestParentColumn = [].concat(parentColumns).reverse()[0];
          latestParentColumn.columns = latestParentColumn.columns || [];
          if (!latestParentColumn.columns.includes(column)) {
            latestParentColumn.columns.push(column);
          }
        }

        headerGroup.headers.push(column);
      });

      headerGroups.push(headerGroup);

      if (parentColumns.length) {
        buildGroup(parentColumns);
      }
    };

    buildGroup(columns);

    return headerGroups.reverse();
  }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VDb2x1bW5zLmpzIl0sIm5hbWVzIjpbInVzZU1lbW8iLCJnZXRGaXJzdERlZmluZWQiLCJnZXRCeSIsInVzZUNvbHVtbnMiLCJkZWJ1ZyIsInByb3BzIiwidXNlckNvbHVtbnMiLCJjb2x1bW5zIiwic3RhdGUiLCJncm91cEJ5IiwiY29uc29sZSIsImluZm8iLCJjb2x1bW5UcmVlIiwiZGVjb3JhdGVDb2x1bW5UcmVlIiwiZmxhdHRlbkJ5IiwibWFwIiwiZmluZCIsImNvbCIsImlkIiwiZyIsImZpbHRlciIsImluY2x1ZGVzIiwiaGVhZGVyR3JvdXBzIiwibWFrZUhlYWRlckdyb3VwcyIsImZpbmRNYXhEZXB0aCIsImhlYWRlcnMiLCJkZXB0aCIsInJlZHVjZSIsInByZXYiLCJjdXJyIiwiTWF0aCIsIm1heCIsImRlY29yYXRlQ29sdW1uIiwiY29sdW1uIiwicGFyZW50IiwiYWNjZXNzb3IiLCJIZWFkZXIiLCJhY2Nlc3NvclN0cmluZyIsInJvdyIsImVycm9yIiwiRXJyb3IiLCJDZWxsIiwiY2VsbCIsInZhbHVlIiwic2hvdyIsImNoaWxkS2V5IiwiZmxhdENvbHVtbnMiLCJyZWN1cnNlIiwiZm9yRWFjaCIsImQiLCJwdXNoIiwibWF4RGVwdGgiLCJyZW1vdmVDaGlsZENvbHVtbnMiLCJidWlsZEdyb3VwIiwiaGVhZGVyR3JvdXAiLCJwYXJlbnRDb2x1bW5zIiwiaGFzUGFyZW50cyIsInNvbWUiLCJpc0ZpcnN0IiwibGVuZ3RoIiwibGF0ZXN0UGFyZW50Q29sdW1uIiwicmV2ZXJzZSIsIm9yaWdpbmFsSUQiLCJqb2luIiwicGxhY2Vob2xkZXJDb2x1bW4iXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLFNBQVNBLE9BQVQsUUFBd0IsT0FBeEI7O0FBRUEsU0FBU0MsZUFBVCxFQUEwQkMsS0FBMUIsUUFBdUMsVUFBdkM7O0FBRUEsT0FBTyxJQUFNQyxhQUFhLFNBQWJBLFVBQWEsUUFBUztBQUFBLE1BRS9CQyxLQUYrQixHQUs3QkMsS0FMNkIsQ0FFL0JELEtBRitCO0FBQUEsTUFHdEJFLFdBSHNCLEdBSzdCRCxLQUw2QixDQUcvQkUsT0FIK0I7QUFBQSxvQ0FLN0JGLEtBTDZCLENBSS9CRyxLQUorQjtBQUFBLE1BSXJCQyxPQUpxQixtQkFJckJBLE9BSnFCOztBQUFBLGlCQU9VVCxRQUN6QyxZQUFNO0FBQ0osUUFBSUksS0FBSixFQUFXTSxRQUFRQyxJQUFSLENBQWEsWUFBYjs7QUFFWDtBQUNBLFFBQUlDLGFBQWFDLG1CQUFtQlAsV0FBbkIsQ0FBakI7O0FBRUE7QUFDQSxRQUFJQyxVQUFVTyxVQUFVRixVQUFWLEVBQXNCLFNBQXRCLENBQWQ7O0FBRUFMLDJDQUNLRSxRQUFRTSxHQUFSLENBQVk7QUFBQSxhQUFLUixRQUFRUyxJQUFSLENBQWE7QUFBQSxlQUFPQyxJQUFJQyxFQUFKLEtBQVdDLENBQWxCO0FBQUEsT0FBYixDQUFMO0FBQUEsS0FBWixDQURMLHNCQUVLWixRQUFRYSxNQUFSLENBQWU7QUFBQSxhQUFPLENBQUNYLFFBQVFZLFFBQVIsQ0FBaUJKLElBQUlDLEVBQXJCLENBQVI7QUFBQSxLQUFmLENBRkw7O0FBS0E7QUFDQSxRQUFNSSxlQUFlQyxpQkFBaUJoQixPQUFqQixFQUEwQmlCLGFBQWFaLFVBQWIsQ0FBMUIsQ0FBckI7QUFDQSxRQUFNYSxVQUFVWCxVQUFVUSxZQUFWLEVBQXdCLFNBQXhCLENBQWhCOztBQUVBLFdBQU87QUFDTGYsc0JBREs7QUFFTGUsZ0NBRks7QUFHTEc7QUFISyxLQUFQO0FBS0QsR0F4QndDLEVBeUJ6QyxDQUFDaEIsT0FBRCxFQUFVSCxXQUFWLENBekJ5QyxDQVBWO0FBQUEsTUFPekJDLE9BUHlCLFlBT3pCQSxPQVB5QjtBQUFBLE1BT2hCZSxZQVBnQixZQU9oQkEsWUFQZ0I7QUFBQSxNQU9GRyxPQVBFLFlBT0ZBLE9BUEU7O0FBbUNqQyxzQkFDS3BCLEtBREw7QUFFRUUsb0JBRkY7QUFHRWUsOEJBSEY7QUFJRUc7QUFKRjs7QUFPQTtBQUNBLFdBQVNELFlBQVQsQ0FBc0JqQixPQUF0QixFQUEwQztBQUFBLFFBQVhtQixLQUFXLHVFQUFILENBQUc7O0FBQ3hDLFdBQU9uQixRQUFRb0IsTUFBUixDQUFlLFVBQUNDLElBQUQsRUFBT0MsSUFBUCxFQUFnQjtBQUNwQyxVQUFJQSxLQUFLdEIsT0FBVCxFQUFrQjtBQUNoQixlQUFPdUIsS0FBS0MsR0FBTCxDQUFTSCxJQUFULEVBQWVKLGFBQWFLLEtBQUt0QixPQUFsQixFQUEyQm1CLFFBQVEsQ0FBbkMsQ0FBZixDQUFQO0FBQ0Q7QUFDRCxhQUFPQSxLQUFQO0FBQ0QsS0FMTSxFQUtKLENBTEksQ0FBUDtBQU1EOztBQUVELFdBQVNNLGNBQVQsQ0FBd0JDLE1BQXhCLEVBQWdDQyxNQUFoQyxFQUF3QztBQUN0QztBQURzQyxrQkFFUEQsTUFGTztBQUFBLFFBRWhDZixFQUZnQyxXQUVoQ0EsRUFGZ0M7QUFBQSxRQUU1QmlCLFFBRjRCLFdBRTVCQSxRQUY0QjtBQUFBLFFBRWxCQyxNQUZrQixXQUVsQkEsTUFGa0I7OztBQUl0QyxRQUFJLE9BQU9ELFFBQVAsS0FBb0IsUUFBeEIsRUFBa0M7QUFDaENqQixXQUFLQSxNQUFNaUIsUUFBWDtBQUNBLFVBQU1FLGlCQUFpQkYsUUFBdkI7QUFDQUEsaUJBQVc7QUFBQSxlQUFPakMsTUFBTW9DLEdBQU4sRUFBV0QsY0FBWCxDQUFQO0FBQUEsT0FBWDtBQUNEOztBQUVELFFBQUksQ0FBQ25CLEVBQUQsSUFBTyxPQUFPa0IsTUFBUCxLQUFrQixRQUE3QixFQUF1QztBQUNyQ2xCLFdBQUtrQixNQUFMO0FBQ0Q7O0FBRUQsUUFBSSxDQUFDbEIsRUFBTCxFQUFTO0FBQ1A7QUFDQVIsY0FBUTZCLEtBQVIsQ0FBY04sTUFBZDtBQUNBLFlBQU0sSUFBSU8sS0FBSixDQUFVLDBCQUFWLENBQU47QUFDRDs7QUFFRFA7QUFDRUcsY0FBUSxFQURWO0FBRUVLLFlBQU07QUFBQSxlQUFRQyxLQUFLQyxLQUFiO0FBQUEsT0FGUjtBQUdFQyxZQUFNO0FBSFIsT0FJS1gsTUFKTDtBQUtFZixZQUxGO0FBTUVpQix3QkFORjtBQU9FRDtBQVBGOztBQVVBLFdBQU9ELE1BQVA7QUFDRDs7QUFFRDtBQUNBLFdBQVNwQixrQkFBVCxDQUE0Qk4sT0FBNUIsRUFBcUMyQixNQUFyQyxFQUF3RDtBQUFBLFFBQVhSLEtBQVcsdUVBQUgsQ0FBRzs7QUFDdEQsV0FBT25CLFFBQVFRLEdBQVIsQ0FBWSxrQkFBVTtBQUMzQmtCLGVBQVNELGVBQWVDLE1BQWYsRUFBdUJDLE1BQXZCLENBQVQ7QUFDQSxVQUFJRCxPQUFPMUIsT0FBWCxFQUFvQjtBQUNsQjBCLGVBQU8xQixPQUFQLEdBQWlCTSxtQkFBbUJvQixPQUFPMUIsT0FBMUIsRUFBbUMwQixNQUFuQyxFQUEyQ1AsUUFBUSxDQUFuRCxDQUFqQjtBQUNEO0FBQ0QsYUFBT08sTUFBUDtBQUNELEtBTk0sQ0FBUDtBQU9EOztBQUVELFdBQVNuQixTQUFULENBQW1CUCxPQUFuQixFQUE0QnNDLFFBQTVCLEVBQXNDO0FBQ3BDLFFBQU1DLGNBQWMsRUFBcEI7O0FBRUEsUUFBTUMsVUFBVSxTQUFWQSxPQUFVLFVBQVc7QUFDekJ4QyxjQUFReUMsT0FBUixDQUFnQixhQUFLO0FBQ25CLFlBQUksQ0FBQ0MsRUFBRUosUUFBRixDQUFMLEVBQWtCO0FBQ2hCQyxzQkFBWUksSUFBWixDQUFpQkQsQ0FBakI7QUFDRCxTQUZELE1BRU87QUFDTEYsa0JBQVFFLEVBQUVKLFFBQUYsQ0FBUjtBQUNEO0FBQ0YsT0FORDtBQU9ELEtBUkQ7O0FBVUFFLFlBQVF4QyxPQUFSOztBQUVBLFdBQU91QyxXQUFQO0FBQ0Q7O0FBRUQ7QUFDQSxXQUFTdkIsZ0JBQVQsQ0FBMEJoQixPQUExQixFQUFtQzRDLFFBQW5DLEVBQTZDO0FBQzNDLFFBQU03QixlQUFlLEVBQXJCOztBQUVBLFFBQU04QixxQkFBcUIsU0FBckJBLGtCQUFxQixTQUFVO0FBQ25DLGFBQU9uQixPQUFPMUIsT0FBZDtBQUNBLFVBQUkwQixPQUFPQyxNQUFYLEVBQW1CO0FBQ2pCa0IsMkJBQW1CbkIsT0FBT0MsTUFBMUI7QUFDRDtBQUNGLEtBTEQ7QUFNQTNCLFlBQVF5QyxPQUFSLENBQWdCSSxrQkFBaEI7O0FBRUEsUUFBTUMsYUFBYSxTQUFiQSxVQUFhLENBQUM5QyxPQUFELEVBQXdCO0FBQUEsVUFBZG1CLEtBQWMsdUVBQU4sQ0FBTTs7QUFDekMsVUFBTTRCLGNBQWM7QUFDbEI3QixpQkFBUztBQURTLE9BQXBCOztBQUlBLFVBQU04QixnQkFBZ0IsRUFBdEI7O0FBRUEsVUFBTUMsYUFBYWpELFFBQVFrRCxJQUFSLENBQWE7QUFBQSxlQUFPeEMsSUFBSWlCLE1BQVg7QUFBQSxPQUFiLENBQW5COztBQUVBM0IsY0FBUXlDLE9BQVIsQ0FBZ0Isa0JBQVU7QUFDeEIsWUFBTVUsVUFBVSxDQUFDSCxjQUFjSSxNQUEvQjtBQUNBLFlBQUlDLHFCQUFxQixVQUFJTCxhQUFKLEVBQW1CTSxPQUFuQixHQUE2QixDQUE3QixDQUF6Qjs7QUFFQTtBQUNBLFlBQUk1QixPQUFPQyxNQUFYLEVBQW1CO0FBQ2pCLGNBQUl3QixXQUFXRSxtQkFBbUJFLFVBQW5CLEtBQWtDN0IsT0FBT0MsTUFBUCxDQUFjaEIsRUFBL0QsRUFBbUU7QUFDakVxQywwQkFBY0wsSUFBZCxjQUNLakIsT0FBT0MsTUFEWjtBQUVFNEIsMEJBQVk3QixPQUFPQyxNQUFQLENBQWNoQixFQUY1QjtBQUdFQSxrQkFBSSxDQUFDZSxPQUFPQyxNQUFQLENBQWNoQixFQUFmLEVBQW1CcUMsY0FBY0ksTUFBakMsRUFBeUNJLElBQXpDLENBQThDLEdBQTlDO0FBSE47QUFLRDtBQUNGLFNBUkQsTUFRTyxJQUFJUCxVQUFKLEVBQWdCO0FBQ3JCO0FBQ0EsY0FBTVEsb0JBQW9CaEMsZUFBZTtBQUN2QzhCLHdCQUFZLENBQUM3QixPQUFPZixFQUFSLEVBQVksYUFBWixFQUEyQmlDLFdBQVd6QixLQUF0QyxFQUE2Q3FDLElBQTdDLENBQWtELEdBQWxELENBRDJCO0FBRXZDN0MsZ0JBQUksQ0FDRmUsT0FBT2YsRUFETCxFQUVGLGFBRkUsRUFHRmlDLFdBQVd6QixLQUhULEVBSUY2QixjQUFjSSxNQUpaLEVBS0ZJLElBTEUsQ0FLRyxHQUxIO0FBRm1DLFdBQWYsQ0FBMUI7QUFTQSxjQUNFTCxXQUNBRSxtQkFBbUJFLFVBQW5CLEtBQWtDRSxrQkFBa0JGLFVBRnRELEVBR0U7QUFDQVAsMEJBQWNMLElBQWQsQ0FBbUJjLGlCQUFuQjtBQUNEO0FBQ0Y7O0FBRUQ7QUFDQSxZQUFJL0IsT0FBT0MsTUFBUCxJQUFpQnNCLFVBQXJCLEVBQWlDO0FBQy9CSSwrQkFBcUIsVUFBSUwsYUFBSixFQUFtQk0sT0FBbkIsR0FBNkIsQ0FBN0IsQ0FBckI7QUFDQUQsNkJBQW1CckQsT0FBbkIsR0FBNkJxRCxtQkFBbUJyRCxPQUFuQixJQUE4QixFQUEzRDtBQUNBLGNBQUksQ0FBQ3FELG1CQUFtQnJELE9BQW5CLENBQTJCYyxRQUEzQixDQUFvQ1ksTUFBcEMsQ0FBTCxFQUFrRDtBQUNoRDJCLCtCQUFtQnJELE9BQW5CLENBQTJCMkMsSUFBM0IsQ0FBZ0NqQixNQUFoQztBQUNEO0FBQ0Y7O0FBRURxQixvQkFBWTdCLE9BQVosQ0FBb0J5QixJQUFwQixDQUF5QmpCLE1BQXpCO0FBQ0QsT0ExQ0Q7O0FBNENBWCxtQkFBYTRCLElBQWIsQ0FBa0JJLFdBQWxCOztBQUVBLFVBQUlDLGNBQWNJLE1BQWxCLEVBQTBCO0FBQ3hCTixtQkFBV0UsYUFBWDtBQUNEO0FBQ0YsS0ExREQ7O0FBNERBRixlQUFXOUMsT0FBWDs7QUFFQSxXQUFPZSxhQUFhdUMsT0FBYixFQUFQO0FBQ0Q7QUFDRixDQTlMTSIsImZpbGUiOiJ1c2VDb2x1bW5zLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlTWVtbyB9IGZyb20gXCJyZWFjdFwiO1xuXG5pbXBvcnQgeyBnZXRGaXJzdERlZmluZWQsIGdldEJ5IH0gZnJvbSBcIi4uL3V0aWxzXCI7XG5cbmV4cG9ydCBjb25zdCB1c2VDb2x1bW5zID0gcHJvcHMgPT4ge1xuICBjb25zdCB7XG4gICAgZGVidWcsXG4gICAgY29sdW1uczogdXNlckNvbHVtbnMsXG4gICAgc3RhdGU6IFt7IGdyb3VwQnkgfV1cbiAgfSA9IHByb3BzO1xuXG4gIGNvbnN0IHsgY29sdW1ucywgaGVhZGVyR3JvdXBzLCBoZWFkZXJzIH0gPSB1c2VNZW1vKFxuICAgICgpID0+IHtcbiAgICAgIGlmIChkZWJ1ZykgY29uc29sZS5pbmZvKFwiZ2V0Q29sdW1uc1wiKTtcblxuICAgICAgLy8gRGVjb3JhdGUgQWxsIHRoZSBjb2x1bW5zXG4gICAgICBsZXQgY29sdW1uVHJlZSA9IGRlY29yYXRlQ29sdW1uVHJlZSh1c2VyQ29sdW1ucyk7XG5cbiAgICAgIC8vIEdldCB0aGUgZmxhdCBsaXN0IG9mIGFsbCBjb2x1bW5zXG4gICAgICBsZXQgY29sdW1ucyA9IGZsYXR0ZW5CeShjb2x1bW5UcmVlLCBcImNvbHVtbnNcIik7XG5cbiAgICAgIGNvbHVtbnMgPSBbXG4gICAgICAgIC4uLmdyb3VwQnkubWFwKGcgPT4gY29sdW1ucy5maW5kKGNvbCA9PiBjb2wuaWQgPT09IGcpKSxcbiAgICAgICAgLi4uY29sdW1ucy5maWx0ZXIoY29sID0+ICFncm91cEJ5LmluY2x1ZGVzKGNvbC5pZCkpXG4gICAgICBdO1xuXG4gICAgICAvLyBHZXQgaGVhZGVyR3JvdXBzXG4gICAgICBjb25zdCBoZWFkZXJHcm91cHMgPSBtYWtlSGVhZGVyR3JvdXBzKGNvbHVtbnMsIGZpbmRNYXhEZXB0aChjb2x1bW5UcmVlKSk7XG4gICAgICBjb25zdCBoZWFkZXJzID0gZmxhdHRlbkJ5KGhlYWRlckdyb3VwcywgXCJoZWFkZXJzXCIpO1xuXG4gICAgICByZXR1cm4ge1xuICAgICAgICBjb2x1bW5zLFxuICAgICAgICBoZWFkZXJHcm91cHMsXG4gICAgICAgIGhlYWRlcnNcbiAgICAgIH07XG4gICAgfSxcbiAgICBbZ3JvdXBCeSwgdXNlckNvbHVtbnNdXG4gICk7XG5cbiAgcmV0dXJuIHtcbiAgICAuLi5wcm9wcyxcbiAgICBjb2x1bW5zLFxuICAgIGhlYWRlckdyb3VwcyxcbiAgICBoZWFkZXJzXG4gIH07XG5cbiAgLy8gRmluZCB0aGUgZGVwdGggb2YgdGhlIGNvbHVtbnNcbiAgZnVuY3Rpb24gZmluZE1heERlcHRoKGNvbHVtbnMsIGRlcHRoID0gMCkge1xuICAgIHJldHVybiBjb2x1bW5zLnJlZHVjZSgocHJldiwgY3VycikgPT4ge1xuICAgICAgaWYgKGN1cnIuY29sdW1ucykge1xuICAgICAgICByZXR1cm4gTWF0aC5tYXgocHJldiwgZmluZE1heERlcHRoKGN1cnIuY29sdW1ucywgZGVwdGggKyAxKSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gZGVwdGg7XG4gICAgfSwgMCk7XG4gIH1cblxuICBmdW5jdGlvbiBkZWNvcmF0ZUNvbHVtbihjb2x1bW4sIHBhcmVudCkge1xuICAgIC8vIEZpcnN0IGNoZWNrIGZvciBzdHJpbmcgYWNjZXNzb3JcbiAgICBsZXQgeyBpZCwgYWNjZXNzb3IsIEhlYWRlciB9ID0gY29sdW1uO1xuXG4gICAgaWYgKHR5cGVvZiBhY2Nlc3NvciA9PT0gXCJzdHJpbmdcIikge1xuICAgICAgaWQgPSBpZCB8fCBhY2Nlc3NvcjtcbiAgICAgIGNvbnN0IGFjY2Vzc29yU3RyaW5nID0gYWNjZXNzb3I7XG4gICAgICBhY2Nlc3NvciA9IHJvdyA9PiBnZXRCeShyb3csIGFjY2Vzc29yU3RyaW5nKTtcbiAgICB9XG5cbiAgICBpZiAoIWlkICYmIHR5cGVvZiBIZWFkZXIgPT09IFwic3RyaW5nXCIpIHtcbiAgICAgIGlkID0gSGVhZGVyO1xuICAgIH1cblxuICAgIGlmICghaWQpIHtcbiAgICAgIC8vIEFjY2Vzc29yLCBidXQgbm8gY29sdW1uIGlkPyBUaGlzIGlzIGJhZC5cbiAgICAgIGNvbnNvbGUuZXJyb3IoY29sdW1uKTtcbiAgICAgIHRocm93IG5ldyBFcnJvcihcIkEgY29sdW1uIGlkIGlzIHJlcXVpcmVkIVwiKTtcbiAgICB9XG5cbiAgICBjb2x1bW4gPSB7XG4gICAgICBIZWFkZXI6IFwiXCIsXG4gICAgICBDZWxsOiBjZWxsID0+IGNlbGwudmFsdWUsXG4gICAgICBzaG93OiB0cnVlLFxuICAgICAgLi4uY29sdW1uLFxuICAgICAgaWQsXG4gICAgICBhY2Nlc3NvcixcbiAgICAgIHBhcmVudFxuICAgIH07XG5cbiAgICByZXR1cm4gY29sdW1uO1xuICB9XG5cbiAgLy8gQnVpbGQgdGhlIHZpc2libGUgY29sdW1ucywgaGVhZGVycyBhbmQgZmxhdCBjb2x1bW4gbGlzdFxuICBmdW5jdGlvbiBkZWNvcmF0ZUNvbHVtblRyZWUoY29sdW1ucywgcGFyZW50LCBkZXB0aCA9IDApIHtcbiAgICByZXR1cm4gY29sdW1ucy5tYXAoY29sdW1uID0+IHtcbiAgICAgIGNvbHVtbiA9IGRlY29yYXRlQ29sdW1uKGNvbHVtbiwgcGFyZW50KTtcbiAgICAgIGlmIChjb2x1bW4uY29sdW1ucykge1xuICAgICAgICBjb2x1bW4uY29sdW1ucyA9IGRlY29yYXRlQ29sdW1uVHJlZShjb2x1bW4uY29sdW1ucywgY29sdW1uLCBkZXB0aCArIDEpO1xuICAgICAgfVxuICAgICAgcmV0dXJuIGNvbHVtbjtcbiAgICB9KTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGZsYXR0ZW5CeShjb2x1bW5zLCBjaGlsZEtleSkge1xuICAgIGNvbnN0IGZsYXRDb2x1bW5zID0gW107XG5cbiAgICBjb25zdCByZWN1cnNlID0gY29sdW1ucyA9PiB7XG4gICAgICBjb2x1bW5zLmZvckVhY2goZCA9PiB7XG4gICAgICAgIGlmICghZFtjaGlsZEtleV0pIHtcbiAgICAgICAgICBmbGF0Q29sdW1ucy5wdXNoKGQpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIHJlY3Vyc2UoZFtjaGlsZEtleV0pO1xuICAgICAgICB9XG4gICAgICB9KTtcbiAgICB9O1xuXG4gICAgcmVjdXJzZShjb2x1bW5zKTtcblxuICAgIHJldHVybiBmbGF0Q29sdW1ucztcbiAgfVxuXG4gIC8vIEJ1aWxkIHRoZSBoZWFkZXIgZ3JvdXBzIGZyb20gdGhlIGJvdHRvbSB1cFxuICBmdW5jdGlvbiBtYWtlSGVhZGVyR3JvdXBzKGNvbHVtbnMsIG1heERlcHRoKSB7XG4gICAgY29uc3QgaGVhZGVyR3JvdXBzID0gW107XG5cbiAgICBjb25zdCByZW1vdmVDaGlsZENvbHVtbnMgPSBjb2x1bW4gPT4ge1xuICAgICAgZGVsZXRlIGNvbHVtbi5jb2x1bW5zO1xuICAgICAgaWYgKGNvbHVtbi5wYXJlbnQpIHtcbiAgICAgICAgcmVtb3ZlQ2hpbGRDb2x1bW5zKGNvbHVtbi5wYXJlbnQpO1xuICAgICAgfVxuICAgIH07XG4gICAgY29sdW1ucy5mb3JFYWNoKHJlbW92ZUNoaWxkQ29sdW1ucyk7XG5cbiAgICBjb25zdCBidWlsZEdyb3VwID0gKGNvbHVtbnMsIGRlcHRoID0gMCkgPT4ge1xuICAgICAgY29uc3QgaGVhZGVyR3JvdXAgPSB7XG4gICAgICAgIGhlYWRlcnM6IFtdXG4gICAgICB9O1xuXG4gICAgICBjb25zdCBwYXJlbnRDb2x1bW5zID0gW107XG5cbiAgICAgIGNvbnN0IGhhc1BhcmVudHMgPSBjb2x1bW5zLnNvbWUoY29sID0+IGNvbC5wYXJlbnQpO1xuXG4gICAgICBjb2x1bW5zLmZvckVhY2goY29sdW1uID0+IHtcbiAgICAgICAgY29uc3QgaXNGaXJzdCA9ICFwYXJlbnRDb2x1bW5zLmxlbmd0aDtcbiAgICAgICAgbGV0IGxhdGVzdFBhcmVudENvbHVtbiA9IFsuLi5wYXJlbnRDb2x1bW5zXS5yZXZlcnNlKClbMF07XG5cbiAgICAgICAgLy8gSWYgdGhlIGNvbHVtbiBoYXMgYSBwYXJlbnQsIGFkZCBpdCBpZiBuZWNlc3NhcnlcbiAgICAgICAgaWYgKGNvbHVtbi5wYXJlbnQpIHtcbiAgICAgICAgICBpZiAoaXNGaXJzdCB8fCBsYXRlc3RQYXJlbnRDb2x1bW4ub3JpZ2luYWxJRCAhPT0gY29sdW1uLnBhcmVudC5pZCkge1xuICAgICAgICAgICAgcGFyZW50Q29sdW1ucy5wdXNoKHtcbiAgICAgICAgICAgICAgLi4uY29sdW1uLnBhcmVudCxcbiAgICAgICAgICAgICAgb3JpZ2luYWxJRDogY29sdW1uLnBhcmVudC5pZCxcbiAgICAgICAgICAgICAgaWQ6IFtjb2x1bW4ucGFyZW50LmlkLCBwYXJlbnRDb2x1bW5zLmxlbmd0aF0uam9pbihcIl9cIilcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIGlmIChoYXNQYXJlbnRzKSB7XG4gICAgICAgICAgLy8gSWYgb3RoZXIgY29sdW1ucyBoYXZlIHBhcmVudHMsIGFkZCBhIHBsYWNlIGhvbGRlciBpZiBuZWNlc3NhcnlcbiAgICAgICAgICBjb25zdCBwbGFjZWhvbGRlckNvbHVtbiA9IGRlY29yYXRlQ29sdW1uKHtcbiAgICAgICAgICAgIG9yaWdpbmFsSUQ6IFtjb2x1bW4uaWQsIFwicGxhY2Vob2xkZXJcIiwgbWF4RGVwdGggLSBkZXB0aF0uam9pbihcIl9cIiksXG4gICAgICAgICAgICBpZDogW1xuICAgICAgICAgICAgICBjb2x1bW4uaWQsXG4gICAgICAgICAgICAgIFwicGxhY2Vob2xkZXJcIixcbiAgICAgICAgICAgICAgbWF4RGVwdGggLSBkZXB0aCxcbiAgICAgICAgICAgICAgcGFyZW50Q29sdW1ucy5sZW5ndGhcbiAgICAgICAgICAgIF0uam9pbihcIl9cIilcbiAgICAgICAgICB9KTtcbiAgICAgICAgICBpZiAoXG4gICAgICAgICAgICBpc0ZpcnN0IHx8XG4gICAgICAgICAgICBsYXRlc3RQYXJlbnRDb2x1bW4ub3JpZ2luYWxJRCAhPT0gcGxhY2Vob2xkZXJDb2x1bW4ub3JpZ2luYWxJRFxuICAgICAgICAgICkge1xuICAgICAgICAgICAgcGFyZW50Q29sdW1ucy5wdXNoKHBsYWNlaG9sZGVyQ29sdW1uKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICAvLyBFc3RhYmxpc2ggdGhlIG5ldyBjb2x1bW5zW10gcmVsYXRpb25zaGlwIG9uIHRoZSBwYXJlbnRcbiAgICAgICAgaWYgKGNvbHVtbi5wYXJlbnQgfHwgaGFzUGFyZW50cykge1xuICAgICAgICAgIGxhdGVzdFBhcmVudENvbHVtbiA9IFsuLi5wYXJlbnRDb2x1bW5zXS5yZXZlcnNlKClbMF07XG4gICAgICAgICAgbGF0ZXN0UGFyZW50Q29sdW1uLmNvbHVtbnMgPSBsYXRlc3RQYXJlbnRDb2x1bW4uY29sdW1ucyB8fCBbXTtcbiAgICAgICAgICBpZiAoIWxhdGVzdFBhcmVudENvbHVtbi5jb2x1bW5zLmluY2x1ZGVzKGNvbHVtbikpIHtcbiAgICAgICAgICAgIGxhdGVzdFBhcmVudENvbHVtbi5jb2x1bW5zLnB1c2goY29sdW1uKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICBoZWFkZXJHcm91cC5oZWFkZXJzLnB1c2goY29sdW1uKTtcbiAgICAgIH0pO1xuXG4gICAgICBoZWFkZXJHcm91cHMucHVzaChoZWFkZXJHcm91cCk7XG5cbiAgICAgIGlmIChwYXJlbnRDb2x1bW5zLmxlbmd0aCkge1xuICAgICAgICBidWlsZEdyb3VwKHBhcmVudENvbHVtbnMpO1xuICAgICAgfVxuICAgIH07XG5cbiAgICBidWlsZEdyb3VwKGNvbHVtbnMpO1xuXG4gICAgcmV0dXJuIGhlYWRlckdyb3Vwcy5yZXZlcnNlKCk7XG4gIH1cbn07XG4iXX0=