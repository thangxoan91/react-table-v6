var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

import { useMemo } from "react";

import { getBy, getFirstDefined, setBy } from "../utils";
import { addActions, actions } from "../actions";
import { defaultState } from "./useTableState";

defaultState.expanded = {};

addActions({
  toggleExpanded: "__toggleExpanded__",
  useExpanded: "__useExpanded__"
});

export var useExpanded = function useExpanded(props) {
  var debug = props.debug,
      columns = props.columns,
      rows = props.rows,
      _props$expandedKey = props.expandedKey,
      expandedKey = _props$expandedKey === undefined ? "expanded" : _props$expandedKey,
      hooks = props.hooks,
      _props$state = _slicedToArray(props.state, 2),
      expanded = _props$state[0].expanded,
      setState = _props$state[1];

  var toggleExpandedByPath = function toggleExpandedByPath(path, set) {
    return setState(function (old) {
      var expanded = old.expanded;

      var existing = getBy(expanded, path);
      set = getFirstDefined(set, !existing);
      return _extends({}, old, {
        expanded: setBy(expanded, path, set)
      });
    }, actions.toggleExpanded);
  };

  hooks.row.push(function (row) {
    var path = row.path;

    row.toggleExpanded = function (set) {
      return toggleExpandedByPath(path, set);
    };
  });

  var expandedRows = useMemo(function () {
    if (debug) console.info("getExpandedRows");

    var expandedRows = [];

    // Here we do some mutation, but it's the last stage in the
    // immutable process so this is safe
    var handleRow = function handleRow(row, index) {
      var depth = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
      var parentPath = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];

      // Compute some final state for the row
      var path = [].concat(_toConsumableArray(parentPath), [index]);

      row.path = path;
      row.depth = depth;

      row.isExpanded = row.original && row.original[expandedKey] || getBy(expanded, path);

      row.cells = columns.map(function (column) {
        var cell = {
          column: column,
          row: row,
          state: null,
          value: row.values[column.id]
        };

        return cell;
      });

      expandedRows.push(row);

      if (row.isExpanded && row.subRows && row.subRows.length) {
        row.subRows.forEach(function (row, i) {
          return handleRow(row, i, depth + 1, path);
        });
      }
    };

    rows.forEach(function (row, i) {
      return handleRow(row, i);
    });

    return expandedRows;
  }, [rows, expanded, columns]);

  var expandedDepth = findExpandedDepth(expanded);

  return _extends({}, props, {
    toggleExpandedByPath: toggleExpandedByPath,
    expandedDepth: expandedDepth,
    rows: expandedRows
  });
};

function findExpandedDepth(obj) {
  var depth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;

  return Object.values(obj).reduce(function (prev, curr) {
    if ((typeof curr === "undefined" ? "undefined" : _typeof(curr)) === "object") {
      return Math.max(prev, findExpandedDepth(curr, depth + 1));
    }
    return depth;
  }, 0);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VFeHBhbmRlZC5qcyJdLCJuYW1lcyI6WyJ1c2VNZW1vIiwiZ2V0QnkiLCJnZXRGaXJzdERlZmluZWQiLCJzZXRCeSIsImFkZEFjdGlvbnMiLCJhY3Rpb25zIiwiZGVmYXVsdFN0YXRlIiwiZXhwYW5kZWQiLCJ0b2dnbGVFeHBhbmRlZCIsInVzZUV4cGFuZGVkIiwiZGVidWciLCJwcm9wcyIsImNvbHVtbnMiLCJyb3dzIiwiZXhwYW5kZWRLZXkiLCJob29rcyIsInN0YXRlIiwic2V0U3RhdGUiLCJ0b2dnbGVFeHBhbmRlZEJ5UGF0aCIsInBhdGgiLCJzZXQiLCJvbGQiLCJleGlzdGluZyIsInJvdyIsInB1c2giLCJleHBhbmRlZFJvd3MiLCJjb25zb2xlIiwiaW5mbyIsImhhbmRsZVJvdyIsImluZGV4IiwiZGVwdGgiLCJwYXJlbnRQYXRoIiwiaXNFeHBhbmRlZCIsIm9yaWdpbmFsIiwiY2VsbHMiLCJtYXAiLCJjZWxsIiwiY29sdW1uIiwidmFsdWUiLCJ2YWx1ZXMiLCJpZCIsInN1YlJvd3MiLCJsZW5ndGgiLCJmb3JFYWNoIiwiaSIsImV4cGFuZGVkRGVwdGgiLCJmaW5kRXhwYW5kZWREZXB0aCIsIm9iaiIsIk9iamVjdCIsInJlZHVjZSIsInByZXYiLCJjdXJyIiwiTWF0aCIsIm1heCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxTQUFTQSxPQUFULFFBQXdCLE9BQXhCOztBQUVBLFNBQVNDLEtBQVQsRUFBZ0JDLGVBQWhCLEVBQWlDQyxLQUFqQyxRQUE4QyxVQUE5QztBQUNBLFNBQVNDLFVBQVQsRUFBcUJDLE9BQXJCLFFBQW9DLFlBQXBDO0FBQ0EsU0FBU0MsWUFBVCxRQUE2QixpQkFBN0I7O0FBRUFBLGFBQWFDLFFBQWIsR0FBd0IsRUFBeEI7O0FBRUFILFdBQVc7QUFDVEksa0JBQWdCLG9CQURQO0FBRVRDLGVBQWE7QUFGSixDQUFYOztBQUtBLE9BQU8sSUFBTUEsY0FBYyxTQUFkQSxXQUFjLFFBQVM7QUFBQSxNQUVoQ0MsS0FGZ0MsR0FROUJDLEtBUjhCLENBRWhDRCxLQUZnQztBQUFBLE1BR2hDRSxPQUhnQyxHQVE5QkQsS0FSOEIsQ0FHaENDLE9BSGdDO0FBQUEsTUFJaENDLElBSmdDLEdBUTlCRixLQVI4QixDQUloQ0UsSUFKZ0M7QUFBQSwyQkFROUJGLEtBUjhCLENBS2hDRyxXQUxnQztBQUFBLE1BS2hDQSxXQUxnQyxzQ0FLbEIsVUFMa0I7QUFBQSxNQU1oQ0MsS0FOZ0MsR0FROUJKLEtBUjhCLENBTWhDSSxLQU5nQztBQUFBLG9DQVE5QkosS0FSOEIsQ0FPaENLLEtBUGdDO0FBQUEsTUFPdEJULFFBUHNCLG1CQU90QkEsUUFQc0I7QUFBQSxNQU9WVSxRQVBVOztBQVVsQyxNQUFNQyx1QkFBdUIsU0FBdkJBLG9CQUF1QixDQUFDQyxJQUFELEVBQU9DLEdBQVAsRUFBZTtBQUMxQyxXQUFPSCxTQUFTLGVBQU87QUFBQSxVQUNiVixRQURhLEdBQ0FjLEdBREEsQ0FDYmQsUUFEYTs7QUFFckIsVUFBTWUsV0FBV3JCLE1BQU1NLFFBQU4sRUFBZ0JZLElBQWhCLENBQWpCO0FBQ0FDLFlBQU1sQixnQkFBZ0JrQixHQUFoQixFQUFxQixDQUFDRSxRQUF0QixDQUFOO0FBQ0EsMEJBQ0tELEdBREw7QUFFRWQsa0JBQVVKLE1BQU1JLFFBQU4sRUFBZ0JZLElBQWhCLEVBQXNCQyxHQUF0QjtBQUZaO0FBSUQsS0FSTSxFQVFKZixRQUFRRyxjQVJKLENBQVA7QUFTRCxHQVZEOztBQVlBTyxRQUFNUSxHQUFOLENBQVVDLElBQVYsQ0FBZSxlQUFPO0FBQUEsUUFDWkwsSUFEWSxHQUNISSxHQURHLENBQ1pKLElBRFk7O0FBRXBCSSxRQUFJZixjQUFKLEdBQXFCO0FBQUEsYUFBT1UscUJBQXFCQyxJQUFyQixFQUEyQkMsR0FBM0IsQ0FBUDtBQUFBLEtBQXJCO0FBQ0QsR0FIRDs7QUFLQSxNQUFNSyxlQUFlekIsUUFDbkIsWUFBTTtBQUNKLFFBQUlVLEtBQUosRUFBV2dCLFFBQVFDLElBQVIsQ0FBYSxpQkFBYjs7QUFFWCxRQUFNRixlQUFlLEVBQXJCOztBQUVBO0FBQ0E7QUFDQSxRQUFNRyxZQUFZLFNBQVpBLFNBQVksQ0FBQ0wsR0FBRCxFQUFNTSxLQUFOLEVBQTRDO0FBQUEsVUFBL0JDLEtBQStCLHVFQUF2QixDQUF1QjtBQUFBLFVBQXBCQyxVQUFvQix1RUFBUCxFQUFPOztBQUM1RDtBQUNBLFVBQU1aLG9DQUFXWSxVQUFYLElBQXVCRixLQUF2QixFQUFOOztBQUVBTixVQUFJSixJQUFKLEdBQVdBLElBQVg7QUFDQUksVUFBSU8sS0FBSixHQUFZQSxLQUFaOztBQUVBUCxVQUFJUyxVQUFKLEdBQ0dULElBQUlVLFFBQUosSUFBZ0JWLElBQUlVLFFBQUosQ0FBYW5CLFdBQWIsQ0FBakIsSUFBK0NiLE1BQU1NLFFBQU4sRUFBZ0JZLElBQWhCLENBRGpEOztBQUdBSSxVQUFJVyxLQUFKLEdBQVl0QixRQUFRdUIsR0FBUixDQUFZLGtCQUFVO0FBQ2hDLFlBQU1DLE9BQU87QUFDWEMsd0JBRFc7QUFFWGQsa0JBRlc7QUFHWFAsaUJBQU8sSUFISTtBQUlYc0IsaUJBQU9mLElBQUlnQixNQUFKLENBQVdGLE9BQU9HLEVBQWxCO0FBSkksU0FBYjs7QUFPQSxlQUFPSixJQUFQO0FBQ0QsT0FUVyxDQUFaOztBQVdBWCxtQkFBYUQsSUFBYixDQUFrQkQsR0FBbEI7O0FBRUEsVUFBSUEsSUFBSVMsVUFBSixJQUFrQlQsSUFBSWtCLE9BQXRCLElBQWlDbEIsSUFBSWtCLE9BQUosQ0FBWUMsTUFBakQsRUFBeUQ7QUFDdkRuQixZQUFJa0IsT0FBSixDQUFZRSxPQUFaLENBQW9CLFVBQUNwQixHQUFELEVBQU1xQixDQUFOO0FBQUEsaUJBQVloQixVQUFVTCxHQUFWLEVBQWVxQixDQUFmLEVBQWtCZCxRQUFRLENBQTFCLEVBQTZCWCxJQUE3QixDQUFaO0FBQUEsU0FBcEI7QUFDRDtBQUNGLEtBMUJEOztBQTRCQU4sU0FBSzhCLE9BQUwsQ0FBYSxVQUFDcEIsR0FBRCxFQUFNcUIsQ0FBTjtBQUFBLGFBQVloQixVQUFVTCxHQUFWLEVBQWVxQixDQUFmLENBQVo7QUFBQSxLQUFiOztBQUVBLFdBQU9uQixZQUFQO0FBQ0QsR0F2Q2tCLEVBd0NuQixDQUFDWixJQUFELEVBQU9OLFFBQVAsRUFBaUJLLE9BQWpCLENBeENtQixDQUFyQjs7QUEyQ0EsTUFBTWlDLGdCQUFnQkMsa0JBQWtCdkMsUUFBbEIsQ0FBdEI7O0FBRUEsc0JBQ0tJLEtBREw7QUFFRU8sOENBRkY7QUFHRTJCLGdDQUhGO0FBSUVoQyxVQUFNWTtBQUpSO0FBTUQsQ0E5RU07O0FBZ0ZQLFNBQVNxQixpQkFBVCxDQUEyQkMsR0FBM0IsRUFBMkM7QUFBQSxNQUFYakIsS0FBVyx1RUFBSCxDQUFHOztBQUN6QyxTQUFPa0IsT0FBT1QsTUFBUCxDQUFjUSxHQUFkLEVBQW1CRSxNQUFuQixDQUEwQixVQUFDQyxJQUFELEVBQU9DLElBQVAsRUFBZ0I7QUFDL0MsUUFBSSxRQUFPQSxJQUFQLHlDQUFPQSxJQUFQLE9BQWdCLFFBQXBCLEVBQThCO0FBQzVCLGFBQU9DLEtBQUtDLEdBQUwsQ0FBU0gsSUFBVCxFQUFlSixrQkFBa0JLLElBQWxCLEVBQXdCckIsUUFBUSxDQUFoQyxDQUFmLENBQVA7QUFDRDtBQUNELFdBQU9BLEtBQVA7QUFDRCxHQUxNLEVBS0osQ0FMSSxDQUFQO0FBTUQiLCJmaWxlIjoidXNlRXhwYW5kZWQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VNZW1vIH0gZnJvbSBcInJlYWN0XCI7XG5cbmltcG9ydCB7IGdldEJ5LCBnZXRGaXJzdERlZmluZWQsIHNldEJ5IH0gZnJvbSBcIi4uL3V0aWxzXCI7XG5pbXBvcnQgeyBhZGRBY3Rpb25zLCBhY3Rpb25zIH0gZnJvbSBcIi4uL2FjdGlvbnNcIjtcbmltcG9ydCB7IGRlZmF1bHRTdGF0ZSB9IGZyb20gXCIuL3VzZVRhYmxlU3RhdGVcIjtcblxuZGVmYXVsdFN0YXRlLmV4cGFuZGVkID0ge307XG5cbmFkZEFjdGlvbnMoe1xuICB0b2dnbGVFeHBhbmRlZDogXCJfX3RvZ2dsZUV4cGFuZGVkX19cIixcbiAgdXNlRXhwYW5kZWQ6IFwiX191c2VFeHBhbmRlZF9fXCJcbn0pO1xuXG5leHBvcnQgY29uc3QgdXNlRXhwYW5kZWQgPSBwcm9wcyA9PiB7XG4gIGNvbnN0IHtcbiAgICBkZWJ1ZyxcbiAgICBjb2x1bW5zLFxuICAgIHJvd3MsXG4gICAgZXhwYW5kZWRLZXkgPSBcImV4cGFuZGVkXCIsXG4gICAgaG9va3MsXG4gICAgc3RhdGU6IFt7IGV4cGFuZGVkIH0sIHNldFN0YXRlXVxuICB9ID0gcHJvcHM7XG5cbiAgY29uc3QgdG9nZ2xlRXhwYW5kZWRCeVBhdGggPSAocGF0aCwgc2V0KSA9PiB7XG4gICAgcmV0dXJuIHNldFN0YXRlKG9sZCA9PiB7XG4gICAgICBjb25zdCB7IGV4cGFuZGVkIH0gPSBvbGQ7XG4gICAgICBjb25zdCBleGlzdGluZyA9IGdldEJ5KGV4cGFuZGVkLCBwYXRoKTtcbiAgICAgIHNldCA9IGdldEZpcnN0RGVmaW5lZChzZXQsICFleGlzdGluZyk7XG4gICAgICByZXR1cm4ge1xuICAgICAgICAuLi5vbGQsXG4gICAgICAgIGV4cGFuZGVkOiBzZXRCeShleHBhbmRlZCwgcGF0aCwgc2V0KVxuICAgICAgfTtcbiAgICB9LCBhY3Rpb25zLnRvZ2dsZUV4cGFuZGVkKTtcbiAgfTtcblxuICBob29rcy5yb3cucHVzaChyb3cgPT4ge1xuICAgIGNvbnN0IHsgcGF0aCB9ID0gcm93O1xuICAgIHJvdy50b2dnbGVFeHBhbmRlZCA9IHNldCA9PiB0b2dnbGVFeHBhbmRlZEJ5UGF0aChwYXRoLCBzZXQpO1xuICB9KTtcblxuICBjb25zdCBleHBhbmRlZFJvd3MgPSB1c2VNZW1vKFxuICAgICgpID0+IHtcbiAgICAgIGlmIChkZWJ1ZykgY29uc29sZS5pbmZvKFwiZ2V0RXhwYW5kZWRSb3dzXCIpO1xuXG4gICAgICBjb25zdCBleHBhbmRlZFJvd3MgPSBbXTtcblxuICAgICAgLy8gSGVyZSB3ZSBkbyBzb21lIG11dGF0aW9uLCBidXQgaXQncyB0aGUgbGFzdCBzdGFnZSBpbiB0aGVcbiAgICAgIC8vIGltbXV0YWJsZSBwcm9jZXNzIHNvIHRoaXMgaXMgc2FmZVxuICAgICAgY29uc3QgaGFuZGxlUm93ID0gKHJvdywgaW5kZXgsIGRlcHRoID0gMCwgcGFyZW50UGF0aCA9IFtdKSA9PiB7XG4gICAgICAgIC8vIENvbXB1dGUgc29tZSBmaW5hbCBzdGF0ZSBmb3IgdGhlIHJvd1xuICAgICAgICBjb25zdCBwYXRoID0gWy4uLnBhcmVudFBhdGgsIGluZGV4XTtcblxuICAgICAgICByb3cucGF0aCA9IHBhdGg7XG4gICAgICAgIHJvdy5kZXB0aCA9IGRlcHRoO1xuXG4gICAgICAgIHJvdy5pc0V4cGFuZGVkID1cbiAgICAgICAgICAocm93Lm9yaWdpbmFsICYmIHJvdy5vcmlnaW5hbFtleHBhbmRlZEtleV0pIHx8IGdldEJ5KGV4cGFuZGVkLCBwYXRoKTtcblxuICAgICAgICByb3cuY2VsbHMgPSBjb2x1bW5zLm1hcChjb2x1bW4gPT4ge1xuICAgICAgICAgIGNvbnN0IGNlbGwgPSB7XG4gICAgICAgICAgICBjb2x1bW4sXG4gICAgICAgICAgICByb3csXG4gICAgICAgICAgICBzdGF0ZTogbnVsbCxcbiAgICAgICAgICAgIHZhbHVlOiByb3cudmFsdWVzW2NvbHVtbi5pZF1cbiAgICAgICAgICB9O1xuXG4gICAgICAgICAgcmV0dXJuIGNlbGw7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIGV4cGFuZGVkUm93cy5wdXNoKHJvdyk7XG5cbiAgICAgICAgaWYgKHJvdy5pc0V4cGFuZGVkICYmIHJvdy5zdWJSb3dzICYmIHJvdy5zdWJSb3dzLmxlbmd0aCkge1xuICAgICAgICAgIHJvdy5zdWJSb3dzLmZvckVhY2goKHJvdywgaSkgPT4gaGFuZGxlUm93KHJvdywgaSwgZGVwdGggKyAxLCBwYXRoKSk7XG4gICAgICAgIH1cbiAgICAgIH07XG5cbiAgICAgIHJvd3MuZm9yRWFjaCgocm93LCBpKSA9PiBoYW5kbGVSb3cocm93LCBpKSk7XG5cbiAgICAgIHJldHVybiBleHBhbmRlZFJvd3M7XG4gICAgfSxcbiAgICBbcm93cywgZXhwYW5kZWQsIGNvbHVtbnNdXG4gICk7XG5cbiAgY29uc3QgZXhwYW5kZWREZXB0aCA9IGZpbmRFeHBhbmRlZERlcHRoKGV4cGFuZGVkKTtcblxuICByZXR1cm4ge1xuICAgIC4uLnByb3BzLFxuICAgIHRvZ2dsZUV4cGFuZGVkQnlQYXRoLFxuICAgIGV4cGFuZGVkRGVwdGgsXG4gICAgcm93czogZXhwYW5kZWRSb3dzXG4gIH07XG59O1xuXG5mdW5jdGlvbiBmaW5kRXhwYW5kZWREZXB0aChvYmosIGRlcHRoID0gMSkge1xuICByZXR1cm4gT2JqZWN0LnZhbHVlcyhvYmopLnJlZHVjZSgocHJldiwgY3VycikgPT4ge1xuICAgIGlmICh0eXBlb2YgY3VyciA9PT0gXCJvYmplY3RcIikge1xuICAgICAgcmV0dXJuIE1hdGgubWF4KHByZXYsIGZpbmRFeHBhbmRlZERlcHRoKGN1cnIsIGRlcHRoICsgMSkpO1xuICAgIH1cbiAgICByZXR1cm4gZGVwdGg7XG4gIH0sIDApO1xufVxuIl19