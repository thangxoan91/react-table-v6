var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

import { useMemo } from "react";

import * as aggregations from "../aggregations";
import { addActions, actions } from "../actions";
import { defaultState } from "./useTableState";
import { mergeProps, applyPropHooks, defaultGroupByFn, getFirstDefined } from "../utils";

defaultState.groupBy = [];

addActions({
  toggleGroupBy: "__toggleGroupBy__"
});

export var useGroupBy = function useGroupBy(api) {
  var props = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var debug = api.debug,
      rows = api.rows,
      columns = api.columns,
      _api$groupByFn = api.groupByFn,
      groupByFn = _api$groupByFn === undefined ? defaultGroupByFn : _api$groupByFn,
      manualGroupBy = api.manualGroupBy,
      disableGrouping = api.disableGrouping,
      _api$aggregations = api.aggregations,
      userAggregations = _api$aggregations === undefined ? {} : _api$aggregations,
      hooks = api.hooks,
      _api$state = _slicedToArray(api.state, 2),
      groupBy = _api$state[0].groupBy,
      setState = _api$state[1];

  columns.forEach(function (column) {
    var id = column.id,
        accessor = column.accessor,
        canGroupBy = column.canGroupBy;

    column.grouped = groupBy.includes(id);

    column.canGroupBy = accessor ? getFirstDefined(canGroupBy, disableGrouping === true ? false : undefined, true) : false;

    column.Aggregated = column.Aggregated || column.Cell;
  });

  var toggleGroupBy = function toggleGroupBy(id, toggle) {
    return setState(function (old) {
      var resolvedToggle = typeof set !== "undefined" ? toggle : !groupBy.includes(id);
      if (resolvedToggle) {
        return _extends({}, old, {
          groupBy: [].concat(_toConsumableArray(groupBy), [id])
        });
      }
      return _extends({}, old, {
        groupBy: groupBy.filter(function (d) {
          return d !== id;
        })
      });
    }, actions.toggleGroupBy);
  };

  hooks.columns.push(function (columns) {
    columns.forEach(function (column) {
      if (column.canGroupBy) {
        column.toggleGroupBy = function () {
          return toggleGroupBy(column.id);
        };
      }
    });
    return columns;
  });

  hooks.getGroupByToggleProps = [];

  var addGroupByToggleProps = function addGroupByToggleProps(columns, api) {
    columns.forEach(function (column) {
      var canGroupBy = column.canGroupBy;

      column.getGroupByToggleProps = function (props) {
        return mergeProps({
          onClick: canGroupBy ? function (e) {
            e.persist();
            column.toggleGroupBy();
          } : undefined,
          style: {
            cursor: canGroupBy ? "pointer" : undefined
          },
          title: "Toggle GroupBy"
        }, applyPropHooks(api.hooks.getGroupByToggleProps, column, api), props);
      };
    });
    return columns;
  };

  hooks.columns.push(addGroupByToggleProps);
  hooks.headers.push(addGroupByToggleProps);

  var groupedRows = useMemo(function () {
    if (manualGroupBy || !groupBy.length) {
      return rows;
    }
    if (debug) console.info("getGroupedRows");
    // Find the columns that can or are aggregating

    // Uses each column to aggregate rows into a single value
    var aggregateRowsToValues = function aggregateRowsToValues(rows) {
      var values = {};
      columns.forEach(function (column) {
        var columnValues = rows.map(function (d) {
          return d.values[column.id];
        });
        var aggregate = userAggregations[column.aggregate] || aggregations[column.aggregate] || column.aggregate;
        if (typeof aggregate === "function") {
          values[column.id] = aggregate(columnValues, rows);
        } else if (aggregate) {
          throw new Error("Invalid aggregate \"" + aggregate + "\" passed to column with ID: \"" + column.id + "\"");
        } else {
          values[column.id] = columnValues[0];
        }
      });
      return values;
    };

    // Recursively group the data
    var groupRecursively = function groupRecursively(rows, groupBy) {
      var depth = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

      // This is the last level, just return the rows
      if (depth >= groupBy.length) {
        return rows;
      }

      // Group the rows together for this level
      var groupedRows = Object.entries(groupByFn(rows, groupBy[depth])).map(function (_ref, index) {
        var _ref2 = _slicedToArray(_ref, 2),
            groupByVal = _ref2[0],
            subRows = _ref2[1];

        // Recurse to sub rows before aggregation
        subRows = groupRecursively(subRows, groupBy, depth + 1);

        var values = aggregateRowsToValues(subRows);

        var row = {
          groupByID: groupBy[depth],
          groupByVal: groupByVal,
          values: values,
          subRows: subRows,
          depth: depth,
          index: index
        };
        return row;
      });

      return groupedRows;
    };

    // Assign the new data
    return groupRecursively(rows, groupBy);
  }, [rows, groupBy, columns, manualGroupBy]);

  return _extends({}, api, {
    rows: groupedRows
  });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VHcm91cEJ5LmpzIl0sIm5hbWVzIjpbInVzZU1lbW8iLCJhZ2dyZWdhdGlvbnMiLCJhZGRBY3Rpb25zIiwiYWN0aW9ucyIsImRlZmF1bHRTdGF0ZSIsIm1lcmdlUHJvcHMiLCJhcHBseVByb3BIb29rcyIsImRlZmF1bHRHcm91cEJ5Rm4iLCJnZXRGaXJzdERlZmluZWQiLCJncm91cEJ5IiwidG9nZ2xlR3JvdXBCeSIsInVzZUdyb3VwQnkiLCJhcGkiLCJwcm9wcyIsImRlYnVnIiwicm93cyIsImNvbHVtbnMiLCJncm91cEJ5Rm4iLCJtYW51YWxHcm91cEJ5IiwiZGlzYWJsZUdyb3VwaW5nIiwidXNlckFnZ3JlZ2F0aW9ucyIsImhvb2tzIiwic3RhdGUiLCJzZXRTdGF0ZSIsImZvckVhY2giLCJpZCIsImNvbHVtbiIsImFjY2Vzc29yIiwiY2FuR3JvdXBCeSIsImdyb3VwZWQiLCJpbmNsdWRlcyIsInVuZGVmaW5lZCIsIkFnZ3JlZ2F0ZWQiLCJDZWxsIiwidG9nZ2xlIiwicmVzb2x2ZWRUb2dnbGUiLCJzZXQiLCJvbGQiLCJmaWx0ZXIiLCJkIiwicHVzaCIsImdldEdyb3VwQnlUb2dnbGVQcm9wcyIsImFkZEdyb3VwQnlUb2dnbGVQcm9wcyIsIm9uQ2xpY2siLCJlIiwicGVyc2lzdCIsInN0eWxlIiwiY3Vyc29yIiwidGl0bGUiLCJoZWFkZXJzIiwiZ3JvdXBlZFJvd3MiLCJsZW5ndGgiLCJjb25zb2xlIiwiaW5mbyIsImFnZ3JlZ2F0ZVJvd3NUb1ZhbHVlcyIsInZhbHVlcyIsImNvbHVtblZhbHVlcyIsIm1hcCIsImFnZ3JlZ2F0ZSIsIkVycm9yIiwiZ3JvdXBSZWN1cnNpdmVseSIsImRlcHRoIiwiT2JqZWN0IiwiZW50cmllcyIsImluZGV4IiwiZ3JvdXBCeVZhbCIsInN1YlJvd3MiLCJyb3ciLCJncm91cEJ5SUQiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLFNBQVNBLE9BQVQsUUFBd0IsT0FBeEI7O0FBRUEsT0FBTyxLQUFLQyxZQUFaLE1BQThCLGlCQUE5QjtBQUNBLFNBQVNDLFVBQVQsRUFBcUJDLE9BQXJCLFFBQW9DLFlBQXBDO0FBQ0EsU0FBU0MsWUFBVCxRQUE2QixpQkFBN0I7QUFDQSxTQUNFQyxVQURGLEVBRUVDLGNBRkYsRUFHRUMsZ0JBSEYsRUFJRUMsZUFKRixRQUtPLFVBTFA7O0FBT0FKLGFBQWFLLE9BQWIsR0FBdUIsRUFBdkI7O0FBRUFQLFdBQVc7QUFDVFEsaUJBQWU7QUFETixDQUFYOztBQUlBLE9BQU8sSUFBTUMsYUFBYSxTQUFiQSxVQUFhLENBQUNDLEdBQUQsRUFBcUI7QUFBQSxNQUFmQyxLQUFlLHVFQUFQLEVBQU87O0FBQUEsTUFFM0NDLEtBRjJDLEdBV3pDRixHQVh5QyxDQUUzQ0UsS0FGMkM7QUFBQSxNQUczQ0MsSUFIMkMsR0FXekNILEdBWHlDLENBRzNDRyxJQUgyQztBQUFBLE1BSTNDQyxPQUoyQyxHQVd6Q0osR0FYeUMsQ0FJM0NJLE9BSjJDO0FBQUEsdUJBV3pDSixHQVh5QyxDQUszQ0ssU0FMMkM7QUFBQSxNQUszQ0EsU0FMMkMsa0NBSy9CVixnQkFMK0I7QUFBQSxNQU0zQ1csYUFOMkMsR0FXekNOLEdBWHlDLENBTTNDTSxhQU4yQztBQUFBLE1BTzNDQyxlQVAyQyxHQVd6Q1AsR0FYeUMsQ0FPM0NPLGVBUDJDO0FBQUEsMEJBV3pDUCxHQVh5QyxDQVEzQ1gsWUFSMkM7QUFBQSxNQVE3Qm1CLGdCQVI2QixxQ0FRVixFQVJVO0FBQUEsTUFTM0NDLEtBVDJDLEdBV3pDVCxHQVh5QyxDQVMzQ1MsS0FUMkM7QUFBQSxrQ0FXekNULEdBWHlDLENBVTNDVSxLQVYyQztBQUFBLE1BVWpDYixPQVZpQyxpQkFVakNBLE9BVmlDO0FBQUEsTUFVdEJjLFFBVnNCOztBQWE3Q1AsVUFBUVEsT0FBUixDQUFnQixrQkFBVTtBQUFBLFFBQ2hCQyxFQURnQixHQUNhQyxNQURiLENBQ2hCRCxFQURnQjtBQUFBLFFBQ1pFLFFBRFksR0FDYUQsTUFEYixDQUNaQyxRQURZO0FBQUEsUUFDRkMsVUFERSxHQUNhRixNQURiLENBQ0ZFLFVBREU7O0FBRXhCRixXQUFPRyxPQUFQLEdBQWlCcEIsUUFBUXFCLFFBQVIsQ0FBaUJMLEVBQWpCLENBQWpCOztBQUVBQyxXQUFPRSxVQUFQLEdBQW9CRCxXQUNoQm5CLGdCQUNFb0IsVUFERixFQUVFVCxvQkFBb0IsSUFBcEIsR0FBMkIsS0FBM0IsR0FBbUNZLFNBRnJDLEVBR0UsSUFIRixDQURnQixHQU1oQixLQU5KOztBQVFBTCxXQUFPTSxVQUFQLEdBQW9CTixPQUFPTSxVQUFQLElBQXFCTixPQUFPTyxJQUFoRDtBQUNELEdBYkQ7O0FBZUEsTUFBTXZCLGdCQUFnQixTQUFoQkEsYUFBZ0IsQ0FBQ2UsRUFBRCxFQUFLUyxNQUFMLEVBQWdCO0FBQ3BDLFdBQU9YLFNBQVMsZUFBTztBQUNyQixVQUFNWSxpQkFDSixPQUFPQyxHQUFQLEtBQWUsV0FBZixHQUE2QkYsTUFBN0IsR0FBc0MsQ0FBQ3pCLFFBQVFxQixRQUFSLENBQWlCTCxFQUFqQixDQUR6QztBQUVBLFVBQUlVLGNBQUosRUFBb0I7QUFDbEIsNEJBQ0tFLEdBREw7QUFFRTVCLGdEQUFhQSxPQUFiLElBQXNCZ0IsRUFBdEI7QUFGRjtBQUlEO0FBQ0QsMEJBQ0tZLEdBREw7QUFFRTVCLGlCQUFTQSxRQUFRNkIsTUFBUixDQUFlO0FBQUEsaUJBQUtDLE1BQU1kLEVBQVg7QUFBQSxTQUFmO0FBRlg7QUFJRCxLQWJNLEVBYUp0QixRQUFRTyxhQWJKLENBQVA7QUFjRCxHQWZEOztBQWlCQVcsUUFBTUwsT0FBTixDQUFjd0IsSUFBZCxDQUFtQixtQkFBVztBQUM1QnhCLFlBQVFRLE9BQVIsQ0FBZ0Isa0JBQVU7QUFDeEIsVUFBSUUsT0FBT0UsVUFBWCxFQUF1QjtBQUNyQkYsZUFBT2hCLGFBQVAsR0FBdUI7QUFBQSxpQkFBTUEsY0FBY2dCLE9BQU9ELEVBQXJCLENBQU47QUFBQSxTQUF2QjtBQUNEO0FBQ0YsS0FKRDtBQUtBLFdBQU9ULE9BQVA7QUFDRCxHQVBEOztBQVNBSyxRQUFNb0IscUJBQU4sR0FBOEIsRUFBOUI7O0FBRUEsTUFBTUMsd0JBQXdCLFNBQXhCQSxxQkFBd0IsQ0FBQzFCLE9BQUQsRUFBVUosR0FBVixFQUFrQjtBQUM5Q0ksWUFBUVEsT0FBUixDQUFnQixrQkFBVTtBQUFBLFVBQ2hCSSxVQURnQixHQUNERixNQURDLENBQ2hCRSxVQURnQjs7QUFFeEJGLGFBQU9lLHFCQUFQLEdBQStCLGlCQUFTO0FBQ3RDLGVBQU9wQyxXQUNMO0FBQ0VzQyxtQkFBU2YsYUFDTCxhQUFLO0FBQ0hnQixjQUFFQyxPQUFGO0FBQ0FuQixtQkFBT2hCLGFBQVA7QUFDRCxXQUpJLEdBS0xxQixTQU5OO0FBT0VlLGlCQUFPO0FBQ0xDLG9CQUFRbkIsYUFBYSxTQUFiLEdBQXlCRztBQUQ1QixXQVBUO0FBVUVpQixpQkFBTztBQVZULFNBREssRUFhTDFDLGVBQWVNLElBQUlTLEtBQUosQ0FBVW9CLHFCQUF6QixFQUFnRGYsTUFBaEQsRUFBd0RkLEdBQXhELENBYkssRUFjTEMsS0FkSyxDQUFQO0FBZ0JELE9BakJEO0FBa0JELEtBcEJEO0FBcUJBLFdBQU9HLE9BQVA7QUFDRCxHQXZCRDs7QUF5QkFLLFFBQU1MLE9BQU4sQ0FBY3dCLElBQWQsQ0FBbUJFLHFCQUFuQjtBQUNBckIsUUFBTTRCLE9BQU4sQ0FBY1QsSUFBZCxDQUFtQkUscUJBQW5COztBQUVBLE1BQU1RLGNBQWNsRCxRQUNsQixZQUFNO0FBQ0osUUFBSWtCLGlCQUFpQixDQUFDVCxRQUFRMEMsTUFBOUIsRUFBc0M7QUFDcEMsYUFBT3BDLElBQVA7QUFDRDtBQUNELFFBQUlELEtBQUosRUFBV3NDLFFBQVFDLElBQVIsQ0FBYSxnQkFBYjtBQUNYOztBQUVBO0FBQ0EsUUFBTUMsd0JBQXdCLFNBQXhCQSxxQkFBd0IsT0FBUTtBQUNwQyxVQUFNQyxTQUFTLEVBQWY7QUFDQXZDLGNBQVFRLE9BQVIsQ0FBZ0Isa0JBQVU7QUFDeEIsWUFBTWdDLGVBQWV6QyxLQUFLMEMsR0FBTCxDQUFTO0FBQUEsaUJBQUtsQixFQUFFZ0IsTUFBRixDQUFTN0IsT0FBT0QsRUFBaEIsQ0FBTDtBQUFBLFNBQVQsQ0FBckI7QUFDQSxZQUFJaUMsWUFDRnRDLGlCQUFpQk0sT0FBT2dDLFNBQXhCLEtBQ0F6RCxhQUFheUIsT0FBT2dDLFNBQXBCLENBREEsSUFFQWhDLE9BQU9nQyxTQUhUO0FBSUEsWUFBSSxPQUFPQSxTQUFQLEtBQXFCLFVBQXpCLEVBQXFDO0FBQ25DSCxpQkFBTzdCLE9BQU9ELEVBQWQsSUFBb0JpQyxVQUFVRixZQUFWLEVBQXdCekMsSUFBeEIsQ0FBcEI7QUFDRCxTQUZELE1BRU8sSUFBSTJDLFNBQUosRUFBZTtBQUNwQixnQkFBTSxJQUFJQyxLQUFKLDBCQUNrQkQsU0FEbEIsdUNBRUZoQyxPQUFPRCxFQUZMLFFBQU47QUFLRCxTQU5NLE1BTUE7QUFDTDhCLGlCQUFPN0IsT0FBT0QsRUFBZCxJQUFvQitCLGFBQWEsQ0FBYixDQUFwQjtBQUNEO0FBQ0YsT0FqQkQ7QUFrQkEsYUFBT0QsTUFBUDtBQUNELEtBckJEOztBQXVCQTtBQUNBLFFBQU1LLG1CQUFtQixTQUFuQkEsZ0JBQW1CLENBQUM3QyxJQUFELEVBQU9OLE9BQVAsRUFBOEI7QUFBQSxVQUFkb0QsS0FBYyx1RUFBTixDQUFNOztBQUNyRDtBQUNBLFVBQUlBLFNBQVNwRCxRQUFRMEMsTUFBckIsRUFBNkI7QUFDM0IsZUFBT3BDLElBQVA7QUFDRDs7QUFFRDtBQUNBLFVBQUltQyxjQUFjWSxPQUFPQyxPQUFQLENBQWU5QyxVQUFVRixJQUFWLEVBQWdCTixRQUFRb0QsS0FBUixDQUFoQixDQUFmLEVBQWdESixHQUFoRCxDQUNoQixnQkFBd0JPLEtBQXhCLEVBQWtDO0FBQUE7QUFBQSxZQUFoQ0MsVUFBZ0M7QUFBQSxZQUFwQkMsT0FBb0I7O0FBQ2hDO0FBQ0FBLGtCQUFVTixpQkFBaUJNLE9BQWpCLEVBQTBCekQsT0FBMUIsRUFBbUNvRCxRQUFRLENBQTNDLENBQVY7O0FBRUEsWUFBTU4sU0FBU0Qsc0JBQXNCWSxPQUF0QixDQUFmOztBQUVBLFlBQU1DLE1BQU07QUFDVkMscUJBQVczRCxRQUFRb0QsS0FBUixDQUREO0FBRVZJLGdDQUZVO0FBR1ZWLHdCQUhVO0FBSVZXLDBCQUpVO0FBS1ZMLHNCQUxVO0FBTVZHO0FBTlUsU0FBWjtBQVFBLGVBQU9HLEdBQVA7QUFDRCxPQWhCZSxDQUFsQjs7QUFtQkEsYUFBT2pCLFdBQVA7QUFDRCxLQTNCRDs7QUE2QkE7QUFDQSxXQUFPVSxpQkFBaUI3QyxJQUFqQixFQUF1Qk4sT0FBdkIsQ0FBUDtBQUNELEdBaEVpQixFQWlFbEIsQ0FBQ00sSUFBRCxFQUFPTixPQUFQLEVBQWdCTyxPQUFoQixFQUF5QkUsYUFBekIsQ0FqRWtCLENBQXBCOztBQW9FQSxzQkFDS04sR0FETDtBQUVFRyxVQUFNbUM7QUFGUjtBQUlELENBNUpNIiwiZmlsZSI6InVzZUdyb3VwQnkuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VNZW1vIH0gZnJvbSBcInJlYWN0XCI7XG5cbmltcG9ydCAqIGFzIGFnZ3JlZ2F0aW9ucyBmcm9tIFwiLi4vYWdncmVnYXRpb25zXCI7XG5pbXBvcnQgeyBhZGRBY3Rpb25zLCBhY3Rpb25zIH0gZnJvbSBcIi4uL2FjdGlvbnNcIjtcbmltcG9ydCB7IGRlZmF1bHRTdGF0ZSB9IGZyb20gXCIuL3VzZVRhYmxlU3RhdGVcIjtcbmltcG9ydCB7XG4gIG1lcmdlUHJvcHMsXG4gIGFwcGx5UHJvcEhvb2tzLFxuICBkZWZhdWx0R3JvdXBCeUZuLFxuICBnZXRGaXJzdERlZmluZWRcbn0gZnJvbSBcIi4uL3V0aWxzXCI7XG5cbmRlZmF1bHRTdGF0ZS5ncm91cEJ5ID0gW107XG5cbmFkZEFjdGlvbnMoe1xuICB0b2dnbGVHcm91cEJ5OiBcIl9fdG9nZ2xlR3JvdXBCeV9fXCJcbn0pO1xuXG5leHBvcnQgY29uc3QgdXNlR3JvdXBCeSA9IChhcGksIHByb3BzID0ge30pID0+IHtcbiAgY29uc3Qge1xuICAgIGRlYnVnLFxuICAgIHJvd3MsXG4gICAgY29sdW1ucyxcbiAgICBncm91cEJ5Rm4gPSBkZWZhdWx0R3JvdXBCeUZuLFxuICAgIG1hbnVhbEdyb3VwQnksXG4gICAgZGlzYWJsZUdyb3VwaW5nLFxuICAgIGFnZ3JlZ2F0aW9uczogdXNlckFnZ3JlZ2F0aW9ucyA9IHt9LFxuICAgIGhvb2tzLFxuICAgIHN0YXRlOiBbeyBncm91cEJ5IH0sIHNldFN0YXRlXVxuICB9ID0gYXBpO1xuXG4gIGNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xuICAgIGNvbnN0IHsgaWQsIGFjY2Vzc29yLCBjYW5Hcm91cEJ5IH0gPSBjb2x1bW47XG4gICAgY29sdW1uLmdyb3VwZWQgPSBncm91cEJ5LmluY2x1ZGVzKGlkKTtcblxuICAgIGNvbHVtbi5jYW5Hcm91cEJ5ID0gYWNjZXNzb3JcbiAgICAgID8gZ2V0Rmlyc3REZWZpbmVkKFxuICAgICAgICAgIGNhbkdyb3VwQnksXG4gICAgICAgICAgZGlzYWJsZUdyb3VwaW5nID09PSB0cnVlID8gZmFsc2UgOiB1bmRlZmluZWQsXG4gICAgICAgICAgdHJ1ZVxuICAgICAgICApXG4gICAgICA6IGZhbHNlO1xuXG4gICAgY29sdW1uLkFnZ3JlZ2F0ZWQgPSBjb2x1bW4uQWdncmVnYXRlZCB8fCBjb2x1bW4uQ2VsbDtcbiAgfSk7XG5cbiAgY29uc3QgdG9nZ2xlR3JvdXBCeSA9IChpZCwgdG9nZ2xlKSA9PiB7XG4gICAgcmV0dXJuIHNldFN0YXRlKG9sZCA9PiB7XG4gICAgICBjb25zdCByZXNvbHZlZFRvZ2dsZSA9XG4gICAgICAgIHR5cGVvZiBzZXQgIT09IFwidW5kZWZpbmVkXCIgPyB0b2dnbGUgOiAhZ3JvdXBCeS5pbmNsdWRlcyhpZCk7XG4gICAgICBpZiAocmVzb2x2ZWRUb2dnbGUpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAuLi5vbGQsXG4gICAgICAgICAgZ3JvdXBCeTogWy4uLmdyb3VwQnksIGlkXVxuICAgICAgICB9O1xuICAgICAgfVxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgLi4ub2xkLFxuICAgICAgICBncm91cEJ5OiBncm91cEJ5LmZpbHRlcihkID0+IGQgIT09IGlkKVxuICAgICAgfTtcbiAgICB9LCBhY3Rpb25zLnRvZ2dsZUdyb3VwQnkpO1xuICB9O1xuXG4gIGhvb2tzLmNvbHVtbnMucHVzaChjb2x1bW5zID0+IHtcbiAgICBjb2x1bW5zLmZvckVhY2goY29sdW1uID0+IHtcbiAgICAgIGlmIChjb2x1bW4uY2FuR3JvdXBCeSkge1xuICAgICAgICBjb2x1bW4udG9nZ2xlR3JvdXBCeSA9ICgpID0+IHRvZ2dsZUdyb3VwQnkoY29sdW1uLmlkKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gY29sdW1ucztcbiAgfSk7XG5cbiAgaG9va3MuZ2V0R3JvdXBCeVRvZ2dsZVByb3BzID0gW107XG5cbiAgY29uc3QgYWRkR3JvdXBCeVRvZ2dsZVByb3BzID0gKGNvbHVtbnMsIGFwaSkgPT4ge1xuICAgIGNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xuICAgICAgY29uc3QgeyBjYW5Hcm91cEJ5IH0gPSBjb2x1bW47XG4gICAgICBjb2x1bW4uZ2V0R3JvdXBCeVRvZ2dsZVByb3BzID0gcHJvcHMgPT4ge1xuICAgICAgICByZXR1cm4gbWVyZ2VQcm9wcyhcbiAgICAgICAgICB7XG4gICAgICAgICAgICBvbkNsaWNrOiBjYW5Hcm91cEJ5XG4gICAgICAgICAgICAgID8gZSA9PiB7XG4gICAgICAgICAgICAgICAgICBlLnBlcnNpc3QoKTtcbiAgICAgICAgICAgICAgICAgIGNvbHVtbi50b2dnbGVHcm91cEJ5KCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICA6IHVuZGVmaW5lZCxcbiAgICAgICAgICAgIHN0eWxlOiB7XG4gICAgICAgICAgICAgIGN1cnNvcjogY2FuR3JvdXBCeSA/IFwicG9pbnRlclwiIDogdW5kZWZpbmVkXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdGl0bGU6IFwiVG9nZ2xlIEdyb3VwQnlcIlxuICAgICAgICAgIH0sXG4gICAgICAgICAgYXBwbHlQcm9wSG9va3MoYXBpLmhvb2tzLmdldEdyb3VwQnlUb2dnbGVQcm9wcywgY29sdW1uLCBhcGkpLFxuICAgICAgICAgIHByb3BzXG4gICAgICAgICk7XG4gICAgICB9O1xuICAgIH0pO1xuICAgIHJldHVybiBjb2x1bW5zO1xuICB9O1xuXG4gIGhvb2tzLmNvbHVtbnMucHVzaChhZGRHcm91cEJ5VG9nZ2xlUHJvcHMpO1xuICBob29rcy5oZWFkZXJzLnB1c2goYWRkR3JvdXBCeVRvZ2dsZVByb3BzKTtcblxuICBjb25zdCBncm91cGVkUm93cyA9IHVzZU1lbW8oXG4gICAgKCkgPT4ge1xuICAgICAgaWYgKG1hbnVhbEdyb3VwQnkgfHwgIWdyb3VwQnkubGVuZ3RoKSB7XG4gICAgICAgIHJldHVybiByb3dzO1xuICAgICAgfVxuICAgICAgaWYgKGRlYnVnKSBjb25zb2xlLmluZm8oXCJnZXRHcm91cGVkUm93c1wiKTtcbiAgICAgIC8vIEZpbmQgdGhlIGNvbHVtbnMgdGhhdCBjYW4gb3IgYXJlIGFnZ3JlZ2F0aW5nXG5cbiAgICAgIC8vIFVzZXMgZWFjaCBjb2x1bW4gdG8gYWdncmVnYXRlIHJvd3MgaW50byBhIHNpbmdsZSB2YWx1ZVxuICAgICAgY29uc3QgYWdncmVnYXRlUm93c1RvVmFsdWVzID0gcm93cyA9PiB7XG4gICAgICAgIGNvbnN0IHZhbHVlcyA9IHt9O1xuICAgICAgICBjb2x1bW5zLmZvckVhY2goY29sdW1uID0+IHtcbiAgICAgICAgICBjb25zdCBjb2x1bW5WYWx1ZXMgPSByb3dzLm1hcChkID0+IGQudmFsdWVzW2NvbHVtbi5pZF0pO1xuICAgICAgICAgIGxldCBhZ2dyZWdhdGUgPVxuICAgICAgICAgICAgdXNlckFnZ3JlZ2F0aW9uc1tjb2x1bW4uYWdncmVnYXRlXSB8fFxuICAgICAgICAgICAgYWdncmVnYXRpb25zW2NvbHVtbi5hZ2dyZWdhdGVdIHx8XG4gICAgICAgICAgICBjb2x1bW4uYWdncmVnYXRlO1xuICAgICAgICAgIGlmICh0eXBlb2YgYWdncmVnYXRlID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgIHZhbHVlc1tjb2x1bW4uaWRdID0gYWdncmVnYXRlKGNvbHVtblZhbHVlcywgcm93cyk7XG4gICAgICAgICAgfSBlbHNlIGlmIChhZ2dyZWdhdGUpIHtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihcbiAgICAgICAgICAgICAgYEludmFsaWQgYWdncmVnYXRlIFwiJHthZ2dyZWdhdGV9XCIgcGFzc2VkIHRvIGNvbHVtbiB3aXRoIElEOiBcIiR7XG4gICAgICAgICAgICAgICAgY29sdW1uLmlkXG4gICAgICAgICAgICAgIH1cImBcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHZhbHVlc1tjb2x1bW4uaWRdID0gY29sdW1uVmFsdWVzWzBdO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiB2YWx1ZXM7XG4gICAgICB9O1xuXG4gICAgICAvLyBSZWN1cnNpdmVseSBncm91cCB0aGUgZGF0YVxuICAgICAgY29uc3QgZ3JvdXBSZWN1cnNpdmVseSA9IChyb3dzLCBncm91cEJ5LCBkZXB0aCA9IDApID0+IHtcbiAgICAgICAgLy8gVGhpcyBpcyB0aGUgbGFzdCBsZXZlbCwganVzdCByZXR1cm4gdGhlIHJvd3NcbiAgICAgICAgaWYgKGRlcHRoID49IGdyb3VwQnkubGVuZ3RoKSB7XG4gICAgICAgICAgcmV0dXJuIHJvd3M7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBHcm91cCB0aGUgcm93cyB0b2dldGhlciBmb3IgdGhpcyBsZXZlbFxuICAgICAgICBsZXQgZ3JvdXBlZFJvd3MgPSBPYmplY3QuZW50cmllcyhncm91cEJ5Rm4ocm93cywgZ3JvdXBCeVtkZXB0aF0pKS5tYXAoXG4gICAgICAgICAgKFtncm91cEJ5VmFsLCBzdWJSb3dzXSwgaW5kZXgpID0+IHtcbiAgICAgICAgICAgIC8vIFJlY3Vyc2UgdG8gc3ViIHJvd3MgYmVmb3JlIGFnZ3JlZ2F0aW9uXG4gICAgICAgICAgICBzdWJSb3dzID0gZ3JvdXBSZWN1cnNpdmVseShzdWJSb3dzLCBncm91cEJ5LCBkZXB0aCArIDEpO1xuXG4gICAgICAgICAgICBjb25zdCB2YWx1ZXMgPSBhZ2dyZWdhdGVSb3dzVG9WYWx1ZXMoc3ViUm93cyk7XG5cbiAgICAgICAgICAgIGNvbnN0IHJvdyA9IHtcbiAgICAgICAgICAgICAgZ3JvdXBCeUlEOiBncm91cEJ5W2RlcHRoXSxcbiAgICAgICAgICAgICAgZ3JvdXBCeVZhbCxcbiAgICAgICAgICAgICAgdmFsdWVzLFxuICAgICAgICAgICAgICBzdWJSb3dzLFxuICAgICAgICAgICAgICBkZXB0aCxcbiAgICAgICAgICAgICAgaW5kZXhcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICByZXR1cm4gcm93O1xuICAgICAgICAgIH1cbiAgICAgICAgKTtcblxuICAgICAgICByZXR1cm4gZ3JvdXBlZFJvd3M7XG4gICAgICB9O1xuXG4gICAgICAvLyBBc3NpZ24gdGhlIG5ldyBkYXRhXG4gICAgICByZXR1cm4gZ3JvdXBSZWN1cnNpdmVseShyb3dzLCBncm91cEJ5KTtcbiAgICB9LFxuICAgIFtyb3dzLCBncm91cEJ5LCBjb2x1bW5zLCBtYW51YWxHcm91cEJ5XVxuICApO1xuXG4gIHJldHVybiB7XG4gICAgLi4uYXBpLFxuICAgIHJvd3M6IGdyb3VwZWRSb3dzXG4gIH07XG59O1xuIl19