var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

import { useMemo, useState } from "react";
import PropTypes from "prop-types";

import { getFirstDefined, sum } from "../utils";

var propTypes = {
  defaultFlex: PropTypes.number
};

export var actions = {};

export var useFlexLayout = function useFlexLayout(props) {
  // Validate props
  PropTypes.checkPropTypes(propTypes, props, "property", "useFlexLayout");

  var _props$defaultFlex = props.defaultFlex,
      defaultFlex = _props$defaultFlex === undefined ? 1 : _props$defaultFlex,
      _props$hooks = props.hooks,
      columnsHooks = _props$hooks.columns,
      getRowProps = _props$hooks.getRowProps,
      getHeaderRowProps = _props$hooks.getHeaderRowProps,
      getHeaderProps = _props$hooks.getHeaderProps,
      getCellProps = _props$hooks.getCellProps;


  columnsHooks.push(function (columns, api) {
    var visibleColumns = columns.filter(function (column) {
      column.visible = typeof column.show === "function" ? column.show(api) : !!column.show;
      return column.visible;
    });

    var columnMeasurements = {};

    var sumWidth = 0;
    visibleColumns.forEach(function (column) {
      var _getSizesForColumn = getSizesForColumn(column, defaultFlex, undefined, undefined, api),
          width = _getSizesForColumn.width,
          minWidth = _getSizesForColumn.minWidth;

      if (width) {
        sumWidth += width;
      } else if (minWidth) {
        sumWidth += minWidth;
      } else {
        sumWidth += defaultFlex;
      }
    });

    var rowStyles = {
      style: {
        display: "flex",
        minWidth: sumWidth + "px"
      }
    };

    api.rowStyles = rowStyles;

    getRowProps.push(function () {
      return rowStyles;
    });
    getHeaderRowProps.push(function () {
      return rowStyles;
    });

    getHeaderProps.push(function (column) {
      return {
        style: _extends({
          boxSizing: "border-box"
        }, getStylesForColumn(column, columnMeasurements, defaultFlex, api))
        // [refKey]: el => {
        //   renderedCellInfoRef.current[key] = {
        //     column,
        //     el
        //   };
        // },
      };
    });

    getCellProps.push(function (cell) {
      return {
        style: _extends({
          display: "block",
          boxSizing: "border-box"
        }, getStylesForColumn(cell.column, columnMeasurements, defaultFlex, undefined, api))
        // [refKey]: el => {
        //   renderedCellInfoRef.current[columnPathStr] = {
        //     column,
        //     el
        //   };
        // }
      };
    });

    return columns;
  });

  return props;
};

// Utils

function getStylesForColumn(column, columnMeasurements, defaultFlex, api) {
  var _getSizesForColumn2 = getSizesForColumn(column, columnMeasurements, defaultFlex, api),
      flex = _getSizesForColumn2.flex,
      width = _getSizesForColumn2.width,
      maxWidth = _getSizesForColumn2.maxWidth;

  return {
    flex: flex + " 0 auto",
    width: width + "px",
    maxWidth: maxWidth + "px"
  };
}

function getSizesForColumn(_ref, columnMeasurements, defaultFlex, api) {
  var columns = _ref.columns,
      id = _ref.id,
      width = _ref.width,
      minWidth = _ref.minWidth,
      maxWidth = _ref.maxWidth;

  if (columns) {
    columns = columns.map(function (column) {
      return getSizesForColumn(column, columnMeasurements, defaultFlex, api);
    }).filter(Boolean);

    if (!columns.length) {
      return false;
    }

    var flex = sum(columns.map(function (col) {
      return col.flex;
    }));
    var _width = sum(columns.map(function (col) {
      return col.width;
    }));
    var _maxWidth = sum(columns.map(function (col) {
      return col.maxWidth;
    }));

    return {
      flex: flex,
      width: _width,
      maxWidth: _maxWidth
    };
  }

  return {
    flex: width ? 0 : defaultFlex,
    width: width === "auto" ? columnMeasurements[id] || defaultFlex : getFirstDefined(width, minWidth, defaultFlex),
    maxWidth: maxWidth
  };
}

// const resetRefs = () => {
//   if (debug) console.info("resetRefs");
//   renderedCellInfoRef.current = {};
// };

// const calculateAutoWidths = () => {
//   RAF(() => {
//     const newColumnMeasurements = {};
//     Object.values(renderedCellInfoRef.current).forEach(({ column, el }) => {
//       if (!el) {
//         return;
//       }

//       let measurement = 0;

//       const measureChildren = children => {
//         if (children) {
//           [].slice.call(children).forEach(child => {
//             measurement = Math.max(
//               measurement,
//               Math.ceil(child.offsetWidth) || 0
//             );
//             measureChildren(child.children);
//           });
//         }
//         return measurement;
//       };

//       const parentDims = getElementDimensions(el);
//       measureChildren(el.children);

//       newColumnMeasurements[column.id] = Math.max(
//         newColumnMeasurements[column.id] || 0,
//         measurement + parentDims.paddingLeft + parentDims.paddingRight
//       );
//     });

//     const oldKeys = Object.keys(columnMeasurements);
//     const newKeys = Object.keys(newColumnMeasurements);

//     const needsUpdate =
//       oldKeys.length !== newKeys.length ||
//       oldKeys.some(key => {
//         return columnMeasurements[key] !== newColumnMeasurements[key];
//       });

//     if (needsUpdate) {
//       setState(old => {
//         return {
//           ...old,
//           columnMeasurements: newColumnMeasurements
//         };
//       }, actions.updateAutoWidth);
//     }
//   });
// };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VGbGV4TGF5b3V0LmpzIl0sIm5hbWVzIjpbInVzZU1lbW8iLCJ1c2VTdGF0ZSIsIlByb3BUeXBlcyIsImdldEZpcnN0RGVmaW5lZCIsInN1bSIsInByb3BUeXBlcyIsImRlZmF1bHRGbGV4IiwibnVtYmVyIiwiYWN0aW9ucyIsInVzZUZsZXhMYXlvdXQiLCJjaGVja1Byb3BUeXBlcyIsInByb3BzIiwiaG9va3MiLCJjb2x1bW5zSG9va3MiLCJjb2x1bW5zIiwiZ2V0Um93UHJvcHMiLCJnZXRIZWFkZXJSb3dQcm9wcyIsImdldEhlYWRlclByb3BzIiwiZ2V0Q2VsbFByb3BzIiwicHVzaCIsImFwaSIsInZpc2libGVDb2x1bW5zIiwiZmlsdGVyIiwiY29sdW1uIiwidmlzaWJsZSIsInNob3ciLCJjb2x1bW5NZWFzdXJlbWVudHMiLCJzdW1XaWR0aCIsImZvckVhY2giLCJnZXRTaXplc0ZvckNvbHVtbiIsInVuZGVmaW5lZCIsIndpZHRoIiwibWluV2lkdGgiLCJyb3dTdHlsZXMiLCJzdHlsZSIsImRpc3BsYXkiLCJib3hTaXppbmciLCJnZXRTdHlsZXNGb3JDb2x1bW4iLCJjZWxsIiwiZmxleCIsIm1heFdpZHRoIiwiaWQiLCJtYXAiLCJCb29sZWFuIiwibGVuZ3RoIiwiY29sIl0sIm1hcHBpbmdzIjoiOztBQUFBLFNBQVNBLE9BQVQsRUFBa0JDLFFBQWxCLFFBQWtDLE9BQWxDO0FBQ0EsT0FBT0MsU0FBUCxNQUFzQixZQUF0Qjs7QUFFQSxTQUFTQyxlQUFULEVBQTBCQyxHQUExQixRQUFxQyxVQUFyQzs7QUFFQSxJQUFNQyxZQUFZO0FBQ2hCQyxlQUFhSixVQUFVSztBQURQLENBQWxCOztBQUlBLE9BQU8sSUFBTUMsVUFBVSxFQUFoQjs7QUFFUCxPQUFPLElBQU1DLGdCQUFnQixTQUFoQkEsYUFBZ0IsUUFBUztBQUNwQztBQUNBUCxZQUFVUSxjQUFWLENBQXlCTCxTQUF6QixFQUFvQ00sS0FBcEMsRUFBMkMsVUFBM0MsRUFBdUQsZUFBdkQ7O0FBRm9DLDJCQWFoQ0EsS0FiZ0MsQ0FLbENMLFdBTGtDO0FBQUEsTUFLbENBLFdBTGtDLHNDQUtwQixDQUxvQjtBQUFBLHFCQWFoQ0ssS0FiZ0MsQ0FNbENDLEtBTmtDO0FBQUEsTUFPdkJDLFlBUHVCLGdCQU9oQ0MsT0FQZ0M7QUFBQSxNQVFoQ0MsV0FSZ0MsZ0JBUWhDQSxXQVJnQztBQUFBLE1BU2hDQyxpQkFUZ0MsZ0JBU2hDQSxpQkFUZ0M7QUFBQSxNQVVoQ0MsY0FWZ0MsZ0JBVWhDQSxjQVZnQztBQUFBLE1BV2hDQyxZQVhnQyxnQkFXaENBLFlBWGdDOzs7QUFlcENMLGVBQWFNLElBQWIsQ0FBa0IsVUFBQ0wsT0FBRCxFQUFVTSxHQUFWLEVBQWtCO0FBQ2xDLFFBQU1DLGlCQUFpQlAsUUFBUVEsTUFBUixDQUFlLGtCQUFVO0FBQzlDQyxhQUFPQyxPQUFQLEdBQ0UsT0FBT0QsT0FBT0UsSUFBZCxLQUF1QixVQUF2QixHQUFvQ0YsT0FBT0UsSUFBUCxDQUFZTCxHQUFaLENBQXBDLEdBQXVELENBQUMsQ0FBQ0csT0FBT0UsSUFEbEU7QUFFQSxhQUFPRixPQUFPQyxPQUFkO0FBQ0QsS0FKc0IsQ0FBdkI7O0FBTUEsUUFBTUUscUJBQXFCLEVBQTNCOztBQUVBLFFBQUlDLFdBQVcsQ0FBZjtBQUNBTixtQkFBZU8sT0FBZixDQUF1QixrQkFBVTtBQUFBLCtCQUNIQyxrQkFDMUJOLE1BRDBCLEVBRTFCakIsV0FGMEIsRUFHMUJ3QixTQUgwQixFQUkxQkEsU0FKMEIsRUFLMUJWLEdBTDBCLENBREc7QUFBQSxVQUN2QlcsS0FEdUIsc0JBQ3ZCQSxLQUR1QjtBQUFBLFVBQ2hCQyxRQURnQixzQkFDaEJBLFFBRGdCOztBQVEvQixVQUFJRCxLQUFKLEVBQVc7QUFDVEosb0JBQVlJLEtBQVo7QUFDRCxPQUZELE1BRU8sSUFBSUMsUUFBSixFQUFjO0FBQ25CTCxvQkFBWUssUUFBWjtBQUNELE9BRk0sTUFFQTtBQUNMTCxvQkFBWXJCLFdBQVo7QUFDRDtBQUNGLEtBZkQ7O0FBaUJBLFFBQU0yQixZQUFZO0FBQ2hCQyxhQUFPO0FBQ0xDLGlCQUFTLE1BREo7QUFFTEgsa0JBQWFMLFFBQWI7QUFGSztBQURTLEtBQWxCOztBQU9BUCxRQUFJYSxTQUFKLEdBQWdCQSxTQUFoQjs7QUFFQWxCLGdCQUFZSSxJQUFaLENBQWlCO0FBQUEsYUFBTWMsU0FBTjtBQUFBLEtBQWpCO0FBQ0FqQixzQkFBa0JHLElBQWxCLENBQXVCO0FBQUEsYUFBTWMsU0FBTjtBQUFBLEtBQXZCOztBQUVBaEIsbUJBQWVFLElBQWYsQ0FBb0I7QUFBQSxhQUFXO0FBQzdCZTtBQUNFRSxxQkFBVztBQURiLFdBRUtDLG1CQUFtQmQsTUFBbkIsRUFBMkJHLGtCQUEzQixFQUErQ3BCLFdBQS9DLEVBQTREYyxHQUE1RCxDQUZMO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVjZCLE9BQVg7QUFBQSxLQUFwQjs7QUFhQUYsaUJBQWFDLElBQWIsQ0FBa0IsZ0JBQVE7QUFDeEIsYUFBTztBQUNMZTtBQUNFQyxtQkFBUyxPQURYO0FBRUVDLHFCQUFXO0FBRmIsV0FHS0MsbUJBQ0RDLEtBQUtmLE1BREosRUFFREcsa0JBRkMsRUFHRHBCLFdBSEMsRUFJRHdCLFNBSkMsRUFLRFYsR0FMQyxDQUhMO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBakJLLE9BQVA7QUFtQkQsS0FwQkQ7O0FBc0JBLFdBQU9OLE9BQVA7QUFDRCxHQTNFRDs7QUE2RUEsU0FBT0gsS0FBUDtBQUNELENBN0ZNOztBQStGUDs7QUFFQSxTQUFTMEIsa0JBQVQsQ0FBNEJkLE1BQTVCLEVBQW9DRyxrQkFBcEMsRUFBd0RwQixXQUF4RCxFQUFxRWMsR0FBckUsRUFBMEU7QUFBQSw0QkFDdENTLGtCQUNoQ04sTUFEZ0MsRUFFaENHLGtCQUZnQyxFQUdoQ3BCLFdBSGdDLEVBSWhDYyxHQUpnQyxDQURzQztBQUFBLE1BQ2hFbUIsSUFEZ0UsdUJBQ2hFQSxJQURnRTtBQUFBLE1BQzFEUixLQUQwRCx1QkFDMURBLEtBRDBEO0FBQUEsTUFDbkRTLFFBRG1ELHVCQUNuREEsUUFEbUQ7O0FBUXhFLFNBQU87QUFDTEQsVUFBU0EsSUFBVCxZQURLO0FBRUxSLFdBQVVBLEtBQVYsT0FGSztBQUdMUyxjQUFhQSxRQUFiO0FBSEssR0FBUDtBQUtEOztBQUVELFNBQVNYLGlCQUFULE9BRUVILGtCQUZGLEVBR0VwQixXQUhGLEVBSUVjLEdBSkYsRUFLRTtBQUFBLE1BSkVOLE9BSUYsUUFKRUEsT0FJRjtBQUFBLE1BSlcyQixFQUlYLFFBSldBLEVBSVg7QUFBQSxNQUplVixLQUlmLFFBSmVBLEtBSWY7QUFBQSxNQUpzQkMsUUFJdEIsUUFKc0JBLFFBSXRCO0FBQUEsTUFKZ0NRLFFBSWhDLFFBSmdDQSxRQUloQzs7QUFDQSxNQUFJMUIsT0FBSixFQUFhO0FBQ1hBLGNBQVVBLFFBQ1A0QixHQURPLENBQ0g7QUFBQSxhQUNIYixrQkFBa0JOLE1BQWxCLEVBQTBCRyxrQkFBMUIsRUFBOENwQixXQUE5QyxFQUEyRGMsR0FBM0QsQ0FERztBQUFBLEtBREcsRUFJUEUsTUFKTyxDQUlBcUIsT0FKQSxDQUFWOztBQU1BLFFBQUksQ0FBQzdCLFFBQVE4QixNQUFiLEVBQXFCO0FBQ25CLGFBQU8sS0FBUDtBQUNEOztBQUVELFFBQU1MLE9BQU9uQyxJQUFJVSxRQUFRNEIsR0FBUixDQUFZO0FBQUEsYUFBT0csSUFBSU4sSUFBWDtBQUFBLEtBQVosQ0FBSixDQUFiO0FBQ0EsUUFBTVIsU0FBUTNCLElBQUlVLFFBQVE0QixHQUFSLENBQVk7QUFBQSxhQUFPRyxJQUFJZCxLQUFYO0FBQUEsS0FBWixDQUFKLENBQWQ7QUFDQSxRQUFNUyxZQUFXcEMsSUFBSVUsUUFBUTRCLEdBQVIsQ0FBWTtBQUFBLGFBQU9HLElBQUlMLFFBQVg7QUFBQSxLQUFaLENBQUosQ0FBakI7O0FBRUEsV0FBTztBQUNMRCxnQkFESztBQUVMUixtQkFGSztBQUdMUztBQUhLLEtBQVA7QUFLRDs7QUFFRCxTQUFPO0FBQ0xELFVBQU1SLFFBQVEsQ0FBUixHQUFZekIsV0FEYjtBQUVMeUIsV0FDRUEsVUFBVSxNQUFWLEdBQ0lMLG1CQUFtQmUsRUFBbkIsS0FBMEJuQyxXQUQ5QixHQUVJSCxnQkFBZ0I0QixLQUFoQixFQUF1QkMsUUFBdkIsRUFBaUMxQixXQUFqQyxDQUxEO0FBTUxrQztBQU5LLEdBQVA7QUFRRDs7QUFFRDtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSIsImZpbGUiOiJ1c2VGbGV4TGF5b3V0LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlTWVtbywgdXNlU3RhdGUgfSBmcm9tIFwicmVhY3RcIjtcbmltcG9ydCBQcm9wVHlwZXMgZnJvbSBcInByb3AtdHlwZXNcIjtcblxuaW1wb3J0IHsgZ2V0Rmlyc3REZWZpbmVkLCBzdW0gfSBmcm9tIFwiLi4vdXRpbHNcIjtcblxuY29uc3QgcHJvcFR5cGVzID0ge1xuICBkZWZhdWx0RmxleDogUHJvcFR5cGVzLm51bWJlclxufTtcblxuZXhwb3J0IGNvbnN0IGFjdGlvbnMgPSB7fTtcblxuZXhwb3J0IGNvbnN0IHVzZUZsZXhMYXlvdXQgPSBwcm9wcyA9PiB7XG4gIC8vIFZhbGlkYXRlIHByb3BzXG4gIFByb3BUeXBlcy5jaGVja1Byb3BUeXBlcyhwcm9wVHlwZXMsIHByb3BzLCBcInByb3BlcnR5XCIsIFwidXNlRmxleExheW91dFwiKTtcblxuICBjb25zdCB7XG4gICAgZGVmYXVsdEZsZXggPSAxLFxuICAgIGhvb2tzOiB7XG4gICAgICBjb2x1bW5zOiBjb2x1bW5zSG9va3MsXG4gICAgICBnZXRSb3dQcm9wcyxcbiAgICAgIGdldEhlYWRlclJvd1Byb3BzLFxuICAgICAgZ2V0SGVhZGVyUHJvcHMsXG4gICAgICBnZXRDZWxsUHJvcHNcbiAgICB9XG4gIH0gPSBwcm9wcztcblxuICBjb2x1bW5zSG9va3MucHVzaCgoY29sdW1ucywgYXBpKSA9PiB7XG4gICAgY29uc3QgdmlzaWJsZUNvbHVtbnMgPSBjb2x1bW5zLmZpbHRlcihjb2x1bW4gPT4ge1xuICAgICAgY29sdW1uLnZpc2libGUgPVxuICAgICAgICB0eXBlb2YgY29sdW1uLnNob3cgPT09IFwiZnVuY3Rpb25cIiA/IGNvbHVtbi5zaG93KGFwaSkgOiAhIWNvbHVtbi5zaG93O1xuICAgICAgcmV0dXJuIGNvbHVtbi52aXNpYmxlO1xuICAgIH0pO1xuXG4gICAgY29uc3QgY29sdW1uTWVhc3VyZW1lbnRzID0ge307XG5cbiAgICBsZXQgc3VtV2lkdGggPSAwO1xuICAgIHZpc2libGVDb2x1bW5zLmZvckVhY2goY29sdW1uID0+IHtcbiAgICAgIGNvbnN0IHsgd2lkdGgsIG1pbldpZHRoIH0gPSBnZXRTaXplc0ZvckNvbHVtbihcbiAgICAgICAgY29sdW1uLFxuICAgICAgICBkZWZhdWx0RmxleCxcbiAgICAgICAgdW5kZWZpbmVkLFxuICAgICAgICB1bmRlZmluZWQsXG4gICAgICAgIGFwaVxuICAgICAgKTtcbiAgICAgIGlmICh3aWR0aCkge1xuICAgICAgICBzdW1XaWR0aCArPSB3aWR0aDtcbiAgICAgIH0gZWxzZSBpZiAobWluV2lkdGgpIHtcbiAgICAgICAgc3VtV2lkdGggKz0gbWluV2lkdGg7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBzdW1XaWR0aCArPSBkZWZhdWx0RmxleDtcbiAgICAgIH1cbiAgICB9KTtcblxuICAgIGNvbnN0IHJvd1N0eWxlcyA9IHtcbiAgICAgIHN0eWxlOiB7XG4gICAgICAgIGRpc3BsYXk6IFwiZmxleFwiLFxuICAgICAgICBtaW5XaWR0aDogYCR7c3VtV2lkdGh9cHhgXG4gICAgICB9XG4gICAgfTtcblxuICAgIGFwaS5yb3dTdHlsZXMgPSByb3dTdHlsZXM7XG5cbiAgICBnZXRSb3dQcm9wcy5wdXNoKCgpID0+IHJvd1N0eWxlcyk7XG4gICAgZ2V0SGVhZGVyUm93UHJvcHMucHVzaCgoKSA9PiByb3dTdHlsZXMpO1xuXG4gICAgZ2V0SGVhZGVyUHJvcHMucHVzaChjb2x1bW4gPT4gKHtcbiAgICAgIHN0eWxlOiB7XG4gICAgICAgIGJveFNpemluZzogXCJib3JkZXItYm94XCIsXG4gICAgICAgIC4uLmdldFN0eWxlc0ZvckNvbHVtbihjb2x1bW4sIGNvbHVtbk1lYXN1cmVtZW50cywgZGVmYXVsdEZsZXgsIGFwaSlcbiAgICAgIH1cbiAgICAgIC8vIFtyZWZLZXldOiBlbCA9PiB7XG4gICAgICAvLyAgIHJlbmRlcmVkQ2VsbEluZm9SZWYuY3VycmVudFtrZXldID0ge1xuICAgICAgLy8gICAgIGNvbHVtbixcbiAgICAgIC8vICAgICBlbFxuICAgICAgLy8gICB9O1xuICAgICAgLy8gfSxcbiAgICB9KSk7XG5cbiAgICBnZXRDZWxsUHJvcHMucHVzaChjZWxsID0+IHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIHN0eWxlOiB7XG4gICAgICAgICAgZGlzcGxheTogXCJibG9ja1wiLFxuICAgICAgICAgIGJveFNpemluZzogXCJib3JkZXItYm94XCIsXG4gICAgICAgICAgLi4uZ2V0U3R5bGVzRm9yQ29sdW1uKFxuICAgICAgICAgICAgY2VsbC5jb2x1bW4sXG4gICAgICAgICAgICBjb2x1bW5NZWFzdXJlbWVudHMsXG4gICAgICAgICAgICBkZWZhdWx0RmxleCxcbiAgICAgICAgICAgIHVuZGVmaW5lZCxcbiAgICAgICAgICAgIGFwaVxuICAgICAgICAgIClcbiAgICAgICAgfVxuICAgICAgICAvLyBbcmVmS2V5XTogZWwgPT4ge1xuICAgICAgICAvLyAgIHJlbmRlcmVkQ2VsbEluZm9SZWYuY3VycmVudFtjb2x1bW5QYXRoU3RyXSA9IHtcbiAgICAgICAgLy8gICAgIGNvbHVtbixcbiAgICAgICAgLy8gICAgIGVsXG4gICAgICAgIC8vICAgfTtcbiAgICAgICAgLy8gfVxuICAgICAgfTtcbiAgICB9KTtcblxuICAgIHJldHVybiBjb2x1bW5zO1xuICB9KTtcblxuICByZXR1cm4gcHJvcHM7XG59O1xuXG4vLyBVdGlsc1xuXG5mdW5jdGlvbiBnZXRTdHlsZXNGb3JDb2x1bW4oY29sdW1uLCBjb2x1bW5NZWFzdXJlbWVudHMsIGRlZmF1bHRGbGV4LCBhcGkpIHtcbiAgY29uc3QgeyBmbGV4LCB3aWR0aCwgbWF4V2lkdGggfSA9IGdldFNpemVzRm9yQ29sdW1uKFxuICAgIGNvbHVtbixcbiAgICBjb2x1bW5NZWFzdXJlbWVudHMsXG4gICAgZGVmYXVsdEZsZXgsXG4gICAgYXBpXG4gICk7XG5cbiAgcmV0dXJuIHtcbiAgICBmbGV4OiBgJHtmbGV4fSAwIGF1dG9gLFxuICAgIHdpZHRoOiBgJHt3aWR0aH1weGAsXG4gICAgbWF4V2lkdGg6IGAke21heFdpZHRofXB4YFxuICB9O1xufVxuXG5mdW5jdGlvbiBnZXRTaXplc0ZvckNvbHVtbihcbiAgeyBjb2x1bW5zLCBpZCwgd2lkdGgsIG1pbldpZHRoLCBtYXhXaWR0aCB9LFxuICBjb2x1bW5NZWFzdXJlbWVudHMsXG4gIGRlZmF1bHRGbGV4LFxuICBhcGlcbikge1xuICBpZiAoY29sdW1ucykge1xuICAgIGNvbHVtbnMgPSBjb2x1bW5zXG4gICAgICAubWFwKGNvbHVtbiA9PlxuICAgICAgICBnZXRTaXplc0ZvckNvbHVtbihjb2x1bW4sIGNvbHVtbk1lYXN1cmVtZW50cywgZGVmYXVsdEZsZXgsIGFwaSlcbiAgICAgIClcbiAgICAgIC5maWx0ZXIoQm9vbGVhbik7XG5cbiAgICBpZiAoIWNvbHVtbnMubGVuZ3RoKSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgY29uc3QgZmxleCA9IHN1bShjb2x1bW5zLm1hcChjb2wgPT4gY29sLmZsZXgpKTtcbiAgICBjb25zdCB3aWR0aCA9IHN1bShjb2x1bW5zLm1hcChjb2wgPT4gY29sLndpZHRoKSk7XG4gICAgY29uc3QgbWF4V2lkdGggPSBzdW0oY29sdW1ucy5tYXAoY29sID0+IGNvbC5tYXhXaWR0aCkpO1xuXG4gICAgcmV0dXJuIHtcbiAgICAgIGZsZXgsXG4gICAgICB3aWR0aCxcbiAgICAgIG1heFdpZHRoXG4gICAgfTtcbiAgfVxuXG4gIHJldHVybiB7XG4gICAgZmxleDogd2lkdGggPyAwIDogZGVmYXVsdEZsZXgsXG4gICAgd2lkdGg6XG4gICAgICB3aWR0aCA9PT0gXCJhdXRvXCJcbiAgICAgICAgPyBjb2x1bW5NZWFzdXJlbWVudHNbaWRdIHx8IGRlZmF1bHRGbGV4XG4gICAgICAgIDogZ2V0Rmlyc3REZWZpbmVkKHdpZHRoLCBtaW5XaWR0aCwgZGVmYXVsdEZsZXgpLFxuICAgIG1heFdpZHRoXG4gIH07XG59XG5cbi8vIGNvbnN0IHJlc2V0UmVmcyA9ICgpID0+IHtcbi8vICAgaWYgKGRlYnVnKSBjb25zb2xlLmluZm8oXCJyZXNldFJlZnNcIik7XG4vLyAgIHJlbmRlcmVkQ2VsbEluZm9SZWYuY3VycmVudCA9IHt9O1xuLy8gfTtcblxuLy8gY29uc3QgY2FsY3VsYXRlQXV0b1dpZHRocyA9ICgpID0+IHtcbi8vICAgUkFGKCgpID0+IHtcbi8vICAgICBjb25zdCBuZXdDb2x1bW5NZWFzdXJlbWVudHMgPSB7fTtcbi8vICAgICBPYmplY3QudmFsdWVzKHJlbmRlcmVkQ2VsbEluZm9SZWYuY3VycmVudCkuZm9yRWFjaCgoeyBjb2x1bW4sIGVsIH0pID0+IHtcbi8vICAgICAgIGlmICghZWwpIHtcbi8vICAgICAgICAgcmV0dXJuO1xuLy8gICAgICAgfVxuXG4vLyAgICAgICBsZXQgbWVhc3VyZW1lbnQgPSAwO1xuXG4vLyAgICAgICBjb25zdCBtZWFzdXJlQ2hpbGRyZW4gPSBjaGlsZHJlbiA9PiB7XG4vLyAgICAgICAgIGlmIChjaGlsZHJlbikge1xuLy8gICAgICAgICAgIFtdLnNsaWNlLmNhbGwoY2hpbGRyZW4pLmZvckVhY2goY2hpbGQgPT4ge1xuLy8gICAgICAgICAgICAgbWVhc3VyZW1lbnQgPSBNYXRoLm1heChcbi8vICAgICAgICAgICAgICAgbWVhc3VyZW1lbnQsXG4vLyAgICAgICAgICAgICAgIE1hdGguY2VpbChjaGlsZC5vZmZzZXRXaWR0aCkgfHwgMFxuLy8gICAgICAgICAgICAgKTtcbi8vICAgICAgICAgICAgIG1lYXN1cmVDaGlsZHJlbihjaGlsZC5jaGlsZHJlbik7XG4vLyAgICAgICAgICAgfSk7XG4vLyAgICAgICAgIH1cbi8vICAgICAgICAgcmV0dXJuIG1lYXN1cmVtZW50O1xuLy8gICAgICAgfTtcblxuLy8gICAgICAgY29uc3QgcGFyZW50RGltcyA9IGdldEVsZW1lbnREaW1lbnNpb25zKGVsKTtcbi8vICAgICAgIG1lYXN1cmVDaGlsZHJlbihlbC5jaGlsZHJlbik7XG5cbi8vICAgICAgIG5ld0NvbHVtbk1lYXN1cmVtZW50c1tjb2x1bW4uaWRdID0gTWF0aC5tYXgoXG4vLyAgICAgICAgIG5ld0NvbHVtbk1lYXN1cmVtZW50c1tjb2x1bW4uaWRdIHx8IDAsXG4vLyAgICAgICAgIG1lYXN1cmVtZW50ICsgcGFyZW50RGltcy5wYWRkaW5nTGVmdCArIHBhcmVudERpbXMucGFkZGluZ1JpZ2h0XG4vLyAgICAgICApO1xuLy8gICAgIH0pO1xuXG4vLyAgICAgY29uc3Qgb2xkS2V5cyA9IE9iamVjdC5rZXlzKGNvbHVtbk1lYXN1cmVtZW50cyk7XG4vLyAgICAgY29uc3QgbmV3S2V5cyA9IE9iamVjdC5rZXlzKG5ld0NvbHVtbk1lYXN1cmVtZW50cyk7XG5cbi8vICAgICBjb25zdCBuZWVkc1VwZGF0ZSA9XG4vLyAgICAgICBvbGRLZXlzLmxlbmd0aCAhPT0gbmV3S2V5cy5sZW5ndGggfHxcbi8vICAgICAgIG9sZEtleXMuc29tZShrZXkgPT4ge1xuLy8gICAgICAgICByZXR1cm4gY29sdW1uTWVhc3VyZW1lbnRzW2tleV0gIT09IG5ld0NvbHVtbk1lYXN1cmVtZW50c1trZXldO1xuLy8gICAgICAgfSk7XG5cbi8vICAgICBpZiAobmVlZHNVcGRhdGUpIHtcbi8vICAgICAgIHNldFN0YXRlKG9sZCA9PiB7XG4vLyAgICAgICAgIHJldHVybiB7XG4vLyAgICAgICAgICAgLi4ub2xkLFxuLy8gICAgICAgICAgIGNvbHVtbk1lYXN1cmVtZW50czogbmV3Q29sdW1uTWVhc3VyZW1lbnRzXG4vLyAgICAgICAgIH07XG4vLyAgICAgICB9LCBhY3Rpb25zLnVwZGF0ZUF1dG9XaWR0aCk7XG4vLyAgICAgfVxuLy8gICB9KTtcbi8vIH07XG4iXX0=