var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

import { useMemo } from "react";

import { defaultFilterFn, getFirstDefined } from "../utils";
import { addActions, actions } from "../actions";
import { defaultState } from "./useTableState";

defaultState.filters = {};
addActions({
  setFilter: "__setFilter__",
  setAllFilters: "__setAllFilters__"
});

var useFilters = function useFilters(props) {
  var debug = props.debug,
      rows = props.rows,
      columns = props.columns,
      _props$filterFn = props.filterFn,
      filterFn = _props$filterFn === undefined ? defaultFilterFn : _props$filterFn,
      manualFilters = props.manualFilters,
      disableFilters = props.disableFilters,
      hooks = props.hooks,
      _props$state = _slicedToArray(props.state, 2),
      filters = _props$state[0].filters,
      setState = _props$state[1];

  columns.forEach(function (column) {
    var id = column.id,
        accessor = column.accessor,
        canFilter = column.canFilter;

    column.canFilter = accessor ? getFirstDefined(canFilter, disableFilters === true ? false : undefined, true) : false;
    // Was going to add this to the filter hook
    column.filterValue = filters[id];
  });

  var setFilter = function setFilter(id, val) {
    return setState(function (old) {
      if (typeof val === "undefined") {
        var prev = filters[id],
            rest = _objectWithoutProperties(filters, [id]);

        return _extends({}, old, {
          filters: _extends({}, rest)
        });
      }

      return _extends({}, old, {
        filters: _extends({}, filters, _defineProperty({}, id, val))
      });
    }, actions.setFilter);
  };

  var setAllFilters = function setAllFilters(filters) {
    return setState(function (old) {
      return _extends({}, old, {
        filters: filters
      });
    }, actions.setAllFilters);
  };

  hooks.columns.push(function (columns) {
    columns.forEach(function (column) {
      if (column.canFilter) {
        column.setFilter = function (val) {
          return setFilter(column.id, val);
        };
      }
    });
    return columns;
  });

  var filteredRows = useMemo(function () {
    if (manualFilters || !Object.keys(filters).length) {
      return rows;
    }

    if (debug) console.info("getFilteredRows");

    // Filters top level and nested rows
    var filterRows = function filterRows(rows) {
      var filteredRows = rows;

      filteredRows = Object.entries(filters).reduce(function (filteredSoFar, _ref) {
        var _ref2 = _slicedToArray(_ref, 2),
            columnID = _ref2[0],
            filterValue = _ref2[1];

        // Find the filters column
        var column = columns.find(function (d) {
          return d.id === columnID;
        });

        // Don't filter hidden columns or columns that have had their filters disabled
        if (!column || column.filterable === false) {
          return filteredSoFar;
        }

        var filterMethod = column.filterMethod || filterFn;

        // If 'filterAll' is set to true, pass the entire dataset to the filter method
        if (column.filterAll) {
          return filterMethod(filteredSoFar, columnID, filterValue, column);
        }
        return filteredSoFar.filter(function (row) {
          return filterMethod(row, columnID, filterValue, column);
        });
      }, rows);

      // Apply the filter to any subRows
      filteredRows = filteredRows.map(function (row) {
        if (!row.subRows) {
          return row;
        }
        return _extends({}, row, {
          subRows: filterRows(row.subRows)
        });
      });

      // then filter any rows without subcolumns because it would be strange to show
      filteredRows = filteredRows.filter(function (row) {
        if (!row.subRows) {
          return true;
        }
        return row.subRows.length > 0;
      });

      return filteredRows;
    };

    return filterRows(rows);
  }, [rows, filters, manualFilters]);

  return _extends({}, props, {
    setFilter: setFilter,
    setAllFilters: setAllFilters,
    rows: filteredRows
  });
};
export { useFilters };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VGaWx0ZXJzLmpzIl0sIm5hbWVzIjpbInVzZU1lbW8iLCJkZWZhdWx0RmlsdGVyRm4iLCJnZXRGaXJzdERlZmluZWQiLCJhZGRBY3Rpb25zIiwiYWN0aW9ucyIsImRlZmF1bHRTdGF0ZSIsImZpbHRlcnMiLCJzZXRGaWx0ZXIiLCJzZXRBbGxGaWx0ZXJzIiwidXNlRmlsdGVycyIsImRlYnVnIiwicHJvcHMiLCJyb3dzIiwiY29sdW1ucyIsImZpbHRlckZuIiwibWFudWFsRmlsdGVycyIsImRpc2FibGVGaWx0ZXJzIiwiaG9va3MiLCJzdGF0ZSIsInNldFN0YXRlIiwiZm9yRWFjaCIsImlkIiwiY29sdW1uIiwiYWNjZXNzb3IiLCJjYW5GaWx0ZXIiLCJ1bmRlZmluZWQiLCJmaWx0ZXJWYWx1ZSIsInZhbCIsInByZXYiLCJyZXN0Iiwib2xkIiwicHVzaCIsImZpbHRlcmVkUm93cyIsIk9iamVjdCIsImtleXMiLCJsZW5ndGgiLCJjb25zb2xlIiwiaW5mbyIsImZpbHRlclJvd3MiLCJlbnRyaWVzIiwicmVkdWNlIiwiZmlsdGVyZWRTb0ZhciIsImNvbHVtbklEIiwiZmluZCIsImQiLCJmaWx0ZXJhYmxlIiwiZmlsdGVyTWV0aG9kIiwiZmlsdGVyQWxsIiwiZmlsdGVyIiwicm93IiwibWFwIiwic3ViUm93cyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7QUFBQSxTQUFTQSxPQUFULFFBQXdCLE9BQXhCOztBQUVBLFNBQVNDLGVBQVQsRUFBMEJDLGVBQTFCLFFBQWlELFVBQWpEO0FBQ0EsU0FBU0MsVUFBVCxFQUFxQkMsT0FBckIsUUFBb0MsWUFBcEM7QUFDQSxTQUFTQyxZQUFULFFBQTZCLGlCQUE3Qjs7QUFFQUEsYUFBYUMsT0FBYixHQUF1QixFQUF2QjtBQUNBSCxXQUFXO0FBQ1RJLGFBQVcsZUFERjtBQUVUQyxpQkFBZTtBQUZOLENBQVg7O0FBS08sSUFBTUMsYUFBYSxTQUFiQSxVQUFhLFFBQVM7QUFBQSxNQUUvQkMsS0FGK0IsR0FVN0JDLEtBVjZCLENBRS9CRCxLQUYrQjtBQUFBLE1BRy9CRSxJQUgrQixHQVU3QkQsS0FWNkIsQ0FHL0JDLElBSCtCO0FBQUEsTUFJL0JDLE9BSitCLEdBVTdCRixLQVY2QixDQUkvQkUsT0FKK0I7QUFBQSx3QkFVN0JGLEtBVjZCLENBSy9CRyxRQUwrQjtBQUFBLE1BSy9CQSxRQUwrQixtQ0FLcEJiLGVBTG9CO0FBQUEsTUFNL0JjLGFBTitCLEdBVTdCSixLQVY2QixDQU0vQkksYUFOK0I7QUFBQSxNQU8vQkMsY0FQK0IsR0FVN0JMLEtBVjZCLENBTy9CSyxjQVArQjtBQUFBLE1BUS9CQyxLQVIrQixHQVU3Qk4sS0FWNkIsQ0FRL0JNLEtBUitCO0FBQUEsb0NBVTdCTixLQVY2QixDQVMvQk8sS0FUK0I7QUFBQSxNQVNyQlosT0FUcUIsbUJBU3JCQSxPQVRxQjtBQUFBLE1BU1ZhLFFBVFU7O0FBWWpDTixVQUFRTyxPQUFSLENBQWdCLGtCQUFVO0FBQUEsUUFDaEJDLEVBRGdCLEdBQ1lDLE1BRFosQ0FDaEJELEVBRGdCO0FBQUEsUUFDWkUsUUFEWSxHQUNZRCxNQURaLENBQ1pDLFFBRFk7QUFBQSxRQUNGQyxTQURFLEdBQ1lGLE1BRFosQ0FDRkUsU0FERTs7QUFFeEJGLFdBQU9FLFNBQVAsR0FBbUJELFdBQ2ZyQixnQkFDRXNCLFNBREYsRUFFRVIsbUJBQW1CLElBQW5CLEdBQTBCLEtBQTFCLEdBQWtDUyxTQUZwQyxFQUdFLElBSEYsQ0FEZSxHQU1mLEtBTko7QUFPQTtBQUNBSCxXQUFPSSxXQUFQLEdBQXFCcEIsUUFBUWUsRUFBUixDQUFyQjtBQUNELEdBWEQ7O0FBYUEsTUFBTWQsWUFBWSxTQUFaQSxTQUFZLENBQUNjLEVBQUQsRUFBS00sR0FBTCxFQUFhO0FBQzdCLFdBQU9SLFNBQVMsZUFBTztBQUNyQixVQUFJLE9BQU9RLEdBQVAsS0FBZSxXQUFuQixFQUFnQztBQUFBLFlBQ2hCQyxJQURnQixHQUNFdEIsT0FERixDQUNyQmUsRUFEcUI7QUFBQSxZQUNQUSxJQURPLDRCQUNFdkIsT0FERixHQUNyQmUsRUFEcUI7O0FBRTlCLDRCQUNLUyxHQURMO0FBRUV4QixnQ0FDS3VCLElBREw7QUFGRjtBQU1EOztBQUVELDBCQUNLQyxHQURMO0FBRUV4Qiw4QkFDS0EsT0FETCxzQkFFR2UsRUFGSCxFQUVRTSxHQUZSO0FBRkY7QUFPRCxLQWxCTSxFQWtCSnZCLFFBQVFHLFNBbEJKLENBQVA7QUFtQkQsR0FwQkQ7O0FBc0JBLE1BQU1DLGdCQUFnQixTQUFoQkEsYUFBZ0IsVUFBVztBQUMvQixXQUFPVyxTQUFTLGVBQU87QUFDckIsMEJBQ0tXLEdBREw7QUFFRXhCO0FBRkY7QUFJRCxLQUxNLEVBS0pGLFFBQVFJLGFBTEosQ0FBUDtBQU1ELEdBUEQ7O0FBU0FTLFFBQU1KLE9BQU4sQ0FBY2tCLElBQWQsQ0FBbUIsbUJBQVc7QUFDNUJsQixZQUFRTyxPQUFSLENBQWdCLGtCQUFVO0FBQ3hCLFVBQUlFLE9BQU9FLFNBQVgsRUFBc0I7QUFDcEJGLGVBQU9mLFNBQVAsR0FBbUI7QUFBQSxpQkFBT0EsVUFBVWUsT0FBT0QsRUFBakIsRUFBcUJNLEdBQXJCLENBQVA7QUFBQSxTQUFuQjtBQUNEO0FBQ0YsS0FKRDtBQUtBLFdBQU9kLE9BQVA7QUFDRCxHQVBEOztBQVNBLE1BQU1tQixlQUFlaEMsUUFDbkIsWUFBTTtBQUNKLFFBQUllLGlCQUFpQixDQUFDa0IsT0FBT0MsSUFBUCxDQUFZNUIsT0FBWixFQUFxQjZCLE1BQTNDLEVBQW1EO0FBQ2pELGFBQU92QixJQUFQO0FBQ0Q7O0FBRUQsUUFBSUYsS0FBSixFQUFXMEIsUUFBUUMsSUFBUixDQUFhLGlCQUFiOztBQUVYO0FBQ0EsUUFBTUMsYUFBYSxTQUFiQSxVQUFhLE9BQVE7QUFDekIsVUFBSU4sZUFBZXBCLElBQW5COztBQUVBb0IscUJBQWVDLE9BQU9NLE9BQVAsQ0FBZWpDLE9BQWYsRUFBd0JrQyxNQUF4QixDQUNiLFVBQUNDLGFBQUQsUUFBNEM7QUFBQTtBQUFBLFlBQTNCQyxRQUEyQjtBQUFBLFlBQWpCaEIsV0FBaUI7O0FBQzFDO0FBQ0EsWUFBTUosU0FBU1QsUUFBUThCLElBQVIsQ0FBYTtBQUFBLGlCQUFLQyxFQUFFdkIsRUFBRixLQUFTcUIsUUFBZDtBQUFBLFNBQWIsQ0FBZjs7QUFFQTtBQUNBLFlBQUksQ0FBQ3BCLE1BQUQsSUFBV0EsT0FBT3VCLFVBQVAsS0FBc0IsS0FBckMsRUFBNEM7QUFDMUMsaUJBQU9KLGFBQVA7QUFDRDs7QUFFRCxZQUFNSyxlQUFleEIsT0FBT3dCLFlBQVAsSUFBdUJoQyxRQUE1Qzs7QUFFQTtBQUNBLFlBQUlRLE9BQU95QixTQUFYLEVBQXNCO0FBQ3BCLGlCQUFPRCxhQUFhTCxhQUFiLEVBQTRCQyxRQUE1QixFQUFzQ2hCLFdBQXRDLEVBQW1ESixNQUFuRCxDQUFQO0FBQ0Q7QUFDRCxlQUFPbUIsY0FBY08sTUFBZCxDQUFxQjtBQUFBLGlCQUMxQkYsYUFBYUcsR0FBYixFQUFrQlAsUUFBbEIsRUFBNEJoQixXQUE1QixFQUF5Q0osTUFBekMsQ0FEMEI7QUFBQSxTQUFyQixDQUFQO0FBR0QsT0FuQlksRUFvQmJWLElBcEJhLENBQWY7O0FBdUJBO0FBQ0FvQixxQkFBZUEsYUFBYWtCLEdBQWIsQ0FBaUIsZUFBTztBQUNyQyxZQUFJLENBQUNELElBQUlFLE9BQVQsRUFBa0I7QUFDaEIsaUJBQU9GLEdBQVA7QUFDRDtBQUNELDRCQUNLQSxHQURMO0FBRUVFLG1CQUFTYixXQUFXVyxJQUFJRSxPQUFmO0FBRlg7QUFJRCxPQVJjLENBQWY7O0FBVUE7QUFDQW5CLHFCQUFlQSxhQUFhZ0IsTUFBYixDQUFvQixlQUFPO0FBQ3hDLFlBQUksQ0FBQ0MsSUFBSUUsT0FBVCxFQUFrQjtBQUNoQixpQkFBTyxJQUFQO0FBQ0Q7QUFDRCxlQUFPRixJQUFJRSxPQUFKLENBQVloQixNQUFaLEdBQXFCLENBQTVCO0FBQ0QsT0FMYyxDQUFmOztBQU9BLGFBQU9ILFlBQVA7QUFDRCxLQTlDRDs7QUFnREEsV0FBT00sV0FBVzFCLElBQVgsQ0FBUDtBQUNELEdBMURrQixFQTJEbkIsQ0FBQ0EsSUFBRCxFQUFPTixPQUFQLEVBQWdCUyxhQUFoQixDQTNEbUIsQ0FBckI7O0FBOERBLHNCQUNLSixLQURMO0FBRUVKLHdCQUZGO0FBR0VDLGdDQUhGO0FBSUVJLFVBQU1vQjtBQUpSO0FBTUQsQ0FySU0iLCJmaWxlIjoidXNlRmlsdGVycy5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IHVzZU1lbW8gfSBmcm9tIFwicmVhY3RcIjtcblxuaW1wb3J0IHsgZGVmYXVsdEZpbHRlckZuLCBnZXRGaXJzdERlZmluZWQgfSBmcm9tIFwiLi4vdXRpbHNcIjtcbmltcG9ydCB7IGFkZEFjdGlvbnMsIGFjdGlvbnMgfSBmcm9tIFwiLi4vYWN0aW9uc1wiO1xuaW1wb3J0IHsgZGVmYXVsdFN0YXRlIH0gZnJvbSBcIi4vdXNlVGFibGVTdGF0ZVwiO1xuXG5kZWZhdWx0U3RhdGUuZmlsdGVycyA9IHt9O1xuYWRkQWN0aW9ucyh7XG4gIHNldEZpbHRlcjogXCJfX3NldEZpbHRlcl9fXCIsXG4gIHNldEFsbEZpbHRlcnM6IFwiX19zZXRBbGxGaWx0ZXJzX19cIlxufSk7XG5cbmV4cG9ydCBjb25zdCB1c2VGaWx0ZXJzID0gcHJvcHMgPT4ge1xuICBjb25zdCB7XG4gICAgZGVidWcsXG4gICAgcm93cyxcbiAgICBjb2x1bW5zLFxuICAgIGZpbHRlckZuID0gZGVmYXVsdEZpbHRlckZuLFxuICAgIG1hbnVhbEZpbHRlcnMsXG4gICAgZGlzYWJsZUZpbHRlcnMsXG4gICAgaG9va3MsXG4gICAgc3RhdGU6IFt7IGZpbHRlcnMgfSwgc2V0U3RhdGVdXG4gIH0gPSBwcm9wcztcblxuICBjb2x1bW5zLmZvckVhY2goY29sdW1uID0+IHtcbiAgICBjb25zdCB7IGlkLCBhY2Nlc3NvciwgY2FuRmlsdGVyIH0gPSBjb2x1bW47XG4gICAgY29sdW1uLmNhbkZpbHRlciA9IGFjY2Vzc29yXG4gICAgICA/IGdldEZpcnN0RGVmaW5lZChcbiAgICAgICAgICBjYW5GaWx0ZXIsXG4gICAgICAgICAgZGlzYWJsZUZpbHRlcnMgPT09IHRydWUgPyBmYWxzZSA6IHVuZGVmaW5lZCxcbiAgICAgICAgICB0cnVlXG4gICAgICAgIClcbiAgICAgIDogZmFsc2U7XG4gICAgLy8gV2FzIGdvaW5nIHRvIGFkZCB0aGlzIHRvIHRoZSBmaWx0ZXIgaG9va1xuICAgIGNvbHVtbi5maWx0ZXJWYWx1ZSA9IGZpbHRlcnNbaWRdO1xuICB9KTtcblxuICBjb25zdCBzZXRGaWx0ZXIgPSAoaWQsIHZhbCkgPT4ge1xuICAgIHJldHVybiBzZXRTdGF0ZShvbGQgPT4ge1xuICAgICAgaWYgKHR5cGVvZiB2YWwgPT09IFwidW5kZWZpbmVkXCIpIHtcbiAgICAgICAgY29uc3QgeyBbaWRdOiBwcmV2LCAuLi5yZXN0IH0gPSBmaWx0ZXJzO1xuICAgICAgICByZXR1cm4ge1xuICAgICAgICAgIC4uLm9sZCxcbiAgICAgICAgICBmaWx0ZXJzOiB7XG4gICAgICAgICAgICAuLi5yZXN0XG4gICAgICAgICAgfVxuICAgICAgICB9O1xuICAgICAgfVxuXG4gICAgICByZXR1cm4ge1xuICAgICAgICAuLi5vbGQsXG4gICAgICAgIGZpbHRlcnM6IHtcbiAgICAgICAgICAuLi5maWx0ZXJzLFxuICAgICAgICAgIFtpZF06IHZhbFxuICAgICAgICB9XG4gICAgICB9O1xuICAgIH0sIGFjdGlvbnMuc2V0RmlsdGVyKTtcbiAgfTtcblxuICBjb25zdCBzZXRBbGxGaWx0ZXJzID0gZmlsdGVycyA9PiB7XG4gICAgcmV0dXJuIHNldFN0YXRlKG9sZCA9PiB7XG4gICAgICByZXR1cm4ge1xuICAgICAgICAuLi5vbGQsXG4gICAgICAgIGZpbHRlcnNcbiAgICAgIH07XG4gICAgfSwgYWN0aW9ucy5zZXRBbGxGaWx0ZXJzKTtcbiAgfTtcblxuICBob29rcy5jb2x1bW5zLnB1c2goY29sdW1ucyA9PiB7XG4gICAgY29sdW1ucy5mb3JFYWNoKGNvbHVtbiA9PiB7XG4gICAgICBpZiAoY29sdW1uLmNhbkZpbHRlcikge1xuICAgICAgICBjb2x1bW4uc2V0RmlsdGVyID0gdmFsID0+IHNldEZpbHRlcihjb2x1bW4uaWQsIHZhbCk7XG4gICAgICB9XG4gICAgfSk7XG4gICAgcmV0dXJuIGNvbHVtbnM7XG4gIH0pO1xuXG4gIGNvbnN0IGZpbHRlcmVkUm93cyA9IHVzZU1lbW8oXG4gICAgKCkgPT4ge1xuICAgICAgaWYgKG1hbnVhbEZpbHRlcnMgfHwgIU9iamVjdC5rZXlzKGZpbHRlcnMpLmxlbmd0aCkge1xuICAgICAgICByZXR1cm4gcm93cztcbiAgICAgIH1cblxuICAgICAgaWYgKGRlYnVnKSBjb25zb2xlLmluZm8oXCJnZXRGaWx0ZXJlZFJvd3NcIik7XG5cbiAgICAgIC8vIEZpbHRlcnMgdG9wIGxldmVsIGFuZCBuZXN0ZWQgcm93c1xuICAgICAgY29uc3QgZmlsdGVyUm93cyA9IHJvd3MgPT4ge1xuICAgICAgICBsZXQgZmlsdGVyZWRSb3dzID0gcm93cztcblxuICAgICAgICBmaWx0ZXJlZFJvd3MgPSBPYmplY3QuZW50cmllcyhmaWx0ZXJzKS5yZWR1Y2UoXG4gICAgICAgICAgKGZpbHRlcmVkU29GYXIsIFtjb2x1bW5JRCwgZmlsdGVyVmFsdWVdKSA9PiB7XG4gICAgICAgICAgICAvLyBGaW5kIHRoZSBmaWx0ZXJzIGNvbHVtblxuICAgICAgICAgICAgY29uc3QgY29sdW1uID0gY29sdW1ucy5maW5kKGQgPT4gZC5pZCA9PT0gY29sdW1uSUQpO1xuXG4gICAgICAgICAgICAvLyBEb24ndCBmaWx0ZXIgaGlkZGVuIGNvbHVtbnMgb3IgY29sdW1ucyB0aGF0IGhhdmUgaGFkIHRoZWlyIGZpbHRlcnMgZGlzYWJsZWRcbiAgICAgICAgICAgIGlmICghY29sdW1uIHx8IGNvbHVtbi5maWx0ZXJhYmxlID09PSBmYWxzZSkge1xuICAgICAgICAgICAgICByZXR1cm4gZmlsdGVyZWRTb0ZhcjtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY29uc3QgZmlsdGVyTWV0aG9kID0gY29sdW1uLmZpbHRlck1ldGhvZCB8fCBmaWx0ZXJGbjtcblxuICAgICAgICAgICAgLy8gSWYgJ2ZpbHRlckFsbCcgaXMgc2V0IHRvIHRydWUsIHBhc3MgdGhlIGVudGlyZSBkYXRhc2V0IHRvIHRoZSBmaWx0ZXIgbWV0aG9kXG4gICAgICAgICAgICBpZiAoY29sdW1uLmZpbHRlckFsbCkge1xuICAgICAgICAgICAgICByZXR1cm4gZmlsdGVyTWV0aG9kKGZpbHRlcmVkU29GYXIsIGNvbHVtbklELCBmaWx0ZXJWYWx1ZSwgY29sdW1uKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBmaWx0ZXJlZFNvRmFyLmZpbHRlcihyb3cgPT5cbiAgICAgICAgICAgICAgZmlsdGVyTWV0aG9kKHJvdywgY29sdW1uSUQsIGZpbHRlclZhbHVlLCBjb2x1bW4pXG4gICAgICAgICAgICApO1xuICAgICAgICAgIH0sXG4gICAgICAgICAgcm93c1xuICAgICAgICApO1xuXG4gICAgICAgIC8vIEFwcGx5IHRoZSBmaWx0ZXIgdG8gYW55IHN1YlJvd3NcbiAgICAgICAgZmlsdGVyZWRSb3dzID0gZmlsdGVyZWRSb3dzLm1hcChyb3cgPT4ge1xuICAgICAgICAgIGlmICghcm93LnN1YlJvd3MpIHtcbiAgICAgICAgICAgIHJldHVybiByb3c7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAuLi5yb3csXG4gICAgICAgICAgICBzdWJSb3dzOiBmaWx0ZXJSb3dzKHJvdy5zdWJSb3dzKVxuICAgICAgICAgIH07XG4gICAgICAgIH0pO1xuXG4gICAgICAgIC8vIHRoZW4gZmlsdGVyIGFueSByb3dzIHdpdGhvdXQgc3ViY29sdW1ucyBiZWNhdXNlIGl0IHdvdWxkIGJlIHN0cmFuZ2UgdG8gc2hvd1xuICAgICAgICBmaWx0ZXJlZFJvd3MgPSBmaWx0ZXJlZFJvd3MuZmlsdGVyKHJvdyA9PiB7XG4gICAgICAgICAgaWYgKCFyb3cuc3ViUm93cykge1xuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiByb3cuc3ViUm93cy5sZW5ndGggPiAwO1xuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gZmlsdGVyZWRSb3dzO1xuICAgICAgfTtcblxuICAgICAgcmV0dXJuIGZpbHRlclJvd3Mocm93cyk7XG4gICAgfSxcbiAgICBbcm93cywgZmlsdGVycywgbWFudWFsRmlsdGVyc11cbiAgKTtcblxuICByZXR1cm4ge1xuICAgIC4uLnByb3BzLFxuICAgIHNldEZpbHRlcixcbiAgICBzZXRBbGxGaWx0ZXJzLFxuICAgIHJvd3M6IGZpbHRlcmVkUm93c1xuICB9O1xufTtcbiJdfQ==