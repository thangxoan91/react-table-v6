var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

import { useMemo, useState, useLayoutEffect } from "react";
import PropTypes from "prop-types";

import { addActions, actions } from "../actions";
import { defaultState } from "./useTableState";

defaultState.pageSize = 10;
defaultState.pageIndex = 0;

addActions({
  pageChange: "__pageChange__"
});

var propTypes = {
  defaultPageSize: PropTypes.number,
  defaultPageIndex: PropTypes.number,
  pageSize: PropTypes.number,
  pages: PropTypes.number,
  pageIndex: PropTypes.number,
  onStateChange: PropTypes.func,
  stateReducer: PropTypes.func,
  debug: PropTypes.bool
};

export var usePagination = function usePagination(props) {
  // Validate props
  PropTypes.checkPropTypes(propTypes, props, "property", "usePagination");

  var parentDebug = props.debug,
      rows = props.rows,
      manualPagination = props.manualPagination,
      _props$debug = props.debug,
      debug = _props$debug === undefined ? parentDebug : _props$debug,
      _props$state = _slicedToArray(props.state, 2),
      _props$state$ = _props$state[0],
      pageSize = _props$state$.pageSize,
      pageIndex = _props$state$.pageIndex,
      userPageCount = _props$state$.pageCount,
      filters = _props$state$.filters,
      groupBy = _props$state$.groupBy,
      sortBy = _props$state$.sortBy,
      setState = _props$state[1];

  useLayoutEffect(function () {
    setState(function (old) {
      return _extends({}, old, {
        pageIndex: 0
      });
    }, actions.pageChange);
  }, [filters, groupBy, sortBy]);

  var _useMemo = useMemo(function () {
    if (manualPagination) {
      return {
        pages: [rows],
        pageCount: userPageCount
      };
    }
    if (debug) console.info("getPages");

    // Create a new pages with the first page ready to go.
    var pages = rows.length ? [] : [[]];

    // Start the pageIndex and currentPage cursors
    var cursor = 0;
    while (cursor < rows.length) {
      var end = cursor + pageSize;
      pages.push(rows.slice(cursor, end));
      cursor = end;
    }

    var pageCount = pages.length;

    return {
      pages: pages,
      pageCount: pageCount,
      pageOptions: pageOptions
    };
  }, [rows, pageSize, userPageCount]),
      pages = _useMemo.pages,
      pageCount = _useMemo.pageCount;

  var pageOptions = [].concat(_toConsumableArray(new Array(pageCount))).map(function (d, i) {
    return i;
  });
  var page = manualPagination ? rows : pages[pageIndex] || [];
  var canPreviousPage = pageIndex > 0;
  var canNextPage = pageIndex < pageCount - 1;

  var gotoPage = function gotoPage(pageIndex) {
    if (debug) console.info("gotoPage");
    return setState(function (old) {
      if (pageIndex < 0 || pageIndex > pageCount - 1) {
        return old;
      }
      return _extends({}, old, {
        pageIndex: pageIndex
      });
    }, actions.pageChange);
  };

  var previousPage = function previousPage() {
    return gotoPage(pageIndex - 1);
  };

  var nextPage = function nextPage() {
    return gotoPage(pageIndex + 1);
  };

  var setPageSize = function setPageSize(pageSize) {
    setState(function (old) {
      var topRowIndex = old.pageSize * old.pageIndex;
      var pageIndex = Math.floor(topRowIndex / pageSize);
      return _extends({}, old, {
        pageIndex: pageIndex,
        pageSize: pageSize
      });
    }, actions.setPageSize);
  };

  return _extends({}, props, {
    pages: pages,
    pageOptions: pageOptions,
    page: page,
    canPreviousPage: canPreviousPage,
    canNextPage: canNextPage,
    gotoPage: gotoPage,
    previousPage: previousPage,
    nextPage: nextPage,
    setPageSize: setPageSize
  });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VQYWdpbmF0aW9uLmpzIl0sIm5hbWVzIjpbInVzZU1lbW8iLCJ1c2VTdGF0ZSIsInVzZUxheW91dEVmZmVjdCIsIlByb3BUeXBlcyIsImFkZEFjdGlvbnMiLCJhY3Rpb25zIiwiZGVmYXVsdFN0YXRlIiwicGFnZVNpemUiLCJwYWdlSW5kZXgiLCJwYWdlQ2hhbmdlIiwicHJvcFR5cGVzIiwiZGVmYXVsdFBhZ2VTaXplIiwibnVtYmVyIiwiZGVmYXVsdFBhZ2VJbmRleCIsInBhZ2VzIiwib25TdGF0ZUNoYW5nZSIsImZ1bmMiLCJzdGF0ZVJlZHVjZXIiLCJkZWJ1ZyIsImJvb2wiLCJ1c2VQYWdpbmF0aW9uIiwiY2hlY2tQcm9wVHlwZXMiLCJwcm9wcyIsInBhcmVudERlYnVnIiwicm93cyIsIm1hbnVhbFBhZ2luYXRpb24iLCJzdGF0ZSIsInVzZXJQYWdlQ291bnQiLCJwYWdlQ291bnQiLCJmaWx0ZXJzIiwiZ3JvdXBCeSIsInNvcnRCeSIsInNldFN0YXRlIiwib2xkIiwiY29uc29sZSIsImluZm8iLCJsZW5ndGgiLCJjdXJzb3IiLCJlbmQiLCJwdXNoIiwic2xpY2UiLCJwYWdlT3B0aW9ucyIsIkFycmF5IiwibWFwIiwiZCIsImkiLCJwYWdlIiwiY2FuUHJldmlvdXNQYWdlIiwiY2FuTmV4dFBhZ2UiLCJnb3RvUGFnZSIsInByZXZpb3VzUGFnZSIsIm5leHRQYWdlIiwic2V0UGFnZVNpemUiLCJ0b3BSb3dJbmRleCIsIk1hdGgiLCJmbG9vciJdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsU0FBU0EsT0FBVCxFQUFrQkMsUUFBbEIsRUFBNEJDLGVBQTVCLFFBQW1ELE9BQW5EO0FBQ0EsT0FBT0MsU0FBUCxNQUFzQixZQUF0Qjs7QUFFQSxTQUFTQyxVQUFULEVBQXFCQyxPQUFyQixRQUFvQyxZQUFwQztBQUNBLFNBQVNDLFlBQVQsUUFBNkIsaUJBQTdCOztBQUVBQSxhQUFhQyxRQUFiLEdBQXdCLEVBQXhCO0FBQ0FELGFBQWFFLFNBQWIsR0FBeUIsQ0FBekI7O0FBRUFKLFdBQVc7QUFDVEssY0FBWTtBQURILENBQVg7O0FBSUEsSUFBTUMsWUFBWTtBQUNoQkMsbUJBQWlCUixVQUFVUyxNQURYO0FBRWhCQyxvQkFBa0JWLFVBQVVTLE1BRlo7QUFHaEJMLFlBQVVKLFVBQVVTLE1BSEo7QUFJaEJFLFNBQU9YLFVBQVVTLE1BSkQ7QUFLaEJKLGFBQVdMLFVBQVVTLE1BTEw7QUFNaEJHLGlCQUFlWixVQUFVYSxJQU5UO0FBT2hCQyxnQkFBY2QsVUFBVWEsSUFQUjtBQVFoQkUsU0FBT2YsVUFBVWdCO0FBUkQsQ0FBbEI7O0FBV0EsT0FBTyxJQUFNQyxnQkFBZ0IsU0FBaEJBLGFBQWdCLFFBQVM7QUFDcEM7QUFDQWpCLFlBQVVrQixjQUFWLENBQXlCWCxTQUF6QixFQUFvQ1ksS0FBcEMsRUFBMkMsVUFBM0MsRUFBdUQsZUFBdkQ7O0FBRm9DLE1BSzNCQyxXQUwyQixHQW9CaENELEtBcEJnQyxDQUtsQ0osS0FMa0M7QUFBQSxNQU1sQ00sSUFOa0MsR0FvQmhDRixLQXBCZ0MsQ0FNbENFLElBTmtDO0FBQUEsTUFPbENDLGdCQVBrQyxHQW9CaENILEtBcEJnQyxDQU9sQ0csZ0JBUGtDO0FBQUEscUJBb0JoQ0gsS0FwQmdDLENBUWxDSixLQVJrQztBQUFBLE1BUWxDQSxLQVJrQyxnQ0FRMUJLLFdBUjBCO0FBQUEsb0NBb0JoQ0QsS0FwQmdDLENBU2xDSSxLQVRrQztBQUFBO0FBQUEsTUFXOUJuQixRQVg4QixpQkFXOUJBLFFBWDhCO0FBQUEsTUFZOUJDLFNBWjhCLGlCQVk5QkEsU0FaOEI7QUFBQSxNQWFuQm1CLGFBYm1CLGlCQWE5QkMsU0FiOEI7QUFBQSxNQWM5QkMsT0FkOEIsaUJBYzlCQSxPQWQ4QjtBQUFBLE1BZTlCQyxPQWY4QixpQkFlOUJBLE9BZjhCO0FBQUEsTUFnQjlCQyxNQWhCOEIsaUJBZ0I5QkEsTUFoQjhCO0FBQUEsTUFrQmhDQyxRQWxCZ0M7O0FBc0JwQzlCLGtCQUNFLFlBQU07QUFDSjhCLGFBQ0U7QUFBQSwwQkFDS0MsR0FETDtBQUVFekIsbUJBQVc7QUFGYjtBQUFBLEtBREYsRUFLRUgsUUFBUUksVUFMVjtBQU9ELEdBVEgsRUFVRSxDQUFDb0IsT0FBRCxFQUFVQyxPQUFWLEVBQW1CQyxNQUFuQixDQVZGOztBQXRCb0MsaUJBbUNQL0IsUUFDM0IsWUFBTTtBQUNKLFFBQUl5QixnQkFBSixFQUFzQjtBQUNwQixhQUFPO0FBQ0xYLGVBQU8sQ0FBQ1UsSUFBRCxDQURGO0FBRUxJLG1CQUFXRDtBQUZOLE9BQVA7QUFJRDtBQUNELFFBQUlULEtBQUosRUFBV2dCLFFBQVFDLElBQVIsQ0FBYSxVQUFiOztBQUVYO0FBQ0EsUUFBTXJCLFFBQVFVLEtBQUtZLE1BQUwsR0FBYyxFQUFkLEdBQW1CLENBQUMsRUFBRCxDQUFqQzs7QUFFQTtBQUNBLFFBQUlDLFNBQVMsQ0FBYjtBQUNBLFdBQU9BLFNBQVNiLEtBQUtZLE1BQXJCLEVBQTZCO0FBQzNCLFVBQU1FLE1BQU1ELFNBQVM5QixRQUFyQjtBQUNBTyxZQUFNeUIsSUFBTixDQUFXZixLQUFLZ0IsS0FBTCxDQUFXSCxNQUFYLEVBQW1CQyxHQUFuQixDQUFYO0FBQ0FELGVBQVNDLEdBQVQ7QUFDRDs7QUFFRCxRQUFNVixZQUFZZCxNQUFNc0IsTUFBeEI7O0FBRUEsV0FBTztBQUNMdEIsa0JBREs7QUFFTGMsMEJBRks7QUFHTGE7QUFISyxLQUFQO0FBS0QsR0E1QjBCLEVBNkIzQixDQUFDakIsSUFBRCxFQUFPakIsUUFBUCxFQUFpQm9CLGFBQWpCLENBN0IyQixDQW5DTztBQUFBLE1BbUM1QmIsS0FuQzRCLFlBbUM1QkEsS0FuQzRCO0FBQUEsTUFtQ3JCYyxTQW5DcUIsWUFtQ3JCQSxTQW5DcUI7O0FBbUVwQyxNQUFNYSxjQUFjLDZCQUFJLElBQUlDLEtBQUosQ0FBVWQsU0FBVixDQUFKLEdBQTBCZSxHQUExQixDQUE4QixVQUFDQyxDQUFELEVBQUlDLENBQUo7QUFBQSxXQUFVQSxDQUFWO0FBQUEsR0FBOUIsQ0FBcEI7QUFDQSxNQUFNQyxPQUFPckIsbUJBQW1CRCxJQUFuQixHQUEwQlYsTUFBTU4sU0FBTixLQUFvQixFQUEzRDtBQUNBLE1BQU11QyxrQkFBa0J2QyxZQUFZLENBQXBDO0FBQ0EsTUFBTXdDLGNBQWN4QyxZQUFZb0IsWUFBWSxDQUE1Qzs7QUFFQSxNQUFNcUIsV0FBVyxTQUFYQSxRQUFXLFlBQWE7QUFDNUIsUUFBSS9CLEtBQUosRUFBV2dCLFFBQVFDLElBQVIsQ0FBYSxVQUFiO0FBQ1gsV0FBT0gsU0FBUyxlQUFPO0FBQ3JCLFVBQUl4QixZQUFZLENBQVosSUFBaUJBLFlBQVlvQixZQUFZLENBQTdDLEVBQWdEO0FBQzlDLGVBQU9LLEdBQVA7QUFDRDtBQUNELDBCQUNLQSxHQURMO0FBRUV6QjtBQUZGO0FBSUQsS0FSTSxFQVFKSCxRQUFRSSxVQVJKLENBQVA7QUFTRCxHQVhEOztBQWFBLE1BQU15QyxlQUFlLFNBQWZBLFlBQWUsR0FBTTtBQUN6QixXQUFPRCxTQUFTekMsWUFBWSxDQUFyQixDQUFQO0FBQ0QsR0FGRDs7QUFJQSxNQUFNMkMsV0FBVyxTQUFYQSxRQUFXLEdBQU07QUFDckIsV0FBT0YsU0FBU3pDLFlBQVksQ0FBckIsQ0FBUDtBQUNELEdBRkQ7O0FBSUEsTUFBTTRDLGNBQWMsU0FBZEEsV0FBYyxXQUFZO0FBQzlCcEIsYUFBUyxlQUFPO0FBQ2QsVUFBTXFCLGNBQWNwQixJQUFJMUIsUUFBSixHQUFlMEIsSUFBSXpCLFNBQXZDO0FBQ0EsVUFBTUEsWUFBWThDLEtBQUtDLEtBQUwsQ0FBV0YsY0FBYzlDLFFBQXpCLENBQWxCO0FBQ0EsMEJBQ0swQixHQURMO0FBRUV6Qiw0QkFGRjtBQUdFRDtBQUhGO0FBS0QsS0FSRCxFQVFHRixRQUFRK0MsV0FSWDtBQVNELEdBVkQ7O0FBWUEsc0JBQ0s5QixLQURMO0FBRUVSLGdCQUZGO0FBR0UyQiw0QkFIRjtBQUlFSyxjQUpGO0FBS0VDLG9DQUxGO0FBTUVDLDRCQU5GO0FBT0VDLHNCQVBGO0FBUUVDLDhCQVJGO0FBU0VDLHNCQVRGO0FBVUVDO0FBVkY7QUFZRCxDQXJITSIsImZpbGUiOiJ1c2VQYWdpbmF0aW9uLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlTWVtbywgdXNlU3RhdGUsIHVzZUxheW91dEVmZmVjdCB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tIFwicHJvcC10eXBlc1wiO1xuXG5pbXBvcnQgeyBhZGRBY3Rpb25zLCBhY3Rpb25zIH0gZnJvbSBcIi4uL2FjdGlvbnNcIjtcbmltcG9ydCB7IGRlZmF1bHRTdGF0ZSB9IGZyb20gXCIuL3VzZVRhYmxlU3RhdGVcIjtcblxuZGVmYXVsdFN0YXRlLnBhZ2VTaXplID0gMTA7XG5kZWZhdWx0U3RhdGUucGFnZUluZGV4ID0gMDtcblxuYWRkQWN0aW9ucyh7XG4gIHBhZ2VDaGFuZ2U6IFwiX19wYWdlQ2hhbmdlX19cIlxufSk7XG5cbmNvbnN0IHByb3BUeXBlcyA9IHtcbiAgZGVmYXVsdFBhZ2VTaXplOiBQcm9wVHlwZXMubnVtYmVyLFxuICBkZWZhdWx0UGFnZUluZGV4OiBQcm9wVHlwZXMubnVtYmVyLFxuICBwYWdlU2l6ZTogUHJvcFR5cGVzLm51bWJlcixcbiAgcGFnZXM6IFByb3BUeXBlcy5udW1iZXIsXG4gIHBhZ2VJbmRleDogUHJvcFR5cGVzLm51bWJlcixcbiAgb25TdGF0ZUNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gIHN0YXRlUmVkdWNlcjogUHJvcFR5cGVzLmZ1bmMsXG4gIGRlYnVnOiBQcm9wVHlwZXMuYm9vbFxufTtcblxuZXhwb3J0IGNvbnN0IHVzZVBhZ2luYXRpb24gPSBwcm9wcyA9PiB7XG4gIC8vIFZhbGlkYXRlIHByb3BzXG4gIFByb3BUeXBlcy5jaGVja1Byb3BUeXBlcyhwcm9wVHlwZXMsIHByb3BzLCBcInByb3BlcnR5XCIsIFwidXNlUGFnaW5hdGlvblwiKTtcblxuICBjb25zdCB7XG4gICAgZGVidWc6IHBhcmVudERlYnVnLFxuICAgIHJvd3MsXG4gICAgbWFudWFsUGFnaW5hdGlvbixcbiAgICBkZWJ1ZyA9IHBhcmVudERlYnVnLFxuICAgIHN0YXRlOiBbXG4gICAgICB7XG4gICAgICAgIHBhZ2VTaXplLFxuICAgICAgICBwYWdlSW5kZXgsXG4gICAgICAgIHBhZ2VDb3VudDogdXNlclBhZ2VDb3VudCxcbiAgICAgICAgZmlsdGVycyxcbiAgICAgICAgZ3JvdXBCeSxcbiAgICAgICAgc29ydEJ5XG4gICAgICB9LFxuICAgICAgc2V0U3RhdGVcbiAgICBdXG4gIH0gPSBwcm9wcztcblxuICB1c2VMYXlvdXRFZmZlY3QoXG4gICAgKCkgPT4ge1xuICAgICAgc2V0U3RhdGUoXG4gICAgICAgIG9sZCA9PiAoe1xuICAgICAgICAgIC4uLm9sZCxcbiAgICAgICAgICBwYWdlSW5kZXg6IDBcbiAgICAgICAgfSksXG4gICAgICAgIGFjdGlvbnMucGFnZUNoYW5nZVxuICAgICAgKTtcbiAgICB9LFxuICAgIFtmaWx0ZXJzLCBncm91cEJ5LCBzb3J0QnldXG4gICk7XG5cbiAgY29uc3QgeyBwYWdlcywgcGFnZUNvdW50IH0gPSB1c2VNZW1vKFxuICAgICgpID0+IHtcbiAgICAgIGlmIChtYW51YWxQYWdpbmF0aW9uKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgcGFnZXM6IFtyb3dzXSxcbiAgICAgICAgICBwYWdlQ291bnQ6IHVzZXJQYWdlQ291bnRcbiAgICAgICAgfTtcbiAgICAgIH1cbiAgICAgIGlmIChkZWJ1ZykgY29uc29sZS5pbmZvKFwiZ2V0UGFnZXNcIik7XG5cbiAgICAgIC8vIENyZWF0ZSBhIG5ldyBwYWdlcyB3aXRoIHRoZSBmaXJzdCBwYWdlIHJlYWR5IHRvIGdvLlxuICAgICAgY29uc3QgcGFnZXMgPSByb3dzLmxlbmd0aCA/IFtdIDogW1tdXTtcblxuICAgICAgLy8gU3RhcnQgdGhlIHBhZ2VJbmRleCBhbmQgY3VycmVudFBhZ2UgY3Vyc29yc1xuICAgICAgbGV0IGN1cnNvciA9IDA7XG4gICAgICB3aGlsZSAoY3Vyc29yIDwgcm93cy5sZW5ndGgpIHtcbiAgICAgICAgY29uc3QgZW5kID0gY3Vyc29yICsgcGFnZVNpemU7XG4gICAgICAgIHBhZ2VzLnB1c2gocm93cy5zbGljZShjdXJzb3IsIGVuZCkpO1xuICAgICAgICBjdXJzb3IgPSBlbmQ7XG4gICAgICB9XG5cbiAgICAgIGNvbnN0IHBhZ2VDb3VudCA9IHBhZ2VzLmxlbmd0aDtcblxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgcGFnZXMsXG4gICAgICAgIHBhZ2VDb3VudCxcbiAgICAgICAgcGFnZU9wdGlvbnNcbiAgICAgIH07XG4gICAgfSxcbiAgICBbcm93cywgcGFnZVNpemUsIHVzZXJQYWdlQ291bnRdXG4gICk7XG5cbiAgY29uc3QgcGFnZU9wdGlvbnMgPSBbLi4ubmV3IEFycmF5KHBhZ2VDb3VudCldLm1hcCgoZCwgaSkgPT4gaSk7XG4gIGNvbnN0IHBhZ2UgPSBtYW51YWxQYWdpbmF0aW9uID8gcm93cyA6IHBhZ2VzW3BhZ2VJbmRleF0gfHwgW107XG4gIGNvbnN0IGNhblByZXZpb3VzUGFnZSA9IHBhZ2VJbmRleCA+IDA7XG4gIGNvbnN0IGNhbk5leHRQYWdlID0gcGFnZUluZGV4IDwgcGFnZUNvdW50IC0gMTtcblxuICBjb25zdCBnb3RvUGFnZSA9IHBhZ2VJbmRleCA9PiB7XG4gICAgaWYgKGRlYnVnKSBjb25zb2xlLmluZm8oXCJnb3RvUGFnZVwiKTtcbiAgICByZXR1cm4gc2V0U3RhdGUob2xkID0+IHtcbiAgICAgIGlmIChwYWdlSW5kZXggPCAwIHx8IHBhZ2VJbmRleCA+IHBhZ2VDb3VudCAtIDEpIHtcbiAgICAgICAgcmV0dXJuIG9sZDtcbiAgICAgIH1cbiAgICAgIHJldHVybiB7XG4gICAgICAgIC4uLm9sZCxcbiAgICAgICAgcGFnZUluZGV4XG4gICAgICB9O1xuICAgIH0sIGFjdGlvbnMucGFnZUNoYW5nZSk7XG4gIH07XG5cbiAgY29uc3QgcHJldmlvdXNQYWdlID0gKCkgPT4ge1xuICAgIHJldHVybiBnb3RvUGFnZShwYWdlSW5kZXggLSAxKTtcbiAgfTtcblxuICBjb25zdCBuZXh0UGFnZSA9ICgpID0+IHtcbiAgICByZXR1cm4gZ290b1BhZ2UocGFnZUluZGV4ICsgMSk7XG4gIH07XG5cbiAgY29uc3Qgc2V0UGFnZVNpemUgPSBwYWdlU2l6ZSA9PiB7XG4gICAgc2V0U3RhdGUob2xkID0+IHtcbiAgICAgIGNvbnN0IHRvcFJvd0luZGV4ID0gb2xkLnBhZ2VTaXplICogb2xkLnBhZ2VJbmRleDtcbiAgICAgIGNvbnN0IHBhZ2VJbmRleCA9IE1hdGguZmxvb3IodG9wUm93SW5kZXggLyBwYWdlU2l6ZSk7XG4gICAgICByZXR1cm4ge1xuICAgICAgICAuLi5vbGQsXG4gICAgICAgIHBhZ2VJbmRleCxcbiAgICAgICAgcGFnZVNpemVcbiAgICAgIH07XG4gICAgfSwgYWN0aW9ucy5zZXRQYWdlU2l6ZSk7XG4gIH07XG5cbiAgcmV0dXJuIHtcbiAgICAuLi5wcm9wcyxcbiAgICBwYWdlcyxcbiAgICBwYWdlT3B0aW9ucyxcbiAgICBwYWdlLFxuICAgIGNhblByZXZpb3VzUGFnZSxcbiAgICBjYW5OZXh0UGFnZSxcbiAgICBnb3RvUGFnZSxcbiAgICBwcmV2aW91c1BhZ2UsXG4gICAgbmV4dFBhZ2UsXG4gICAgc2V0UGFnZVNpemVcbiAgfTtcbn07XG4iXX0=