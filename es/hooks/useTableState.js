var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

import React, { useState, useMemo } from "react";

export var defaultState = {};

var defaultReducer = function defaultReducer(old, newState, type) {
  return newState;
};

export var useTableState = function useTableState() {
  var initialState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var overrides = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var _ref = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
      _ref$reducer = _ref.reducer,
      reducer = _ref$reducer === undefined ? defaultReducer : _ref$reducer,
      _ref$useState = _ref.useState,
      userUseState = _ref$useState === undefined ? useState : _ref$useState;

  var _userUseState = userUseState(_extends({}, defaultState, initialState)),
      _userUseState2 = _slicedToArray(_userUseState, 2),
      state = _userUseState2[0],
      setState = _userUseState2[1];

  var overriddenState = useMemo(function () {
    var newState = _extends({}, state);
    Object.keys(overrides).forEach(function (key) {
      newState[key] = overrides[key];
    });
    return newState;
  }, [state].concat(_toConsumableArray(Object.values(overrides))));

  var reducedSetState = function reducedSetState(updater, type) {
    return setState(function (old) {
      var newState = updater(old);
      return reducer(old, newState, type);
    });
  };

  return [overriddenState, reducedSetState];
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VUYWJsZVN0YXRlLmpzIl0sIm5hbWVzIjpbIlJlYWN0IiwidXNlU3RhdGUiLCJ1c2VNZW1vIiwiZGVmYXVsdFN0YXRlIiwiZGVmYXVsdFJlZHVjZXIiLCJvbGQiLCJuZXdTdGF0ZSIsInR5cGUiLCJ1c2VUYWJsZVN0YXRlIiwiaW5pdGlhbFN0YXRlIiwib3ZlcnJpZGVzIiwicmVkdWNlciIsInVzZXJVc2VTdGF0ZSIsInN0YXRlIiwic2V0U3RhdGUiLCJvdmVycmlkZGVuU3RhdGUiLCJPYmplY3QiLCJrZXlzIiwiZm9yRWFjaCIsImtleSIsInZhbHVlcyIsInJlZHVjZWRTZXRTdGF0ZSIsInVwZGF0ZXIiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLE9BQU9BLEtBQVAsSUFBZ0JDLFFBQWhCLEVBQTBCQyxPQUExQixRQUF5QyxPQUF6Qzs7QUFFQSxPQUFPLElBQU1DLGVBQWUsRUFBckI7O0FBRVAsSUFBTUMsaUJBQWlCLFNBQWpCQSxjQUFpQixDQUFDQyxHQUFELEVBQU1DLFFBQU4sRUFBZ0JDLElBQWhCO0FBQUEsU0FBeUJELFFBQXpCO0FBQUEsQ0FBdkI7O0FBRUEsT0FBTyxJQUFNRSxnQkFBZ0IsU0FBaEJBLGFBQWdCLEdBSXhCO0FBQUEsTUFISEMsWUFHRyx1RUFIWSxFQUdaO0FBQUEsTUFGSEMsU0FFRyx1RUFGUyxFQUVUOztBQUFBLGlGQUQrRCxFQUMvRDtBQUFBLDBCQUREQyxPQUNDO0FBQUEsTUFEREEsT0FDQyxnQ0FEU1AsY0FDVDtBQUFBLDJCQUR5QkgsUUFDekI7QUFBQSxNQURtQ1csWUFDbkMsaUNBRGtEWCxRQUNsRDs7QUFBQSxzQkFDcUJXLDBCQUNuQlQsWUFEbUIsRUFFbkJNLFlBRm1CLEVBRHJCO0FBQUE7QUFBQSxNQUNFSSxLQURGO0FBQUEsTUFDU0MsUUFEVDs7QUFNSCxNQUFNQyxrQkFBa0JiLFFBQ3RCLFlBQU07QUFDSixRQUFNSSx3QkFDRE8sS0FEQyxDQUFOO0FBR0FHLFdBQU9DLElBQVAsQ0FBWVAsU0FBWixFQUF1QlEsT0FBdkIsQ0FBK0IsZUFBTztBQUNwQ1osZUFBU2EsR0FBVCxJQUFnQlQsVUFBVVMsR0FBVixDQUFoQjtBQUNELEtBRkQ7QUFHQSxXQUFPYixRQUFQO0FBQ0QsR0FUcUIsR0FVckJPLEtBVnFCLDRCQVVYRyxPQUFPSSxNQUFQLENBQWNWLFNBQWQsQ0FWVyxHQUF4Qjs7QUFhQSxNQUFNVyxrQkFBa0IsU0FBbEJBLGVBQWtCLENBQUNDLE9BQUQsRUFBVWYsSUFBVjtBQUFBLFdBQ3RCTyxTQUFTLGVBQU87QUFDZCxVQUFNUixXQUFXZ0IsUUFBUWpCLEdBQVIsQ0FBakI7QUFDQSxhQUFPTSxRQUFRTixHQUFSLEVBQWFDLFFBQWIsRUFBdUJDLElBQXZCLENBQVA7QUFDRCxLQUhELENBRHNCO0FBQUEsR0FBeEI7O0FBTUEsU0FBTyxDQUFDUSxlQUFELEVBQWtCTSxlQUFsQixDQUFQO0FBQ0QsQ0E5Qk0iLCJmaWxlIjoidXNlVGFibGVTdGF0ZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSwgdXNlTWVtbyB9IGZyb20gXCJyZWFjdFwiO1xuXG5leHBvcnQgY29uc3QgZGVmYXVsdFN0YXRlID0ge307XG5cbmNvbnN0IGRlZmF1bHRSZWR1Y2VyID0gKG9sZCwgbmV3U3RhdGUsIHR5cGUpID0+IG5ld1N0YXRlO1xuXG5leHBvcnQgY29uc3QgdXNlVGFibGVTdGF0ZSA9IChcbiAgaW5pdGlhbFN0YXRlID0ge30sXG4gIG92ZXJyaWRlcyA9IHt9LFxuICB7IHJlZHVjZXIgPSBkZWZhdWx0UmVkdWNlciwgdXNlU3RhdGU6IHVzZXJVc2VTdGF0ZSA9IHVzZVN0YXRlIH0gPSB7fVxuKSA9PiB7XG4gIGxldCBbc3RhdGUsIHNldFN0YXRlXSA9IHVzZXJVc2VTdGF0ZSh7XG4gICAgLi4uZGVmYXVsdFN0YXRlLFxuICAgIC4uLmluaXRpYWxTdGF0ZVxuICB9KTtcblxuICBjb25zdCBvdmVycmlkZGVuU3RhdGUgPSB1c2VNZW1vKFxuICAgICgpID0+IHtcbiAgICAgIGNvbnN0IG5ld1N0YXRlID0ge1xuICAgICAgICAuLi5zdGF0ZVxuICAgICAgfTtcbiAgICAgIE9iamVjdC5rZXlzKG92ZXJyaWRlcykuZm9yRWFjaChrZXkgPT4ge1xuICAgICAgICBuZXdTdGF0ZVtrZXldID0gb3ZlcnJpZGVzW2tleV07XG4gICAgICB9KTtcbiAgICAgIHJldHVybiBuZXdTdGF0ZTtcbiAgICB9LFxuICAgIFtzdGF0ZSwgLi4uT2JqZWN0LnZhbHVlcyhvdmVycmlkZXMpXVxuICApO1xuXG4gIGNvbnN0IHJlZHVjZWRTZXRTdGF0ZSA9ICh1cGRhdGVyLCB0eXBlKSA9PlxuICAgIHNldFN0YXRlKG9sZCA9PiB7XG4gICAgICBjb25zdCBuZXdTdGF0ZSA9IHVwZGF0ZXIob2xkKTtcbiAgICAgIHJldHVybiByZWR1Y2VyKG9sZCwgbmV3U3RhdGUsIHR5cGUpO1xuICAgIH0pO1xuXG4gIHJldHVybiBbb3ZlcnJpZGRlblN0YXRlLCByZWR1Y2VkU2V0U3RhdGVdO1xufTtcbiJdfQ==