var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

import { useMemo } from "react";

import { addActions, actions } from "../actions";
import { defaultState } from "./useTableState";
import { mergeProps, applyPropHooks, getFirstDefined, defaultOrderByFn, defaultSortByFn } from "../utils";

defaultState.sortBy = [];

addActions({
  sortByChange: "__sortByChange__"
});

export var useSortBy = function useSortBy(api) {
  var debug = api.debug,
      rows = api.rows,
      columns = api.columns,
      _api$orderByFn = api.orderByFn,
      orderByFn = _api$orderByFn === undefined ? defaultOrderByFn : _api$orderByFn,
      _api$sortByFn = api.sortByFn,
      sortByFn = _api$sortByFn === undefined ? defaultSortByFn : _api$sortByFn,
      manualSorting = api.manualSorting,
      disableSorting = api.disableSorting,
      defaultSortDesc = api.defaultSortDesc,
      hooks = api.hooks,
      _api$state = _slicedToArray(api.state, 2),
      sortBy = _api$state[0].sortBy,
      setState = _api$state[1];

  columns.forEach(function (column) {
    var accessor = column.accessor,
        canSortBy = column.canSortBy;

    column.canSortBy = accessor ? getFirstDefined(canSortBy, disableSorting === true ? false : undefined, true) : false;
  });

  // Updates sorting based on a columnID, desc flag and multi flag
  var toggleSortByID = function toggleSortByID(columnID, desc, multi) {
    return setState(function (old) {
      var sortBy = old.sortBy;

      // Find the column for this columnID

      var column = columns.find(function (d) {
        return d.id === columnID;
      });
      var resolvedDefaultSortDesc = getFirstDefined(column.defaultSortDesc, defaultSortDesc);

      // Find any existing sortBy for this column
      var existingSortBy = sortBy.find(function (d) {
        return d.id === columnID;
      });
      var hasDescDefined = typeof desc !== "undefined" && desc !== null;

      var newSortBy = [];

      // What should we do with this filter?
      var action = void 0;

      if (!multi) {
        if (sortBy.length <= 1 && existingSortBy) {
          if (existingSortBy.desc) {
            action = "remove";
          } else {
            action = "toggle";
          }
        } else {
          action = "replace";
        }
      } else {
        if (!existingSortBy) {
          action = "add";
        } else {
          if (hasDescDefined) {
            action = "set";
          } else {
            action = "toggle";
          }
        }
      }

      if (action === "replace") {
        newSortBy = [{
          id: columnID,
          desc: hasDescDefined ? desc : resolvedDefaultSortDesc
        }];
      } else if (action === "add") {
        newSortBy = [].concat(_toConsumableArray(sortBy), [{
          id: columnID,
          desc: hasDescDefined ? desc : resolvedDefaultSortDesc
        }]);
      } else if (action === "set") {
        newSortBy = sortBy.map(function (d) {
          if (d.id === columnID) {
            return _extends({}, d, {
              desc: desc
            });
          }
          return d;
        });
      } else if (action === "toggle") {
        newSortBy = sortBy.map(function (d) {
          if (d.id === columnID) {
            return _extends({}, d, {
              desc: !existingSortBy.desc
            });
          }
          return d;
        });
      } else if (action === "remove") {
        newSortBy = [];
      }

      return _extends({}, old, {
        sortBy: newSortBy
      });
    }, actions.sortByChange);
  };

  hooks.columns.push(function (columns) {
    columns.forEach(function (column) {
      if (column.canSortBy) {
        column.toggleSortBy = function (desc, multi) {
          return toggleSortByID(column.id, desc, multi);
        };
      }
    });
    return columns;
  });

  hooks.getSortByToggleProps = [];

  var addSortByToggleProps = function addSortByToggleProps(columns, api) {
    columns.forEach(function (column) {
      var canSortBy = column.canSortBy;

      column.getSortByToggleProps = function (props) {
        return mergeProps({
          onClick: canSortBy ? function (e) {
            e.persist();
            column.toggleSortBy(undefined, !api.disableMultiSort && e.shiftKey);
          } : undefined,
          style: {
            cursor: canSortBy ? "pointer" : undefined
          },
          title: "Toggle SortBy"
        }, applyPropHooks(api.hooks.getSortByToggleProps, column, api), props);
      };
    });
    return columns;
  };

  hooks.columns.push(addSortByToggleProps);
  hooks.headers.push(addSortByToggleProps);

  // Mutate columns to reflect sorting state
  columns.forEach(function (column) {
    var id = column.id;

    column.sorted = sortBy.find(function (d) {
      return d.id === id;
    });
    column.sortedIndex = sortBy.findIndex(function (d) {
      return d.id === id;
    });
    column.sortedDesc = column.sorted ? column.sorted.desc : undefined;
  });

  var sortedRows = useMemo(function () {
    if (manualSorting || !sortBy.length) {
      return rows;
    }
    if (debug) console.info("getSortedRows");

    var sortMethodsByColumnID = {};

    columns.filter(function (col) {
      return col.sortMethod;
    }).forEach(function (col) {
      sortMethodsByColumnID[col.id] = col.sortMethod;
    });

    var sortData = function sortData(rows) {
      // Use the orderByFn to compose multiple sortBy's together.
      // This will also perform a stable sorting using the row index
      // if needed.
      var sortedData = orderByFn(rows, sortBy.map(function (sort) {
        // Support custom sorting methods for each column
        var columnSortBy = sortMethodsByColumnID[sort.id];

        // Return the correct sortFn
        return function (a, b) {
          return (columnSortBy ? columnSortBy : sortByFn)(a.values[sort.id], b.values[sort.id], sort.desc);
        };
      }),
      // Map the directions
      sortBy.map(function (d) {
        return !d.desc;
      }));

      // TODO: this should be optimized. Not good to loop again
      sortedData.forEach(function (row) {
        if (!row.subRows) {
          return;
        }
        row.subRows = sortData(row.subRows);
      });

      return sortedData;
    };

    return sortData(rows);
  }, [rows, columns, sortBy, manualSorting]);

  return _extends({}, api, {
    rows: sortedRows
  });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VTb3J0QnkuanMiXSwibmFtZXMiOlsidXNlTWVtbyIsImFkZEFjdGlvbnMiLCJhY3Rpb25zIiwiZGVmYXVsdFN0YXRlIiwibWVyZ2VQcm9wcyIsImFwcGx5UHJvcEhvb2tzIiwiZ2V0Rmlyc3REZWZpbmVkIiwiZGVmYXVsdE9yZGVyQnlGbiIsImRlZmF1bHRTb3J0QnlGbiIsInNvcnRCeSIsInNvcnRCeUNoYW5nZSIsInVzZVNvcnRCeSIsImRlYnVnIiwiYXBpIiwicm93cyIsImNvbHVtbnMiLCJvcmRlckJ5Rm4iLCJzb3J0QnlGbiIsIm1hbnVhbFNvcnRpbmciLCJkaXNhYmxlU29ydGluZyIsImRlZmF1bHRTb3J0RGVzYyIsImhvb2tzIiwic3RhdGUiLCJzZXRTdGF0ZSIsImZvckVhY2giLCJhY2Nlc3NvciIsImNvbHVtbiIsImNhblNvcnRCeSIsInVuZGVmaW5lZCIsInRvZ2dsZVNvcnRCeUlEIiwiY29sdW1uSUQiLCJkZXNjIiwibXVsdGkiLCJvbGQiLCJmaW5kIiwiZCIsImlkIiwicmVzb2x2ZWREZWZhdWx0U29ydERlc2MiLCJleGlzdGluZ1NvcnRCeSIsImhhc0Rlc2NEZWZpbmVkIiwibmV3U29ydEJ5IiwiYWN0aW9uIiwibGVuZ3RoIiwibWFwIiwicHVzaCIsInRvZ2dsZVNvcnRCeSIsImdldFNvcnRCeVRvZ2dsZVByb3BzIiwiYWRkU29ydEJ5VG9nZ2xlUHJvcHMiLCJvbkNsaWNrIiwiZSIsInBlcnNpc3QiLCJkaXNhYmxlTXVsdGlTb3J0Iiwic2hpZnRLZXkiLCJzdHlsZSIsImN1cnNvciIsInRpdGxlIiwicHJvcHMiLCJoZWFkZXJzIiwic29ydGVkIiwic29ydGVkSW5kZXgiLCJmaW5kSW5kZXgiLCJzb3J0ZWREZXNjIiwic29ydGVkUm93cyIsImNvbnNvbGUiLCJpbmZvIiwic29ydE1ldGhvZHNCeUNvbHVtbklEIiwiZmlsdGVyIiwiY29sIiwic29ydE1ldGhvZCIsInNvcnREYXRhIiwic29ydGVkRGF0YSIsImNvbHVtblNvcnRCeSIsInNvcnQiLCJhIiwiYiIsInZhbHVlcyIsInJvdyIsInN1YlJvd3MiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLFNBQVNBLE9BQVQsUUFBd0IsT0FBeEI7O0FBRUEsU0FBU0MsVUFBVCxFQUFxQkMsT0FBckIsUUFBb0MsWUFBcEM7QUFDQSxTQUFTQyxZQUFULFFBQTZCLGlCQUE3QjtBQUNBLFNBQ0VDLFVBREYsRUFFRUMsY0FGRixFQUdFQyxlQUhGLEVBSUVDLGdCQUpGLEVBS0VDLGVBTEYsUUFNTyxVQU5QOztBQVFBTCxhQUFhTSxNQUFiLEdBQXNCLEVBQXRCOztBQUVBUixXQUFXO0FBQ1RTLGdCQUFjO0FBREwsQ0FBWDs7QUFJQSxPQUFPLElBQU1DLFlBQVksU0FBWkEsU0FBWSxNQUFPO0FBQUEsTUFFNUJDLEtBRjRCLEdBWTFCQyxHQVowQixDQUU1QkQsS0FGNEI7QUFBQSxNQUc1QkUsSUFINEIsR0FZMUJELEdBWjBCLENBRzVCQyxJQUg0QjtBQUFBLE1BSTVCQyxPQUo0QixHQVkxQkYsR0FaMEIsQ0FJNUJFLE9BSjRCO0FBQUEsdUJBWTFCRixHQVowQixDQUs1QkcsU0FMNEI7QUFBQSxNQUs1QkEsU0FMNEIsa0NBS2hCVCxnQkFMZ0I7QUFBQSxzQkFZMUJNLEdBWjBCLENBTTVCSSxRQU40QjtBQUFBLE1BTTVCQSxRQU40QixpQ0FNakJULGVBTmlCO0FBQUEsTUFPNUJVLGFBUDRCLEdBWTFCTCxHQVowQixDQU81QkssYUFQNEI7QUFBQSxNQVE1QkMsY0FSNEIsR0FZMUJOLEdBWjBCLENBUTVCTSxjQVI0QjtBQUFBLE1BUzVCQyxlQVQ0QixHQVkxQlAsR0FaMEIsQ0FTNUJPLGVBVDRCO0FBQUEsTUFVNUJDLEtBVjRCLEdBWTFCUixHQVowQixDQVU1QlEsS0FWNEI7QUFBQSxrQ0FZMUJSLEdBWjBCLENBVzVCUyxLQVg0QjtBQUFBLE1BV2xCYixNQVhrQixpQkFXbEJBLE1BWGtCO0FBQUEsTUFXUmMsUUFYUTs7QUFjOUJSLFVBQVFTLE9BQVIsQ0FBZ0Isa0JBQVU7QUFBQSxRQUNoQkMsUUFEZ0IsR0FDUUMsTUFEUixDQUNoQkQsUUFEZ0I7QUFBQSxRQUNORSxTQURNLEdBQ1FELE1BRFIsQ0FDTkMsU0FETTs7QUFFeEJELFdBQU9DLFNBQVAsR0FBbUJGLFdBQ2ZuQixnQkFDRXFCLFNBREYsRUFFRVIsbUJBQW1CLElBQW5CLEdBQTBCLEtBQTFCLEdBQWtDUyxTQUZwQyxFQUdFLElBSEYsQ0FEZSxHQU1mLEtBTko7QUFPRCxHQVREOztBQVdBO0FBQ0EsTUFBTUMsaUJBQWlCLFNBQWpCQSxjQUFpQixDQUFDQyxRQUFELEVBQVdDLElBQVgsRUFBaUJDLEtBQWpCLEVBQTJCO0FBQ2hELFdBQU9ULFNBQVMsZUFBTztBQUFBLFVBQ2JkLE1BRGEsR0FDRndCLEdBREUsQ0FDYnhCLE1BRGE7O0FBR3JCOztBQUNBLFVBQU1pQixTQUFTWCxRQUFRbUIsSUFBUixDQUFhO0FBQUEsZUFBS0MsRUFBRUMsRUFBRixLQUFTTixRQUFkO0FBQUEsT0FBYixDQUFmO0FBQ0EsVUFBTU8sMEJBQTBCL0IsZ0JBQzlCb0IsT0FBT04sZUFEdUIsRUFFOUJBLGVBRjhCLENBQWhDOztBQUtBO0FBQ0EsVUFBTWtCLGlCQUFpQjdCLE9BQU95QixJQUFQLENBQVk7QUFBQSxlQUFLQyxFQUFFQyxFQUFGLEtBQVNOLFFBQWQ7QUFBQSxPQUFaLENBQXZCO0FBQ0EsVUFBTVMsaUJBQWlCLE9BQU9SLElBQVAsS0FBZ0IsV0FBaEIsSUFBK0JBLFNBQVMsSUFBL0Q7O0FBRUEsVUFBSVMsWUFBWSxFQUFoQjs7QUFFQTtBQUNBLFVBQUlDLGVBQUo7O0FBRUEsVUFBSSxDQUFDVCxLQUFMLEVBQVk7QUFDVixZQUFJdkIsT0FBT2lDLE1BQVAsSUFBaUIsQ0FBakIsSUFBc0JKLGNBQTFCLEVBQTBDO0FBQ3hDLGNBQUlBLGVBQWVQLElBQW5CLEVBQXlCO0FBQ3ZCVSxxQkFBUyxRQUFUO0FBQ0QsV0FGRCxNQUVPO0FBQ0xBLHFCQUFTLFFBQVQ7QUFDRDtBQUNGLFNBTkQsTUFNTztBQUNMQSxtQkFBUyxTQUFUO0FBQ0Q7QUFDRixPQVZELE1BVU87QUFDTCxZQUFJLENBQUNILGNBQUwsRUFBcUI7QUFDbkJHLG1CQUFTLEtBQVQ7QUFDRCxTQUZELE1BRU87QUFDTCxjQUFJRixjQUFKLEVBQW9CO0FBQ2xCRSxxQkFBUyxLQUFUO0FBQ0QsV0FGRCxNQUVPO0FBQ0xBLHFCQUFTLFFBQVQ7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsVUFBSUEsV0FBVyxTQUFmLEVBQTBCO0FBQ3hCRCxvQkFBWSxDQUNWO0FBQ0VKLGNBQUlOLFFBRE47QUFFRUMsZ0JBQU1RLGlCQUFpQlIsSUFBakIsR0FBd0JNO0FBRmhDLFNBRFUsQ0FBWjtBQU1ELE9BUEQsTUFPTyxJQUFJSSxXQUFXLEtBQWYsRUFBc0I7QUFDM0JELGlEQUNLL0IsTUFETCxJQUVFO0FBQ0UyQixjQUFJTixRQUROO0FBRUVDLGdCQUFNUSxpQkFBaUJSLElBQWpCLEdBQXdCTTtBQUZoQyxTQUZGO0FBT0QsT0FSTSxNQVFBLElBQUlJLFdBQVcsS0FBZixFQUFzQjtBQUMzQkQsb0JBQVkvQixPQUFPa0MsR0FBUCxDQUFXLGFBQUs7QUFDMUIsY0FBSVIsRUFBRUMsRUFBRixLQUFTTixRQUFiLEVBQXVCO0FBQ3JCLGdDQUNLSyxDQURMO0FBRUVKO0FBRkY7QUFJRDtBQUNELGlCQUFPSSxDQUFQO0FBQ0QsU0FSVyxDQUFaO0FBU0QsT0FWTSxNQVVBLElBQUlNLFdBQVcsUUFBZixFQUF5QjtBQUM5QkQsb0JBQVkvQixPQUFPa0MsR0FBUCxDQUFXLGFBQUs7QUFDMUIsY0FBSVIsRUFBRUMsRUFBRixLQUFTTixRQUFiLEVBQXVCO0FBQ3JCLGdDQUNLSyxDQURMO0FBRUVKLG9CQUFNLENBQUNPLGVBQWVQO0FBRnhCO0FBSUQ7QUFDRCxpQkFBT0ksQ0FBUDtBQUNELFNBUlcsQ0FBWjtBQVNELE9BVk0sTUFVQSxJQUFJTSxXQUFXLFFBQWYsRUFBeUI7QUFDOUJELG9CQUFZLEVBQVo7QUFDRDs7QUFFRCwwQkFDS1AsR0FETDtBQUVFeEIsZ0JBQVErQjtBQUZWO0FBSUQsS0FwRk0sRUFvRkp0QyxRQUFRUSxZQXBGSixDQUFQO0FBcUZELEdBdEZEOztBQXdGQVcsUUFBTU4sT0FBTixDQUFjNkIsSUFBZCxDQUFtQixtQkFBVztBQUM1QjdCLFlBQVFTLE9BQVIsQ0FBZ0Isa0JBQVU7QUFDeEIsVUFBSUUsT0FBT0MsU0FBWCxFQUFzQjtBQUNwQkQsZUFBT21CLFlBQVAsR0FBc0IsVUFBQ2QsSUFBRCxFQUFPQyxLQUFQO0FBQUEsaUJBQ3BCSCxlQUFlSCxPQUFPVSxFQUF0QixFQUEwQkwsSUFBMUIsRUFBZ0NDLEtBQWhDLENBRG9CO0FBQUEsU0FBdEI7QUFFRDtBQUNGLEtBTEQ7QUFNQSxXQUFPakIsT0FBUDtBQUNELEdBUkQ7O0FBVUFNLFFBQU15QixvQkFBTixHQUE2QixFQUE3Qjs7QUFFQSxNQUFNQyx1QkFBdUIsU0FBdkJBLG9CQUF1QixDQUFDaEMsT0FBRCxFQUFVRixHQUFWLEVBQWtCO0FBQzdDRSxZQUFRUyxPQUFSLENBQWdCLGtCQUFVO0FBQUEsVUFDaEJHLFNBRGdCLEdBQ0ZELE1BREUsQ0FDaEJDLFNBRGdCOztBQUV4QkQsYUFBT29CLG9CQUFQLEdBQThCLGlCQUFTO0FBQ3JDLGVBQU8xQyxXQUNMO0FBQ0U0QyxtQkFBU3JCLFlBQ0wsYUFBSztBQUNIc0IsY0FBRUMsT0FBRjtBQUNBeEIsbUJBQU9tQixZQUFQLENBQ0VqQixTQURGLEVBRUUsQ0FBQ2YsSUFBSXNDLGdCQUFMLElBQXlCRixFQUFFRyxRQUY3QjtBQUlELFdBUEksR0FRTHhCLFNBVE47QUFVRXlCLGlCQUFPO0FBQ0xDLG9CQUFRM0IsWUFBWSxTQUFaLEdBQXdCQztBQUQzQixXQVZUO0FBYUUyQixpQkFBTztBQWJULFNBREssRUFnQkxsRCxlQUFlUSxJQUFJUSxLQUFKLENBQVV5QixvQkFBekIsRUFBK0NwQixNQUEvQyxFQUF1RGIsR0FBdkQsQ0FoQkssRUFpQkwyQyxLQWpCSyxDQUFQO0FBbUJELE9BcEJEO0FBcUJELEtBdkJEO0FBd0JBLFdBQU96QyxPQUFQO0FBQ0QsR0ExQkQ7O0FBNEJBTSxRQUFNTixPQUFOLENBQWM2QixJQUFkLENBQW1CRyxvQkFBbkI7QUFDQTFCLFFBQU1vQyxPQUFOLENBQWNiLElBQWQsQ0FBbUJHLG9CQUFuQjs7QUFFQTtBQUNBaEMsVUFBUVMsT0FBUixDQUFnQixrQkFBVTtBQUFBLFFBQ2hCWSxFQURnQixHQUNUVixNQURTLENBQ2hCVSxFQURnQjs7QUFFeEJWLFdBQU9nQyxNQUFQLEdBQWdCakQsT0FBT3lCLElBQVAsQ0FBWTtBQUFBLGFBQUtDLEVBQUVDLEVBQUYsS0FBU0EsRUFBZDtBQUFBLEtBQVosQ0FBaEI7QUFDQVYsV0FBT2lDLFdBQVAsR0FBcUJsRCxPQUFPbUQsU0FBUCxDQUFpQjtBQUFBLGFBQUt6QixFQUFFQyxFQUFGLEtBQVNBLEVBQWQ7QUFBQSxLQUFqQixDQUFyQjtBQUNBVixXQUFPbUMsVUFBUCxHQUFvQm5DLE9BQU9nQyxNQUFQLEdBQWdCaEMsT0FBT2dDLE1BQVAsQ0FBYzNCLElBQTlCLEdBQXFDSCxTQUF6RDtBQUNELEdBTEQ7O0FBT0EsTUFBTWtDLGFBQWE5RCxRQUNqQixZQUFNO0FBQ0osUUFBSWtCLGlCQUFpQixDQUFDVCxPQUFPaUMsTUFBN0IsRUFBcUM7QUFDbkMsYUFBTzVCLElBQVA7QUFDRDtBQUNELFFBQUlGLEtBQUosRUFBV21ELFFBQVFDLElBQVIsQ0FBYSxlQUFiOztBQUVYLFFBQU1DLHdCQUF3QixFQUE5Qjs7QUFFQWxELFlBQ0dtRCxNQURILENBQ1U7QUFBQSxhQUFPQyxJQUFJQyxVQUFYO0FBQUEsS0FEVixFQUVHNUMsT0FGSCxDQUVXLGVBQU87QUFDZHlDLDRCQUFzQkUsSUFBSS9CLEVBQTFCLElBQWdDK0IsSUFBSUMsVUFBcEM7QUFDRCxLQUpIOztBQU1BLFFBQU1DLFdBQVcsU0FBWEEsUUFBVyxPQUFRO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBLFVBQU1DLGFBQWF0RCxVQUNqQkYsSUFEaUIsRUFFakJMLE9BQU9rQyxHQUFQLENBQVcsZ0JBQVE7QUFDakI7QUFDQSxZQUFNNEIsZUFBZU4sc0JBQXNCTyxLQUFLcEMsRUFBM0IsQ0FBckI7O0FBRUE7QUFDQSxlQUFPLFVBQUNxQyxDQUFELEVBQUlDLENBQUo7QUFBQSxpQkFDTCxDQUFDSCxlQUFlQSxZQUFmLEdBQThCdEQsUUFBL0IsRUFDRXdELEVBQUVFLE1BQUYsQ0FBU0gsS0FBS3BDLEVBQWQsQ0FERixFQUVFc0MsRUFBRUMsTUFBRixDQUFTSCxLQUFLcEMsRUFBZCxDQUZGLEVBR0VvQyxLQUFLekMsSUFIUCxDQURLO0FBQUEsU0FBUDtBQU1ELE9BWEQsQ0FGaUI7QUFjakI7QUFDQXRCLGFBQU9rQyxHQUFQLENBQVc7QUFBQSxlQUFLLENBQUNSLEVBQUVKLElBQVI7QUFBQSxPQUFYLENBZmlCLENBQW5COztBQWtCQTtBQUNBdUMsaUJBQVc5QyxPQUFYLENBQW1CLGVBQU87QUFDeEIsWUFBSSxDQUFDb0QsSUFBSUMsT0FBVCxFQUFrQjtBQUNoQjtBQUNEO0FBQ0RELFlBQUlDLE9BQUosR0FBY1IsU0FBU08sSUFBSUMsT0FBYixDQUFkO0FBQ0QsT0FMRDs7QUFPQSxhQUFPUCxVQUFQO0FBQ0QsS0EvQkQ7O0FBaUNBLFdBQU9ELFNBQVN2RCxJQUFULENBQVA7QUFDRCxHQWpEZ0IsRUFrRGpCLENBQUNBLElBQUQsRUFBT0MsT0FBUCxFQUFnQk4sTUFBaEIsRUFBd0JTLGFBQXhCLENBbERpQixDQUFuQjs7QUFxREEsc0JBQ0tMLEdBREw7QUFFRUMsVUFBTWdEO0FBRlI7QUFJRCxDQTlOTSIsImZpbGUiOiJ1c2VTb3J0QnkuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VNZW1vIH0gZnJvbSBcInJlYWN0XCI7XG5cbmltcG9ydCB7IGFkZEFjdGlvbnMsIGFjdGlvbnMgfSBmcm9tIFwiLi4vYWN0aW9uc1wiO1xuaW1wb3J0IHsgZGVmYXVsdFN0YXRlIH0gZnJvbSBcIi4vdXNlVGFibGVTdGF0ZVwiO1xuaW1wb3J0IHtcbiAgbWVyZ2VQcm9wcyxcbiAgYXBwbHlQcm9wSG9va3MsXG4gIGdldEZpcnN0RGVmaW5lZCxcbiAgZGVmYXVsdE9yZGVyQnlGbixcbiAgZGVmYXVsdFNvcnRCeUZuXG59IGZyb20gXCIuLi91dGlsc1wiO1xuXG5kZWZhdWx0U3RhdGUuc29ydEJ5ID0gW107XG5cbmFkZEFjdGlvbnMoe1xuICBzb3J0QnlDaGFuZ2U6IFwiX19zb3J0QnlDaGFuZ2VfX1wiXG59KTtcblxuZXhwb3J0IGNvbnN0IHVzZVNvcnRCeSA9IGFwaSA9PiB7XG4gIGNvbnN0IHtcbiAgICBkZWJ1ZyxcbiAgICByb3dzLFxuICAgIGNvbHVtbnMsXG4gICAgb3JkZXJCeUZuID0gZGVmYXVsdE9yZGVyQnlGbixcbiAgICBzb3J0QnlGbiA9IGRlZmF1bHRTb3J0QnlGbixcbiAgICBtYW51YWxTb3J0aW5nLFxuICAgIGRpc2FibGVTb3J0aW5nLFxuICAgIGRlZmF1bHRTb3J0RGVzYyxcbiAgICBob29rcyxcbiAgICBzdGF0ZTogW3sgc29ydEJ5IH0sIHNldFN0YXRlXVxuICB9ID0gYXBpO1xuXG4gIGNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xuICAgIGNvbnN0IHsgYWNjZXNzb3IsIGNhblNvcnRCeSB9ID0gY29sdW1uO1xuICAgIGNvbHVtbi5jYW5Tb3J0QnkgPSBhY2Nlc3NvclxuICAgICAgPyBnZXRGaXJzdERlZmluZWQoXG4gICAgICAgICAgY2FuU29ydEJ5LFxuICAgICAgICAgIGRpc2FibGVTb3J0aW5nID09PSB0cnVlID8gZmFsc2UgOiB1bmRlZmluZWQsXG4gICAgICAgICAgdHJ1ZVxuICAgICAgICApXG4gICAgICA6IGZhbHNlO1xuICB9KTtcblxuICAvLyBVcGRhdGVzIHNvcnRpbmcgYmFzZWQgb24gYSBjb2x1bW5JRCwgZGVzYyBmbGFnIGFuZCBtdWx0aSBmbGFnXG4gIGNvbnN0IHRvZ2dsZVNvcnRCeUlEID0gKGNvbHVtbklELCBkZXNjLCBtdWx0aSkgPT4ge1xuICAgIHJldHVybiBzZXRTdGF0ZShvbGQgPT4ge1xuICAgICAgY29uc3QgeyBzb3J0QnkgfSA9IG9sZDtcblxuICAgICAgLy8gRmluZCB0aGUgY29sdW1uIGZvciB0aGlzIGNvbHVtbklEXG4gICAgICBjb25zdCBjb2x1bW4gPSBjb2x1bW5zLmZpbmQoZCA9PiBkLmlkID09PSBjb2x1bW5JRCk7XG4gICAgICBjb25zdCByZXNvbHZlZERlZmF1bHRTb3J0RGVzYyA9IGdldEZpcnN0RGVmaW5lZChcbiAgICAgICAgY29sdW1uLmRlZmF1bHRTb3J0RGVzYyxcbiAgICAgICAgZGVmYXVsdFNvcnREZXNjXG4gICAgICApO1xuXG4gICAgICAvLyBGaW5kIGFueSBleGlzdGluZyBzb3J0QnkgZm9yIHRoaXMgY29sdW1uXG4gICAgICBjb25zdCBleGlzdGluZ1NvcnRCeSA9IHNvcnRCeS5maW5kKGQgPT4gZC5pZCA9PT0gY29sdW1uSUQpO1xuICAgICAgY29uc3QgaGFzRGVzY0RlZmluZWQgPSB0eXBlb2YgZGVzYyAhPT0gXCJ1bmRlZmluZWRcIiAmJiBkZXNjICE9PSBudWxsO1xuXG4gICAgICBsZXQgbmV3U29ydEJ5ID0gW107XG5cbiAgICAgIC8vIFdoYXQgc2hvdWxkIHdlIGRvIHdpdGggdGhpcyBmaWx0ZXI/XG4gICAgICBsZXQgYWN0aW9uO1xuXG4gICAgICBpZiAoIW11bHRpKSB7XG4gICAgICAgIGlmIChzb3J0QnkubGVuZ3RoIDw9IDEgJiYgZXhpc3RpbmdTb3J0QnkpIHtcbiAgICAgICAgICBpZiAoZXhpc3RpbmdTb3J0QnkuZGVzYykge1xuICAgICAgICAgICAgYWN0aW9uID0gXCJyZW1vdmVcIjtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgYWN0aW9uID0gXCJ0b2dnbGVcIjtcbiAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgYWN0aW9uID0gXCJyZXBsYWNlXCI7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmICghZXhpc3RpbmdTb3J0QnkpIHtcbiAgICAgICAgICBhY3Rpb24gPSBcImFkZFwiO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGlmIChoYXNEZXNjRGVmaW5lZCkge1xuICAgICAgICAgICAgYWN0aW9uID0gXCJzZXRcIjtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgYWN0aW9uID0gXCJ0b2dnbGVcIjtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKGFjdGlvbiA9PT0gXCJyZXBsYWNlXCIpIHtcbiAgICAgICAgbmV3U29ydEJ5ID0gW1xuICAgICAgICAgIHtcbiAgICAgICAgICAgIGlkOiBjb2x1bW5JRCxcbiAgICAgICAgICAgIGRlc2M6IGhhc0Rlc2NEZWZpbmVkID8gZGVzYyA6IHJlc29sdmVkRGVmYXVsdFNvcnREZXNjXG4gICAgICAgICAgfVxuICAgICAgICBdO1xuICAgICAgfSBlbHNlIGlmIChhY3Rpb24gPT09IFwiYWRkXCIpIHtcbiAgICAgICAgbmV3U29ydEJ5ID0gW1xuICAgICAgICAgIC4uLnNvcnRCeSxcbiAgICAgICAgICB7XG4gICAgICAgICAgICBpZDogY29sdW1uSUQsXG4gICAgICAgICAgICBkZXNjOiBoYXNEZXNjRGVmaW5lZCA/IGRlc2MgOiByZXNvbHZlZERlZmF1bHRTb3J0RGVzY1xuICAgICAgICAgIH1cbiAgICAgICAgXTtcbiAgICAgIH0gZWxzZSBpZiAoYWN0aW9uID09PSBcInNldFwiKSB7XG4gICAgICAgIG5ld1NvcnRCeSA9IHNvcnRCeS5tYXAoZCA9PiB7XG4gICAgICAgICAgaWYgKGQuaWQgPT09IGNvbHVtbklEKSB7XG4gICAgICAgICAgICByZXR1cm4ge1xuICAgICAgICAgICAgICAuLi5kLFxuICAgICAgICAgICAgICBkZXNjXG4gICAgICAgICAgICB9O1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gZDtcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2UgaWYgKGFjdGlvbiA9PT0gXCJ0b2dnbGVcIikge1xuICAgICAgICBuZXdTb3J0QnkgPSBzb3J0QnkubWFwKGQgPT4ge1xuICAgICAgICAgIGlmIChkLmlkID09PSBjb2x1bW5JRCkge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgLi4uZCxcbiAgICAgICAgICAgICAgZGVzYzogIWV4aXN0aW5nU29ydEJ5LmRlc2NcbiAgICAgICAgICAgIH07XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBkO1xuICAgICAgICB9KTtcbiAgICAgIH0gZWxzZSBpZiAoYWN0aW9uID09PSBcInJlbW92ZVwiKSB7XG4gICAgICAgIG5ld1NvcnRCeSA9IFtdO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4ge1xuICAgICAgICAuLi5vbGQsXG4gICAgICAgIHNvcnRCeTogbmV3U29ydEJ5XG4gICAgICB9O1xuICAgIH0sIGFjdGlvbnMuc29ydEJ5Q2hhbmdlKTtcbiAgfTtcblxuICBob29rcy5jb2x1bW5zLnB1c2goY29sdW1ucyA9PiB7XG4gICAgY29sdW1ucy5mb3JFYWNoKGNvbHVtbiA9PiB7XG4gICAgICBpZiAoY29sdW1uLmNhblNvcnRCeSkge1xuICAgICAgICBjb2x1bW4udG9nZ2xlU29ydEJ5ID0gKGRlc2MsIG11bHRpKSA9PlxuICAgICAgICAgIHRvZ2dsZVNvcnRCeUlEKGNvbHVtbi5pZCwgZGVzYywgbXVsdGkpO1xuICAgICAgfVxuICAgIH0pO1xuICAgIHJldHVybiBjb2x1bW5zO1xuICB9KTtcblxuICBob29rcy5nZXRTb3J0QnlUb2dnbGVQcm9wcyA9IFtdO1xuXG4gIGNvbnN0IGFkZFNvcnRCeVRvZ2dsZVByb3BzID0gKGNvbHVtbnMsIGFwaSkgPT4ge1xuICAgIGNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xuICAgICAgY29uc3QgeyBjYW5Tb3J0QnkgfSA9IGNvbHVtbjtcbiAgICAgIGNvbHVtbi5nZXRTb3J0QnlUb2dnbGVQcm9wcyA9IHByb3BzID0+IHtcbiAgICAgICAgcmV0dXJuIG1lcmdlUHJvcHMoXG4gICAgICAgICAge1xuICAgICAgICAgICAgb25DbGljazogY2FuU29ydEJ5XG4gICAgICAgICAgICAgID8gZSA9PiB7XG4gICAgICAgICAgICAgICAgICBlLnBlcnNpc3QoKTtcbiAgICAgICAgICAgICAgICAgIGNvbHVtbi50b2dnbGVTb3J0QnkoXG4gICAgICAgICAgICAgICAgICAgIHVuZGVmaW5lZCxcbiAgICAgICAgICAgICAgICAgICAgIWFwaS5kaXNhYmxlTXVsdGlTb3J0ICYmIGUuc2hpZnRLZXlcbiAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICA6IHVuZGVmaW5lZCxcbiAgICAgICAgICAgIHN0eWxlOiB7XG4gICAgICAgICAgICAgIGN1cnNvcjogY2FuU29ydEJ5ID8gXCJwb2ludGVyXCIgOiB1bmRlZmluZWRcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICB0aXRsZTogXCJUb2dnbGUgU29ydEJ5XCJcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFwcGx5UHJvcEhvb2tzKGFwaS5ob29rcy5nZXRTb3J0QnlUb2dnbGVQcm9wcywgY29sdW1uLCBhcGkpLFxuICAgICAgICAgIHByb3BzXG4gICAgICAgICk7XG4gICAgICB9O1xuICAgIH0pO1xuICAgIHJldHVybiBjb2x1bW5zO1xuICB9O1xuXG4gIGhvb2tzLmNvbHVtbnMucHVzaChhZGRTb3J0QnlUb2dnbGVQcm9wcyk7XG4gIGhvb2tzLmhlYWRlcnMucHVzaChhZGRTb3J0QnlUb2dnbGVQcm9wcyk7XG5cbiAgLy8gTXV0YXRlIGNvbHVtbnMgdG8gcmVmbGVjdCBzb3J0aW5nIHN0YXRlXG4gIGNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xuICAgIGNvbnN0IHsgaWQgfSA9IGNvbHVtbjtcbiAgICBjb2x1bW4uc29ydGVkID0gc29ydEJ5LmZpbmQoZCA9PiBkLmlkID09PSBpZCk7XG4gICAgY29sdW1uLnNvcnRlZEluZGV4ID0gc29ydEJ5LmZpbmRJbmRleChkID0+IGQuaWQgPT09IGlkKTtcbiAgICBjb2x1bW4uc29ydGVkRGVzYyA9IGNvbHVtbi5zb3J0ZWQgPyBjb2x1bW4uc29ydGVkLmRlc2MgOiB1bmRlZmluZWQ7XG4gIH0pO1xuXG4gIGNvbnN0IHNvcnRlZFJvd3MgPSB1c2VNZW1vKFxuICAgICgpID0+IHtcbiAgICAgIGlmIChtYW51YWxTb3J0aW5nIHx8ICFzb3J0QnkubGVuZ3RoKSB7XG4gICAgICAgIHJldHVybiByb3dzO1xuICAgICAgfVxuICAgICAgaWYgKGRlYnVnKSBjb25zb2xlLmluZm8oXCJnZXRTb3J0ZWRSb3dzXCIpO1xuXG4gICAgICBjb25zdCBzb3J0TWV0aG9kc0J5Q29sdW1uSUQgPSB7fTtcblxuICAgICAgY29sdW1uc1xuICAgICAgICAuZmlsdGVyKGNvbCA9PiBjb2wuc29ydE1ldGhvZClcbiAgICAgICAgLmZvckVhY2goY29sID0+IHtcbiAgICAgICAgICBzb3J0TWV0aG9kc0J5Q29sdW1uSURbY29sLmlkXSA9IGNvbC5zb3J0TWV0aG9kO1xuICAgICAgICB9KTtcblxuICAgICAgY29uc3Qgc29ydERhdGEgPSByb3dzID0+IHtcbiAgICAgICAgLy8gVXNlIHRoZSBvcmRlckJ5Rm4gdG8gY29tcG9zZSBtdWx0aXBsZSBzb3J0QnkncyB0b2dldGhlci5cbiAgICAgICAgLy8gVGhpcyB3aWxsIGFsc28gcGVyZm9ybSBhIHN0YWJsZSBzb3J0aW5nIHVzaW5nIHRoZSByb3cgaW5kZXhcbiAgICAgICAgLy8gaWYgbmVlZGVkLlxuICAgICAgICBjb25zdCBzb3J0ZWREYXRhID0gb3JkZXJCeUZuKFxuICAgICAgICAgIHJvd3MsXG4gICAgICAgICAgc29ydEJ5Lm1hcChzb3J0ID0+IHtcbiAgICAgICAgICAgIC8vIFN1cHBvcnQgY3VzdG9tIHNvcnRpbmcgbWV0aG9kcyBmb3IgZWFjaCBjb2x1bW5cbiAgICAgICAgICAgIGNvbnN0IGNvbHVtblNvcnRCeSA9IHNvcnRNZXRob2RzQnlDb2x1bW5JRFtzb3J0LmlkXTtcblxuICAgICAgICAgICAgLy8gUmV0dXJuIHRoZSBjb3JyZWN0IHNvcnRGblxuICAgICAgICAgICAgcmV0dXJuIChhLCBiKSA9PlxuICAgICAgICAgICAgICAoY29sdW1uU29ydEJ5ID8gY29sdW1uU29ydEJ5IDogc29ydEJ5Rm4pKFxuICAgICAgICAgICAgICAgIGEudmFsdWVzW3NvcnQuaWRdLFxuICAgICAgICAgICAgICAgIGIudmFsdWVzW3NvcnQuaWRdLFxuICAgICAgICAgICAgICAgIHNvcnQuZGVzY1xuICAgICAgICAgICAgICApO1xuICAgICAgICAgIH0pLFxuICAgICAgICAgIC8vIE1hcCB0aGUgZGlyZWN0aW9uc1xuICAgICAgICAgIHNvcnRCeS5tYXAoZCA9PiAhZC5kZXNjKVxuICAgICAgICApO1xuXG4gICAgICAgIC8vIFRPRE86IHRoaXMgc2hvdWxkIGJlIG9wdGltaXplZC4gTm90IGdvb2QgdG8gbG9vcCBhZ2FpblxuICAgICAgICBzb3J0ZWREYXRhLmZvckVhY2gocm93ID0+IHtcbiAgICAgICAgICBpZiAoIXJvdy5zdWJSb3dzKSB7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgfVxuICAgICAgICAgIHJvdy5zdWJSb3dzID0gc29ydERhdGEocm93LnN1YlJvd3MpO1xuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gc29ydGVkRGF0YTtcbiAgICAgIH07XG5cbiAgICAgIHJldHVybiBzb3J0RGF0YShyb3dzKTtcbiAgICB9LFxuICAgIFtyb3dzLCBjb2x1bW5zLCBzb3J0QnksIG1hbnVhbFNvcnRpbmddXG4gICk7XG5cbiAgcmV0dXJuIHtcbiAgICAuLi5hcGksXG4gICAgcm93czogc29ydGVkUm93c1xuICB9O1xufTtcbiJdfQ==