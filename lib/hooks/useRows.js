"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useRows = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require("react");

var useRows = exports.useRows = function useRows(props) {
  var debug = props.debug,
      columns = props.columns,
      _props$subRowsKey = props.subRowsKey,
      subRowsKey = _props$subRowsKey === undefined ? "subRows" : _props$subRowsKey,
      data = props.data;


  var accessedRows = (0, _react.useMemo)(function () {
    if (debug) console.info("getAccessedRows");

    // Access the row's data
    var accessRow = function accessRow(originalRow, i) {
      var depth = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

      // Keep the original reference around
      var original = originalRow;

      // Process any subRows
      var subRows = originalRow[subRowsKey] ? originalRow[subRowsKey].map(function (d, i) {
        return accessRow(d, i, depth + 1);
      }) : undefined;

      var row = {
        original: original,
        index: i,
        subRows: subRows,
        depth: depth
      };

      // Create the cells and values
      row.values = {};
      columns.forEach(function (column) {
        row.values[column.id] = column.accessor ? column.accessor(originalRow, i, { subRows: subRows, depth: depth, data: data }) : undefined;
      });

      return row;
    };

    // Use the resolved data
    return data.map(function (d, i) {
      return accessRow(d, i);
    });
  }, [data, columns]);

  return _extends({}, props, {
    rows: accessedRows
  });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VSb3dzLmpzIl0sIm5hbWVzIjpbInVzZVJvd3MiLCJkZWJ1ZyIsInByb3BzIiwiY29sdW1ucyIsInN1YlJvd3NLZXkiLCJkYXRhIiwiYWNjZXNzZWRSb3dzIiwiY29uc29sZSIsImluZm8iLCJhY2Nlc3NSb3ciLCJvcmlnaW5hbFJvdyIsImkiLCJkZXB0aCIsIm9yaWdpbmFsIiwic3ViUm93cyIsIm1hcCIsImQiLCJ1bmRlZmluZWQiLCJyb3ciLCJpbmRleCIsInZhbHVlcyIsImZvckVhY2giLCJjb2x1bW4iLCJpZCIsImFjY2Vzc29yIiwicm93cyJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUE7O0FBRU8sSUFBTUEsNEJBQVUsU0FBVkEsT0FBVSxRQUFTO0FBQUEsTUFDdEJDLEtBRHNCLEdBQzJCQyxLQUQzQixDQUN0QkQsS0FEc0I7QUFBQSxNQUNmRSxPQURlLEdBQzJCRCxLQUQzQixDQUNmQyxPQURlO0FBQUEsMEJBQzJCRCxLQUQzQixDQUNORSxVQURNO0FBQUEsTUFDTkEsVUFETSxxQ0FDTyxTQURQO0FBQUEsTUFDa0JDLElBRGxCLEdBQzJCSCxLQUQzQixDQUNrQkcsSUFEbEI7OztBQUc5QixNQUFNQyxlQUFlLG9CQUNuQixZQUFNO0FBQ0osUUFBSUwsS0FBSixFQUFXTSxRQUFRQyxJQUFSLENBQWEsaUJBQWI7O0FBRVg7QUFDQSxRQUFNQyxZQUFZLFNBQVpBLFNBQVksQ0FBQ0MsV0FBRCxFQUFjQyxDQUFkLEVBQStCO0FBQUEsVUFBZEMsS0FBYyx1RUFBTixDQUFNOztBQUMvQztBQUNBLFVBQU1DLFdBQVdILFdBQWpCOztBQUVBO0FBQ0EsVUFBTUksVUFBVUosWUFBWU4sVUFBWixJQUNaTSxZQUFZTixVQUFaLEVBQXdCVyxHQUF4QixDQUE0QixVQUFDQyxDQUFELEVBQUlMLENBQUo7QUFBQSxlQUFVRixVQUFVTyxDQUFWLEVBQWFMLENBQWIsRUFBZ0JDLFFBQVEsQ0FBeEIsQ0FBVjtBQUFBLE9BQTVCLENBRFksR0FFWkssU0FGSjs7QUFJQSxVQUFNQyxNQUFNO0FBQ1ZMLDBCQURVO0FBRVZNLGVBQU9SLENBRkc7QUFHVkcsd0JBSFU7QUFJVkY7QUFKVSxPQUFaOztBQU9BO0FBQ0FNLFVBQUlFLE1BQUosR0FBYSxFQUFiO0FBQ0FqQixjQUFRa0IsT0FBUixDQUFnQixrQkFBVTtBQUN4QkgsWUFBSUUsTUFBSixDQUFXRSxPQUFPQyxFQUFsQixJQUF3QkQsT0FBT0UsUUFBUCxHQUNwQkYsT0FBT0UsUUFBUCxDQUFnQmQsV0FBaEIsRUFBNkJDLENBQTdCLEVBQWdDLEVBQUVHLGdCQUFGLEVBQVdGLFlBQVgsRUFBa0JQLFVBQWxCLEVBQWhDLENBRG9CLEdBRXBCWSxTQUZKO0FBR0QsT0FKRDs7QUFNQSxhQUFPQyxHQUFQO0FBQ0QsS0F6QkQ7O0FBMkJBO0FBQ0EsV0FBT2IsS0FBS1UsR0FBTCxDQUFTLFVBQUNDLENBQUQsRUFBSUwsQ0FBSjtBQUFBLGFBQVVGLFVBQVVPLENBQVYsRUFBYUwsQ0FBYixDQUFWO0FBQUEsS0FBVCxDQUFQO0FBQ0QsR0FsQ2tCLEVBbUNuQixDQUFDTixJQUFELEVBQU9GLE9BQVAsQ0FuQ21CLENBQXJCOztBQXNDQSxzQkFDS0QsS0FETDtBQUVFdUIsVUFBTW5CO0FBRlI7QUFJRCxDQTdDTSIsImZpbGUiOiJ1c2VSb3dzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlTWVtbyB9IGZyb20gXCJyZWFjdFwiO1xuXG5leHBvcnQgY29uc3QgdXNlUm93cyA9IHByb3BzID0+IHtcbiAgY29uc3QgeyBkZWJ1ZywgY29sdW1ucywgc3ViUm93c0tleSA9IFwic3ViUm93c1wiLCBkYXRhIH0gPSBwcm9wcztcblxuICBjb25zdCBhY2Nlc3NlZFJvd3MgPSB1c2VNZW1vKFxuICAgICgpID0+IHtcbiAgICAgIGlmIChkZWJ1ZykgY29uc29sZS5pbmZvKFwiZ2V0QWNjZXNzZWRSb3dzXCIpO1xuXG4gICAgICAvLyBBY2Nlc3MgdGhlIHJvdydzIGRhdGFcbiAgICAgIGNvbnN0IGFjY2Vzc1JvdyA9IChvcmlnaW5hbFJvdywgaSwgZGVwdGggPSAwKSA9PiB7XG4gICAgICAgIC8vIEtlZXAgdGhlIG9yaWdpbmFsIHJlZmVyZW5jZSBhcm91bmRcbiAgICAgICAgY29uc3Qgb3JpZ2luYWwgPSBvcmlnaW5hbFJvdztcblxuICAgICAgICAvLyBQcm9jZXNzIGFueSBzdWJSb3dzXG4gICAgICAgIGNvbnN0IHN1YlJvd3MgPSBvcmlnaW5hbFJvd1tzdWJSb3dzS2V5XVxuICAgICAgICAgID8gb3JpZ2luYWxSb3dbc3ViUm93c0tleV0ubWFwKChkLCBpKSA9PiBhY2Nlc3NSb3coZCwgaSwgZGVwdGggKyAxKSlcbiAgICAgICAgICA6IHVuZGVmaW5lZDtcblxuICAgICAgICBjb25zdCByb3cgPSB7XG4gICAgICAgICAgb3JpZ2luYWwsXG4gICAgICAgICAgaW5kZXg6IGksXG4gICAgICAgICAgc3ViUm93cyxcbiAgICAgICAgICBkZXB0aFxuICAgICAgICB9O1xuXG4gICAgICAgIC8vIENyZWF0ZSB0aGUgY2VsbHMgYW5kIHZhbHVlc1xuICAgICAgICByb3cudmFsdWVzID0ge307XG4gICAgICAgIGNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xuICAgICAgICAgIHJvdy52YWx1ZXNbY29sdW1uLmlkXSA9IGNvbHVtbi5hY2Nlc3NvclxuICAgICAgICAgICAgPyBjb2x1bW4uYWNjZXNzb3Iob3JpZ2luYWxSb3csIGksIHsgc3ViUm93cywgZGVwdGgsIGRhdGEgfSlcbiAgICAgICAgICAgIDogdW5kZWZpbmVkO1xuICAgICAgICB9KTtcblxuICAgICAgICByZXR1cm4gcm93O1xuICAgICAgfTtcblxuICAgICAgLy8gVXNlIHRoZSByZXNvbHZlZCBkYXRhXG4gICAgICByZXR1cm4gZGF0YS5tYXAoKGQsIGkpID0+IGFjY2Vzc1JvdyhkLCBpKSk7XG4gICAgfSxcbiAgICBbZGF0YSwgY29sdW1uc11cbiAgKTtcblxuICByZXR1cm4ge1xuICAgIC4uLnByb3BzLFxuICAgIHJvd3M6IGFjY2Vzc2VkUm93c1xuICB9O1xufTtcbiJdfQ==