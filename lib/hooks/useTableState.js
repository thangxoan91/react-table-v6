"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useTableState = exports.defaultState = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var defaultState = exports.defaultState = {};

var defaultReducer = function defaultReducer(old, newState, type) {
  return newState;
};

var useTableState = exports.useTableState = function useTableState() {
  var initialState = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var overrides = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var _ref = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
      _ref$reducer = _ref.reducer,
      reducer = _ref$reducer === undefined ? defaultReducer : _ref$reducer,
      _ref$useState = _ref.useState,
      userUseState = _ref$useState === undefined ? _react.useState : _ref$useState;

  var _userUseState = userUseState(_extends({}, defaultState, initialState)),
      _userUseState2 = _slicedToArray(_userUseState, 2),
      state = _userUseState2[0],
      setState = _userUseState2[1];

  var overriddenState = (0, _react.useMemo)(function () {
    var newState = _extends({}, state);
    Object.keys(overrides).forEach(function (key) {
      newState[key] = overrides[key];
    });
    return newState;
  }, [state].concat(_toConsumableArray(Object.values(overrides))));

  var reducedSetState = function reducedSetState(updater, type) {
    return setState(function (old) {
      var newState = updater(old);
      return reducer(old, newState, type);
    });
  };

  return [overriddenState, reducedSetState];
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VUYWJsZVN0YXRlLmpzIl0sIm5hbWVzIjpbImRlZmF1bHRTdGF0ZSIsImRlZmF1bHRSZWR1Y2VyIiwib2xkIiwibmV3U3RhdGUiLCJ0eXBlIiwidXNlVGFibGVTdGF0ZSIsImluaXRpYWxTdGF0ZSIsIm92ZXJyaWRlcyIsInJlZHVjZXIiLCJ1c2VTdGF0ZSIsInVzZXJVc2VTdGF0ZSIsInN0YXRlIiwic2V0U3RhdGUiLCJvdmVycmlkZGVuU3RhdGUiLCJPYmplY3QiLCJrZXlzIiwiZm9yRWFjaCIsImtleSIsInZhbHVlcyIsInJlZHVjZWRTZXRTdGF0ZSIsInVwZGF0ZXIiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7Ozs7Ozs7O0FBRU8sSUFBTUEsc0NBQWUsRUFBckI7O0FBRVAsSUFBTUMsaUJBQWlCLFNBQWpCQSxjQUFpQixDQUFDQyxHQUFELEVBQU1DLFFBQU4sRUFBZ0JDLElBQWhCO0FBQUEsU0FBeUJELFFBQXpCO0FBQUEsQ0FBdkI7O0FBRU8sSUFBTUUsd0NBQWdCLFNBQWhCQSxhQUFnQixHQUl4QjtBQUFBLE1BSEhDLFlBR0csdUVBSFksRUFHWjtBQUFBLE1BRkhDLFNBRUcsdUVBRlMsRUFFVDs7QUFBQSxpRkFEK0QsRUFDL0Q7QUFBQSwwQkFEREMsT0FDQztBQUFBLE1BRERBLE9BQ0MsZ0NBRFNQLGNBQ1Q7QUFBQSwyQkFEeUJRLFFBQ3pCO0FBQUEsTUFEbUNDLFlBQ25DLGlDQURrREQsZUFDbEQ7O0FBQUEsc0JBQ3FCQywwQkFDbkJWLFlBRG1CLEVBRW5CTSxZQUZtQixFQURyQjtBQUFBO0FBQUEsTUFDRUssS0FERjtBQUFBLE1BQ1NDLFFBRFQ7O0FBTUgsTUFBTUMsa0JBQWtCLG9CQUN0QixZQUFNO0FBQ0osUUFBTVYsd0JBQ0RRLEtBREMsQ0FBTjtBQUdBRyxXQUFPQyxJQUFQLENBQVlSLFNBQVosRUFBdUJTLE9BQXZCLENBQStCLGVBQU87QUFDcENiLGVBQVNjLEdBQVQsSUFBZ0JWLFVBQVVVLEdBQVYsQ0FBaEI7QUFDRCxLQUZEO0FBR0EsV0FBT2QsUUFBUDtBQUNELEdBVHFCLEdBVXJCUSxLQVZxQiw0QkFVWEcsT0FBT0ksTUFBUCxDQUFjWCxTQUFkLENBVlcsR0FBeEI7O0FBYUEsTUFBTVksa0JBQWtCLFNBQWxCQSxlQUFrQixDQUFDQyxPQUFELEVBQVVoQixJQUFWO0FBQUEsV0FDdEJRLFNBQVMsZUFBTztBQUNkLFVBQU1ULFdBQVdpQixRQUFRbEIsR0FBUixDQUFqQjtBQUNBLGFBQU9NLFFBQVFOLEdBQVIsRUFBYUMsUUFBYixFQUF1QkMsSUFBdkIsQ0FBUDtBQUNELEtBSEQsQ0FEc0I7QUFBQSxHQUF4Qjs7QUFNQSxTQUFPLENBQUNTLGVBQUQsRUFBa0JNLGVBQWxCLENBQVA7QUFDRCxDQTlCTSIsImZpbGUiOiJ1c2VUYWJsZVN0YXRlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IHVzZVN0YXRlLCB1c2VNZW1vIH0gZnJvbSBcInJlYWN0XCI7XG5cbmV4cG9ydCBjb25zdCBkZWZhdWx0U3RhdGUgPSB7fTtcblxuY29uc3QgZGVmYXVsdFJlZHVjZXIgPSAob2xkLCBuZXdTdGF0ZSwgdHlwZSkgPT4gbmV3U3RhdGU7XG5cbmV4cG9ydCBjb25zdCB1c2VUYWJsZVN0YXRlID0gKFxuICBpbml0aWFsU3RhdGUgPSB7fSxcbiAgb3ZlcnJpZGVzID0ge30sXG4gIHsgcmVkdWNlciA9IGRlZmF1bHRSZWR1Y2VyLCB1c2VTdGF0ZTogdXNlclVzZVN0YXRlID0gdXNlU3RhdGUgfSA9IHt9XG4pID0+IHtcbiAgbGV0IFtzdGF0ZSwgc2V0U3RhdGVdID0gdXNlclVzZVN0YXRlKHtcbiAgICAuLi5kZWZhdWx0U3RhdGUsXG4gICAgLi4uaW5pdGlhbFN0YXRlXG4gIH0pO1xuXG4gIGNvbnN0IG92ZXJyaWRkZW5TdGF0ZSA9IHVzZU1lbW8oXG4gICAgKCkgPT4ge1xuICAgICAgY29uc3QgbmV3U3RhdGUgPSB7XG4gICAgICAgIC4uLnN0YXRlXG4gICAgICB9O1xuICAgICAgT2JqZWN0LmtleXMob3ZlcnJpZGVzKS5mb3JFYWNoKGtleSA9PiB7XG4gICAgICAgIG5ld1N0YXRlW2tleV0gPSBvdmVycmlkZXNba2V5XTtcbiAgICAgIH0pO1xuICAgICAgcmV0dXJuIG5ld1N0YXRlO1xuICAgIH0sXG4gICAgW3N0YXRlLCAuLi5PYmplY3QudmFsdWVzKG92ZXJyaWRlcyldXG4gICk7XG5cbiAgY29uc3QgcmVkdWNlZFNldFN0YXRlID0gKHVwZGF0ZXIsIHR5cGUpID0+XG4gICAgc2V0U3RhdGUob2xkID0+IHtcbiAgICAgIGNvbnN0IG5ld1N0YXRlID0gdXBkYXRlcihvbGQpO1xuICAgICAgcmV0dXJuIHJlZHVjZXIob2xkLCBuZXdTdGF0ZSwgdHlwZSk7XG4gICAgfSk7XG5cbiAgcmV0dXJuIFtvdmVycmlkZGVuU3RhdGUsIHJlZHVjZWRTZXRTdGF0ZV07XG59O1xuIl19