"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useExpanded = undefined;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _react = require("react");

var _utils = require("../utils");

var _actions = require("../actions");

var _useTableState = require("./useTableState");

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

_useTableState.defaultState.expanded = {};

(0, _actions.addActions)({
  toggleExpanded: "__toggleExpanded__",
  useExpanded: "__useExpanded__"
});

var useExpanded = exports.useExpanded = function useExpanded(props) {
  var debug = props.debug,
      columns = props.columns,
      rows = props.rows,
      _props$expandedKey = props.expandedKey,
      expandedKey = _props$expandedKey === undefined ? "expanded" : _props$expandedKey,
      hooks = props.hooks,
      _props$state = _slicedToArray(props.state, 2),
      expanded = _props$state[0].expanded,
      setState = _props$state[1];

  var toggleExpandedByPath = function toggleExpandedByPath(path, set) {
    return setState(function (old) {
      var expanded = old.expanded;

      var existing = (0, _utils.getBy)(expanded, path);
      set = (0, _utils.getFirstDefined)(set, !existing);
      return _extends({}, old, {
        expanded: (0, _utils.setBy)(expanded, path, set)
      });
    }, _actions.actions.toggleExpanded);
  };

  hooks.row.push(function (row) {
    var path = row.path;

    row.toggleExpanded = function (set) {
      return toggleExpandedByPath(path, set);
    };
  });

  var expandedRows = (0, _react.useMemo)(function () {
    if (debug) console.info("getExpandedRows");

    var expandedRows = [];

    // Here we do some mutation, but it's the last stage in the
    // immutable process so this is safe
    var handleRow = function handleRow(row, index) {
      var depth = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
      var parentPath = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];

      // Compute some final state for the row
      var path = [].concat(_toConsumableArray(parentPath), [index]);

      row.path = path;
      row.depth = depth;

      row.isExpanded = row.original && row.original[expandedKey] || (0, _utils.getBy)(expanded, path);

      row.cells = columns.map(function (column) {
        var cell = {
          column: column,
          row: row,
          state: null,
          value: row.values[column.id]
        };

        return cell;
      });

      expandedRows.push(row);

      if (row.isExpanded && row.subRows && row.subRows.length) {
        row.subRows.forEach(function (row, i) {
          return handleRow(row, i, depth + 1, path);
        });
      }
    };

    rows.forEach(function (row, i) {
      return handleRow(row, i);
    });

    return expandedRows;
  }, [rows, expanded, columns]);

  var expandedDepth = findExpandedDepth(expanded);

  return _extends({}, props, {
    toggleExpandedByPath: toggleExpandedByPath,
    expandedDepth: expandedDepth,
    rows: expandedRows
  });
};

function findExpandedDepth(obj) {
  var depth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;

  return Object.values(obj).reduce(function (prev, curr) {
    if ((typeof curr === "undefined" ? "undefined" : _typeof(curr)) === "object") {
      return Math.max(prev, findExpandedDepth(curr, depth + 1));
    }
    return depth;
  }, 0);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VFeHBhbmRlZC5qcyJdLCJuYW1lcyI6WyJkZWZhdWx0U3RhdGUiLCJleHBhbmRlZCIsInRvZ2dsZUV4cGFuZGVkIiwidXNlRXhwYW5kZWQiLCJkZWJ1ZyIsInByb3BzIiwiY29sdW1ucyIsInJvd3MiLCJleHBhbmRlZEtleSIsImhvb2tzIiwic3RhdGUiLCJzZXRTdGF0ZSIsInRvZ2dsZUV4cGFuZGVkQnlQYXRoIiwicGF0aCIsInNldCIsIm9sZCIsImV4aXN0aW5nIiwiYWN0aW9ucyIsInJvdyIsInB1c2giLCJleHBhbmRlZFJvd3MiLCJjb25zb2xlIiwiaW5mbyIsImhhbmRsZVJvdyIsImluZGV4IiwiZGVwdGgiLCJwYXJlbnRQYXRoIiwiaXNFeHBhbmRlZCIsIm9yaWdpbmFsIiwiY2VsbHMiLCJtYXAiLCJjZWxsIiwiY29sdW1uIiwidmFsdWUiLCJ2YWx1ZXMiLCJpZCIsInN1YlJvd3MiLCJsZW5ndGgiLCJmb3JFYWNoIiwiaSIsImV4cGFuZGVkRGVwdGgiLCJmaW5kRXhwYW5kZWREZXB0aCIsIm9iaiIsIk9iamVjdCIsInJlZHVjZSIsInByZXYiLCJjdXJyIiwiTWF0aCIsIm1heCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUFBOztBQUVBOztBQUNBOztBQUNBOzs7O0FBRUFBLDRCQUFhQyxRQUFiLEdBQXdCLEVBQXhCOztBQUVBLHlCQUFXO0FBQ1RDLGtCQUFnQixvQkFEUDtBQUVUQyxlQUFhO0FBRkosQ0FBWDs7QUFLTyxJQUFNQSxvQ0FBYyxTQUFkQSxXQUFjLFFBQVM7QUFBQSxNQUVoQ0MsS0FGZ0MsR0FROUJDLEtBUjhCLENBRWhDRCxLQUZnQztBQUFBLE1BR2hDRSxPQUhnQyxHQVE5QkQsS0FSOEIsQ0FHaENDLE9BSGdDO0FBQUEsTUFJaENDLElBSmdDLEdBUTlCRixLQVI4QixDQUloQ0UsSUFKZ0M7QUFBQSwyQkFROUJGLEtBUjhCLENBS2hDRyxXQUxnQztBQUFBLE1BS2hDQSxXQUxnQyxzQ0FLbEIsVUFMa0I7QUFBQSxNQU1oQ0MsS0FOZ0MsR0FROUJKLEtBUjhCLENBTWhDSSxLQU5nQztBQUFBLG9DQVE5QkosS0FSOEIsQ0FPaENLLEtBUGdDO0FBQUEsTUFPdEJULFFBUHNCLG1CQU90QkEsUUFQc0I7QUFBQSxNQU9WVSxRQVBVOztBQVVsQyxNQUFNQyx1QkFBdUIsU0FBdkJBLG9CQUF1QixDQUFDQyxJQUFELEVBQU9DLEdBQVAsRUFBZTtBQUMxQyxXQUFPSCxTQUFTLGVBQU87QUFBQSxVQUNiVixRQURhLEdBQ0FjLEdBREEsQ0FDYmQsUUFEYTs7QUFFckIsVUFBTWUsV0FBVyxrQkFBTWYsUUFBTixFQUFnQlksSUFBaEIsQ0FBakI7QUFDQUMsWUFBTSw0QkFBZ0JBLEdBQWhCLEVBQXFCLENBQUNFLFFBQXRCLENBQU47QUFDQSwwQkFDS0QsR0FETDtBQUVFZCxrQkFBVSxrQkFBTUEsUUFBTixFQUFnQlksSUFBaEIsRUFBc0JDLEdBQXRCO0FBRlo7QUFJRCxLQVJNLEVBUUpHLGlCQUFRZixjQVJKLENBQVA7QUFTRCxHQVZEOztBQVlBTyxRQUFNUyxHQUFOLENBQVVDLElBQVYsQ0FBZSxlQUFPO0FBQUEsUUFDWk4sSUFEWSxHQUNISyxHQURHLENBQ1pMLElBRFk7O0FBRXBCSyxRQUFJaEIsY0FBSixHQUFxQjtBQUFBLGFBQU9VLHFCQUFxQkMsSUFBckIsRUFBMkJDLEdBQTNCLENBQVA7QUFBQSxLQUFyQjtBQUNELEdBSEQ7O0FBS0EsTUFBTU0sZUFBZSxvQkFDbkIsWUFBTTtBQUNKLFFBQUloQixLQUFKLEVBQVdpQixRQUFRQyxJQUFSLENBQWEsaUJBQWI7O0FBRVgsUUFBTUYsZUFBZSxFQUFyQjs7QUFFQTtBQUNBO0FBQ0EsUUFBTUcsWUFBWSxTQUFaQSxTQUFZLENBQUNMLEdBQUQsRUFBTU0sS0FBTixFQUE0QztBQUFBLFVBQS9CQyxLQUErQix1RUFBdkIsQ0FBdUI7QUFBQSxVQUFwQkMsVUFBb0IsdUVBQVAsRUFBTzs7QUFDNUQ7QUFDQSxVQUFNYixvQ0FBV2EsVUFBWCxJQUF1QkYsS0FBdkIsRUFBTjs7QUFFQU4sVUFBSUwsSUFBSixHQUFXQSxJQUFYO0FBQ0FLLFVBQUlPLEtBQUosR0FBWUEsS0FBWjs7QUFFQVAsVUFBSVMsVUFBSixHQUNHVCxJQUFJVSxRQUFKLElBQWdCVixJQUFJVSxRQUFKLENBQWFwQixXQUFiLENBQWpCLElBQStDLGtCQUFNUCxRQUFOLEVBQWdCWSxJQUFoQixDQURqRDs7QUFHQUssVUFBSVcsS0FBSixHQUFZdkIsUUFBUXdCLEdBQVIsQ0FBWSxrQkFBVTtBQUNoQyxZQUFNQyxPQUFPO0FBQ1hDLHdCQURXO0FBRVhkLGtCQUZXO0FBR1hSLGlCQUFPLElBSEk7QUFJWHVCLGlCQUFPZixJQUFJZ0IsTUFBSixDQUFXRixPQUFPRyxFQUFsQjtBQUpJLFNBQWI7O0FBT0EsZUFBT0osSUFBUDtBQUNELE9BVFcsQ0FBWjs7QUFXQVgsbUJBQWFELElBQWIsQ0FBa0JELEdBQWxCOztBQUVBLFVBQUlBLElBQUlTLFVBQUosSUFBa0JULElBQUlrQixPQUF0QixJQUFpQ2xCLElBQUlrQixPQUFKLENBQVlDLE1BQWpELEVBQXlEO0FBQ3ZEbkIsWUFBSWtCLE9BQUosQ0FBWUUsT0FBWixDQUFvQixVQUFDcEIsR0FBRCxFQUFNcUIsQ0FBTjtBQUFBLGlCQUFZaEIsVUFBVUwsR0FBVixFQUFlcUIsQ0FBZixFQUFrQmQsUUFBUSxDQUExQixFQUE2QlosSUFBN0IsQ0FBWjtBQUFBLFNBQXBCO0FBQ0Q7QUFDRixLQTFCRDs7QUE0QkFOLFNBQUsrQixPQUFMLENBQWEsVUFBQ3BCLEdBQUQsRUFBTXFCLENBQU47QUFBQSxhQUFZaEIsVUFBVUwsR0FBVixFQUFlcUIsQ0FBZixDQUFaO0FBQUEsS0FBYjs7QUFFQSxXQUFPbkIsWUFBUDtBQUNELEdBdkNrQixFQXdDbkIsQ0FBQ2IsSUFBRCxFQUFPTixRQUFQLEVBQWlCSyxPQUFqQixDQXhDbUIsQ0FBckI7O0FBMkNBLE1BQU1rQyxnQkFBZ0JDLGtCQUFrQnhDLFFBQWxCLENBQXRCOztBQUVBLHNCQUNLSSxLQURMO0FBRUVPLDhDQUZGO0FBR0U0QixnQ0FIRjtBQUlFakMsVUFBTWE7QUFKUjtBQU1ELENBOUVNOztBQWdGUCxTQUFTcUIsaUJBQVQsQ0FBMkJDLEdBQTNCLEVBQTJDO0FBQUEsTUFBWGpCLEtBQVcsdUVBQUgsQ0FBRzs7QUFDekMsU0FBT2tCLE9BQU9ULE1BQVAsQ0FBY1EsR0FBZCxFQUFtQkUsTUFBbkIsQ0FBMEIsVUFBQ0MsSUFBRCxFQUFPQyxJQUFQLEVBQWdCO0FBQy9DLFFBQUksUUFBT0EsSUFBUCx5Q0FBT0EsSUFBUCxPQUFnQixRQUFwQixFQUE4QjtBQUM1QixhQUFPQyxLQUFLQyxHQUFMLENBQVNILElBQVQsRUFBZUosa0JBQWtCSyxJQUFsQixFQUF3QnJCLFFBQVEsQ0FBaEMsQ0FBZixDQUFQO0FBQ0Q7QUFDRCxXQUFPQSxLQUFQO0FBQ0QsR0FMTSxFQUtKLENBTEksQ0FBUDtBQU1EIiwiZmlsZSI6InVzZUV4cGFuZGVkLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlTWVtbyB9IGZyb20gXCJyZWFjdFwiO1xuXG5pbXBvcnQgeyBnZXRCeSwgZ2V0Rmlyc3REZWZpbmVkLCBzZXRCeSB9IGZyb20gXCIuLi91dGlsc1wiO1xuaW1wb3J0IHsgYWRkQWN0aW9ucywgYWN0aW9ucyB9IGZyb20gXCIuLi9hY3Rpb25zXCI7XG5pbXBvcnQgeyBkZWZhdWx0U3RhdGUgfSBmcm9tIFwiLi91c2VUYWJsZVN0YXRlXCI7XG5cbmRlZmF1bHRTdGF0ZS5leHBhbmRlZCA9IHt9O1xuXG5hZGRBY3Rpb25zKHtcbiAgdG9nZ2xlRXhwYW5kZWQ6IFwiX190b2dnbGVFeHBhbmRlZF9fXCIsXG4gIHVzZUV4cGFuZGVkOiBcIl9fdXNlRXhwYW5kZWRfX1wiXG59KTtcblxuZXhwb3J0IGNvbnN0IHVzZUV4cGFuZGVkID0gcHJvcHMgPT4ge1xuICBjb25zdCB7XG4gICAgZGVidWcsXG4gICAgY29sdW1ucyxcbiAgICByb3dzLFxuICAgIGV4cGFuZGVkS2V5ID0gXCJleHBhbmRlZFwiLFxuICAgIGhvb2tzLFxuICAgIHN0YXRlOiBbeyBleHBhbmRlZCB9LCBzZXRTdGF0ZV1cbiAgfSA9IHByb3BzO1xuXG4gIGNvbnN0IHRvZ2dsZUV4cGFuZGVkQnlQYXRoID0gKHBhdGgsIHNldCkgPT4ge1xuICAgIHJldHVybiBzZXRTdGF0ZShvbGQgPT4ge1xuICAgICAgY29uc3QgeyBleHBhbmRlZCB9ID0gb2xkO1xuICAgICAgY29uc3QgZXhpc3RpbmcgPSBnZXRCeShleHBhbmRlZCwgcGF0aCk7XG4gICAgICBzZXQgPSBnZXRGaXJzdERlZmluZWQoc2V0LCAhZXhpc3RpbmcpO1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgLi4ub2xkLFxuICAgICAgICBleHBhbmRlZDogc2V0QnkoZXhwYW5kZWQsIHBhdGgsIHNldClcbiAgICAgIH07XG4gICAgfSwgYWN0aW9ucy50b2dnbGVFeHBhbmRlZCk7XG4gIH07XG5cbiAgaG9va3Mucm93LnB1c2gocm93ID0+IHtcbiAgICBjb25zdCB7IHBhdGggfSA9IHJvdztcbiAgICByb3cudG9nZ2xlRXhwYW5kZWQgPSBzZXQgPT4gdG9nZ2xlRXhwYW5kZWRCeVBhdGgocGF0aCwgc2V0KTtcbiAgfSk7XG5cbiAgY29uc3QgZXhwYW5kZWRSb3dzID0gdXNlTWVtbyhcbiAgICAoKSA9PiB7XG4gICAgICBpZiAoZGVidWcpIGNvbnNvbGUuaW5mbyhcImdldEV4cGFuZGVkUm93c1wiKTtcblxuICAgICAgY29uc3QgZXhwYW5kZWRSb3dzID0gW107XG5cbiAgICAgIC8vIEhlcmUgd2UgZG8gc29tZSBtdXRhdGlvbiwgYnV0IGl0J3MgdGhlIGxhc3Qgc3RhZ2UgaW4gdGhlXG4gICAgICAvLyBpbW11dGFibGUgcHJvY2VzcyBzbyB0aGlzIGlzIHNhZmVcbiAgICAgIGNvbnN0IGhhbmRsZVJvdyA9IChyb3csIGluZGV4LCBkZXB0aCA9IDAsIHBhcmVudFBhdGggPSBbXSkgPT4ge1xuICAgICAgICAvLyBDb21wdXRlIHNvbWUgZmluYWwgc3RhdGUgZm9yIHRoZSByb3dcbiAgICAgICAgY29uc3QgcGF0aCA9IFsuLi5wYXJlbnRQYXRoLCBpbmRleF07XG5cbiAgICAgICAgcm93LnBhdGggPSBwYXRoO1xuICAgICAgICByb3cuZGVwdGggPSBkZXB0aDtcblxuICAgICAgICByb3cuaXNFeHBhbmRlZCA9XG4gICAgICAgICAgKHJvdy5vcmlnaW5hbCAmJiByb3cub3JpZ2luYWxbZXhwYW5kZWRLZXldKSB8fCBnZXRCeShleHBhbmRlZCwgcGF0aCk7XG5cbiAgICAgICAgcm93LmNlbGxzID0gY29sdW1ucy5tYXAoY29sdW1uID0+IHtcbiAgICAgICAgICBjb25zdCBjZWxsID0ge1xuICAgICAgICAgICAgY29sdW1uLFxuICAgICAgICAgICAgcm93LFxuICAgICAgICAgICAgc3RhdGU6IG51bGwsXG4gICAgICAgICAgICB2YWx1ZTogcm93LnZhbHVlc1tjb2x1bW4uaWRdXG4gICAgICAgICAgfTtcblxuICAgICAgICAgIHJldHVybiBjZWxsO1xuICAgICAgICB9KTtcblxuICAgICAgICBleHBhbmRlZFJvd3MucHVzaChyb3cpO1xuXG4gICAgICAgIGlmIChyb3cuaXNFeHBhbmRlZCAmJiByb3cuc3ViUm93cyAmJiByb3cuc3ViUm93cy5sZW5ndGgpIHtcbiAgICAgICAgICByb3cuc3ViUm93cy5mb3JFYWNoKChyb3csIGkpID0+IGhhbmRsZVJvdyhyb3csIGksIGRlcHRoICsgMSwgcGF0aCkpO1xuICAgICAgICB9XG4gICAgICB9O1xuXG4gICAgICByb3dzLmZvckVhY2goKHJvdywgaSkgPT4gaGFuZGxlUm93KHJvdywgaSkpO1xuXG4gICAgICByZXR1cm4gZXhwYW5kZWRSb3dzO1xuICAgIH0sXG4gICAgW3Jvd3MsIGV4cGFuZGVkLCBjb2x1bW5zXVxuICApO1xuXG4gIGNvbnN0IGV4cGFuZGVkRGVwdGggPSBmaW5kRXhwYW5kZWREZXB0aChleHBhbmRlZCk7XG5cbiAgcmV0dXJuIHtcbiAgICAuLi5wcm9wcyxcbiAgICB0b2dnbGVFeHBhbmRlZEJ5UGF0aCxcbiAgICBleHBhbmRlZERlcHRoLFxuICAgIHJvd3M6IGV4cGFuZGVkUm93c1xuICB9O1xufTtcblxuZnVuY3Rpb24gZmluZEV4cGFuZGVkRGVwdGgob2JqLCBkZXB0aCA9IDEpIHtcbiAgcmV0dXJuIE9iamVjdC52YWx1ZXMob2JqKS5yZWR1Y2UoKHByZXYsIGN1cnIpID0+IHtcbiAgICBpZiAodHlwZW9mIGN1cnIgPT09IFwib2JqZWN0XCIpIHtcbiAgICAgIHJldHVybiBNYXRoLm1heChwcmV2LCBmaW5kRXhwYW5kZWREZXB0aChjdXJyLCBkZXB0aCArIDEpKTtcbiAgICB9XG4gICAgcmV0dXJuIGRlcHRoO1xuICB9LCAwKTtcbn1cbiJdfQ==