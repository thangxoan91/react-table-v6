"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useSortBy = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _react = require("react");

var _actions = require("../actions");

var _useTableState = require("./useTableState");

var _utils = require("../utils");

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

_useTableState.defaultState.sortBy = [];

(0, _actions.addActions)({
  sortByChange: "__sortByChange__"
});

var useSortBy = exports.useSortBy = function useSortBy(api) {
  var debug = api.debug,
      rows = api.rows,
      columns = api.columns,
      _api$orderByFn = api.orderByFn,
      orderByFn = _api$orderByFn === undefined ? _utils.defaultOrderByFn : _api$orderByFn,
      _api$sortByFn = api.sortByFn,
      sortByFn = _api$sortByFn === undefined ? _utils.defaultSortByFn : _api$sortByFn,
      manualSorting = api.manualSorting,
      disableSorting = api.disableSorting,
      defaultSortDesc = api.defaultSortDesc,
      hooks = api.hooks,
      _api$state = _slicedToArray(api.state, 2),
      sortBy = _api$state[0].sortBy,
      setState = _api$state[1];

  columns.forEach(function (column) {
    var accessor = column.accessor,
        canSortBy = column.canSortBy;

    column.canSortBy = accessor ? (0, _utils.getFirstDefined)(canSortBy, disableSorting === true ? false : undefined, true) : false;
  });

  // Updates sorting based on a columnID, desc flag and multi flag
  var toggleSortByID = function toggleSortByID(columnID, desc, multi) {
    return setState(function (old) {
      var sortBy = old.sortBy;

      // Find the column for this columnID

      var column = columns.find(function (d) {
        return d.id === columnID;
      });
      var resolvedDefaultSortDesc = (0, _utils.getFirstDefined)(column.defaultSortDesc, defaultSortDesc);

      // Find any existing sortBy for this column
      var existingSortBy = sortBy.find(function (d) {
        return d.id === columnID;
      });
      var hasDescDefined = typeof desc !== "undefined" && desc !== null;

      var newSortBy = [];

      // What should we do with this filter?
      var action = void 0;

      if (!multi) {
        if (sortBy.length <= 1 && existingSortBy) {
          if (existingSortBy.desc) {
            action = "remove";
          } else {
            action = "toggle";
          }
        } else {
          action = "replace";
        }
      } else {
        if (!existingSortBy) {
          action = "add";
        } else {
          if (hasDescDefined) {
            action = "set";
          } else {
            action = "toggle";
          }
        }
      }

      if (action === "replace") {
        newSortBy = [{
          id: columnID,
          desc: hasDescDefined ? desc : resolvedDefaultSortDesc
        }];
      } else if (action === "add") {
        newSortBy = [].concat(_toConsumableArray(sortBy), [{
          id: columnID,
          desc: hasDescDefined ? desc : resolvedDefaultSortDesc
        }]);
      } else if (action === "set") {
        newSortBy = sortBy.map(function (d) {
          if (d.id === columnID) {
            return _extends({}, d, {
              desc: desc
            });
          }
          return d;
        });
      } else if (action === "toggle") {
        newSortBy = sortBy.map(function (d) {
          if (d.id === columnID) {
            return _extends({}, d, {
              desc: !existingSortBy.desc
            });
          }
          return d;
        });
      } else if (action === "remove") {
        newSortBy = [];
      }

      return _extends({}, old, {
        sortBy: newSortBy
      });
    }, _actions.actions.sortByChange);
  };

  hooks.columns.push(function (columns) {
    columns.forEach(function (column) {
      if (column.canSortBy) {
        column.toggleSortBy = function (desc, multi) {
          return toggleSortByID(column.id, desc, multi);
        };
      }
    });
    return columns;
  });

  hooks.getSortByToggleProps = [];

  var addSortByToggleProps = function addSortByToggleProps(columns, api) {
    columns.forEach(function (column) {
      var canSortBy = column.canSortBy;

      column.getSortByToggleProps = function (props) {
        return (0, _utils.mergeProps)({
          onClick: canSortBy ? function (e) {
            e.persist();
            column.toggleSortBy(undefined, !api.disableMultiSort && e.shiftKey);
          } : undefined,
          style: {
            cursor: canSortBy ? "pointer" : undefined
          },
          title: "Toggle SortBy"
        }, (0, _utils.applyPropHooks)(api.hooks.getSortByToggleProps, column, api), props);
      };
    });
    return columns;
  };

  hooks.columns.push(addSortByToggleProps);
  hooks.headers.push(addSortByToggleProps);

  // Mutate columns to reflect sorting state
  columns.forEach(function (column) {
    var id = column.id;

    column.sorted = sortBy.find(function (d) {
      return d.id === id;
    });
    column.sortedIndex = sortBy.findIndex(function (d) {
      return d.id === id;
    });
    column.sortedDesc = column.sorted ? column.sorted.desc : undefined;
  });

  var sortedRows = (0, _react.useMemo)(function () {
    if (manualSorting || !sortBy.length) {
      return rows;
    }
    if (debug) console.info("getSortedRows");

    var sortMethodsByColumnID = {};

    columns.filter(function (col) {
      return col.sortMethod;
    }).forEach(function (col) {
      sortMethodsByColumnID[col.id] = col.sortMethod;
    });

    var sortData = function sortData(rows) {
      // Use the orderByFn to compose multiple sortBy's together.
      // This will also perform a stable sorting using the row index
      // if needed.
      var sortedData = orderByFn(rows, sortBy.map(function (sort) {
        // Support custom sorting methods for each column
        var columnSortBy = sortMethodsByColumnID[sort.id];

        // Return the correct sortFn
        return function (a, b) {
          return (columnSortBy ? columnSortBy : sortByFn)(a.values[sort.id], b.values[sort.id], sort.desc);
        };
      }),
      // Map the directions
      sortBy.map(function (d) {
        return !d.desc;
      }));

      // TODO: this should be optimized. Not good to loop again
      sortedData.forEach(function (row) {
        if (!row.subRows) {
          return;
        }
        row.subRows = sortData(row.subRows);
      });

      return sortedData;
    };

    return sortData(rows);
  }, [rows, columns, sortBy, manualSorting]);

  return _extends({}, api, {
    rows: sortedRows
  });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VTb3J0QnkuanMiXSwibmFtZXMiOlsiZGVmYXVsdFN0YXRlIiwic29ydEJ5Iiwic29ydEJ5Q2hhbmdlIiwidXNlU29ydEJ5IiwiZGVidWciLCJhcGkiLCJyb3dzIiwiY29sdW1ucyIsIm9yZGVyQnlGbiIsImRlZmF1bHRPcmRlckJ5Rm4iLCJzb3J0QnlGbiIsImRlZmF1bHRTb3J0QnlGbiIsIm1hbnVhbFNvcnRpbmciLCJkaXNhYmxlU29ydGluZyIsImRlZmF1bHRTb3J0RGVzYyIsImhvb2tzIiwic3RhdGUiLCJzZXRTdGF0ZSIsImZvckVhY2giLCJhY2Nlc3NvciIsImNvbHVtbiIsImNhblNvcnRCeSIsInVuZGVmaW5lZCIsInRvZ2dsZVNvcnRCeUlEIiwiY29sdW1uSUQiLCJkZXNjIiwibXVsdGkiLCJvbGQiLCJmaW5kIiwiZCIsImlkIiwicmVzb2x2ZWREZWZhdWx0U29ydERlc2MiLCJleGlzdGluZ1NvcnRCeSIsImhhc0Rlc2NEZWZpbmVkIiwibmV3U29ydEJ5IiwiYWN0aW9uIiwibGVuZ3RoIiwibWFwIiwiYWN0aW9ucyIsInB1c2giLCJ0b2dnbGVTb3J0QnkiLCJnZXRTb3J0QnlUb2dnbGVQcm9wcyIsImFkZFNvcnRCeVRvZ2dsZVByb3BzIiwib25DbGljayIsImUiLCJwZXJzaXN0IiwiZGlzYWJsZU11bHRpU29ydCIsInNoaWZ0S2V5Iiwic3R5bGUiLCJjdXJzb3IiLCJ0aXRsZSIsInByb3BzIiwiaGVhZGVycyIsInNvcnRlZCIsInNvcnRlZEluZGV4IiwiZmluZEluZGV4Iiwic29ydGVkRGVzYyIsInNvcnRlZFJvd3MiLCJjb25zb2xlIiwiaW5mbyIsInNvcnRNZXRob2RzQnlDb2x1bW5JRCIsImZpbHRlciIsImNvbCIsInNvcnRNZXRob2QiLCJzb3J0RGF0YSIsInNvcnRlZERhdGEiLCJjb2x1bW5Tb3J0QnkiLCJzb3J0IiwiYSIsImIiLCJ2YWx1ZXMiLCJyb3ciLCJzdWJSb3dzIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBOztBQUVBOztBQUNBOztBQUNBOzs7O0FBUUFBLDRCQUFhQyxNQUFiLEdBQXNCLEVBQXRCOztBQUVBLHlCQUFXO0FBQ1RDLGdCQUFjO0FBREwsQ0FBWDs7QUFJTyxJQUFNQyxnQ0FBWSxTQUFaQSxTQUFZLE1BQU87QUFBQSxNQUU1QkMsS0FGNEIsR0FZMUJDLEdBWjBCLENBRTVCRCxLQUY0QjtBQUFBLE1BRzVCRSxJQUg0QixHQVkxQkQsR0FaMEIsQ0FHNUJDLElBSDRCO0FBQUEsTUFJNUJDLE9BSjRCLEdBWTFCRixHQVowQixDQUk1QkUsT0FKNEI7QUFBQSx1QkFZMUJGLEdBWjBCLENBSzVCRyxTQUw0QjtBQUFBLE1BSzVCQSxTQUw0QixrQ0FLaEJDLHVCQUxnQjtBQUFBLHNCQVkxQkosR0FaMEIsQ0FNNUJLLFFBTjRCO0FBQUEsTUFNNUJBLFFBTjRCLGlDQU1qQkMsc0JBTmlCO0FBQUEsTUFPNUJDLGFBUDRCLEdBWTFCUCxHQVowQixDQU81Qk8sYUFQNEI7QUFBQSxNQVE1QkMsY0FSNEIsR0FZMUJSLEdBWjBCLENBUTVCUSxjQVI0QjtBQUFBLE1BUzVCQyxlQVQ0QixHQVkxQlQsR0FaMEIsQ0FTNUJTLGVBVDRCO0FBQUEsTUFVNUJDLEtBVjRCLEdBWTFCVixHQVowQixDQVU1QlUsS0FWNEI7QUFBQSxrQ0FZMUJWLEdBWjBCLENBVzVCVyxLQVg0QjtBQUFBLE1BV2xCZixNQVhrQixpQkFXbEJBLE1BWGtCO0FBQUEsTUFXUmdCLFFBWFE7O0FBYzlCVixVQUFRVyxPQUFSLENBQWdCLGtCQUFVO0FBQUEsUUFDaEJDLFFBRGdCLEdBQ1FDLE1BRFIsQ0FDaEJELFFBRGdCO0FBQUEsUUFDTkUsU0FETSxHQUNRRCxNQURSLENBQ05DLFNBRE07O0FBRXhCRCxXQUFPQyxTQUFQLEdBQW1CRixXQUNmLDRCQUNFRSxTQURGLEVBRUVSLG1CQUFtQixJQUFuQixHQUEwQixLQUExQixHQUFrQ1MsU0FGcEMsRUFHRSxJQUhGLENBRGUsR0FNZixLQU5KO0FBT0QsR0FURDs7QUFXQTtBQUNBLE1BQU1DLGlCQUFpQixTQUFqQkEsY0FBaUIsQ0FBQ0MsUUFBRCxFQUFXQyxJQUFYLEVBQWlCQyxLQUFqQixFQUEyQjtBQUNoRCxXQUFPVCxTQUFTLGVBQU87QUFBQSxVQUNiaEIsTUFEYSxHQUNGMEIsR0FERSxDQUNiMUIsTUFEYTs7QUFHckI7O0FBQ0EsVUFBTW1CLFNBQVNiLFFBQVFxQixJQUFSLENBQWE7QUFBQSxlQUFLQyxFQUFFQyxFQUFGLEtBQVNOLFFBQWQ7QUFBQSxPQUFiLENBQWY7QUFDQSxVQUFNTywwQkFBMEIsNEJBQzlCWCxPQUFPTixlQUR1QixFQUU5QkEsZUFGOEIsQ0FBaEM7O0FBS0E7QUFDQSxVQUFNa0IsaUJBQWlCL0IsT0FBTzJCLElBQVAsQ0FBWTtBQUFBLGVBQUtDLEVBQUVDLEVBQUYsS0FBU04sUUFBZDtBQUFBLE9BQVosQ0FBdkI7QUFDQSxVQUFNUyxpQkFBaUIsT0FBT1IsSUFBUCxLQUFnQixXQUFoQixJQUErQkEsU0FBUyxJQUEvRDs7QUFFQSxVQUFJUyxZQUFZLEVBQWhCOztBQUVBO0FBQ0EsVUFBSUMsZUFBSjs7QUFFQSxVQUFJLENBQUNULEtBQUwsRUFBWTtBQUNWLFlBQUl6QixPQUFPbUMsTUFBUCxJQUFpQixDQUFqQixJQUFzQkosY0FBMUIsRUFBMEM7QUFDeEMsY0FBSUEsZUFBZVAsSUFBbkIsRUFBeUI7QUFDdkJVLHFCQUFTLFFBQVQ7QUFDRCxXQUZELE1BRU87QUFDTEEscUJBQVMsUUFBVDtBQUNEO0FBQ0YsU0FORCxNQU1PO0FBQ0xBLG1CQUFTLFNBQVQ7QUFDRDtBQUNGLE9BVkQsTUFVTztBQUNMLFlBQUksQ0FBQ0gsY0FBTCxFQUFxQjtBQUNuQkcsbUJBQVMsS0FBVDtBQUNELFNBRkQsTUFFTztBQUNMLGNBQUlGLGNBQUosRUFBb0I7QUFDbEJFLHFCQUFTLEtBQVQ7QUFDRCxXQUZELE1BRU87QUFDTEEscUJBQVMsUUFBVDtBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxVQUFJQSxXQUFXLFNBQWYsRUFBMEI7QUFDeEJELG9CQUFZLENBQ1Y7QUFDRUosY0FBSU4sUUFETjtBQUVFQyxnQkFBTVEsaUJBQWlCUixJQUFqQixHQUF3Qk07QUFGaEMsU0FEVSxDQUFaO0FBTUQsT0FQRCxNQU9PLElBQUlJLFdBQVcsS0FBZixFQUFzQjtBQUMzQkQsaURBQ0tqQyxNQURMLElBRUU7QUFDRTZCLGNBQUlOLFFBRE47QUFFRUMsZ0JBQU1RLGlCQUFpQlIsSUFBakIsR0FBd0JNO0FBRmhDLFNBRkY7QUFPRCxPQVJNLE1BUUEsSUFBSUksV0FBVyxLQUFmLEVBQXNCO0FBQzNCRCxvQkFBWWpDLE9BQU9vQyxHQUFQLENBQVcsYUFBSztBQUMxQixjQUFJUixFQUFFQyxFQUFGLEtBQVNOLFFBQWIsRUFBdUI7QUFDckIsZ0NBQ0tLLENBREw7QUFFRUo7QUFGRjtBQUlEO0FBQ0QsaUJBQU9JLENBQVA7QUFDRCxTQVJXLENBQVo7QUFTRCxPQVZNLE1BVUEsSUFBSU0sV0FBVyxRQUFmLEVBQXlCO0FBQzlCRCxvQkFBWWpDLE9BQU9vQyxHQUFQLENBQVcsYUFBSztBQUMxQixjQUFJUixFQUFFQyxFQUFGLEtBQVNOLFFBQWIsRUFBdUI7QUFDckIsZ0NBQ0tLLENBREw7QUFFRUosb0JBQU0sQ0FBQ08sZUFBZVA7QUFGeEI7QUFJRDtBQUNELGlCQUFPSSxDQUFQO0FBQ0QsU0FSVyxDQUFaO0FBU0QsT0FWTSxNQVVBLElBQUlNLFdBQVcsUUFBZixFQUF5QjtBQUM5QkQsb0JBQVksRUFBWjtBQUNEOztBQUVELDBCQUNLUCxHQURMO0FBRUUxQixnQkFBUWlDO0FBRlY7QUFJRCxLQXBGTSxFQW9GSkksaUJBQVFwQyxZQXBGSixDQUFQO0FBcUZELEdBdEZEOztBQXdGQWEsUUFBTVIsT0FBTixDQUFjZ0MsSUFBZCxDQUFtQixtQkFBVztBQUM1QmhDLFlBQVFXLE9BQVIsQ0FBZ0Isa0JBQVU7QUFDeEIsVUFBSUUsT0FBT0MsU0FBWCxFQUFzQjtBQUNwQkQsZUFBT29CLFlBQVAsR0FBc0IsVUFBQ2YsSUFBRCxFQUFPQyxLQUFQO0FBQUEsaUJBQ3BCSCxlQUFlSCxPQUFPVSxFQUF0QixFQUEwQkwsSUFBMUIsRUFBZ0NDLEtBQWhDLENBRG9CO0FBQUEsU0FBdEI7QUFFRDtBQUNGLEtBTEQ7QUFNQSxXQUFPbkIsT0FBUDtBQUNELEdBUkQ7O0FBVUFRLFFBQU0wQixvQkFBTixHQUE2QixFQUE3Qjs7QUFFQSxNQUFNQyx1QkFBdUIsU0FBdkJBLG9CQUF1QixDQUFDbkMsT0FBRCxFQUFVRixHQUFWLEVBQWtCO0FBQzdDRSxZQUFRVyxPQUFSLENBQWdCLGtCQUFVO0FBQUEsVUFDaEJHLFNBRGdCLEdBQ0ZELE1BREUsQ0FDaEJDLFNBRGdCOztBQUV4QkQsYUFBT3FCLG9CQUFQLEdBQThCLGlCQUFTO0FBQ3JDLGVBQU8sdUJBQ0w7QUFDRUUsbUJBQVN0QixZQUNMLGFBQUs7QUFDSHVCLGNBQUVDLE9BQUY7QUFDQXpCLG1CQUFPb0IsWUFBUCxDQUNFbEIsU0FERixFQUVFLENBQUNqQixJQUFJeUMsZ0JBQUwsSUFBeUJGLEVBQUVHLFFBRjdCO0FBSUQsV0FQSSxHQVFMekIsU0FUTjtBQVVFMEIsaUJBQU87QUFDTEMsb0JBQVE1QixZQUFZLFNBQVosR0FBd0JDO0FBRDNCLFdBVlQ7QUFhRTRCLGlCQUFPO0FBYlQsU0FESyxFQWdCTCwyQkFBZTdDLElBQUlVLEtBQUosQ0FBVTBCLG9CQUF6QixFQUErQ3JCLE1BQS9DLEVBQXVEZixHQUF2RCxDQWhCSyxFQWlCTDhDLEtBakJLLENBQVA7QUFtQkQsT0FwQkQ7QUFxQkQsS0F2QkQ7QUF3QkEsV0FBTzVDLE9BQVA7QUFDRCxHQTFCRDs7QUE0QkFRLFFBQU1SLE9BQU4sQ0FBY2dDLElBQWQsQ0FBbUJHLG9CQUFuQjtBQUNBM0IsUUFBTXFDLE9BQU4sQ0FBY2IsSUFBZCxDQUFtQkcsb0JBQW5COztBQUVBO0FBQ0FuQyxVQUFRVyxPQUFSLENBQWdCLGtCQUFVO0FBQUEsUUFDaEJZLEVBRGdCLEdBQ1RWLE1BRFMsQ0FDaEJVLEVBRGdCOztBQUV4QlYsV0FBT2lDLE1BQVAsR0FBZ0JwRCxPQUFPMkIsSUFBUCxDQUFZO0FBQUEsYUFBS0MsRUFBRUMsRUFBRixLQUFTQSxFQUFkO0FBQUEsS0FBWixDQUFoQjtBQUNBVixXQUFPa0MsV0FBUCxHQUFxQnJELE9BQU9zRCxTQUFQLENBQWlCO0FBQUEsYUFBSzFCLEVBQUVDLEVBQUYsS0FBU0EsRUFBZDtBQUFBLEtBQWpCLENBQXJCO0FBQ0FWLFdBQU9vQyxVQUFQLEdBQW9CcEMsT0FBT2lDLE1BQVAsR0FBZ0JqQyxPQUFPaUMsTUFBUCxDQUFjNUIsSUFBOUIsR0FBcUNILFNBQXpEO0FBQ0QsR0FMRDs7QUFPQSxNQUFNbUMsYUFBYSxvQkFDakIsWUFBTTtBQUNKLFFBQUk3QyxpQkFBaUIsQ0FBQ1gsT0FBT21DLE1BQTdCLEVBQXFDO0FBQ25DLGFBQU85QixJQUFQO0FBQ0Q7QUFDRCxRQUFJRixLQUFKLEVBQVdzRCxRQUFRQyxJQUFSLENBQWEsZUFBYjs7QUFFWCxRQUFNQyx3QkFBd0IsRUFBOUI7O0FBRUFyRCxZQUNHc0QsTUFESCxDQUNVO0FBQUEsYUFBT0MsSUFBSUMsVUFBWDtBQUFBLEtBRFYsRUFFRzdDLE9BRkgsQ0FFVyxlQUFPO0FBQ2QwQyw0QkFBc0JFLElBQUloQyxFQUExQixJQUFnQ2dDLElBQUlDLFVBQXBDO0FBQ0QsS0FKSDs7QUFNQSxRQUFNQyxXQUFXLFNBQVhBLFFBQVcsT0FBUTtBQUN2QjtBQUNBO0FBQ0E7QUFDQSxVQUFNQyxhQUFhekQsVUFDakJGLElBRGlCLEVBRWpCTCxPQUFPb0MsR0FBUCxDQUFXLGdCQUFRO0FBQ2pCO0FBQ0EsWUFBTTZCLGVBQWVOLHNCQUFzQk8sS0FBS3JDLEVBQTNCLENBQXJCOztBQUVBO0FBQ0EsZUFBTyxVQUFDc0MsQ0FBRCxFQUFJQyxDQUFKO0FBQUEsaUJBQ0wsQ0FBQ0gsZUFBZUEsWUFBZixHQUE4QnhELFFBQS9CLEVBQ0UwRCxFQUFFRSxNQUFGLENBQVNILEtBQUtyQyxFQUFkLENBREYsRUFFRXVDLEVBQUVDLE1BQUYsQ0FBU0gsS0FBS3JDLEVBQWQsQ0FGRixFQUdFcUMsS0FBSzFDLElBSFAsQ0FESztBQUFBLFNBQVA7QUFNRCxPQVhELENBRmlCO0FBY2pCO0FBQ0F4QixhQUFPb0MsR0FBUCxDQUFXO0FBQUEsZUFBSyxDQUFDUixFQUFFSixJQUFSO0FBQUEsT0FBWCxDQWZpQixDQUFuQjs7QUFrQkE7QUFDQXdDLGlCQUFXL0MsT0FBWCxDQUFtQixlQUFPO0FBQ3hCLFlBQUksQ0FBQ3FELElBQUlDLE9BQVQsRUFBa0I7QUFDaEI7QUFDRDtBQUNERCxZQUFJQyxPQUFKLEdBQWNSLFNBQVNPLElBQUlDLE9BQWIsQ0FBZDtBQUNELE9BTEQ7O0FBT0EsYUFBT1AsVUFBUDtBQUNELEtBL0JEOztBQWlDQSxXQUFPRCxTQUFTMUQsSUFBVCxDQUFQO0FBQ0QsR0FqRGdCLEVBa0RqQixDQUFDQSxJQUFELEVBQU9DLE9BQVAsRUFBZ0JOLE1BQWhCLEVBQXdCVyxhQUF4QixDQWxEaUIsQ0FBbkI7O0FBcURBLHNCQUNLUCxHQURMO0FBRUVDLFVBQU1tRDtBQUZSO0FBSUQsQ0E5Tk0iLCJmaWxlIjoidXNlU29ydEJ5LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlTWVtbyB9IGZyb20gXCJyZWFjdFwiO1xuXG5pbXBvcnQgeyBhZGRBY3Rpb25zLCBhY3Rpb25zIH0gZnJvbSBcIi4uL2FjdGlvbnNcIjtcbmltcG9ydCB7IGRlZmF1bHRTdGF0ZSB9IGZyb20gXCIuL3VzZVRhYmxlU3RhdGVcIjtcbmltcG9ydCB7XG4gIG1lcmdlUHJvcHMsXG4gIGFwcGx5UHJvcEhvb2tzLFxuICBnZXRGaXJzdERlZmluZWQsXG4gIGRlZmF1bHRPcmRlckJ5Rm4sXG4gIGRlZmF1bHRTb3J0QnlGblxufSBmcm9tIFwiLi4vdXRpbHNcIjtcblxuZGVmYXVsdFN0YXRlLnNvcnRCeSA9IFtdO1xuXG5hZGRBY3Rpb25zKHtcbiAgc29ydEJ5Q2hhbmdlOiBcIl9fc29ydEJ5Q2hhbmdlX19cIlxufSk7XG5cbmV4cG9ydCBjb25zdCB1c2VTb3J0QnkgPSBhcGkgPT4ge1xuICBjb25zdCB7XG4gICAgZGVidWcsXG4gICAgcm93cyxcbiAgICBjb2x1bW5zLFxuICAgIG9yZGVyQnlGbiA9IGRlZmF1bHRPcmRlckJ5Rm4sXG4gICAgc29ydEJ5Rm4gPSBkZWZhdWx0U29ydEJ5Rm4sXG4gICAgbWFudWFsU29ydGluZyxcbiAgICBkaXNhYmxlU29ydGluZyxcbiAgICBkZWZhdWx0U29ydERlc2MsXG4gICAgaG9va3MsXG4gICAgc3RhdGU6IFt7IHNvcnRCeSB9LCBzZXRTdGF0ZV1cbiAgfSA9IGFwaTtcblxuICBjb2x1bW5zLmZvckVhY2goY29sdW1uID0+IHtcbiAgICBjb25zdCB7IGFjY2Vzc29yLCBjYW5Tb3J0QnkgfSA9IGNvbHVtbjtcbiAgICBjb2x1bW4uY2FuU29ydEJ5ID0gYWNjZXNzb3JcbiAgICAgID8gZ2V0Rmlyc3REZWZpbmVkKFxuICAgICAgICAgIGNhblNvcnRCeSxcbiAgICAgICAgICBkaXNhYmxlU29ydGluZyA9PT0gdHJ1ZSA/IGZhbHNlIDogdW5kZWZpbmVkLFxuICAgICAgICAgIHRydWVcbiAgICAgICAgKVxuICAgICAgOiBmYWxzZTtcbiAgfSk7XG5cbiAgLy8gVXBkYXRlcyBzb3J0aW5nIGJhc2VkIG9uIGEgY29sdW1uSUQsIGRlc2MgZmxhZyBhbmQgbXVsdGkgZmxhZ1xuICBjb25zdCB0b2dnbGVTb3J0QnlJRCA9IChjb2x1bW5JRCwgZGVzYywgbXVsdGkpID0+IHtcbiAgICByZXR1cm4gc2V0U3RhdGUob2xkID0+IHtcbiAgICAgIGNvbnN0IHsgc29ydEJ5IH0gPSBvbGQ7XG5cbiAgICAgIC8vIEZpbmQgdGhlIGNvbHVtbiBmb3IgdGhpcyBjb2x1bW5JRFxuICAgICAgY29uc3QgY29sdW1uID0gY29sdW1ucy5maW5kKGQgPT4gZC5pZCA9PT0gY29sdW1uSUQpO1xuICAgICAgY29uc3QgcmVzb2x2ZWREZWZhdWx0U29ydERlc2MgPSBnZXRGaXJzdERlZmluZWQoXG4gICAgICAgIGNvbHVtbi5kZWZhdWx0U29ydERlc2MsXG4gICAgICAgIGRlZmF1bHRTb3J0RGVzY1xuICAgICAgKTtcblxuICAgICAgLy8gRmluZCBhbnkgZXhpc3Rpbmcgc29ydEJ5IGZvciB0aGlzIGNvbHVtblxuICAgICAgY29uc3QgZXhpc3RpbmdTb3J0QnkgPSBzb3J0QnkuZmluZChkID0+IGQuaWQgPT09IGNvbHVtbklEKTtcbiAgICAgIGNvbnN0IGhhc0Rlc2NEZWZpbmVkID0gdHlwZW9mIGRlc2MgIT09IFwidW5kZWZpbmVkXCIgJiYgZGVzYyAhPT0gbnVsbDtcblxuICAgICAgbGV0IG5ld1NvcnRCeSA9IFtdO1xuXG4gICAgICAvLyBXaGF0IHNob3VsZCB3ZSBkbyB3aXRoIHRoaXMgZmlsdGVyP1xuICAgICAgbGV0IGFjdGlvbjtcblxuICAgICAgaWYgKCFtdWx0aSkge1xuICAgICAgICBpZiAoc29ydEJ5Lmxlbmd0aCA8PSAxICYmIGV4aXN0aW5nU29ydEJ5KSB7XG4gICAgICAgICAgaWYgKGV4aXN0aW5nU29ydEJ5LmRlc2MpIHtcbiAgICAgICAgICAgIGFjdGlvbiA9IFwicmVtb3ZlXCI7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGFjdGlvbiA9IFwidG9nZ2xlXCI7XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGFjdGlvbiA9IFwicmVwbGFjZVwiO1xuICAgICAgICB9XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBpZiAoIWV4aXN0aW5nU29ydEJ5KSB7XG4gICAgICAgICAgYWN0aW9uID0gXCJhZGRcIjtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBpZiAoaGFzRGVzY0RlZmluZWQpIHtcbiAgICAgICAgICAgIGFjdGlvbiA9IFwic2V0XCI7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGFjdGlvbiA9IFwidG9nZ2xlXCI7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChhY3Rpb24gPT09IFwicmVwbGFjZVwiKSB7XG4gICAgICAgIG5ld1NvcnRCeSA9IFtcbiAgICAgICAgICB7XG4gICAgICAgICAgICBpZDogY29sdW1uSUQsXG4gICAgICAgICAgICBkZXNjOiBoYXNEZXNjRGVmaW5lZCA/IGRlc2MgOiByZXNvbHZlZERlZmF1bHRTb3J0RGVzY1xuICAgICAgICAgIH1cbiAgICAgICAgXTtcbiAgICAgIH0gZWxzZSBpZiAoYWN0aW9uID09PSBcImFkZFwiKSB7XG4gICAgICAgIG5ld1NvcnRCeSA9IFtcbiAgICAgICAgICAuLi5zb3J0QnksXG4gICAgICAgICAge1xuICAgICAgICAgICAgaWQ6IGNvbHVtbklELFxuICAgICAgICAgICAgZGVzYzogaGFzRGVzY0RlZmluZWQgPyBkZXNjIDogcmVzb2x2ZWREZWZhdWx0U29ydERlc2NcbiAgICAgICAgICB9XG4gICAgICAgIF07XG4gICAgICB9IGVsc2UgaWYgKGFjdGlvbiA9PT0gXCJzZXRcIikge1xuICAgICAgICBuZXdTb3J0QnkgPSBzb3J0QnkubWFwKGQgPT4ge1xuICAgICAgICAgIGlmIChkLmlkID09PSBjb2x1bW5JRCkge1xuICAgICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgICAgLi4uZCxcbiAgICAgICAgICAgICAgZGVzY1xuICAgICAgICAgICAgfTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIGQ7XG4gICAgICAgIH0pO1xuICAgICAgfSBlbHNlIGlmIChhY3Rpb24gPT09IFwidG9nZ2xlXCIpIHtcbiAgICAgICAgbmV3U29ydEJ5ID0gc29ydEJ5Lm1hcChkID0+IHtcbiAgICAgICAgICBpZiAoZC5pZCA9PT0gY29sdW1uSUQpIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgIC4uLmQsXG4gICAgICAgICAgICAgIGRlc2M6ICFleGlzdGluZ1NvcnRCeS5kZXNjXG4gICAgICAgICAgICB9O1xuICAgICAgICAgIH1cbiAgICAgICAgICByZXR1cm4gZDtcbiAgICAgICAgfSk7XG4gICAgICB9IGVsc2UgaWYgKGFjdGlvbiA9PT0gXCJyZW1vdmVcIikge1xuICAgICAgICBuZXdTb3J0QnkgPSBbXTtcbiAgICAgIH1cblxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgLi4ub2xkLFxuICAgICAgICBzb3J0Qnk6IG5ld1NvcnRCeVxuICAgICAgfTtcbiAgICB9LCBhY3Rpb25zLnNvcnRCeUNoYW5nZSk7XG4gIH07XG5cbiAgaG9va3MuY29sdW1ucy5wdXNoKGNvbHVtbnMgPT4ge1xuICAgIGNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xuICAgICAgaWYgKGNvbHVtbi5jYW5Tb3J0QnkpIHtcbiAgICAgICAgY29sdW1uLnRvZ2dsZVNvcnRCeSA9IChkZXNjLCBtdWx0aSkgPT5cbiAgICAgICAgICB0b2dnbGVTb3J0QnlJRChjb2x1bW4uaWQsIGRlc2MsIG11bHRpKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gY29sdW1ucztcbiAgfSk7XG5cbiAgaG9va3MuZ2V0U29ydEJ5VG9nZ2xlUHJvcHMgPSBbXTtcblxuICBjb25zdCBhZGRTb3J0QnlUb2dnbGVQcm9wcyA9IChjb2x1bW5zLCBhcGkpID0+IHtcbiAgICBjb2x1bW5zLmZvckVhY2goY29sdW1uID0+IHtcbiAgICAgIGNvbnN0IHsgY2FuU29ydEJ5IH0gPSBjb2x1bW47XG4gICAgICBjb2x1bW4uZ2V0U29ydEJ5VG9nZ2xlUHJvcHMgPSBwcm9wcyA9PiB7XG4gICAgICAgIHJldHVybiBtZXJnZVByb3BzKFxuICAgICAgICAgIHtcbiAgICAgICAgICAgIG9uQ2xpY2s6IGNhblNvcnRCeVxuICAgICAgICAgICAgICA/IGUgPT4ge1xuICAgICAgICAgICAgICAgICAgZS5wZXJzaXN0KCk7XG4gICAgICAgICAgICAgICAgICBjb2x1bW4udG9nZ2xlU29ydEJ5KFxuICAgICAgICAgICAgICAgICAgICB1bmRlZmluZWQsXG4gICAgICAgICAgICAgICAgICAgICFhcGkuZGlzYWJsZU11bHRpU29ydCAmJiBlLnNoaWZ0S2V5XG4gICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgOiB1bmRlZmluZWQsXG4gICAgICAgICAgICBzdHlsZToge1xuICAgICAgICAgICAgICBjdXJzb3I6IGNhblNvcnRCeSA/IFwicG9pbnRlclwiIDogdW5kZWZpbmVkXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgdGl0bGU6IFwiVG9nZ2xlIFNvcnRCeVwiXG4gICAgICAgICAgfSxcbiAgICAgICAgICBhcHBseVByb3BIb29rcyhhcGkuaG9va3MuZ2V0U29ydEJ5VG9nZ2xlUHJvcHMsIGNvbHVtbiwgYXBpKSxcbiAgICAgICAgICBwcm9wc1xuICAgICAgICApO1xuICAgICAgfTtcbiAgICB9KTtcbiAgICByZXR1cm4gY29sdW1ucztcbiAgfTtcblxuICBob29rcy5jb2x1bW5zLnB1c2goYWRkU29ydEJ5VG9nZ2xlUHJvcHMpO1xuICBob29rcy5oZWFkZXJzLnB1c2goYWRkU29ydEJ5VG9nZ2xlUHJvcHMpO1xuXG4gIC8vIE11dGF0ZSBjb2x1bW5zIHRvIHJlZmxlY3Qgc29ydGluZyBzdGF0ZVxuICBjb2x1bW5zLmZvckVhY2goY29sdW1uID0+IHtcbiAgICBjb25zdCB7IGlkIH0gPSBjb2x1bW47XG4gICAgY29sdW1uLnNvcnRlZCA9IHNvcnRCeS5maW5kKGQgPT4gZC5pZCA9PT0gaWQpO1xuICAgIGNvbHVtbi5zb3J0ZWRJbmRleCA9IHNvcnRCeS5maW5kSW5kZXgoZCA9PiBkLmlkID09PSBpZCk7XG4gICAgY29sdW1uLnNvcnRlZERlc2MgPSBjb2x1bW4uc29ydGVkID8gY29sdW1uLnNvcnRlZC5kZXNjIDogdW5kZWZpbmVkO1xuICB9KTtcblxuICBjb25zdCBzb3J0ZWRSb3dzID0gdXNlTWVtbyhcbiAgICAoKSA9PiB7XG4gICAgICBpZiAobWFudWFsU29ydGluZyB8fCAhc29ydEJ5Lmxlbmd0aCkge1xuICAgICAgICByZXR1cm4gcm93cztcbiAgICAgIH1cbiAgICAgIGlmIChkZWJ1ZykgY29uc29sZS5pbmZvKFwiZ2V0U29ydGVkUm93c1wiKTtcblxuICAgICAgY29uc3Qgc29ydE1ldGhvZHNCeUNvbHVtbklEID0ge307XG5cbiAgICAgIGNvbHVtbnNcbiAgICAgICAgLmZpbHRlcihjb2wgPT4gY29sLnNvcnRNZXRob2QpXG4gICAgICAgIC5mb3JFYWNoKGNvbCA9PiB7XG4gICAgICAgICAgc29ydE1ldGhvZHNCeUNvbHVtbklEW2NvbC5pZF0gPSBjb2wuc29ydE1ldGhvZDtcbiAgICAgICAgfSk7XG5cbiAgICAgIGNvbnN0IHNvcnREYXRhID0gcm93cyA9PiB7XG4gICAgICAgIC8vIFVzZSB0aGUgb3JkZXJCeUZuIHRvIGNvbXBvc2UgbXVsdGlwbGUgc29ydEJ5J3MgdG9nZXRoZXIuXG4gICAgICAgIC8vIFRoaXMgd2lsbCBhbHNvIHBlcmZvcm0gYSBzdGFibGUgc29ydGluZyB1c2luZyB0aGUgcm93IGluZGV4XG4gICAgICAgIC8vIGlmIG5lZWRlZC5cbiAgICAgICAgY29uc3Qgc29ydGVkRGF0YSA9IG9yZGVyQnlGbihcbiAgICAgICAgICByb3dzLFxuICAgICAgICAgIHNvcnRCeS5tYXAoc29ydCA9PiB7XG4gICAgICAgICAgICAvLyBTdXBwb3J0IGN1c3RvbSBzb3J0aW5nIG1ldGhvZHMgZm9yIGVhY2ggY29sdW1uXG4gICAgICAgICAgICBjb25zdCBjb2x1bW5Tb3J0QnkgPSBzb3J0TWV0aG9kc0J5Q29sdW1uSURbc29ydC5pZF07XG5cbiAgICAgICAgICAgIC8vIFJldHVybiB0aGUgY29ycmVjdCBzb3J0Rm5cbiAgICAgICAgICAgIHJldHVybiAoYSwgYikgPT5cbiAgICAgICAgICAgICAgKGNvbHVtblNvcnRCeSA/IGNvbHVtblNvcnRCeSA6IHNvcnRCeUZuKShcbiAgICAgICAgICAgICAgICBhLnZhbHVlc1tzb3J0LmlkXSxcbiAgICAgICAgICAgICAgICBiLnZhbHVlc1tzb3J0LmlkXSxcbiAgICAgICAgICAgICAgICBzb3J0LmRlc2NcbiAgICAgICAgICAgICAgKTtcbiAgICAgICAgICB9KSxcbiAgICAgICAgICAvLyBNYXAgdGhlIGRpcmVjdGlvbnNcbiAgICAgICAgICBzb3J0QnkubWFwKGQgPT4gIWQuZGVzYylcbiAgICAgICAgKTtcblxuICAgICAgICAvLyBUT0RPOiB0aGlzIHNob3VsZCBiZSBvcHRpbWl6ZWQuIE5vdCBnb29kIHRvIGxvb3AgYWdhaW5cbiAgICAgICAgc29ydGVkRGF0YS5mb3JFYWNoKHJvdyA9PiB7XG4gICAgICAgICAgaWYgKCFyb3cuc3ViUm93cykge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICAgIH1cbiAgICAgICAgICByb3cuc3ViUm93cyA9IHNvcnREYXRhKHJvdy5zdWJSb3dzKTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgcmV0dXJuIHNvcnRlZERhdGE7XG4gICAgICB9O1xuXG4gICAgICByZXR1cm4gc29ydERhdGEocm93cyk7XG4gICAgfSxcbiAgICBbcm93cywgY29sdW1ucywgc29ydEJ5LCBtYW51YWxTb3J0aW5nXVxuICApO1xuXG4gIHJldHVybiB7XG4gICAgLi4uYXBpLFxuICAgIHJvd3M6IHNvcnRlZFJvd3NcbiAgfTtcbn07XG4iXX0=