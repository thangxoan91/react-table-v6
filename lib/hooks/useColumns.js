"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useColumns = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _react = require("react");

var _utils = require("../utils");

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var useColumns = exports.useColumns = function useColumns(props) {
  var debug = props.debug,
      userColumns = props.columns,
      _props$state = _slicedToArray(props.state, 1),
      groupBy = _props$state[0].groupBy;

  var _useMemo = (0, _react.useMemo)(function () {
    if (debug) console.info("getColumns");

    // Decorate All the columns
    var columnTree = decorateColumnTree(userColumns);

    // Get the flat list of all columns
    var columns = flattenBy(columnTree, "columns");

    columns = [].concat(_toConsumableArray(groupBy.map(function (g) {
      return columns.find(function (col) {
        return col.id === g;
      });
    })), _toConsumableArray(columns.filter(function (col) {
      return !groupBy.includes(col.id);
    })));

    // Get headerGroups
    var headerGroups = makeHeaderGroups(columns, findMaxDepth(columnTree));
    var headers = flattenBy(headerGroups, "headers");

    return {
      columns: columns,
      headerGroups: headerGroups,
      headers: headers
    };
  }, [groupBy, userColumns]),
      columns = _useMemo.columns,
      headerGroups = _useMemo.headerGroups,
      headers = _useMemo.headers;

  return _extends({}, props, {
    columns: columns,
    headerGroups: headerGroups,
    headers: headers
  });

  // Find the depth of the columns
  function findMaxDepth(columns) {
    var depth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

    return columns.reduce(function (prev, curr) {
      if (curr.columns) {
        return Math.max(prev, findMaxDepth(curr.columns, depth + 1));
      }
      return depth;
    }, 0);
  }

  function decorateColumn(column, parent) {
    // First check for string accessor
    var _column = column,
        id = _column.id,
        accessor = _column.accessor,
        Header = _column.Header;


    if (typeof accessor === "string") {
      id = id || accessor;
      var accessorString = accessor;
      accessor = function accessor(row) {
        return (0, _utils.getBy)(row, accessorString);
      };
    }

    if (!id && typeof Header === "string") {
      id = Header;
    }

    if (!id) {
      // Accessor, but no column id? This is bad.
      console.error(column);
      throw new Error("A column id is required!");
    }

    column = _extends({
      Header: "",
      Cell: function Cell(cell) {
        return cell.value;
      },
      show: true
    }, column, {
      id: id,
      accessor: accessor,
      parent: parent
    });

    return column;
  }

  // Build the visible columns, headers and flat column list
  function decorateColumnTree(columns, parent) {
    var depth = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

    return columns.map(function (column) {
      column = decorateColumn(column, parent);
      if (column.columns) {
        column.columns = decorateColumnTree(column.columns, column, depth + 1);
      }
      return column;
    });
  }

  function flattenBy(columns, childKey) {
    var flatColumns = [];

    var recurse = function recurse(columns) {
      columns.forEach(function (d) {
        if (!d[childKey]) {
          flatColumns.push(d);
        } else {
          recurse(d[childKey]);
        }
      });
    };

    recurse(columns);

    return flatColumns;
  }

  // Build the header groups from the bottom up
  function makeHeaderGroups(columns, maxDepth) {
    var headerGroups = [];

    var removeChildColumns = function removeChildColumns(column) {
      delete column.columns;
      if (column.parent) {
        removeChildColumns(column.parent);
      }
    };
    columns.forEach(removeChildColumns);

    var buildGroup = function buildGroup(columns) {
      var depth = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

      var headerGroup = {
        headers: []
      };

      var parentColumns = [];

      var hasParents = columns.some(function (col) {
        return col.parent;
      });

      columns.forEach(function (column) {
        var isFirst = !parentColumns.length;
        var latestParentColumn = [].concat(parentColumns).reverse()[0];

        // If the column has a parent, add it if necessary
        if (column.parent) {
          if (isFirst || latestParentColumn.originalID !== column.parent.id) {
            parentColumns.push(_extends({}, column.parent, {
              originalID: column.parent.id,
              id: [column.parent.id, parentColumns.length].join("_")
            }));
          }
        } else if (hasParents) {
          // If other columns have parents, add a place holder if necessary
          var placeholderColumn = decorateColumn({
            originalID: [column.id, "placeholder", maxDepth - depth].join("_"),
            id: [column.id, "placeholder", maxDepth - depth, parentColumns.length].join("_")
          });
          if (isFirst || latestParentColumn.originalID !== placeholderColumn.originalID) {
            parentColumns.push(placeholderColumn);
          }
        }

        // Establish the new columns[] relationship on the parent
        if (column.parent || hasParents) {
          latestParentColumn = [].concat(parentColumns).reverse()[0];
          latestParentColumn.columns = latestParentColumn.columns || [];
          if (!latestParentColumn.columns.includes(column)) {
            latestParentColumn.columns.push(column);
          }
        }

        headerGroup.headers.push(column);
      });

      headerGroups.push(headerGroup);

      if (parentColumns.length) {
        buildGroup(parentColumns);
      }
    };

    buildGroup(columns);

    return headerGroups.reverse();
  }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VDb2x1bW5zLmpzIl0sIm5hbWVzIjpbInVzZUNvbHVtbnMiLCJkZWJ1ZyIsInByb3BzIiwidXNlckNvbHVtbnMiLCJjb2x1bW5zIiwic3RhdGUiLCJncm91cEJ5IiwiY29uc29sZSIsImluZm8iLCJjb2x1bW5UcmVlIiwiZGVjb3JhdGVDb2x1bW5UcmVlIiwiZmxhdHRlbkJ5IiwibWFwIiwiZmluZCIsImNvbCIsImlkIiwiZyIsImZpbHRlciIsImluY2x1ZGVzIiwiaGVhZGVyR3JvdXBzIiwibWFrZUhlYWRlckdyb3VwcyIsImZpbmRNYXhEZXB0aCIsImhlYWRlcnMiLCJkZXB0aCIsInJlZHVjZSIsInByZXYiLCJjdXJyIiwiTWF0aCIsIm1heCIsImRlY29yYXRlQ29sdW1uIiwiY29sdW1uIiwicGFyZW50IiwiYWNjZXNzb3IiLCJIZWFkZXIiLCJhY2Nlc3NvclN0cmluZyIsInJvdyIsImVycm9yIiwiRXJyb3IiLCJDZWxsIiwiY2VsbCIsInZhbHVlIiwic2hvdyIsImNoaWxkS2V5IiwiZmxhdENvbHVtbnMiLCJyZWN1cnNlIiwiZm9yRWFjaCIsImQiLCJwdXNoIiwibWF4RGVwdGgiLCJyZW1vdmVDaGlsZENvbHVtbnMiLCJidWlsZEdyb3VwIiwiaGVhZGVyR3JvdXAiLCJwYXJlbnRDb2x1bW5zIiwiaGFzUGFyZW50cyIsInNvbWUiLCJpc0ZpcnN0IiwibGVuZ3RoIiwibGF0ZXN0UGFyZW50Q29sdW1uIiwicmV2ZXJzZSIsIm9yaWdpbmFsSUQiLCJqb2luIiwicGxhY2Vob2xkZXJDb2x1bW4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7O0FBRUE7Ozs7QUFFTyxJQUFNQSxrQ0FBYSxTQUFiQSxVQUFhLFFBQVM7QUFBQSxNQUUvQkMsS0FGK0IsR0FLN0JDLEtBTDZCLENBRS9CRCxLQUYrQjtBQUFBLE1BR3RCRSxXQUhzQixHQUs3QkQsS0FMNkIsQ0FHL0JFLE9BSCtCO0FBQUEsb0NBSzdCRixLQUw2QixDQUkvQkcsS0FKK0I7QUFBQSxNQUlyQkMsT0FKcUIsbUJBSXJCQSxPQUpxQjs7QUFBQSxpQkFPVSxvQkFDekMsWUFBTTtBQUNKLFFBQUlMLEtBQUosRUFBV00sUUFBUUMsSUFBUixDQUFhLFlBQWI7O0FBRVg7QUFDQSxRQUFJQyxhQUFhQyxtQkFBbUJQLFdBQW5CLENBQWpCOztBQUVBO0FBQ0EsUUFBSUMsVUFBVU8sVUFBVUYsVUFBVixFQUFzQixTQUF0QixDQUFkOztBQUVBTCwyQ0FDS0UsUUFBUU0sR0FBUixDQUFZO0FBQUEsYUFBS1IsUUFBUVMsSUFBUixDQUFhO0FBQUEsZUFBT0MsSUFBSUMsRUFBSixLQUFXQyxDQUFsQjtBQUFBLE9BQWIsQ0FBTDtBQUFBLEtBQVosQ0FETCxzQkFFS1osUUFBUWEsTUFBUixDQUFlO0FBQUEsYUFBTyxDQUFDWCxRQUFRWSxRQUFSLENBQWlCSixJQUFJQyxFQUFyQixDQUFSO0FBQUEsS0FBZixDQUZMOztBQUtBO0FBQ0EsUUFBTUksZUFBZUMsaUJBQWlCaEIsT0FBakIsRUFBMEJpQixhQUFhWixVQUFiLENBQTFCLENBQXJCO0FBQ0EsUUFBTWEsVUFBVVgsVUFBVVEsWUFBVixFQUF3QixTQUF4QixDQUFoQjs7QUFFQSxXQUFPO0FBQ0xmLHNCQURLO0FBRUxlLGdDQUZLO0FBR0xHO0FBSEssS0FBUDtBQUtELEdBeEJ3QyxFQXlCekMsQ0FBQ2hCLE9BQUQsRUFBVUgsV0FBVixDQXpCeUMsQ0FQVjtBQUFBLE1BT3pCQyxPQVB5QixZQU96QkEsT0FQeUI7QUFBQSxNQU9oQmUsWUFQZ0IsWUFPaEJBLFlBUGdCO0FBQUEsTUFPRkcsT0FQRSxZQU9GQSxPQVBFOztBQW1DakMsc0JBQ0twQixLQURMO0FBRUVFLG9CQUZGO0FBR0VlLDhCQUhGO0FBSUVHO0FBSkY7O0FBT0E7QUFDQSxXQUFTRCxZQUFULENBQXNCakIsT0FBdEIsRUFBMEM7QUFBQSxRQUFYbUIsS0FBVyx1RUFBSCxDQUFHOztBQUN4QyxXQUFPbkIsUUFBUW9CLE1BQVIsQ0FBZSxVQUFDQyxJQUFELEVBQU9DLElBQVAsRUFBZ0I7QUFDcEMsVUFBSUEsS0FBS3RCLE9BQVQsRUFBa0I7QUFDaEIsZUFBT3VCLEtBQUtDLEdBQUwsQ0FBU0gsSUFBVCxFQUFlSixhQUFhSyxLQUFLdEIsT0FBbEIsRUFBMkJtQixRQUFRLENBQW5DLENBQWYsQ0FBUDtBQUNEO0FBQ0QsYUFBT0EsS0FBUDtBQUNELEtBTE0sRUFLSixDQUxJLENBQVA7QUFNRDs7QUFFRCxXQUFTTSxjQUFULENBQXdCQyxNQUF4QixFQUFnQ0MsTUFBaEMsRUFBd0M7QUFDdEM7QUFEc0Msa0JBRVBELE1BRk87QUFBQSxRQUVoQ2YsRUFGZ0MsV0FFaENBLEVBRmdDO0FBQUEsUUFFNUJpQixRQUY0QixXQUU1QkEsUUFGNEI7QUFBQSxRQUVsQkMsTUFGa0IsV0FFbEJBLE1BRmtCOzs7QUFJdEMsUUFBSSxPQUFPRCxRQUFQLEtBQW9CLFFBQXhCLEVBQWtDO0FBQ2hDakIsV0FBS0EsTUFBTWlCLFFBQVg7QUFDQSxVQUFNRSxpQkFBaUJGLFFBQXZCO0FBQ0FBLGlCQUFXO0FBQUEsZUFBTyxrQkFBTUcsR0FBTixFQUFXRCxjQUFYLENBQVA7QUFBQSxPQUFYO0FBQ0Q7O0FBRUQsUUFBSSxDQUFDbkIsRUFBRCxJQUFPLE9BQU9rQixNQUFQLEtBQWtCLFFBQTdCLEVBQXVDO0FBQ3JDbEIsV0FBS2tCLE1BQUw7QUFDRDs7QUFFRCxRQUFJLENBQUNsQixFQUFMLEVBQVM7QUFDUDtBQUNBUixjQUFRNkIsS0FBUixDQUFjTixNQUFkO0FBQ0EsWUFBTSxJQUFJTyxLQUFKLENBQVUsMEJBQVYsQ0FBTjtBQUNEOztBQUVEUDtBQUNFRyxjQUFRLEVBRFY7QUFFRUssWUFBTTtBQUFBLGVBQVFDLEtBQUtDLEtBQWI7QUFBQSxPQUZSO0FBR0VDLFlBQU07QUFIUixPQUlLWCxNQUpMO0FBS0VmLFlBTEY7QUFNRWlCLHdCQU5GO0FBT0VEO0FBUEY7O0FBVUEsV0FBT0QsTUFBUDtBQUNEOztBQUVEO0FBQ0EsV0FBU3BCLGtCQUFULENBQTRCTixPQUE1QixFQUFxQzJCLE1BQXJDLEVBQXdEO0FBQUEsUUFBWFIsS0FBVyx1RUFBSCxDQUFHOztBQUN0RCxXQUFPbkIsUUFBUVEsR0FBUixDQUFZLGtCQUFVO0FBQzNCa0IsZUFBU0QsZUFBZUMsTUFBZixFQUF1QkMsTUFBdkIsQ0FBVDtBQUNBLFVBQUlELE9BQU8xQixPQUFYLEVBQW9CO0FBQ2xCMEIsZUFBTzFCLE9BQVAsR0FBaUJNLG1CQUFtQm9CLE9BQU8xQixPQUExQixFQUFtQzBCLE1BQW5DLEVBQTJDUCxRQUFRLENBQW5ELENBQWpCO0FBQ0Q7QUFDRCxhQUFPTyxNQUFQO0FBQ0QsS0FOTSxDQUFQO0FBT0Q7O0FBRUQsV0FBU25CLFNBQVQsQ0FBbUJQLE9BQW5CLEVBQTRCc0MsUUFBNUIsRUFBc0M7QUFDcEMsUUFBTUMsY0FBYyxFQUFwQjs7QUFFQSxRQUFNQyxVQUFVLFNBQVZBLE9BQVUsVUFBVztBQUN6QnhDLGNBQVF5QyxPQUFSLENBQWdCLGFBQUs7QUFDbkIsWUFBSSxDQUFDQyxFQUFFSixRQUFGLENBQUwsRUFBa0I7QUFDaEJDLHNCQUFZSSxJQUFaLENBQWlCRCxDQUFqQjtBQUNELFNBRkQsTUFFTztBQUNMRixrQkFBUUUsRUFBRUosUUFBRixDQUFSO0FBQ0Q7QUFDRixPQU5EO0FBT0QsS0FSRDs7QUFVQUUsWUFBUXhDLE9BQVI7O0FBRUEsV0FBT3VDLFdBQVA7QUFDRDs7QUFFRDtBQUNBLFdBQVN2QixnQkFBVCxDQUEwQmhCLE9BQTFCLEVBQW1DNEMsUUFBbkMsRUFBNkM7QUFDM0MsUUFBTTdCLGVBQWUsRUFBckI7O0FBRUEsUUFBTThCLHFCQUFxQixTQUFyQkEsa0JBQXFCLFNBQVU7QUFDbkMsYUFBT25CLE9BQU8xQixPQUFkO0FBQ0EsVUFBSTBCLE9BQU9DLE1BQVgsRUFBbUI7QUFDakJrQiwyQkFBbUJuQixPQUFPQyxNQUExQjtBQUNEO0FBQ0YsS0FMRDtBQU1BM0IsWUFBUXlDLE9BQVIsQ0FBZ0JJLGtCQUFoQjs7QUFFQSxRQUFNQyxhQUFhLFNBQWJBLFVBQWEsQ0FBQzlDLE9BQUQsRUFBd0I7QUFBQSxVQUFkbUIsS0FBYyx1RUFBTixDQUFNOztBQUN6QyxVQUFNNEIsY0FBYztBQUNsQjdCLGlCQUFTO0FBRFMsT0FBcEI7O0FBSUEsVUFBTThCLGdCQUFnQixFQUF0Qjs7QUFFQSxVQUFNQyxhQUFhakQsUUFBUWtELElBQVIsQ0FBYTtBQUFBLGVBQU94QyxJQUFJaUIsTUFBWDtBQUFBLE9BQWIsQ0FBbkI7O0FBRUEzQixjQUFReUMsT0FBUixDQUFnQixrQkFBVTtBQUN4QixZQUFNVSxVQUFVLENBQUNILGNBQWNJLE1BQS9CO0FBQ0EsWUFBSUMscUJBQXFCLFVBQUlMLGFBQUosRUFBbUJNLE9BQW5CLEdBQTZCLENBQTdCLENBQXpCOztBQUVBO0FBQ0EsWUFBSTVCLE9BQU9DLE1BQVgsRUFBbUI7QUFDakIsY0FBSXdCLFdBQVdFLG1CQUFtQkUsVUFBbkIsS0FBa0M3QixPQUFPQyxNQUFQLENBQWNoQixFQUEvRCxFQUFtRTtBQUNqRXFDLDBCQUFjTCxJQUFkLGNBQ0tqQixPQUFPQyxNQURaO0FBRUU0QiwwQkFBWTdCLE9BQU9DLE1BQVAsQ0FBY2hCLEVBRjVCO0FBR0VBLGtCQUFJLENBQUNlLE9BQU9DLE1BQVAsQ0FBY2hCLEVBQWYsRUFBbUJxQyxjQUFjSSxNQUFqQyxFQUF5Q0ksSUFBekMsQ0FBOEMsR0FBOUM7QUFITjtBQUtEO0FBQ0YsU0FSRCxNQVFPLElBQUlQLFVBQUosRUFBZ0I7QUFDckI7QUFDQSxjQUFNUSxvQkFBb0JoQyxlQUFlO0FBQ3ZDOEIsd0JBQVksQ0FBQzdCLE9BQU9mLEVBQVIsRUFBWSxhQUFaLEVBQTJCaUMsV0FBV3pCLEtBQXRDLEVBQTZDcUMsSUFBN0MsQ0FBa0QsR0FBbEQsQ0FEMkI7QUFFdkM3QyxnQkFBSSxDQUNGZSxPQUFPZixFQURMLEVBRUYsYUFGRSxFQUdGaUMsV0FBV3pCLEtBSFQsRUFJRjZCLGNBQWNJLE1BSlosRUFLRkksSUFMRSxDQUtHLEdBTEg7QUFGbUMsV0FBZixDQUExQjtBQVNBLGNBQ0VMLFdBQ0FFLG1CQUFtQkUsVUFBbkIsS0FBa0NFLGtCQUFrQkYsVUFGdEQsRUFHRTtBQUNBUCwwQkFBY0wsSUFBZCxDQUFtQmMsaUJBQW5CO0FBQ0Q7QUFDRjs7QUFFRDtBQUNBLFlBQUkvQixPQUFPQyxNQUFQLElBQWlCc0IsVUFBckIsRUFBaUM7QUFDL0JJLCtCQUFxQixVQUFJTCxhQUFKLEVBQW1CTSxPQUFuQixHQUE2QixDQUE3QixDQUFyQjtBQUNBRCw2QkFBbUJyRCxPQUFuQixHQUE2QnFELG1CQUFtQnJELE9BQW5CLElBQThCLEVBQTNEO0FBQ0EsY0FBSSxDQUFDcUQsbUJBQW1CckQsT0FBbkIsQ0FBMkJjLFFBQTNCLENBQW9DWSxNQUFwQyxDQUFMLEVBQWtEO0FBQ2hEMkIsK0JBQW1CckQsT0FBbkIsQ0FBMkIyQyxJQUEzQixDQUFnQ2pCLE1BQWhDO0FBQ0Q7QUFDRjs7QUFFRHFCLG9CQUFZN0IsT0FBWixDQUFvQnlCLElBQXBCLENBQXlCakIsTUFBekI7QUFDRCxPQTFDRDs7QUE0Q0FYLG1CQUFhNEIsSUFBYixDQUFrQkksV0FBbEI7O0FBRUEsVUFBSUMsY0FBY0ksTUFBbEIsRUFBMEI7QUFDeEJOLG1CQUFXRSxhQUFYO0FBQ0Q7QUFDRixLQTFERDs7QUE0REFGLGVBQVc5QyxPQUFYOztBQUVBLFdBQU9lLGFBQWF1QyxPQUFiLEVBQVA7QUFDRDtBQUNGLENBOUxNIiwiZmlsZSI6InVzZUNvbHVtbnMuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VNZW1vIH0gZnJvbSBcInJlYWN0XCI7XG5cbmltcG9ydCB7IGdldEZpcnN0RGVmaW5lZCwgZ2V0QnkgfSBmcm9tIFwiLi4vdXRpbHNcIjtcblxuZXhwb3J0IGNvbnN0IHVzZUNvbHVtbnMgPSBwcm9wcyA9PiB7XG4gIGNvbnN0IHtcbiAgICBkZWJ1ZyxcbiAgICBjb2x1bW5zOiB1c2VyQ29sdW1ucyxcbiAgICBzdGF0ZTogW3sgZ3JvdXBCeSB9XVxuICB9ID0gcHJvcHM7XG5cbiAgY29uc3QgeyBjb2x1bW5zLCBoZWFkZXJHcm91cHMsIGhlYWRlcnMgfSA9IHVzZU1lbW8oXG4gICAgKCkgPT4ge1xuICAgICAgaWYgKGRlYnVnKSBjb25zb2xlLmluZm8oXCJnZXRDb2x1bW5zXCIpO1xuXG4gICAgICAvLyBEZWNvcmF0ZSBBbGwgdGhlIGNvbHVtbnNcbiAgICAgIGxldCBjb2x1bW5UcmVlID0gZGVjb3JhdGVDb2x1bW5UcmVlKHVzZXJDb2x1bW5zKTtcblxuICAgICAgLy8gR2V0IHRoZSBmbGF0IGxpc3Qgb2YgYWxsIGNvbHVtbnNcbiAgICAgIGxldCBjb2x1bW5zID0gZmxhdHRlbkJ5KGNvbHVtblRyZWUsIFwiY29sdW1uc1wiKTtcblxuICAgICAgY29sdW1ucyA9IFtcbiAgICAgICAgLi4uZ3JvdXBCeS5tYXAoZyA9PiBjb2x1bW5zLmZpbmQoY29sID0+IGNvbC5pZCA9PT0gZykpLFxuICAgICAgICAuLi5jb2x1bW5zLmZpbHRlcihjb2wgPT4gIWdyb3VwQnkuaW5jbHVkZXMoY29sLmlkKSlcbiAgICAgIF07XG5cbiAgICAgIC8vIEdldCBoZWFkZXJHcm91cHNcbiAgICAgIGNvbnN0IGhlYWRlckdyb3VwcyA9IG1ha2VIZWFkZXJHcm91cHMoY29sdW1ucywgZmluZE1heERlcHRoKGNvbHVtblRyZWUpKTtcbiAgICAgIGNvbnN0IGhlYWRlcnMgPSBmbGF0dGVuQnkoaGVhZGVyR3JvdXBzLCBcImhlYWRlcnNcIik7XG5cbiAgICAgIHJldHVybiB7XG4gICAgICAgIGNvbHVtbnMsXG4gICAgICAgIGhlYWRlckdyb3VwcyxcbiAgICAgICAgaGVhZGVyc1xuICAgICAgfTtcbiAgICB9LFxuICAgIFtncm91cEJ5LCB1c2VyQ29sdW1uc11cbiAgKTtcblxuICByZXR1cm4ge1xuICAgIC4uLnByb3BzLFxuICAgIGNvbHVtbnMsXG4gICAgaGVhZGVyR3JvdXBzLFxuICAgIGhlYWRlcnNcbiAgfTtcblxuICAvLyBGaW5kIHRoZSBkZXB0aCBvZiB0aGUgY29sdW1uc1xuICBmdW5jdGlvbiBmaW5kTWF4RGVwdGgoY29sdW1ucywgZGVwdGggPSAwKSB7XG4gICAgcmV0dXJuIGNvbHVtbnMucmVkdWNlKChwcmV2LCBjdXJyKSA9PiB7XG4gICAgICBpZiAoY3Vyci5jb2x1bW5zKSB7XG4gICAgICAgIHJldHVybiBNYXRoLm1heChwcmV2LCBmaW5kTWF4RGVwdGgoY3Vyci5jb2x1bW5zLCBkZXB0aCArIDEpKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBkZXB0aDtcbiAgICB9LCAwKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGRlY29yYXRlQ29sdW1uKGNvbHVtbiwgcGFyZW50KSB7XG4gICAgLy8gRmlyc3QgY2hlY2sgZm9yIHN0cmluZyBhY2Nlc3NvclxuICAgIGxldCB7IGlkLCBhY2Nlc3NvciwgSGVhZGVyIH0gPSBjb2x1bW47XG5cbiAgICBpZiAodHlwZW9mIGFjY2Vzc29yID09PSBcInN0cmluZ1wiKSB7XG4gICAgICBpZCA9IGlkIHx8IGFjY2Vzc29yO1xuICAgICAgY29uc3QgYWNjZXNzb3JTdHJpbmcgPSBhY2Nlc3NvcjtcbiAgICAgIGFjY2Vzc29yID0gcm93ID0+IGdldEJ5KHJvdywgYWNjZXNzb3JTdHJpbmcpO1xuICAgIH1cblxuICAgIGlmICghaWQgJiYgdHlwZW9mIEhlYWRlciA9PT0gXCJzdHJpbmdcIikge1xuICAgICAgaWQgPSBIZWFkZXI7XG4gICAgfVxuXG4gICAgaWYgKCFpZCkge1xuICAgICAgLy8gQWNjZXNzb3IsIGJ1dCBubyBjb2x1bW4gaWQ/IFRoaXMgaXMgYmFkLlxuICAgICAgY29uc29sZS5lcnJvcihjb2x1bW4pO1xuICAgICAgdGhyb3cgbmV3IEVycm9yKFwiQSBjb2x1bW4gaWQgaXMgcmVxdWlyZWQhXCIpO1xuICAgIH1cblxuICAgIGNvbHVtbiA9IHtcbiAgICAgIEhlYWRlcjogXCJcIixcbiAgICAgIENlbGw6IGNlbGwgPT4gY2VsbC52YWx1ZSxcbiAgICAgIHNob3c6IHRydWUsXG4gICAgICAuLi5jb2x1bW4sXG4gICAgICBpZCxcbiAgICAgIGFjY2Vzc29yLFxuICAgICAgcGFyZW50XG4gICAgfTtcblxuICAgIHJldHVybiBjb2x1bW47XG4gIH1cblxuICAvLyBCdWlsZCB0aGUgdmlzaWJsZSBjb2x1bW5zLCBoZWFkZXJzIGFuZCBmbGF0IGNvbHVtbiBsaXN0XG4gIGZ1bmN0aW9uIGRlY29yYXRlQ29sdW1uVHJlZShjb2x1bW5zLCBwYXJlbnQsIGRlcHRoID0gMCkge1xuICAgIHJldHVybiBjb2x1bW5zLm1hcChjb2x1bW4gPT4ge1xuICAgICAgY29sdW1uID0gZGVjb3JhdGVDb2x1bW4oY29sdW1uLCBwYXJlbnQpO1xuICAgICAgaWYgKGNvbHVtbi5jb2x1bW5zKSB7XG4gICAgICAgIGNvbHVtbi5jb2x1bW5zID0gZGVjb3JhdGVDb2x1bW5UcmVlKGNvbHVtbi5jb2x1bW5zLCBjb2x1bW4sIGRlcHRoICsgMSk7XG4gICAgICB9XG4gICAgICByZXR1cm4gY29sdW1uO1xuICAgIH0pO1xuICB9XG5cbiAgZnVuY3Rpb24gZmxhdHRlbkJ5KGNvbHVtbnMsIGNoaWxkS2V5KSB7XG4gICAgY29uc3QgZmxhdENvbHVtbnMgPSBbXTtcblxuICAgIGNvbnN0IHJlY3Vyc2UgPSBjb2x1bW5zID0+IHtcbiAgICAgIGNvbHVtbnMuZm9yRWFjaChkID0+IHtcbiAgICAgICAgaWYgKCFkW2NoaWxkS2V5XSkge1xuICAgICAgICAgIGZsYXRDb2x1bW5zLnB1c2goZCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgcmVjdXJzZShkW2NoaWxkS2V5XSk7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH07XG5cbiAgICByZWN1cnNlKGNvbHVtbnMpO1xuXG4gICAgcmV0dXJuIGZsYXRDb2x1bW5zO1xuICB9XG5cbiAgLy8gQnVpbGQgdGhlIGhlYWRlciBncm91cHMgZnJvbSB0aGUgYm90dG9tIHVwXG4gIGZ1bmN0aW9uIG1ha2VIZWFkZXJHcm91cHMoY29sdW1ucywgbWF4RGVwdGgpIHtcbiAgICBjb25zdCBoZWFkZXJHcm91cHMgPSBbXTtcblxuICAgIGNvbnN0IHJlbW92ZUNoaWxkQ29sdW1ucyA9IGNvbHVtbiA9PiB7XG4gICAgICBkZWxldGUgY29sdW1uLmNvbHVtbnM7XG4gICAgICBpZiAoY29sdW1uLnBhcmVudCkge1xuICAgICAgICByZW1vdmVDaGlsZENvbHVtbnMoY29sdW1uLnBhcmVudCk7XG4gICAgICB9XG4gICAgfTtcbiAgICBjb2x1bW5zLmZvckVhY2gocmVtb3ZlQ2hpbGRDb2x1bW5zKTtcblxuICAgIGNvbnN0IGJ1aWxkR3JvdXAgPSAoY29sdW1ucywgZGVwdGggPSAwKSA9PiB7XG4gICAgICBjb25zdCBoZWFkZXJHcm91cCA9IHtcbiAgICAgICAgaGVhZGVyczogW11cbiAgICAgIH07XG5cbiAgICAgIGNvbnN0IHBhcmVudENvbHVtbnMgPSBbXTtcblxuICAgICAgY29uc3QgaGFzUGFyZW50cyA9IGNvbHVtbnMuc29tZShjb2wgPT4gY29sLnBhcmVudCk7XG5cbiAgICAgIGNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xuICAgICAgICBjb25zdCBpc0ZpcnN0ID0gIXBhcmVudENvbHVtbnMubGVuZ3RoO1xuICAgICAgICBsZXQgbGF0ZXN0UGFyZW50Q29sdW1uID0gWy4uLnBhcmVudENvbHVtbnNdLnJldmVyc2UoKVswXTtcblxuICAgICAgICAvLyBJZiB0aGUgY29sdW1uIGhhcyBhIHBhcmVudCwgYWRkIGl0IGlmIG5lY2Vzc2FyeVxuICAgICAgICBpZiAoY29sdW1uLnBhcmVudCkge1xuICAgICAgICAgIGlmIChpc0ZpcnN0IHx8IGxhdGVzdFBhcmVudENvbHVtbi5vcmlnaW5hbElEICE9PSBjb2x1bW4ucGFyZW50LmlkKSB7XG4gICAgICAgICAgICBwYXJlbnRDb2x1bW5zLnB1c2goe1xuICAgICAgICAgICAgICAuLi5jb2x1bW4ucGFyZW50LFxuICAgICAgICAgICAgICBvcmlnaW5hbElEOiBjb2x1bW4ucGFyZW50LmlkLFxuICAgICAgICAgICAgICBpZDogW2NvbHVtbi5wYXJlbnQuaWQsIHBhcmVudENvbHVtbnMubGVuZ3RoXS5qb2luKFwiX1wiKVxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2UgaWYgKGhhc1BhcmVudHMpIHtcbiAgICAgICAgICAvLyBJZiBvdGhlciBjb2x1bW5zIGhhdmUgcGFyZW50cywgYWRkIGEgcGxhY2UgaG9sZGVyIGlmIG5lY2Vzc2FyeVxuICAgICAgICAgIGNvbnN0IHBsYWNlaG9sZGVyQ29sdW1uID0gZGVjb3JhdGVDb2x1bW4oe1xuICAgICAgICAgICAgb3JpZ2luYWxJRDogW2NvbHVtbi5pZCwgXCJwbGFjZWhvbGRlclwiLCBtYXhEZXB0aCAtIGRlcHRoXS5qb2luKFwiX1wiKSxcbiAgICAgICAgICAgIGlkOiBbXG4gICAgICAgICAgICAgIGNvbHVtbi5pZCxcbiAgICAgICAgICAgICAgXCJwbGFjZWhvbGRlclwiLFxuICAgICAgICAgICAgICBtYXhEZXB0aCAtIGRlcHRoLFxuICAgICAgICAgICAgICBwYXJlbnRDb2x1bW5zLmxlbmd0aFxuICAgICAgICAgICAgXS5qb2luKFwiX1wiKVxuICAgICAgICAgIH0pO1xuICAgICAgICAgIGlmIChcbiAgICAgICAgICAgIGlzRmlyc3QgfHxcbiAgICAgICAgICAgIGxhdGVzdFBhcmVudENvbHVtbi5vcmlnaW5hbElEICE9PSBwbGFjZWhvbGRlckNvbHVtbi5vcmlnaW5hbElEXG4gICAgICAgICAgKSB7XG4gICAgICAgICAgICBwYXJlbnRDb2x1bW5zLnB1c2gocGxhY2Vob2xkZXJDb2x1bW4pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIC8vIEVzdGFibGlzaCB0aGUgbmV3IGNvbHVtbnNbXSByZWxhdGlvbnNoaXAgb24gdGhlIHBhcmVudFxuICAgICAgICBpZiAoY29sdW1uLnBhcmVudCB8fCBoYXNQYXJlbnRzKSB7XG4gICAgICAgICAgbGF0ZXN0UGFyZW50Q29sdW1uID0gWy4uLnBhcmVudENvbHVtbnNdLnJldmVyc2UoKVswXTtcbiAgICAgICAgICBsYXRlc3RQYXJlbnRDb2x1bW4uY29sdW1ucyA9IGxhdGVzdFBhcmVudENvbHVtbi5jb2x1bW5zIHx8IFtdO1xuICAgICAgICAgIGlmICghbGF0ZXN0UGFyZW50Q29sdW1uLmNvbHVtbnMuaW5jbHVkZXMoY29sdW1uKSkge1xuICAgICAgICAgICAgbGF0ZXN0UGFyZW50Q29sdW1uLmNvbHVtbnMucHVzaChjb2x1bW4pO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuXG4gICAgICAgIGhlYWRlckdyb3VwLmhlYWRlcnMucHVzaChjb2x1bW4pO1xuICAgICAgfSk7XG5cbiAgICAgIGhlYWRlckdyb3Vwcy5wdXNoKGhlYWRlckdyb3VwKTtcblxuICAgICAgaWYgKHBhcmVudENvbHVtbnMubGVuZ3RoKSB7XG4gICAgICAgIGJ1aWxkR3JvdXAocGFyZW50Q29sdW1ucyk7XG4gICAgICB9XG4gICAgfTtcblxuICAgIGJ1aWxkR3JvdXAoY29sdW1ucyk7XG5cbiAgICByZXR1cm4gaGVhZGVyR3JvdXBzLnJldmVyc2UoKTtcbiAgfVxufTtcbiJdfQ==