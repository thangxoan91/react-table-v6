"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useFlexLayout = exports.actions = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require("react");

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _utils = require("../utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var propTypes = {
  defaultFlex: _propTypes2.default.number
};

var actions = exports.actions = {};

var useFlexLayout = exports.useFlexLayout = function useFlexLayout(props) {
  // Validate props
  _propTypes2.default.checkPropTypes(propTypes, props, "property", "useFlexLayout");

  var _props$defaultFlex = props.defaultFlex,
      defaultFlex = _props$defaultFlex === undefined ? 1 : _props$defaultFlex,
      _props$hooks = props.hooks,
      columnsHooks = _props$hooks.columns,
      getRowProps = _props$hooks.getRowProps,
      getHeaderRowProps = _props$hooks.getHeaderRowProps,
      getHeaderProps = _props$hooks.getHeaderProps,
      getCellProps = _props$hooks.getCellProps;


  columnsHooks.push(function (columns, api) {
    var visibleColumns = columns.filter(function (column) {
      column.visible = typeof column.show === "function" ? column.show(api) : !!column.show;
      return column.visible;
    });

    var columnMeasurements = {};

    var sumWidth = 0;
    visibleColumns.forEach(function (column) {
      var _getSizesForColumn = getSizesForColumn(column, defaultFlex, undefined, undefined, api),
          width = _getSizesForColumn.width,
          minWidth = _getSizesForColumn.minWidth;

      if (width) {
        sumWidth += width;
      } else if (minWidth) {
        sumWidth += minWidth;
      } else {
        sumWidth += defaultFlex;
      }
    });

    var rowStyles = {
      style: {
        display: "flex",
        minWidth: sumWidth + "px"
      }
    };

    api.rowStyles = rowStyles;

    getRowProps.push(function () {
      return rowStyles;
    });
    getHeaderRowProps.push(function () {
      return rowStyles;
    });

    getHeaderProps.push(function (column) {
      return {
        style: _extends({
          boxSizing: "border-box"
        }, getStylesForColumn(column, columnMeasurements, defaultFlex, api))
        // [refKey]: el => {
        //   renderedCellInfoRef.current[key] = {
        //     column,
        //     el
        //   };
        // },
      };
    });

    getCellProps.push(function (cell) {
      return {
        style: _extends({
          display: "block",
          boxSizing: "border-box"
        }, getStylesForColumn(cell.column, columnMeasurements, defaultFlex, undefined, api))
        // [refKey]: el => {
        //   renderedCellInfoRef.current[columnPathStr] = {
        //     column,
        //     el
        //   };
        // }
      };
    });

    return columns;
  });

  return props;
};

// Utils

function getStylesForColumn(column, columnMeasurements, defaultFlex, api) {
  var _getSizesForColumn2 = getSizesForColumn(column, columnMeasurements, defaultFlex, api),
      flex = _getSizesForColumn2.flex,
      width = _getSizesForColumn2.width,
      maxWidth = _getSizesForColumn2.maxWidth;

  return {
    flex: flex + " 0 auto",
    width: width + "px",
    maxWidth: maxWidth + "px"
  };
}

function getSizesForColumn(_ref, columnMeasurements, defaultFlex, api) {
  var columns = _ref.columns,
      id = _ref.id,
      width = _ref.width,
      minWidth = _ref.minWidth,
      maxWidth = _ref.maxWidth;

  if (columns) {
    columns = columns.map(function (column) {
      return getSizesForColumn(column, columnMeasurements, defaultFlex, api);
    }).filter(Boolean);

    if (!columns.length) {
      return false;
    }

    var flex = (0, _utils.sum)(columns.map(function (col) {
      return col.flex;
    }));
    var _width = (0, _utils.sum)(columns.map(function (col) {
      return col.width;
    }));
    var _maxWidth = (0, _utils.sum)(columns.map(function (col) {
      return col.maxWidth;
    }));

    return {
      flex: flex,
      width: _width,
      maxWidth: _maxWidth
    };
  }

  return {
    flex: width ? 0 : defaultFlex,
    width: width === "auto" ? columnMeasurements[id] || defaultFlex : (0, _utils.getFirstDefined)(width, minWidth, defaultFlex),
    maxWidth: maxWidth
  };
}

// const resetRefs = () => {
//   if (debug) console.info("resetRefs");
//   renderedCellInfoRef.current = {};
// };

// const calculateAutoWidths = () => {
//   RAF(() => {
//     const newColumnMeasurements = {};
//     Object.values(renderedCellInfoRef.current).forEach(({ column, el }) => {
//       if (!el) {
//         return;
//       }

//       let measurement = 0;

//       const measureChildren = children => {
//         if (children) {
//           [].slice.call(children).forEach(child => {
//             measurement = Math.max(
//               measurement,
//               Math.ceil(child.offsetWidth) || 0
//             );
//             measureChildren(child.children);
//           });
//         }
//         return measurement;
//       };

//       const parentDims = getElementDimensions(el);
//       measureChildren(el.children);

//       newColumnMeasurements[column.id] = Math.max(
//         newColumnMeasurements[column.id] || 0,
//         measurement + parentDims.paddingLeft + parentDims.paddingRight
//       );
//     });

//     const oldKeys = Object.keys(columnMeasurements);
//     const newKeys = Object.keys(newColumnMeasurements);

//     const needsUpdate =
//       oldKeys.length !== newKeys.length ||
//       oldKeys.some(key => {
//         return columnMeasurements[key] !== newColumnMeasurements[key];
//       });

//     if (needsUpdate) {
//       setState(old => {
//         return {
//           ...old,
//           columnMeasurements: newColumnMeasurements
//         };
//       }, actions.updateAutoWidth);
//     }
//   });
// };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VGbGV4TGF5b3V0LmpzIl0sIm5hbWVzIjpbInByb3BUeXBlcyIsImRlZmF1bHRGbGV4IiwiUHJvcFR5cGVzIiwibnVtYmVyIiwiYWN0aW9ucyIsInVzZUZsZXhMYXlvdXQiLCJjaGVja1Byb3BUeXBlcyIsInByb3BzIiwiaG9va3MiLCJjb2x1bW5zSG9va3MiLCJjb2x1bW5zIiwiZ2V0Um93UHJvcHMiLCJnZXRIZWFkZXJSb3dQcm9wcyIsImdldEhlYWRlclByb3BzIiwiZ2V0Q2VsbFByb3BzIiwicHVzaCIsImFwaSIsInZpc2libGVDb2x1bW5zIiwiZmlsdGVyIiwiY29sdW1uIiwidmlzaWJsZSIsInNob3ciLCJjb2x1bW5NZWFzdXJlbWVudHMiLCJzdW1XaWR0aCIsImZvckVhY2giLCJnZXRTaXplc0ZvckNvbHVtbiIsInVuZGVmaW5lZCIsIndpZHRoIiwibWluV2lkdGgiLCJyb3dTdHlsZXMiLCJzdHlsZSIsImRpc3BsYXkiLCJib3hTaXppbmciLCJnZXRTdHlsZXNGb3JDb2x1bW4iLCJjZWxsIiwiZmxleCIsIm1heFdpZHRoIiwiaWQiLCJtYXAiLCJCb29sZWFuIiwibGVuZ3RoIiwiY29sIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7QUFBQTs7QUFDQTs7OztBQUVBOzs7O0FBRUEsSUFBTUEsWUFBWTtBQUNoQkMsZUFBYUMsb0JBQVVDO0FBRFAsQ0FBbEI7O0FBSU8sSUFBTUMsNEJBQVUsRUFBaEI7O0FBRUEsSUFBTUMsd0NBQWdCLFNBQWhCQSxhQUFnQixRQUFTO0FBQ3BDO0FBQ0FILHNCQUFVSSxjQUFWLENBQXlCTixTQUF6QixFQUFvQ08sS0FBcEMsRUFBMkMsVUFBM0MsRUFBdUQsZUFBdkQ7O0FBRm9DLDJCQWFoQ0EsS0FiZ0MsQ0FLbENOLFdBTGtDO0FBQUEsTUFLbENBLFdBTGtDLHNDQUtwQixDQUxvQjtBQUFBLHFCQWFoQ00sS0FiZ0MsQ0FNbENDLEtBTmtDO0FBQUEsTUFPdkJDLFlBUHVCLGdCQU9oQ0MsT0FQZ0M7QUFBQSxNQVFoQ0MsV0FSZ0MsZ0JBUWhDQSxXQVJnQztBQUFBLE1BU2hDQyxpQkFUZ0MsZ0JBU2hDQSxpQkFUZ0M7QUFBQSxNQVVoQ0MsY0FWZ0MsZ0JBVWhDQSxjQVZnQztBQUFBLE1BV2hDQyxZQVhnQyxnQkFXaENBLFlBWGdDOzs7QUFlcENMLGVBQWFNLElBQWIsQ0FBa0IsVUFBQ0wsT0FBRCxFQUFVTSxHQUFWLEVBQWtCO0FBQ2xDLFFBQU1DLGlCQUFpQlAsUUFBUVEsTUFBUixDQUFlLGtCQUFVO0FBQzlDQyxhQUFPQyxPQUFQLEdBQ0UsT0FBT0QsT0FBT0UsSUFBZCxLQUF1QixVQUF2QixHQUFvQ0YsT0FBT0UsSUFBUCxDQUFZTCxHQUFaLENBQXBDLEdBQXVELENBQUMsQ0FBQ0csT0FBT0UsSUFEbEU7QUFFQSxhQUFPRixPQUFPQyxPQUFkO0FBQ0QsS0FKc0IsQ0FBdkI7O0FBTUEsUUFBTUUscUJBQXFCLEVBQTNCOztBQUVBLFFBQUlDLFdBQVcsQ0FBZjtBQUNBTixtQkFBZU8sT0FBZixDQUF1QixrQkFBVTtBQUFBLCtCQUNIQyxrQkFDMUJOLE1BRDBCLEVBRTFCbEIsV0FGMEIsRUFHMUJ5QixTQUgwQixFQUkxQkEsU0FKMEIsRUFLMUJWLEdBTDBCLENBREc7QUFBQSxVQUN2QlcsS0FEdUIsc0JBQ3ZCQSxLQUR1QjtBQUFBLFVBQ2hCQyxRQURnQixzQkFDaEJBLFFBRGdCOztBQVEvQixVQUFJRCxLQUFKLEVBQVc7QUFDVEosb0JBQVlJLEtBQVo7QUFDRCxPQUZELE1BRU8sSUFBSUMsUUFBSixFQUFjO0FBQ25CTCxvQkFBWUssUUFBWjtBQUNELE9BRk0sTUFFQTtBQUNMTCxvQkFBWXRCLFdBQVo7QUFDRDtBQUNGLEtBZkQ7O0FBaUJBLFFBQU00QixZQUFZO0FBQ2hCQyxhQUFPO0FBQ0xDLGlCQUFTLE1BREo7QUFFTEgsa0JBQWFMLFFBQWI7QUFGSztBQURTLEtBQWxCOztBQU9BUCxRQUFJYSxTQUFKLEdBQWdCQSxTQUFoQjs7QUFFQWxCLGdCQUFZSSxJQUFaLENBQWlCO0FBQUEsYUFBTWMsU0FBTjtBQUFBLEtBQWpCO0FBQ0FqQixzQkFBa0JHLElBQWxCLENBQXVCO0FBQUEsYUFBTWMsU0FBTjtBQUFBLEtBQXZCOztBQUVBaEIsbUJBQWVFLElBQWYsQ0FBb0I7QUFBQSxhQUFXO0FBQzdCZTtBQUNFRSxxQkFBVztBQURiLFdBRUtDLG1CQUFtQmQsTUFBbkIsRUFBMkJHLGtCQUEzQixFQUErQ3JCLFdBQS9DLEVBQTREZSxHQUE1RCxDQUZMO0FBSUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBVjZCLE9BQVg7QUFBQSxLQUFwQjs7QUFhQUYsaUJBQWFDLElBQWIsQ0FBa0IsZ0JBQVE7QUFDeEIsYUFBTztBQUNMZTtBQUNFQyxtQkFBUyxPQURYO0FBRUVDLHFCQUFXO0FBRmIsV0FHS0MsbUJBQ0RDLEtBQUtmLE1BREosRUFFREcsa0JBRkMsRUFHRHJCLFdBSEMsRUFJRHlCLFNBSkMsRUFLRFYsR0FMQyxDQUhMO0FBV0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBakJLLE9BQVA7QUFtQkQsS0FwQkQ7O0FBc0JBLFdBQU9OLE9BQVA7QUFDRCxHQTNFRDs7QUE2RUEsU0FBT0gsS0FBUDtBQUNELENBN0ZNOztBQStGUDs7QUFFQSxTQUFTMEIsa0JBQVQsQ0FBNEJkLE1BQTVCLEVBQW9DRyxrQkFBcEMsRUFBd0RyQixXQUF4RCxFQUFxRWUsR0FBckUsRUFBMEU7QUFBQSw0QkFDdENTLGtCQUNoQ04sTUFEZ0MsRUFFaENHLGtCQUZnQyxFQUdoQ3JCLFdBSGdDLEVBSWhDZSxHQUpnQyxDQURzQztBQUFBLE1BQ2hFbUIsSUFEZ0UsdUJBQ2hFQSxJQURnRTtBQUFBLE1BQzFEUixLQUQwRCx1QkFDMURBLEtBRDBEO0FBQUEsTUFDbkRTLFFBRG1ELHVCQUNuREEsUUFEbUQ7O0FBUXhFLFNBQU87QUFDTEQsVUFBU0EsSUFBVCxZQURLO0FBRUxSLFdBQVVBLEtBQVYsT0FGSztBQUdMUyxjQUFhQSxRQUFiO0FBSEssR0FBUDtBQUtEOztBQUVELFNBQVNYLGlCQUFULE9BRUVILGtCQUZGLEVBR0VyQixXQUhGLEVBSUVlLEdBSkYsRUFLRTtBQUFBLE1BSkVOLE9BSUYsUUFKRUEsT0FJRjtBQUFBLE1BSlcyQixFQUlYLFFBSldBLEVBSVg7QUFBQSxNQUplVixLQUlmLFFBSmVBLEtBSWY7QUFBQSxNQUpzQkMsUUFJdEIsUUFKc0JBLFFBSXRCO0FBQUEsTUFKZ0NRLFFBSWhDLFFBSmdDQSxRQUloQzs7QUFDQSxNQUFJMUIsT0FBSixFQUFhO0FBQ1hBLGNBQVVBLFFBQ1A0QixHQURPLENBQ0g7QUFBQSxhQUNIYixrQkFBa0JOLE1BQWxCLEVBQTBCRyxrQkFBMUIsRUFBOENyQixXQUE5QyxFQUEyRGUsR0FBM0QsQ0FERztBQUFBLEtBREcsRUFJUEUsTUFKTyxDQUlBcUIsT0FKQSxDQUFWOztBQU1BLFFBQUksQ0FBQzdCLFFBQVE4QixNQUFiLEVBQXFCO0FBQ25CLGFBQU8sS0FBUDtBQUNEOztBQUVELFFBQU1MLE9BQU8sZ0JBQUl6QixRQUFRNEIsR0FBUixDQUFZO0FBQUEsYUFBT0csSUFBSU4sSUFBWDtBQUFBLEtBQVosQ0FBSixDQUFiO0FBQ0EsUUFBTVIsU0FBUSxnQkFBSWpCLFFBQVE0QixHQUFSLENBQVk7QUFBQSxhQUFPRyxJQUFJZCxLQUFYO0FBQUEsS0FBWixDQUFKLENBQWQ7QUFDQSxRQUFNUyxZQUFXLGdCQUFJMUIsUUFBUTRCLEdBQVIsQ0FBWTtBQUFBLGFBQU9HLElBQUlMLFFBQVg7QUFBQSxLQUFaLENBQUosQ0FBakI7O0FBRUEsV0FBTztBQUNMRCxnQkFESztBQUVMUixtQkFGSztBQUdMUztBQUhLLEtBQVA7QUFLRDs7QUFFRCxTQUFPO0FBQ0xELFVBQU1SLFFBQVEsQ0FBUixHQUFZMUIsV0FEYjtBQUVMMEIsV0FDRUEsVUFBVSxNQUFWLEdBQ0lMLG1CQUFtQmUsRUFBbkIsS0FBMEJwQyxXQUQ5QixHQUVJLDRCQUFnQjBCLEtBQWhCLEVBQXVCQyxRQUF2QixFQUFpQzNCLFdBQWpDLENBTEQ7QUFNTG1DO0FBTkssR0FBUDtBQVFEOztBQUVEO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6InVzZUZsZXhMYXlvdXQuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VNZW1vLCB1c2VTdGF0ZSB9IGZyb20gXCJyZWFjdFwiO1xuaW1wb3J0IFByb3BUeXBlcyBmcm9tIFwicHJvcC10eXBlc1wiO1xuXG5pbXBvcnQgeyBnZXRGaXJzdERlZmluZWQsIHN1bSB9IGZyb20gXCIuLi91dGlsc1wiO1xuXG5jb25zdCBwcm9wVHlwZXMgPSB7XG4gIGRlZmF1bHRGbGV4OiBQcm9wVHlwZXMubnVtYmVyXG59O1xuXG5leHBvcnQgY29uc3QgYWN0aW9ucyA9IHt9O1xuXG5leHBvcnQgY29uc3QgdXNlRmxleExheW91dCA9IHByb3BzID0+IHtcbiAgLy8gVmFsaWRhdGUgcHJvcHNcbiAgUHJvcFR5cGVzLmNoZWNrUHJvcFR5cGVzKHByb3BUeXBlcywgcHJvcHMsIFwicHJvcGVydHlcIiwgXCJ1c2VGbGV4TGF5b3V0XCIpO1xuXG4gIGNvbnN0IHtcbiAgICBkZWZhdWx0RmxleCA9IDEsXG4gICAgaG9va3M6IHtcbiAgICAgIGNvbHVtbnM6IGNvbHVtbnNIb29rcyxcbiAgICAgIGdldFJvd1Byb3BzLFxuICAgICAgZ2V0SGVhZGVyUm93UHJvcHMsXG4gICAgICBnZXRIZWFkZXJQcm9wcyxcbiAgICAgIGdldENlbGxQcm9wc1xuICAgIH1cbiAgfSA9IHByb3BzO1xuXG4gIGNvbHVtbnNIb29rcy5wdXNoKChjb2x1bW5zLCBhcGkpID0+IHtcbiAgICBjb25zdCB2aXNpYmxlQ29sdW1ucyA9IGNvbHVtbnMuZmlsdGVyKGNvbHVtbiA9PiB7XG4gICAgICBjb2x1bW4udmlzaWJsZSA9XG4gICAgICAgIHR5cGVvZiBjb2x1bW4uc2hvdyA9PT0gXCJmdW5jdGlvblwiID8gY29sdW1uLnNob3coYXBpKSA6ICEhY29sdW1uLnNob3c7XG4gICAgICByZXR1cm4gY29sdW1uLnZpc2libGU7XG4gICAgfSk7XG5cbiAgICBjb25zdCBjb2x1bW5NZWFzdXJlbWVudHMgPSB7fTtcblxuICAgIGxldCBzdW1XaWR0aCA9IDA7XG4gICAgdmlzaWJsZUNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xuICAgICAgY29uc3QgeyB3aWR0aCwgbWluV2lkdGggfSA9IGdldFNpemVzRm9yQ29sdW1uKFxuICAgICAgICBjb2x1bW4sXG4gICAgICAgIGRlZmF1bHRGbGV4LFxuICAgICAgICB1bmRlZmluZWQsXG4gICAgICAgIHVuZGVmaW5lZCxcbiAgICAgICAgYXBpXG4gICAgICApO1xuICAgICAgaWYgKHdpZHRoKSB7XG4gICAgICAgIHN1bVdpZHRoICs9IHdpZHRoO1xuICAgICAgfSBlbHNlIGlmIChtaW5XaWR0aCkge1xuICAgICAgICBzdW1XaWR0aCArPSBtaW5XaWR0aDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHN1bVdpZHRoICs9IGRlZmF1bHRGbGV4O1xuICAgICAgfVxuICAgIH0pO1xuXG4gICAgY29uc3Qgcm93U3R5bGVzID0ge1xuICAgICAgc3R5bGU6IHtcbiAgICAgICAgZGlzcGxheTogXCJmbGV4XCIsXG4gICAgICAgIG1pbldpZHRoOiBgJHtzdW1XaWR0aH1weGBcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgYXBpLnJvd1N0eWxlcyA9IHJvd1N0eWxlcztcblxuICAgIGdldFJvd1Byb3BzLnB1c2goKCkgPT4gcm93U3R5bGVzKTtcbiAgICBnZXRIZWFkZXJSb3dQcm9wcy5wdXNoKCgpID0+IHJvd1N0eWxlcyk7XG5cbiAgICBnZXRIZWFkZXJQcm9wcy5wdXNoKGNvbHVtbiA9PiAoe1xuICAgICAgc3R5bGU6IHtcbiAgICAgICAgYm94U2l6aW5nOiBcImJvcmRlci1ib3hcIixcbiAgICAgICAgLi4uZ2V0U3R5bGVzRm9yQ29sdW1uKGNvbHVtbiwgY29sdW1uTWVhc3VyZW1lbnRzLCBkZWZhdWx0RmxleCwgYXBpKVxuICAgICAgfVxuICAgICAgLy8gW3JlZktleV06IGVsID0+IHtcbiAgICAgIC8vICAgcmVuZGVyZWRDZWxsSW5mb1JlZi5jdXJyZW50W2tleV0gPSB7XG4gICAgICAvLyAgICAgY29sdW1uLFxuICAgICAgLy8gICAgIGVsXG4gICAgICAvLyAgIH07XG4gICAgICAvLyB9LFxuICAgIH0pKTtcblxuICAgIGdldENlbGxQcm9wcy5wdXNoKGNlbGwgPT4ge1xuICAgICAgcmV0dXJuIHtcbiAgICAgICAgc3R5bGU6IHtcbiAgICAgICAgICBkaXNwbGF5OiBcImJsb2NrXCIsXG4gICAgICAgICAgYm94U2l6aW5nOiBcImJvcmRlci1ib3hcIixcbiAgICAgICAgICAuLi5nZXRTdHlsZXNGb3JDb2x1bW4oXG4gICAgICAgICAgICBjZWxsLmNvbHVtbixcbiAgICAgICAgICAgIGNvbHVtbk1lYXN1cmVtZW50cyxcbiAgICAgICAgICAgIGRlZmF1bHRGbGV4LFxuICAgICAgICAgICAgdW5kZWZpbmVkLFxuICAgICAgICAgICAgYXBpXG4gICAgICAgICAgKVxuICAgICAgICB9XG4gICAgICAgIC8vIFtyZWZLZXldOiBlbCA9PiB7XG4gICAgICAgIC8vICAgcmVuZGVyZWRDZWxsSW5mb1JlZi5jdXJyZW50W2NvbHVtblBhdGhTdHJdID0ge1xuICAgICAgICAvLyAgICAgY29sdW1uLFxuICAgICAgICAvLyAgICAgZWxcbiAgICAgICAgLy8gICB9O1xuICAgICAgICAvLyB9XG4gICAgICB9O1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIGNvbHVtbnM7XG4gIH0pO1xuXG4gIHJldHVybiBwcm9wcztcbn07XG5cbi8vIFV0aWxzXG5cbmZ1bmN0aW9uIGdldFN0eWxlc0ZvckNvbHVtbihjb2x1bW4sIGNvbHVtbk1lYXN1cmVtZW50cywgZGVmYXVsdEZsZXgsIGFwaSkge1xuICBjb25zdCB7IGZsZXgsIHdpZHRoLCBtYXhXaWR0aCB9ID0gZ2V0U2l6ZXNGb3JDb2x1bW4oXG4gICAgY29sdW1uLFxuICAgIGNvbHVtbk1lYXN1cmVtZW50cyxcbiAgICBkZWZhdWx0RmxleCxcbiAgICBhcGlcbiAgKTtcblxuICByZXR1cm4ge1xuICAgIGZsZXg6IGAke2ZsZXh9IDAgYXV0b2AsXG4gICAgd2lkdGg6IGAke3dpZHRofXB4YCxcbiAgICBtYXhXaWR0aDogYCR7bWF4V2lkdGh9cHhgXG4gIH07XG59XG5cbmZ1bmN0aW9uIGdldFNpemVzRm9yQ29sdW1uKFxuICB7IGNvbHVtbnMsIGlkLCB3aWR0aCwgbWluV2lkdGgsIG1heFdpZHRoIH0sXG4gIGNvbHVtbk1lYXN1cmVtZW50cyxcbiAgZGVmYXVsdEZsZXgsXG4gIGFwaVxuKSB7XG4gIGlmIChjb2x1bW5zKSB7XG4gICAgY29sdW1ucyA9IGNvbHVtbnNcbiAgICAgIC5tYXAoY29sdW1uID0+XG4gICAgICAgIGdldFNpemVzRm9yQ29sdW1uKGNvbHVtbiwgY29sdW1uTWVhc3VyZW1lbnRzLCBkZWZhdWx0RmxleCwgYXBpKVxuICAgICAgKVxuICAgICAgLmZpbHRlcihCb29sZWFuKTtcblxuICAgIGlmICghY29sdW1ucy5sZW5ndGgpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBjb25zdCBmbGV4ID0gc3VtKGNvbHVtbnMubWFwKGNvbCA9PiBjb2wuZmxleCkpO1xuICAgIGNvbnN0IHdpZHRoID0gc3VtKGNvbHVtbnMubWFwKGNvbCA9PiBjb2wud2lkdGgpKTtcbiAgICBjb25zdCBtYXhXaWR0aCA9IHN1bShjb2x1bW5zLm1hcChjb2wgPT4gY29sLm1heFdpZHRoKSk7XG5cbiAgICByZXR1cm4ge1xuICAgICAgZmxleCxcbiAgICAgIHdpZHRoLFxuICAgICAgbWF4V2lkdGhcbiAgICB9O1xuICB9XG5cbiAgcmV0dXJuIHtcbiAgICBmbGV4OiB3aWR0aCA/IDAgOiBkZWZhdWx0RmxleCxcbiAgICB3aWR0aDpcbiAgICAgIHdpZHRoID09PSBcImF1dG9cIlxuICAgICAgICA/IGNvbHVtbk1lYXN1cmVtZW50c1tpZF0gfHwgZGVmYXVsdEZsZXhcbiAgICAgICAgOiBnZXRGaXJzdERlZmluZWQod2lkdGgsIG1pbldpZHRoLCBkZWZhdWx0RmxleCksXG4gICAgbWF4V2lkdGhcbiAgfTtcbn1cblxuLy8gY29uc3QgcmVzZXRSZWZzID0gKCkgPT4ge1xuLy8gICBpZiAoZGVidWcpIGNvbnNvbGUuaW5mbyhcInJlc2V0UmVmc1wiKTtcbi8vICAgcmVuZGVyZWRDZWxsSW5mb1JlZi5jdXJyZW50ID0ge307XG4vLyB9O1xuXG4vLyBjb25zdCBjYWxjdWxhdGVBdXRvV2lkdGhzID0gKCkgPT4ge1xuLy8gICBSQUYoKCkgPT4ge1xuLy8gICAgIGNvbnN0IG5ld0NvbHVtbk1lYXN1cmVtZW50cyA9IHt9O1xuLy8gICAgIE9iamVjdC52YWx1ZXMocmVuZGVyZWRDZWxsSW5mb1JlZi5jdXJyZW50KS5mb3JFYWNoKCh7IGNvbHVtbiwgZWwgfSkgPT4ge1xuLy8gICAgICAgaWYgKCFlbCkge1xuLy8gICAgICAgICByZXR1cm47XG4vLyAgICAgICB9XG5cbi8vICAgICAgIGxldCBtZWFzdXJlbWVudCA9IDA7XG5cbi8vICAgICAgIGNvbnN0IG1lYXN1cmVDaGlsZHJlbiA9IGNoaWxkcmVuID0+IHtcbi8vICAgICAgICAgaWYgKGNoaWxkcmVuKSB7XG4vLyAgICAgICAgICAgW10uc2xpY2UuY2FsbChjaGlsZHJlbikuZm9yRWFjaChjaGlsZCA9PiB7XG4vLyAgICAgICAgICAgICBtZWFzdXJlbWVudCA9IE1hdGgubWF4KFxuLy8gICAgICAgICAgICAgICBtZWFzdXJlbWVudCxcbi8vICAgICAgICAgICAgICAgTWF0aC5jZWlsKGNoaWxkLm9mZnNldFdpZHRoKSB8fCAwXG4vLyAgICAgICAgICAgICApO1xuLy8gICAgICAgICAgICAgbWVhc3VyZUNoaWxkcmVuKGNoaWxkLmNoaWxkcmVuKTtcbi8vICAgICAgICAgICB9KTtcbi8vICAgICAgICAgfVxuLy8gICAgICAgICByZXR1cm4gbWVhc3VyZW1lbnQ7XG4vLyAgICAgICB9O1xuXG4vLyAgICAgICBjb25zdCBwYXJlbnREaW1zID0gZ2V0RWxlbWVudERpbWVuc2lvbnMoZWwpO1xuLy8gICAgICAgbWVhc3VyZUNoaWxkcmVuKGVsLmNoaWxkcmVuKTtcblxuLy8gICAgICAgbmV3Q29sdW1uTWVhc3VyZW1lbnRzW2NvbHVtbi5pZF0gPSBNYXRoLm1heChcbi8vICAgICAgICAgbmV3Q29sdW1uTWVhc3VyZW1lbnRzW2NvbHVtbi5pZF0gfHwgMCxcbi8vICAgICAgICAgbWVhc3VyZW1lbnQgKyBwYXJlbnREaW1zLnBhZGRpbmdMZWZ0ICsgcGFyZW50RGltcy5wYWRkaW5nUmlnaHRcbi8vICAgICAgICk7XG4vLyAgICAgfSk7XG5cbi8vICAgICBjb25zdCBvbGRLZXlzID0gT2JqZWN0LmtleXMoY29sdW1uTWVhc3VyZW1lbnRzKTtcbi8vICAgICBjb25zdCBuZXdLZXlzID0gT2JqZWN0LmtleXMobmV3Q29sdW1uTWVhc3VyZW1lbnRzKTtcblxuLy8gICAgIGNvbnN0IG5lZWRzVXBkYXRlID1cbi8vICAgICAgIG9sZEtleXMubGVuZ3RoICE9PSBuZXdLZXlzLmxlbmd0aCB8fFxuLy8gICAgICAgb2xkS2V5cy5zb21lKGtleSA9PiB7XG4vLyAgICAgICAgIHJldHVybiBjb2x1bW5NZWFzdXJlbWVudHNba2V5XSAhPT0gbmV3Q29sdW1uTWVhc3VyZW1lbnRzW2tleV07XG4vLyAgICAgICB9KTtcblxuLy8gICAgIGlmIChuZWVkc1VwZGF0ZSkge1xuLy8gICAgICAgc2V0U3RhdGUob2xkID0+IHtcbi8vICAgICAgICAgcmV0dXJuIHtcbi8vICAgICAgICAgICAuLi5vbGQsXG4vLyAgICAgICAgICAgY29sdW1uTWVhc3VyZW1lbnRzOiBuZXdDb2x1bW5NZWFzdXJlbWVudHNcbi8vICAgICAgICAgfTtcbi8vICAgICAgIH0sIGFjdGlvbnMudXBkYXRlQXV0b1dpZHRoKTtcbi8vICAgICB9XG4vLyAgIH0pO1xuLy8gfTtcbiJdfQ==