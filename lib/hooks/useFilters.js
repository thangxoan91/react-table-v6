"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useFilters = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _react = require("react");

var _utils = require("../utils");

var _actions = require("../actions");

var _useTableState = require("./useTableState");

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

_useTableState.defaultState.filters = {};
(0, _actions.addActions)({
  setFilter: "__setFilter__",
  setAllFilters: "__setAllFilters__"
});

var useFilters = function useFilters(props) {
  var debug = props.debug,
      rows = props.rows,
      columns = props.columns,
      _props$filterFn = props.filterFn,
      filterFn = _props$filterFn === undefined ? _utils.defaultFilterFn : _props$filterFn,
      manualFilters = props.manualFilters,
      disableFilters = props.disableFilters,
      hooks = props.hooks,
      _props$state = _slicedToArray(props.state, 2),
      filters = _props$state[0].filters,
      setState = _props$state[1];

  columns.forEach(function (column) {
    var id = column.id,
        accessor = column.accessor,
        canFilter = column.canFilter;

    column.canFilter = accessor ? (0, _utils.getFirstDefined)(canFilter, disableFilters === true ? false : undefined, true) : false;
    // Was going to add this to the filter hook
    column.filterValue = filters[id];
  });

  var setFilter = function setFilter(id, val) {
    return setState(function (old) {
      if (typeof val === "undefined") {
        var prev = filters[id],
            rest = _objectWithoutProperties(filters, [id]);

        return _extends({}, old, {
          filters: _extends({}, rest)
        });
      }

      return _extends({}, old, {
        filters: _extends({}, filters, _defineProperty({}, id, val))
      });
    }, _actions.actions.setFilter);
  };

  var setAllFilters = function setAllFilters(filters) {
    return setState(function (old) {
      return _extends({}, old, {
        filters: filters
      });
    }, _actions.actions.setAllFilters);
  };

  hooks.columns.push(function (columns) {
    columns.forEach(function (column) {
      if (column.canFilter) {
        column.setFilter = function (val) {
          return setFilter(column.id, val);
        };
      }
    });
    return columns;
  });

  var filteredRows = (0, _react.useMemo)(function () {
    if (manualFilters || !Object.keys(filters).length) {
      return rows;
    }

    if (debug) console.info("getFilteredRows");

    // Filters top level and nested rows
    var filterRows = function filterRows(rows) {
      var filteredRows = rows;

      filteredRows = Object.entries(filters).reduce(function (filteredSoFar, _ref) {
        var _ref2 = _slicedToArray(_ref, 2),
            columnID = _ref2[0],
            filterValue = _ref2[1];

        // Find the filters column
        var column = columns.find(function (d) {
          return d.id === columnID;
        });

        // Don't filter hidden columns or columns that have had their filters disabled
        if (!column || column.filterable === false) {
          return filteredSoFar;
        }

        var filterMethod = column.filterMethod || filterFn;

        // If 'filterAll' is set to true, pass the entire dataset to the filter method
        if (column.filterAll) {
          return filterMethod(filteredSoFar, columnID, filterValue, column);
        }
        return filteredSoFar.filter(function (row) {
          return filterMethod(row, columnID, filterValue, column);
        });
      }, rows);

      // Apply the filter to any subRows
      filteredRows = filteredRows.map(function (row) {
        if (!row.subRows) {
          return row;
        }
        return _extends({}, row, {
          subRows: filterRows(row.subRows)
        });
      });

      // then filter any rows without subcolumns because it would be strange to show
      filteredRows = filteredRows.filter(function (row) {
        if (!row.subRows) {
          return true;
        }
        return row.subRows.length > 0;
      });

      return filteredRows;
    };

    return filterRows(rows);
  }, [rows, filters, manualFilters]);

  return _extends({}, props, {
    setFilter: setFilter,
    setAllFilters: setAllFilters,
    rows: filteredRows
  });
};
exports.useFilters = useFilters;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VGaWx0ZXJzLmpzIl0sIm5hbWVzIjpbImRlZmF1bHRTdGF0ZSIsImZpbHRlcnMiLCJzZXRGaWx0ZXIiLCJzZXRBbGxGaWx0ZXJzIiwidXNlRmlsdGVycyIsImRlYnVnIiwicHJvcHMiLCJyb3dzIiwiY29sdW1ucyIsImZpbHRlckZuIiwiZGVmYXVsdEZpbHRlckZuIiwibWFudWFsRmlsdGVycyIsImRpc2FibGVGaWx0ZXJzIiwiaG9va3MiLCJzdGF0ZSIsInNldFN0YXRlIiwiZm9yRWFjaCIsImlkIiwiY29sdW1uIiwiYWNjZXNzb3IiLCJjYW5GaWx0ZXIiLCJ1bmRlZmluZWQiLCJmaWx0ZXJWYWx1ZSIsInZhbCIsInByZXYiLCJyZXN0Iiwib2xkIiwiYWN0aW9ucyIsInB1c2giLCJmaWx0ZXJlZFJvd3MiLCJPYmplY3QiLCJrZXlzIiwibGVuZ3RoIiwiY29uc29sZSIsImluZm8iLCJmaWx0ZXJSb3dzIiwiZW50cmllcyIsInJlZHVjZSIsImZpbHRlcmVkU29GYXIiLCJjb2x1bW5JRCIsImZpbmQiLCJkIiwiZmlsdGVyYWJsZSIsImZpbHRlck1ldGhvZCIsImZpbHRlckFsbCIsImZpbHRlciIsInJvdyIsIm1hcCIsInN1YlJvd3MiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7O0FBQUE7O0FBRUE7O0FBQ0E7O0FBQ0E7Ozs7OztBQUVBQSw0QkFBYUMsT0FBYixHQUF1QixFQUF2QjtBQUNBLHlCQUFXO0FBQ1RDLGFBQVcsZUFERjtBQUVUQyxpQkFBZTtBQUZOLENBQVg7O0FBS08sSUFBTUMsYUFBYSxTQUFiQSxVQUFhLFFBQVM7QUFBQSxNQUUvQkMsS0FGK0IsR0FVN0JDLEtBVjZCLENBRS9CRCxLQUYrQjtBQUFBLE1BRy9CRSxJQUgrQixHQVU3QkQsS0FWNkIsQ0FHL0JDLElBSCtCO0FBQUEsTUFJL0JDLE9BSitCLEdBVTdCRixLQVY2QixDQUkvQkUsT0FKK0I7QUFBQSx3QkFVN0JGLEtBVjZCLENBSy9CRyxRQUwrQjtBQUFBLE1BSy9CQSxRQUwrQixtQ0FLcEJDLHNCQUxvQjtBQUFBLE1BTS9CQyxhQU4rQixHQVU3QkwsS0FWNkIsQ0FNL0JLLGFBTitCO0FBQUEsTUFPL0JDLGNBUCtCLEdBVTdCTixLQVY2QixDQU8vQk0sY0FQK0I7QUFBQSxNQVEvQkMsS0FSK0IsR0FVN0JQLEtBVjZCLENBUS9CTyxLQVIrQjtBQUFBLG9DQVU3QlAsS0FWNkIsQ0FTL0JRLEtBVCtCO0FBQUEsTUFTckJiLE9BVHFCLG1CQVNyQkEsT0FUcUI7QUFBQSxNQVNWYyxRQVRVOztBQVlqQ1AsVUFBUVEsT0FBUixDQUFnQixrQkFBVTtBQUFBLFFBQ2hCQyxFQURnQixHQUNZQyxNQURaLENBQ2hCRCxFQURnQjtBQUFBLFFBQ1pFLFFBRFksR0FDWUQsTUFEWixDQUNaQyxRQURZO0FBQUEsUUFDRkMsU0FERSxHQUNZRixNQURaLENBQ0ZFLFNBREU7O0FBRXhCRixXQUFPRSxTQUFQLEdBQW1CRCxXQUNmLDRCQUNFQyxTQURGLEVBRUVSLG1CQUFtQixJQUFuQixHQUEwQixLQUExQixHQUFrQ1MsU0FGcEMsRUFHRSxJQUhGLENBRGUsR0FNZixLQU5KO0FBT0E7QUFDQUgsV0FBT0ksV0FBUCxHQUFxQnJCLFFBQVFnQixFQUFSLENBQXJCO0FBQ0QsR0FYRDs7QUFhQSxNQUFNZixZQUFZLFNBQVpBLFNBQVksQ0FBQ2UsRUFBRCxFQUFLTSxHQUFMLEVBQWE7QUFDN0IsV0FBT1IsU0FBUyxlQUFPO0FBQ3JCLFVBQUksT0FBT1EsR0FBUCxLQUFlLFdBQW5CLEVBQWdDO0FBQUEsWUFDaEJDLElBRGdCLEdBQ0V2QixPQURGLENBQ3JCZ0IsRUFEcUI7QUFBQSxZQUNQUSxJQURPLDRCQUNFeEIsT0FERixHQUNyQmdCLEVBRHFCOztBQUU5Qiw0QkFDS1MsR0FETDtBQUVFekIsZ0NBQ0t3QixJQURMO0FBRkY7QUFNRDs7QUFFRCwwQkFDS0MsR0FETDtBQUVFekIsOEJBQ0tBLE9BREwsc0JBRUdnQixFQUZILEVBRVFNLEdBRlI7QUFGRjtBQU9ELEtBbEJNLEVBa0JKSSxpQkFBUXpCLFNBbEJKLENBQVA7QUFtQkQsR0FwQkQ7O0FBc0JBLE1BQU1DLGdCQUFnQixTQUFoQkEsYUFBZ0IsVUFBVztBQUMvQixXQUFPWSxTQUFTLGVBQU87QUFDckIsMEJBQ0tXLEdBREw7QUFFRXpCO0FBRkY7QUFJRCxLQUxNLEVBS0owQixpQkFBUXhCLGFBTEosQ0FBUDtBQU1ELEdBUEQ7O0FBU0FVLFFBQU1MLE9BQU4sQ0FBY29CLElBQWQsQ0FBbUIsbUJBQVc7QUFDNUJwQixZQUFRUSxPQUFSLENBQWdCLGtCQUFVO0FBQ3hCLFVBQUlFLE9BQU9FLFNBQVgsRUFBc0I7QUFDcEJGLGVBQU9oQixTQUFQLEdBQW1CO0FBQUEsaUJBQU9BLFVBQVVnQixPQUFPRCxFQUFqQixFQUFxQk0sR0FBckIsQ0FBUDtBQUFBLFNBQW5CO0FBQ0Q7QUFDRixLQUpEO0FBS0EsV0FBT2YsT0FBUDtBQUNELEdBUEQ7O0FBU0EsTUFBTXFCLGVBQWUsb0JBQ25CLFlBQU07QUFDSixRQUFJbEIsaUJBQWlCLENBQUNtQixPQUFPQyxJQUFQLENBQVk5QixPQUFaLEVBQXFCK0IsTUFBM0MsRUFBbUQ7QUFDakQsYUFBT3pCLElBQVA7QUFDRDs7QUFFRCxRQUFJRixLQUFKLEVBQVc0QixRQUFRQyxJQUFSLENBQWEsaUJBQWI7O0FBRVg7QUFDQSxRQUFNQyxhQUFhLFNBQWJBLFVBQWEsT0FBUTtBQUN6QixVQUFJTixlQUFldEIsSUFBbkI7O0FBRUFzQixxQkFBZUMsT0FBT00sT0FBUCxDQUFlbkMsT0FBZixFQUF3Qm9DLE1BQXhCLENBQ2IsVUFBQ0MsYUFBRCxRQUE0QztBQUFBO0FBQUEsWUFBM0JDLFFBQTJCO0FBQUEsWUFBakJqQixXQUFpQjs7QUFDMUM7QUFDQSxZQUFNSixTQUFTVixRQUFRZ0MsSUFBUixDQUFhO0FBQUEsaUJBQUtDLEVBQUV4QixFQUFGLEtBQVNzQixRQUFkO0FBQUEsU0FBYixDQUFmOztBQUVBO0FBQ0EsWUFBSSxDQUFDckIsTUFBRCxJQUFXQSxPQUFPd0IsVUFBUCxLQUFzQixLQUFyQyxFQUE0QztBQUMxQyxpQkFBT0osYUFBUDtBQUNEOztBQUVELFlBQU1LLGVBQWV6QixPQUFPeUIsWUFBUCxJQUF1QmxDLFFBQTVDOztBQUVBO0FBQ0EsWUFBSVMsT0FBTzBCLFNBQVgsRUFBc0I7QUFDcEIsaUJBQU9ELGFBQWFMLGFBQWIsRUFBNEJDLFFBQTVCLEVBQXNDakIsV0FBdEMsRUFBbURKLE1BQW5ELENBQVA7QUFDRDtBQUNELGVBQU9vQixjQUFjTyxNQUFkLENBQXFCO0FBQUEsaUJBQzFCRixhQUFhRyxHQUFiLEVBQWtCUCxRQUFsQixFQUE0QmpCLFdBQTVCLEVBQXlDSixNQUF6QyxDQUQwQjtBQUFBLFNBQXJCLENBQVA7QUFHRCxPQW5CWSxFQW9CYlgsSUFwQmEsQ0FBZjs7QUF1QkE7QUFDQXNCLHFCQUFlQSxhQUFha0IsR0FBYixDQUFpQixlQUFPO0FBQ3JDLFlBQUksQ0FBQ0QsSUFBSUUsT0FBVCxFQUFrQjtBQUNoQixpQkFBT0YsR0FBUDtBQUNEO0FBQ0QsNEJBQ0tBLEdBREw7QUFFRUUsbUJBQVNiLFdBQVdXLElBQUlFLE9BQWY7QUFGWDtBQUlELE9BUmMsQ0FBZjs7QUFVQTtBQUNBbkIscUJBQWVBLGFBQWFnQixNQUFiLENBQW9CLGVBQU87QUFDeEMsWUFBSSxDQUFDQyxJQUFJRSxPQUFULEVBQWtCO0FBQ2hCLGlCQUFPLElBQVA7QUFDRDtBQUNELGVBQU9GLElBQUlFLE9BQUosQ0FBWWhCLE1BQVosR0FBcUIsQ0FBNUI7QUFDRCxPQUxjLENBQWY7O0FBT0EsYUFBT0gsWUFBUDtBQUNELEtBOUNEOztBQWdEQSxXQUFPTSxXQUFXNUIsSUFBWCxDQUFQO0FBQ0QsR0ExRGtCLEVBMkRuQixDQUFDQSxJQUFELEVBQU9OLE9BQVAsRUFBZ0JVLGFBQWhCLENBM0RtQixDQUFyQjs7QUE4REEsc0JBQ0tMLEtBREw7QUFFRUosd0JBRkY7QUFHRUMsZ0NBSEY7QUFJRUksVUFBTXNCO0FBSlI7QUFNRCxDQXJJTSIsImZpbGUiOiJ1c2VGaWx0ZXJzLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlTWVtbyB9IGZyb20gXCJyZWFjdFwiO1xuXG5pbXBvcnQgeyBkZWZhdWx0RmlsdGVyRm4sIGdldEZpcnN0RGVmaW5lZCB9IGZyb20gXCIuLi91dGlsc1wiO1xuaW1wb3J0IHsgYWRkQWN0aW9ucywgYWN0aW9ucyB9IGZyb20gXCIuLi9hY3Rpb25zXCI7XG5pbXBvcnQgeyBkZWZhdWx0U3RhdGUgfSBmcm9tIFwiLi91c2VUYWJsZVN0YXRlXCI7XG5cbmRlZmF1bHRTdGF0ZS5maWx0ZXJzID0ge307XG5hZGRBY3Rpb25zKHtcbiAgc2V0RmlsdGVyOiBcIl9fc2V0RmlsdGVyX19cIixcbiAgc2V0QWxsRmlsdGVyczogXCJfX3NldEFsbEZpbHRlcnNfX1wiXG59KTtcblxuZXhwb3J0IGNvbnN0IHVzZUZpbHRlcnMgPSBwcm9wcyA9PiB7XG4gIGNvbnN0IHtcbiAgICBkZWJ1ZyxcbiAgICByb3dzLFxuICAgIGNvbHVtbnMsXG4gICAgZmlsdGVyRm4gPSBkZWZhdWx0RmlsdGVyRm4sXG4gICAgbWFudWFsRmlsdGVycyxcbiAgICBkaXNhYmxlRmlsdGVycyxcbiAgICBob29rcyxcbiAgICBzdGF0ZTogW3sgZmlsdGVycyB9LCBzZXRTdGF0ZV1cbiAgfSA9IHByb3BzO1xuXG4gIGNvbHVtbnMuZm9yRWFjaChjb2x1bW4gPT4ge1xuICAgIGNvbnN0IHsgaWQsIGFjY2Vzc29yLCBjYW5GaWx0ZXIgfSA9IGNvbHVtbjtcbiAgICBjb2x1bW4uY2FuRmlsdGVyID0gYWNjZXNzb3JcbiAgICAgID8gZ2V0Rmlyc3REZWZpbmVkKFxuICAgICAgICAgIGNhbkZpbHRlcixcbiAgICAgICAgICBkaXNhYmxlRmlsdGVycyA9PT0gdHJ1ZSA/IGZhbHNlIDogdW5kZWZpbmVkLFxuICAgICAgICAgIHRydWVcbiAgICAgICAgKVxuICAgICAgOiBmYWxzZTtcbiAgICAvLyBXYXMgZ29pbmcgdG8gYWRkIHRoaXMgdG8gdGhlIGZpbHRlciBob29rXG4gICAgY29sdW1uLmZpbHRlclZhbHVlID0gZmlsdGVyc1tpZF07XG4gIH0pO1xuXG4gIGNvbnN0IHNldEZpbHRlciA9IChpZCwgdmFsKSA9PiB7XG4gICAgcmV0dXJuIHNldFN0YXRlKG9sZCA9PiB7XG4gICAgICBpZiAodHlwZW9mIHZhbCA9PT0gXCJ1bmRlZmluZWRcIikge1xuICAgICAgICBjb25zdCB7IFtpZF06IHByZXYsIC4uLnJlc3QgfSA9IGZpbHRlcnM7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgLi4ub2xkLFxuICAgICAgICAgIGZpbHRlcnM6IHtcbiAgICAgICAgICAgIC4uLnJlc3RcbiAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICB9XG5cbiAgICAgIHJldHVybiB7XG4gICAgICAgIC4uLm9sZCxcbiAgICAgICAgZmlsdGVyczoge1xuICAgICAgICAgIC4uLmZpbHRlcnMsXG4gICAgICAgICAgW2lkXTogdmFsXG4gICAgICAgIH1cbiAgICAgIH07XG4gICAgfSwgYWN0aW9ucy5zZXRGaWx0ZXIpO1xuICB9O1xuXG4gIGNvbnN0IHNldEFsbEZpbHRlcnMgPSBmaWx0ZXJzID0+IHtcbiAgICByZXR1cm4gc2V0U3RhdGUob2xkID0+IHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIC4uLm9sZCxcbiAgICAgICAgZmlsdGVyc1xuICAgICAgfTtcbiAgICB9LCBhY3Rpb25zLnNldEFsbEZpbHRlcnMpO1xuICB9O1xuXG4gIGhvb2tzLmNvbHVtbnMucHVzaChjb2x1bW5zID0+IHtcbiAgICBjb2x1bW5zLmZvckVhY2goY29sdW1uID0+IHtcbiAgICAgIGlmIChjb2x1bW4uY2FuRmlsdGVyKSB7XG4gICAgICAgIGNvbHVtbi5zZXRGaWx0ZXIgPSB2YWwgPT4gc2V0RmlsdGVyKGNvbHVtbi5pZCwgdmFsKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgICByZXR1cm4gY29sdW1ucztcbiAgfSk7XG5cbiAgY29uc3QgZmlsdGVyZWRSb3dzID0gdXNlTWVtbyhcbiAgICAoKSA9PiB7XG4gICAgICBpZiAobWFudWFsRmlsdGVycyB8fCAhT2JqZWN0LmtleXMoZmlsdGVycykubGVuZ3RoKSB7XG4gICAgICAgIHJldHVybiByb3dzO1xuICAgICAgfVxuXG4gICAgICBpZiAoZGVidWcpIGNvbnNvbGUuaW5mbyhcImdldEZpbHRlcmVkUm93c1wiKTtcblxuICAgICAgLy8gRmlsdGVycyB0b3AgbGV2ZWwgYW5kIG5lc3RlZCByb3dzXG4gICAgICBjb25zdCBmaWx0ZXJSb3dzID0gcm93cyA9PiB7XG4gICAgICAgIGxldCBmaWx0ZXJlZFJvd3MgPSByb3dzO1xuXG4gICAgICAgIGZpbHRlcmVkUm93cyA9IE9iamVjdC5lbnRyaWVzKGZpbHRlcnMpLnJlZHVjZShcbiAgICAgICAgICAoZmlsdGVyZWRTb0ZhciwgW2NvbHVtbklELCBmaWx0ZXJWYWx1ZV0pID0+IHtcbiAgICAgICAgICAgIC8vIEZpbmQgdGhlIGZpbHRlcnMgY29sdW1uXG4gICAgICAgICAgICBjb25zdCBjb2x1bW4gPSBjb2x1bW5zLmZpbmQoZCA9PiBkLmlkID09PSBjb2x1bW5JRCk7XG5cbiAgICAgICAgICAgIC8vIERvbid0IGZpbHRlciBoaWRkZW4gY29sdW1ucyBvciBjb2x1bW5zIHRoYXQgaGF2ZSBoYWQgdGhlaXIgZmlsdGVycyBkaXNhYmxlZFxuICAgICAgICAgICAgaWYgKCFjb2x1bW4gfHwgY29sdW1uLmZpbHRlcmFibGUgPT09IGZhbHNlKSB7XG4gICAgICAgICAgICAgIHJldHVybiBmaWx0ZXJlZFNvRmFyO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBjb25zdCBmaWx0ZXJNZXRob2QgPSBjb2x1bW4uZmlsdGVyTWV0aG9kIHx8IGZpbHRlckZuO1xuXG4gICAgICAgICAgICAvLyBJZiAnZmlsdGVyQWxsJyBpcyBzZXQgdG8gdHJ1ZSwgcGFzcyB0aGUgZW50aXJlIGRhdGFzZXQgdG8gdGhlIGZpbHRlciBtZXRob2RcbiAgICAgICAgICAgIGlmIChjb2x1bW4uZmlsdGVyQWxsKSB7XG4gICAgICAgICAgICAgIHJldHVybiBmaWx0ZXJNZXRob2QoZmlsdGVyZWRTb0ZhciwgY29sdW1uSUQsIGZpbHRlclZhbHVlLCBjb2x1bW4pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIGZpbHRlcmVkU29GYXIuZmlsdGVyKHJvdyA9PlxuICAgICAgICAgICAgICBmaWx0ZXJNZXRob2Qocm93LCBjb2x1bW5JRCwgZmlsdGVyVmFsdWUsIGNvbHVtbilcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgfSxcbiAgICAgICAgICByb3dzXG4gICAgICAgICk7XG5cbiAgICAgICAgLy8gQXBwbHkgdGhlIGZpbHRlciB0byBhbnkgc3ViUm93c1xuICAgICAgICBmaWx0ZXJlZFJvd3MgPSBmaWx0ZXJlZFJvd3MubWFwKHJvdyA9PiB7XG4gICAgICAgICAgaWYgKCFyb3cuc3ViUm93cykge1xuICAgICAgICAgICAgcmV0dXJuIHJvdztcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIC4uLnJvdyxcbiAgICAgICAgICAgIHN1YlJvd3M6IGZpbHRlclJvd3Mocm93LnN1YlJvd3MpXG4gICAgICAgICAgfTtcbiAgICAgICAgfSk7XG5cbiAgICAgICAgLy8gdGhlbiBmaWx0ZXIgYW55IHJvd3Mgd2l0aG91dCBzdWJjb2x1bW5zIGJlY2F1c2UgaXQgd291bGQgYmUgc3RyYW5nZSB0byBzaG93XG4gICAgICAgIGZpbHRlcmVkUm93cyA9IGZpbHRlcmVkUm93cy5maWx0ZXIocm93ID0+IHtcbiAgICAgICAgICBpZiAoIXJvdy5zdWJSb3dzKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICB9XG4gICAgICAgICAgcmV0dXJuIHJvdy5zdWJSb3dzLmxlbmd0aCA+IDA7XG4gICAgICAgIH0pO1xuXG4gICAgICAgIHJldHVybiBmaWx0ZXJlZFJvd3M7XG4gICAgICB9O1xuXG4gICAgICByZXR1cm4gZmlsdGVyUm93cyhyb3dzKTtcbiAgICB9LFxuICAgIFtyb3dzLCBmaWx0ZXJzLCBtYW51YWxGaWx0ZXJzXVxuICApO1xuXG4gIHJldHVybiB7XG4gICAgLi4ucHJvcHMsXG4gICAgc2V0RmlsdGVyLFxuICAgIHNldEFsbEZpbHRlcnMsXG4gICAgcm93czogZmlsdGVyZWRSb3dzXG4gIH07XG59O1xuIl19