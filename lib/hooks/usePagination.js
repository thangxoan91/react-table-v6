"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.usePagination = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _react = require("react");

var _propTypes = require("prop-types");

var _propTypes2 = _interopRequireDefault(_propTypes);

var _actions = require("../actions");

var _useTableState = require("./useTableState");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

_useTableState.defaultState.pageSize = 10;
_useTableState.defaultState.pageIndex = 0;

(0, _actions.addActions)({
  pageChange: "__pageChange__"
});

var propTypes = {
  defaultPageSize: _propTypes2.default.number,
  defaultPageIndex: _propTypes2.default.number,
  pageSize: _propTypes2.default.number,
  pages: _propTypes2.default.number,
  pageIndex: _propTypes2.default.number,
  onStateChange: _propTypes2.default.func,
  stateReducer: _propTypes2.default.func,
  debug: _propTypes2.default.bool
};

var usePagination = exports.usePagination = function usePagination(props) {
  // Validate props
  _propTypes2.default.checkPropTypes(propTypes, props, "property", "usePagination");

  var parentDebug = props.debug,
      rows = props.rows,
      manualPagination = props.manualPagination,
      _props$debug = props.debug,
      debug = _props$debug === undefined ? parentDebug : _props$debug,
      _props$state = _slicedToArray(props.state, 2),
      _props$state$ = _props$state[0],
      pageSize = _props$state$.pageSize,
      pageIndex = _props$state$.pageIndex,
      userPageCount = _props$state$.pageCount,
      filters = _props$state$.filters,
      groupBy = _props$state$.groupBy,
      sortBy = _props$state$.sortBy,
      setState = _props$state[1];

  (0, _react.useLayoutEffect)(function () {
    setState(function (old) {
      return _extends({}, old, {
        pageIndex: 0
      });
    }, _actions.actions.pageChange);
  }, [filters, groupBy, sortBy]);

  var _useMemo = (0, _react.useMemo)(function () {
    if (manualPagination) {
      return {
        pages: [rows],
        pageCount: userPageCount
      };
    }
    if (debug) console.info("getPages");

    // Create a new pages with the first page ready to go.
    var pages = rows.length ? [] : [[]];

    // Start the pageIndex and currentPage cursors
    var cursor = 0;
    while (cursor < rows.length) {
      var end = cursor + pageSize;
      pages.push(rows.slice(cursor, end));
      cursor = end;
    }

    var pageCount = pages.length;

    return {
      pages: pages,
      pageCount: pageCount,
      pageOptions: pageOptions
    };
  }, [rows, pageSize, userPageCount]),
      pages = _useMemo.pages,
      pageCount = _useMemo.pageCount;

  var pageOptions = [].concat(_toConsumableArray(new Array(pageCount))).map(function (d, i) {
    return i;
  });
  var page = manualPagination ? rows : pages[pageIndex] || [];
  var canPreviousPage = pageIndex > 0;
  var canNextPage = pageIndex < pageCount - 1;

  var gotoPage = function gotoPage(pageIndex) {
    if (debug) console.info("gotoPage");
    return setState(function (old) {
      if (pageIndex < 0 || pageIndex > pageCount - 1) {
        return old;
      }
      return _extends({}, old, {
        pageIndex: pageIndex
      });
    }, _actions.actions.pageChange);
  };

  var previousPage = function previousPage() {
    return gotoPage(pageIndex - 1);
  };

  var nextPage = function nextPage() {
    return gotoPage(pageIndex + 1);
  };

  var setPageSize = function setPageSize(pageSize) {
    setState(function (old) {
      var topRowIndex = old.pageSize * old.pageIndex;
      var pageIndex = Math.floor(topRowIndex / pageSize);
      return _extends({}, old, {
        pageIndex: pageIndex,
        pageSize: pageSize
      });
    }, _actions.actions.setPageSize);
  };

  return _extends({}, props, {
    pages: pages,
    pageOptions: pageOptions,
    page: page,
    canPreviousPage: canPreviousPage,
    canNextPage: canNextPage,
    gotoPage: gotoPage,
    previousPage: previousPage,
    nextPage: nextPage,
    setPageSize: setPageSize
  });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VQYWdpbmF0aW9uLmpzIl0sIm5hbWVzIjpbImRlZmF1bHRTdGF0ZSIsInBhZ2VTaXplIiwicGFnZUluZGV4IiwicGFnZUNoYW5nZSIsInByb3BUeXBlcyIsImRlZmF1bHRQYWdlU2l6ZSIsIlByb3BUeXBlcyIsIm51bWJlciIsImRlZmF1bHRQYWdlSW5kZXgiLCJwYWdlcyIsIm9uU3RhdGVDaGFuZ2UiLCJmdW5jIiwic3RhdGVSZWR1Y2VyIiwiZGVidWciLCJib29sIiwidXNlUGFnaW5hdGlvbiIsImNoZWNrUHJvcFR5cGVzIiwicHJvcHMiLCJwYXJlbnREZWJ1ZyIsInJvd3MiLCJtYW51YWxQYWdpbmF0aW9uIiwic3RhdGUiLCJ1c2VyUGFnZUNvdW50IiwicGFnZUNvdW50IiwiZmlsdGVycyIsImdyb3VwQnkiLCJzb3J0QnkiLCJzZXRTdGF0ZSIsIm9sZCIsImFjdGlvbnMiLCJjb25zb2xlIiwiaW5mbyIsImxlbmd0aCIsImN1cnNvciIsImVuZCIsInB1c2giLCJzbGljZSIsInBhZ2VPcHRpb25zIiwiQXJyYXkiLCJtYXAiLCJkIiwiaSIsInBhZ2UiLCJjYW5QcmV2aW91c1BhZ2UiLCJjYW5OZXh0UGFnZSIsImdvdG9QYWdlIiwicHJldmlvdXNQYWdlIiwibmV4dFBhZ2UiLCJzZXRQYWdlU2l6ZSIsInRvcFJvd0luZGV4IiwiTWF0aCIsImZsb29yIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBOztBQUNBOzs7O0FBRUE7O0FBQ0E7Ozs7OztBQUVBQSw0QkFBYUMsUUFBYixHQUF3QixFQUF4QjtBQUNBRCw0QkFBYUUsU0FBYixHQUF5QixDQUF6Qjs7QUFFQSx5QkFBVztBQUNUQyxjQUFZO0FBREgsQ0FBWDs7QUFJQSxJQUFNQyxZQUFZO0FBQ2hCQyxtQkFBaUJDLG9CQUFVQyxNQURYO0FBRWhCQyxvQkFBa0JGLG9CQUFVQyxNQUZaO0FBR2hCTixZQUFVSyxvQkFBVUMsTUFISjtBQUloQkUsU0FBT0gsb0JBQVVDLE1BSkQ7QUFLaEJMLGFBQVdJLG9CQUFVQyxNQUxMO0FBTWhCRyxpQkFBZUosb0JBQVVLLElBTlQ7QUFPaEJDLGdCQUFjTixvQkFBVUssSUFQUjtBQVFoQkUsU0FBT1Asb0JBQVVRO0FBUkQsQ0FBbEI7O0FBV08sSUFBTUMsd0NBQWdCLFNBQWhCQSxhQUFnQixRQUFTO0FBQ3BDO0FBQ0FULHNCQUFVVSxjQUFWLENBQXlCWixTQUF6QixFQUFvQ2EsS0FBcEMsRUFBMkMsVUFBM0MsRUFBdUQsZUFBdkQ7O0FBRm9DLE1BSzNCQyxXQUwyQixHQW9CaENELEtBcEJnQyxDQUtsQ0osS0FMa0M7QUFBQSxNQU1sQ00sSUFOa0MsR0FvQmhDRixLQXBCZ0MsQ0FNbENFLElBTmtDO0FBQUEsTUFPbENDLGdCQVBrQyxHQW9CaENILEtBcEJnQyxDQU9sQ0csZ0JBUGtDO0FBQUEscUJBb0JoQ0gsS0FwQmdDLENBUWxDSixLQVJrQztBQUFBLE1BUWxDQSxLQVJrQyxnQ0FRMUJLLFdBUjBCO0FBQUEsb0NBb0JoQ0QsS0FwQmdDLENBU2xDSSxLQVRrQztBQUFBO0FBQUEsTUFXOUJwQixRQVg4QixpQkFXOUJBLFFBWDhCO0FBQUEsTUFZOUJDLFNBWjhCLGlCQVk5QkEsU0FaOEI7QUFBQSxNQWFuQm9CLGFBYm1CLGlCQWE5QkMsU0FiOEI7QUFBQSxNQWM5QkMsT0FkOEIsaUJBYzlCQSxPQWQ4QjtBQUFBLE1BZTlCQyxPQWY4QixpQkFlOUJBLE9BZjhCO0FBQUEsTUFnQjlCQyxNQWhCOEIsaUJBZ0I5QkEsTUFoQjhCO0FBQUEsTUFrQmhDQyxRQWxCZ0M7O0FBc0JwQyw4QkFDRSxZQUFNO0FBQ0pBLGFBQ0U7QUFBQSwwQkFDS0MsR0FETDtBQUVFMUIsbUJBQVc7QUFGYjtBQUFBLEtBREYsRUFLRTJCLGlCQUFRMUIsVUFMVjtBQU9ELEdBVEgsRUFVRSxDQUFDcUIsT0FBRCxFQUFVQyxPQUFWLEVBQW1CQyxNQUFuQixDQVZGOztBQXRCb0MsaUJBbUNQLG9CQUMzQixZQUFNO0FBQ0osUUFBSU4sZ0JBQUosRUFBc0I7QUFDcEIsYUFBTztBQUNMWCxlQUFPLENBQUNVLElBQUQsQ0FERjtBQUVMSSxtQkFBV0Q7QUFGTixPQUFQO0FBSUQ7QUFDRCxRQUFJVCxLQUFKLEVBQVdpQixRQUFRQyxJQUFSLENBQWEsVUFBYjs7QUFFWDtBQUNBLFFBQU10QixRQUFRVSxLQUFLYSxNQUFMLEdBQWMsRUFBZCxHQUFtQixDQUFDLEVBQUQsQ0FBakM7O0FBRUE7QUFDQSxRQUFJQyxTQUFTLENBQWI7QUFDQSxXQUFPQSxTQUFTZCxLQUFLYSxNQUFyQixFQUE2QjtBQUMzQixVQUFNRSxNQUFNRCxTQUFTaEMsUUFBckI7QUFDQVEsWUFBTTBCLElBQU4sQ0FBV2hCLEtBQUtpQixLQUFMLENBQVdILE1BQVgsRUFBbUJDLEdBQW5CLENBQVg7QUFDQUQsZUFBU0MsR0FBVDtBQUNEOztBQUVELFFBQU1YLFlBQVlkLE1BQU11QixNQUF4Qjs7QUFFQSxXQUFPO0FBQ0x2QixrQkFESztBQUVMYywwQkFGSztBQUdMYztBQUhLLEtBQVA7QUFLRCxHQTVCMEIsRUE2QjNCLENBQUNsQixJQUFELEVBQU9sQixRQUFQLEVBQWlCcUIsYUFBakIsQ0E3QjJCLENBbkNPO0FBQUEsTUFtQzVCYixLQW5DNEIsWUFtQzVCQSxLQW5DNEI7QUFBQSxNQW1DckJjLFNBbkNxQixZQW1DckJBLFNBbkNxQjs7QUFtRXBDLE1BQU1jLGNBQWMsNkJBQUksSUFBSUMsS0FBSixDQUFVZixTQUFWLENBQUosR0FBMEJnQixHQUExQixDQUE4QixVQUFDQyxDQUFELEVBQUlDLENBQUo7QUFBQSxXQUFVQSxDQUFWO0FBQUEsR0FBOUIsQ0FBcEI7QUFDQSxNQUFNQyxPQUFPdEIsbUJBQW1CRCxJQUFuQixHQUEwQlYsTUFBTVAsU0FBTixLQUFvQixFQUEzRDtBQUNBLE1BQU15QyxrQkFBa0J6QyxZQUFZLENBQXBDO0FBQ0EsTUFBTTBDLGNBQWMxQyxZQUFZcUIsWUFBWSxDQUE1Qzs7QUFFQSxNQUFNc0IsV0FBVyxTQUFYQSxRQUFXLFlBQWE7QUFDNUIsUUFBSWhDLEtBQUosRUFBV2lCLFFBQVFDLElBQVIsQ0FBYSxVQUFiO0FBQ1gsV0FBT0osU0FBUyxlQUFPO0FBQ3JCLFVBQUl6QixZQUFZLENBQVosSUFBaUJBLFlBQVlxQixZQUFZLENBQTdDLEVBQWdEO0FBQzlDLGVBQU9LLEdBQVA7QUFDRDtBQUNELDBCQUNLQSxHQURMO0FBRUUxQjtBQUZGO0FBSUQsS0FSTSxFQVFKMkIsaUJBQVExQixVQVJKLENBQVA7QUFTRCxHQVhEOztBQWFBLE1BQU0yQyxlQUFlLFNBQWZBLFlBQWUsR0FBTTtBQUN6QixXQUFPRCxTQUFTM0MsWUFBWSxDQUFyQixDQUFQO0FBQ0QsR0FGRDs7QUFJQSxNQUFNNkMsV0FBVyxTQUFYQSxRQUFXLEdBQU07QUFDckIsV0FBT0YsU0FBUzNDLFlBQVksQ0FBckIsQ0FBUDtBQUNELEdBRkQ7O0FBSUEsTUFBTThDLGNBQWMsU0FBZEEsV0FBYyxXQUFZO0FBQzlCckIsYUFBUyxlQUFPO0FBQ2QsVUFBTXNCLGNBQWNyQixJQUFJM0IsUUFBSixHQUFlMkIsSUFBSTFCLFNBQXZDO0FBQ0EsVUFBTUEsWUFBWWdELEtBQUtDLEtBQUwsQ0FBV0YsY0FBY2hELFFBQXpCLENBQWxCO0FBQ0EsMEJBQ0syQixHQURMO0FBRUUxQiw0QkFGRjtBQUdFRDtBQUhGO0FBS0QsS0FSRCxFQVFHNEIsaUJBQVFtQixXQVJYO0FBU0QsR0FWRDs7QUFZQSxzQkFDSy9CLEtBREw7QUFFRVIsZ0JBRkY7QUFHRTRCLDRCQUhGO0FBSUVLLGNBSkY7QUFLRUMsb0NBTEY7QUFNRUMsNEJBTkY7QUFPRUMsc0JBUEY7QUFRRUMsOEJBUkY7QUFTRUMsc0JBVEY7QUFVRUM7QUFWRjtBQVlELENBckhNIiwiZmlsZSI6InVzZVBhZ2luYXRpb24uanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyB1c2VNZW1vLCB1c2VTdGF0ZSwgdXNlTGF5b3V0RWZmZWN0IH0gZnJvbSBcInJlYWN0XCI7XG5pbXBvcnQgUHJvcFR5cGVzIGZyb20gXCJwcm9wLXR5cGVzXCI7XG5cbmltcG9ydCB7IGFkZEFjdGlvbnMsIGFjdGlvbnMgfSBmcm9tIFwiLi4vYWN0aW9uc1wiO1xuaW1wb3J0IHsgZGVmYXVsdFN0YXRlIH0gZnJvbSBcIi4vdXNlVGFibGVTdGF0ZVwiO1xuXG5kZWZhdWx0U3RhdGUucGFnZVNpemUgPSAxMDtcbmRlZmF1bHRTdGF0ZS5wYWdlSW5kZXggPSAwO1xuXG5hZGRBY3Rpb25zKHtcbiAgcGFnZUNoYW5nZTogXCJfX3BhZ2VDaGFuZ2VfX1wiXG59KTtcblxuY29uc3QgcHJvcFR5cGVzID0ge1xuICBkZWZhdWx0UGFnZVNpemU6IFByb3BUeXBlcy5udW1iZXIsXG4gIGRlZmF1bHRQYWdlSW5kZXg6IFByb3BUeXBlcy5udW1iZXIsXG4gIHBhZ2VTaXplOiBQcm9wVHlwZXMubnVtYmVyLFxuICBwYWdlczogUHJvcFR5cGVzLm51bWJlcixcbiAgcGFnZUluZGV4OiBQcm9wVHlwZXMubnVtYmVyLFxuICBvblN0YXRlQ2hhbmdlOiBQcm9wVHlwZXMuZnVuYyxcbiAgc3RhdGVSZWR1Y2VyOiBQcm9wVHlwZXMuZnVuYyxcbiAgZGVidWc6IFByb3BUeXBlcy5ib29sXG59O1xuXG5leHBvcnQgY29uc3QgdXNlUGFnaW5hdGlvbiA9IHByb3BzID0+IHtcbiAgLy8gVmFsaWRhdGUgcHJvcHNcbiAgUHJvcFR5cGVzLmNoZWNrUHJvcFR5cGVzKHByb3BUeXBlcywgcHJvcHMsIFwicHJvcGVydHlcIiwgXCJ1c2VQYWdpbmF0aW9uXCIpO1xuXG4gIGNvbnN0IHtcbiAgICBkZWJ1ZzogcGFyZW50RGVidWcsXG4gICAgcm93cyxcbiAgICBtYW51YWxQYWdpbmF0aW9uLFxuICAgIGRlYnVnID0gcGFyZW50RGVidWcsXG4gICAgc3RhdGU6IFtcbiAgICAgIHtcbiAgICAgICAgcGFnZVNpemUsXG4gICAgICAgIHBhZ2VJbmRleCxcbiAgICAgICAgcGFnZUNvdW50OiB1c2VyUGFnZUNvdW50LFxuICAgICAgICBmaWx0ZXJzLFxuICAgICAgICBncm91cEJ5LFxuICAgICAgICBzb3J0QnlcbiAgICAgIH0sXG4gICAgICBzZXRTdGF0ZVxuICAgIF1cbiAgfSA9IHByb3BzO1xuXG4gIHVzZUxheW91dEVmZmVjdChcbiAgICAoKSA9PiB7XG4gICAgICBzZXRTdGF0ZShcbiAgICAgICAgb2xkID0+ICh7XG4gICAgICAgICAgLi4ub2xkLFxuICAgICAgICAgIHBhZ2VJbmRleDogMFxuICAgICAgICB9KSxcbiAgICAgICAgYWN0aW9ucy5wYWdlQ2hhbmdlXG4gICAgICApO1xuICAgIH0sXG4gICAgW2ZpbHRlcnMsIGdyb3VwQnksIHNvcnRCeV1cbiAgKTtcblxuICBjb25zdCB7IHBhZ2VzLCBwYWdlQ291bnQgfSA9IHVzZU1lbW8oXG4gICAgKCkgPT4ge1xuICAgICAgaWYgKG1hbnVhbFBhZ2luYXRpb24pIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICBwYWdlczogW3Jvd3NdLFxuICAgICAgICAgIHBhZ2VDb3VudDogdXNlclBhZ2VDb3VudFxuICAgICAgICB9O1xuICAgICAgfVxuICAgICAgaWYgKGRlYnVnKSBjb25zb2xlLmluZm8oXCJnZXRQYWdlc1wiKTtcblxuICAgICAgLy8gQ3JlYXRlIGEgbmV3IHBhZ2VzIHdpdGggdGhlIGZpcnN0IHBhZ2UgcmVhZHkgdG8gZ28uXG4gICAgICBjb25zdCBwYWdlcyA9IHJvd3MubGVuZ3RoID8gW10gOiBbW11dO1xuXG4gICAgICAvLyBTdGFydCB0aGUgcGFnZUluZGV4IGFuZCBjdXJyZW50UGFnZSBjdXJzb3JzXG4gICAgICBsZXQgY3Vyc29yID0gMDtcbiAgICAgIHdoaWxlIChjdXJzb3IgPCByb3dzLmxlbmd0aCkge1xuICAgICAgICBjb25zdCBlbmQgPSBjdXJzb3IgKyBwYWdlU2l6ZTtcbiAgICAgICAgcGFnZXMucHVzaChyb3dzLnNsaWNlKGN1cnNvciwgZW5kKSk7XG4gICAgICAgIGN1cnNvciA9IGVuZDtcbiAgICAgIH1cblxuICAgICAgY29uc3QgcGFnZUNvdW50ID0gcGFnZXMubGVuZ3RoO1xuXG4gICAgICByZXR1cm4ge1xuICAgICAgICBwYWdlcyxcbiAgICAgICAgcGFnZUNvdW50LFxuICAgICAgICBwYWdlT3B0aW9uc1xuICAgICAgfTtcbiAgICB9LFxuICAgIFtyb3dzLCBwYWdlU2l6ZSwgdXNlclBhZ2VDb3VudF1cbiAgKTtcblxuICBjb25zdCBwYWdlT3B0aW9ucyA9IFsuLi5uZXcgQXJyYXkocGFnZUNvdW50KV0ubWFwKChkLCBpKSA9PiBpKTtcbiAgY29uc3QgcGFnZSA9IG1hbnVhbFBhZ2luYXRpb24gPyByb3dzIDogcGFnZXNbcGFnZUluZGV4XSB8fCBbXTtcbiAgY29uc3QgY2FuUHJldmlvdXNQYWdlID0gcGFnZUluZGV4ID4gMDtcbiAgY29uc3QgY2FuTmV4dFBhZ2UgPSBwYWdlSW5kZXggPCBwYWdlQ291bnQgLSAxO1xuXG4gIGNvbnN0IGdvdG9QYWdlID0gcGFnZUluZGV4ID0+IHtcbiAgICBpZiAoZGVidWcpIGNvbnNvbGUuaW5mbyhcImdvdG9QYWdlXCIpO1xuICAgIHJldHVybiBzZXRTdGF0ZShvbGQgPT4ge1xuICAgICAgaWYgKHBhZ2VJbmRleCA8IDAgfHwgcGFnZUluZGV4ID4gcGFnZUNvdW50IC0gMSkge1xuICAgICAgICByZXR1cm4gb2xkO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgLi4ub2xkLFxuICAgICAgICBwYWdlSW5kZXhcbiAgICAgIH07XG4gICAgfSwgYWN0aW9ucy5wYWdlQ2hhbmdlKTtcbiAgfTtcblxuICBjb25zdCBwcmV2aW91c1BhZ2UgPSAoKSA9PiB7XG4gICAgcmV0dXJuIGdvdG9QYWdlKHBhZ2VJbmRleCAtIDEpO1xuICB9O1xuXG4gIGNvbnN0IG5leHRQYWdlID0gKCkgPT4ge1xuICAgIHJldHVybiBnb3RvUGFnZShwYWdlSW5kZXggKyAxKTtcbiAgfTtcblxuICBjb25zdCBzZXRQYWdlU2l6ZSA9IHBhZ2VTaXplID0+IHtcbiAgICBzZXRTdGF0ZShvbGQgPT4ge1xuICAgICAgY29uc3QgdG9wUm93SW5kZXggPSBvbGQucGFnZVNpemUgKiBvbGQucGFnZUluZGV4O1xuICAgICAgY29uc3QgcGFnZUluZGV4ID0gTWF0aC5mbG9vcih0b3BSb3dJbmRleCAvIHBhZ2VTaXplKTtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIC4uLm9sZCxcbiAgICAgICAgcGFnZUluZGV4LFxuICAgICAgICBwYWdlU2l6ZVxuICAgICAgfTtcbiAgICB9LCBhY3Rpb25zLnNldFBhZ2VTaXplKTtcbiAgfTtcblxuICByZXR1cm4ge1xuICAgIC4uLnByb3BzLFxuICAgIHBhZ2VzLFxuICAgIHBhZ2VPcHRpb25zLFxuICAgIHBhZ2UsXG4gICAgY2FuUHJldmlvdXNQYWdlLFxuICAgIGNhbk5leHRQYWdlLFxuICAgIGdvdG9QYWdlLFxuICAgIHByZXZpb3VzUGFnZSxcbiAgICBuZXh0UGFnZSxcbiAgICBzZXRQYWdlU2l6ZVxuICB9O1xufTtcbiJdfQ==