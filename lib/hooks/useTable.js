'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useTable = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
//


var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _utils = require('../utils');

var _useTableState = require('./useTableState');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var renderErr = 'You must specify a render "type". This could be "Header", "Filter", or any other custom renderers you have set on your column.';

var propTypes = {
  // General
  data: _propTypes2.default.any,
  columns: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    aggregate: _propTypes2.default.func,
    filterFn: _propTypes2.default.func,
    filterAll: _propTypes2.default.bool,
    sortByFn: _propTypes2.default.func,
    resolvedDefaultSortDesc: _propTypes2.default.bool,
    canSortBy: _propTypes2.default.bool,
    canGroupBy: _propTypes2.default.bool,
    Cell: _propTypes2.default.any,
    Header: _propTypes2.default.any,
    Filter: _propTypes2.default.any
  })),

  filterFn: _propTypes2.default.func,
  sortByFn: _propTypes2.default.func,
  orderByFn: _propTypes2.default.func,
  groupByFn: _propTypes2.default.func,

  manualGrouping: _propTypes2.default.bool,
  manualFilters: _propTypes2.default.bool,
  manualSorting: _propTypes2.default.bool,

  defaultSortDesc: _propTypes2.default.bool,
  disableMultiSort: _propTypes2.default.bool,
  subRowsKey: _propTypes2.default.string,
  expandedKey: _propTypes2.default.string,
  userAggregations: _propTypes2.default.object,

  debug: _propTypes2.default.bool
};

var useTable = exports.useTable = function useTable(props) {
  for (var _len = arguments.length, plugins = Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    plugins[_key - 1] = arguments[_key];
  }

  // Validate props
  _propTypes2.default.checkPropTypes(propTypes, props, 'property', 'useTable');

  // Destructure props
  var _props$data = props.data,
      data = _props$data === undefined ? [] : _props$data,
      userState = props.state,
      debug = props.debug;

  // Always provide a default state

  var defaultState = (0, _useTableState.useTableState)();

  // But use the users state if provided
  var state = userState || defaultState;

  // These are hooks that plugins can use right before render
  var hooks = {
    beforeRender: [],
    columns: [],
    headers: [],
    headerGroups: [],
    rows: [],
    row: [],
    renderableRows: [],
    getTableProps: [],
    getRowProps: [],
    getHeaderRowProps: [],
    getHeaderProps: [],
    getCellProps: []

    // The initial api
  };var api = _extends({}, props, {
    data: data,
    state: state,
    hooks: hooks
  });

  if (debug) console.time('hooks');
  // Loop through plugins to build the api out
  api = plugins.filter(Boolean).reduce(function (prev, next) {
    return next(prev);
  }, api);
  if (debug) console.timeEnd('hooks');

  // Run the beforeRender hook
  if (debug) console.time('hooks.beforeRender');
  (0, _utils.applyHooks)(api.hooks.beforeRender, undefined, api);
  if (debug) console.timeEnd('hooks.beforeRender');

  if (debug) console.time('hooks.columns');
  api.columns = (0, _utils.applyHooks)(api.hooks.columns, api.columns, api);
  if (debug) console.timeEnd('hooks.columns');

  if (debug) console.time('hooks.headers');
  api.headers = (0, _utils.applyHooks)(api.hooks.headers, api.headers, api);
  if (debug) console.timeEnd('hooks.headers');
  [].concat(_toConsumableArray(api.columns), _toConsumableArray(api.headers)).forEach(function (column) {
    // Give columns/headers rendering power
    column.render = function (type) {
      var userProps = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      if (!type) {
        throw new Error(renderErr);
      }
      return (0, _utils.flexRender)(column[type], _extends({}, api, column, userProps));
    };

    // Give columns/headers getHeaderProps
    column.getHeaderProps = function (props) {
      return (0, _utils.mergeProps)({
        key: ['header', column.id].join('_')
      }, (0, _utils.applyPropHooks)(api.hooks.getHeaderProps, column, api), props);
    };
  });

  if (debug) console.time('hooks.headerGroups');
  api.headerGroups = (0, _utils.applyHooks)(api.hooks.headerGroups, api.headerGroups, api).filter(function (headerGroup, i) {
    // Filter out any headers and headerGroups that don't have visible columns
    headerGroup.headers = headerGroup.headers.filter(function (header) {
      var recurse = function recurse(columns) {
        return columns.filter(function (column) {
          if (column.columns) {
            return recurse(column.columns);
          }
          return column.visible;
        }).length;
      };
      if (header.columns) {
        return recurse(header.columns);
      }
      return header.visible;
    });

    // Give headerGroups getRowProps
    if (headerGroup.headers.length) {
      headerGroup.getRowProps = function () {
        var props = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
        return (0, _utils.mergeProps)({
          key: ['header' + i].join('_')
        }, (0, _utils.applyPropHooks)(api.hooks.getHeaderRowProps, headerGroup, api), props);
      };
      return true;
    }

    return false;
  });
  if (debug) console.timeEnd('hooks.headerGroups');

  // Run the rows (this could be a dangerous hook with a ton of data)
  if (debug) console.time('hooks.rows');
  api.rows = (0, _utils.applyHooks)(api.hooks.rows, api.rows, api);
  if (debug) console.timeEnd('hooks.rows');

  // This function is absolutely necessary and MUST be called on
  // any rows the user wishes to be displayed.
  api.prepareRow = function (row) {
    var index = row.index;

    row.getRowProps = function (props) {
      return (0, _utils.mergeProps)({ key: ['row', index].join('_') }, (0, _utils.applyHooks)(api.hooks.getRowProps, row, api), props);
    };

    row.cells = row.cells.filter(function (cell) {
      return cell.column.visible;
    });

    row.cells.forEach(function (cell) {
      if (!cell) {
        return;
      }

      var column = cell.column;


      cell.getCellProps = function (props) {
        var columnPathStr = [index, column.id].join('_');
        return (0, _utils.mergeProps)({
          key: ['cell', columnPathStr].join('_')
        }, (0, _utils.applyPropHooks)(api.hooks.getCellProps, cell, api), props);
      };

      cell.render = function (type) {
        var userProps = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        if (!type) {
          throw new Error('You must specify a render "type". This could be "Cell", "Header", "Filter", "Aggregated" or any other custom renderers you have set on your column.');
        }
        return (0, _utils.flexRender)(column[type], _extends({}, api, cell, userProps));
      };
    });
  };

  api.getTableProps = function (userProps) {
    return (0, _utils.mergeProps)((0, _utils.applyPropHooks)(api.hooks.getTableProps, api), userProps);
  };

  api.getRowProps = function (userProps) {
    return (0, _utils.mergeProps)((0, _utils.applyPropHooks)(api.hooks.getRowProps, api), userProps);
  };

  return api;
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VUYWJsZS5qcyJdLCJuYW1lcyI6WyJyZW5kZXJFcnIiLCJwcm9wVHlwZXMiLCJkYXRhIiwiUHJvcFR5cGVzIiwiYW55IiwiY29sdW1ucyIsImFycmF5T2YiLCJzaGFwZSIsImFnZ3JlZ2F0ZSIsImZ1bmMiLCJmaWx0ZXJGbiIsImZpbHRlckFsbCIsImJvb2wiLCJzb3J0QnlGbiIsInJlc29sdmVkRGVmYXVsdFNvcnREZXNjIiwiY2FuU29ydEJ5IiwiY2FuR3JvdXBCeSIsIkNlbGwiLCJIZWFkZXIiLCJGaWx0ZXIiLCJvcmRlckJ5Rm4iLCJncm91cEJ5Rm4iLCJtYW51YWxHcm91cGluZyIsIm1hbnVhbEZpbHRlcnMiLCJtYW51YWxTb3J0aW5nIiwiZGVmYXVsdFNvcnREZXNjIiwiZGlzYWJsZU11bHRpU29ydCIsInN1YlJvd3NLZXkiLCJzdHJpbmciLCJleHBhbmRlZEtleSIsInVzZXJBZ2dyZWdhdGlvbnMiLCJvYmplY3QiLCJkZWJ1ZyIsInVzZVRhYmxlIiwicHJvcHMiLCJwbHVnaW5zIiwiY2hlY2tQcm9wVHlwZXMiLCJ1c2VyU3RhdGUiLCJzdGF0ZSIsImRlZmF1bHRTdGF0ZSIsImhvb2tzIiwiYmVmb3JlUmVuZGVyIiwiaGVhZGVycyIsImhlYWRlckdyb3VwcyIsInJvd3MiLCJyb3ciLCJyZW5kZXJhYmxlUm93cyIsImdldFRhYmxlUHJvcHMiLCJnZXRSb3dQcm9wcyIsImdldEhlYWRlclJvd1Byb3BzIiwiZ2V0SGVhZGVyUHJvcHMiLCJnZXRDZWxsUHJvcHMiLCJhcGkiLCJjb25zb2xlIiwidGltZSIsImZpbHRlciIsIkJvb2xlYW4iLCJyZWR1Y2UiLCJwcmV2IiwibmV4dCIsInRpbWVFbmQiLCJ1bmRlZmluZWQiLCJmb3JFYWNoIiwiY29sdW1uIiwicmVuZGVyIiwidHlwZSIsInVzZXJQcm9wcyIsIkVycm9yIiwia2V5IiwiaWQiLCJqb2luIiwiaGVhZGVyR3JvdXAiLCJpIiwicmVjdXJzZSIsInZpc2libGUiLCJsZW5ndGgiLCJoZWFkZXIiLCJwcmVwYXJlUm93IiwiaW5kZXgiLCJjZWxscyIsImNlbGwiLCJjb2x1bW5QYXRoU3RyIl0sIm1hcHBpbmdzIjoiOzs7Ozs7OztBQUNBOzs7QUFEQTs7OztBQUVBOztBQUVBOzs7Ozs7QUFFQSxJQUFNQSxZQUNKLGdJQURGOztBQUdBLElBQU1DLFlBQVk7QUFDaEI7QUFDQUMsUUFBTUMsb0JBQVVDLEdBRkE7QUFHaEJDLFdBQVNGLG9CQUFVRyxPQUFWLENBQ1BILG9CQUFVSSxLQUFWLENBQWdCO0FBQ2RDLGVBQVdMLG9CQUFVTSxJQURQO0FBRWRDLGNBQVVQLG9CQUFVTSxJQUZOO0FBR2RFLGVBQVdSLG9CQUFVUyxJQUhQO0FBSWRDLGNBQVVWLG9CQUFVTSxJQUpOO0FBS2RLLDZCQUF5Qlgsb0JBQVVTLElBTHJCO0FBTWRHLGVBQVdaLG9CQUFVUyxJQU5QO0FBT2RJLGdCQUFZYixvQkFBVVMsSUFQUjtBQVFkSyxVQUFNZCxvQkFBVUMsR0FSRjtBQVNkYyxZQUFRZixvQkFBVUMsR0FUSjtBQVVkZSxZQUFRaEIsb0JBQVVDO0FBVkosR0FBaEIsQ0FETyxDQUhPOztBQWtCaEJNLFlBQVVQLG9CQUFVTSxJQWxCSjtBQW1CaEJJLFlBQVVWLG9CQUFVTSxJQW5CSjtBQW9CaEJXLGFBQVdqQixvQkFBVU0sSUFwQkw7QUFxQmhCWSxhQUFXbEIsb0JBQVVNLElBckJMOztBQXVCaEJhLGtCQUFnQm5CLG9CQUFVUyxJQXZCVjtBQXdCaEJXLGlCQUFlcEIsb0JBQVVTLElBeEJUO0FBeUJoQlksaUJBQWVyQixvQkFBVVMsSUF6QlQ7O0FBMkJoQmEsbUJBQWlCdEIsb0JBQVVTLElBM0JYO0FBNEJoQmMsb0JBQWtCdkIsb0JBQVVTLElBNUJaO0FBNkJoQmUsY0FBWXhCLG9CQUFVeUIsTUE3Qk47QUE4QmhCQyxlQUFhMUIsb0JBQVV5QixNQTlCUDtBQStCaEJFLG9CQUFrQjNCLG9CQUFVNEIsTUEvQlo7O0FBaUNoQkMsU0FBTzdCLG9CQUFVUztBQWpDRCxDQUFsQjs7QUFvQ08sSUFBTXFCLDhCQUFXLFNBQVhBLFFBQVcsQ0FBQ0MsS0FBRCxFQUF1QjtBQUFBLG9DQUFaQyxPQUFZO0FBQVpBLFdBQVk7QUFBQTs7QUFDN0M7QUFDQWhDLHNCQUFVaUMsY0FBVixDQUF5Qm5DLFNBQXpCLEVBQW9DaUMsS0FBcEMsRUFBMkMsVUFBM0MsRUFBdUQsVUFBdkQ7O0FBRUE7QUFKNkMsb0JBS0VBLEtBTEYsQ0FLckNoQyxJQUxxQztBQUFBLE1BS3JDQSxJQUxxQywrQkFLOUIsRUFMOEI7QUFBQSxNQUtuQm1DLFNBTG1CLEdBS0VILEtBTEYsQ0FLMUJJLEtBTDBCO0FBQUEsTUFLUk4sS0FMUSxHQUtFRSxLQUxGLENBS1JGLEtBTFE7O0FBTzdDOztBQUNBLE1BQU1PLGVBQWUsbUNBQXJCOztBQUVBO0FBQ0EsTUFBTUQsUUFBUUQsYUFBYUUsWUFBM0I7O0FBRUE7QUFDQSxNQUFNQyxRQUFRO0FBQ1pDLGtCQUFjLEVBREY7QUFFWnBDLGFBQVMsRUFGRztBQUdacUMsYUFBUyxFQUhHO0FBSVpDLGtCQUFjLEVBSkY7QUFLWkMsVUFBTSxFQUxNO0FBTVpDLFNBQUssRUFOTztBQU9aQyxvQkFBZ0IsRUFQSjtBQVFaQyxtQkFBZSxFQVJIO0FBU1pDLGlCQUFhLEVBVEQ7QUFVWkMsdUJBQW1CLEVBVlA7QUFXWkMsb0JBQWdCLEVBWEo7QUFZWkMsa0JBQWM7O0FBR2hCO0FBZmMsR0FBZCxDQWdCQSxJQUFJQyxtQkFDQ2xCLEtBREQ7QUFFRmhDLGNBRkU7QUFHRm9DLGdCQUhFO0FBSUZFO0FBSkUsSUFBSjs7QUFPQSxNQUFJUixLQUFKLEVBQVdxQixRQUFRQyxJQUFSLENBQWEsT0FBYjtBQUNYO0FBQ0FGLFFBQU1qQixRQUFRb0IsTUFBUixDQUFlQyxPQUFmLEVBQXdCQyxNQUF4QixDQUErQixVQUFDQyxJQUFELEVBQU9DLElBQVA7QUFBQSxXQUFnQkEsS0FBS0QsSUFBTCxDQUFoQjtBQUFBLEdBQS9CLEVBQTJETixHQUEzRCxDQUFOO0FBQ0EsTUFBSXBCLEtBQUosRUFBV3FCLFFBQVFPLE9BQVIsQ0FBZ0IsT0FBaEI7O0FBRVg7QUFDQSxNQUFJNUIsS0FBSixFQUFXcUIsUUFBUUMsSUFBUixDQUFhLG9CQUFiO0FBQ1gseUJBQVdGLElBQUlaLEtBQUosQ0FBVUMsWUFBckIsRUFBbUNvQixTQUFuQyxFQUE4Q1QsR0FBOUM7QUFDQSxNQUFJcEIsS0FBSixFQUFXcUIsUUFBUU8sT0FBUixDQUFnQixvQkFBaEI7O0FBRVgsTUFBSTVCLEtBQUosRUFBV3FCLFFBQVFDLElBQVIsQ0FBYSxlQUFiO0FBQ1hGLE1BQUkvQyxPQUFKLEdBQWMsdUJBQVcrQyxJQUFJWixLQUFKLENBQVVuQyxPQUFyQixFQUE4QitDLElBQUkvQyxPQUFsQyxFQUEyQytDLEdBQTNDLENBQWQ7QUFDQSxNQUFJcEIsS0FBSixFQUFXcUIsUUFBUU8sT0FBUixDQUFnQixlQUFoQjs7QUFFWCxNQUFJNUIsS0FBSixFQUFXcUIsUUFBUUMsSUFBUixDQUFhLGVBQWI7QUFDWEYsTUFBSVYsT0FBSixHQUFjLHVCQUFXVSxJQUFJWixLQUFKLENBQVVFLE9BQXJCLEVBQThCVSxJQUFJVixPQUFsQyxFQUEyQ1UsR0FBM0MsQ0FBZDtBQUNBLE1BQUlwQixLQUFKLEVBQVdxQixRQUFRTyxPQUFSLENBQWdCLGVBQWhCO0FBQ1gsK0JBQUlSLElBQUkvQyxPQUFSLHNCQUFvQitDLElBQUlWLE9BQXhCLEdBQWlDb0IsT0FBakMsQ0FBeUMsa0JBQVU7QUFDakQ7QUFDQUMsV0FBT0MsTUFBUCxHQUFnQixVQUFDQyxJQUFELEVBQTBCO0FBQUEsVUFBbkJDLFNBQW1CLHVFQUFQLEVBQU87O0FBQ3hDLFVBQUksQ0FBQ0QsSUFBTCxFQUFXO0FBQ1QsY0FBTSxJQUFJRSxLQUFKLENBQVVuRSxTQUFWLENBQU47QUFDRDtBQUNELGFBQU8sdUJBQVcrRCxPQUFPRSxJQUFQLENBQVgsZUFDRmIsR0FERSxFQUVGVyxNQUZFLEVBR0ZHLFNBSEUsRUFBUDtBQUtELEtBVEQ7O0FBV0E7QUFDQUgsV0FBT2IsY0FBUCxHQUF3QjtBQUFBLGFBQ3RCLHVCQUNFO0FBQ0VrQixhQUFLLENBQUMsUUFBRCxFQUFXTCxPQUFPTSxFQUFsQixFQUFzQkMsSUFBdEIsQ0FBMkIsR0FBM0I7QUFEUCxPQURGLEVBSUUsMkJBQWVsQixJQUFJWixLQUFKLENBQVVVLGNBQXpCLEVBQXlDYSxNQUF6QyxFQUFpRFgsR0FBakQsQ0FKRixFQUtFbEIsS0FMRixDQURzQjtBQUFBLEtBQXhCO0FBUUQsR0F0QkQ7O0FBd0JBLE1BQUlGLEtBQUosRUFBV3FCLFFBQVFDLElBQVIsQ0FBYSxvQkFBYjtBQUNYRixNQUFJVCxZQUFKLEdBQW1CLHVCQUFXUyxJQUFJWixLQUFKLENBQVVHLFlBQXJCLEVBQW1DUyxJQUFJVCxZQUF2QyxFQUFxRFMsR0FBckQsRUFBMERHLE1BQTFELENBQ2pCLFVBQUNnQixXQUFELEVBQWNDLENBQWQsRUFBb0I7QUFDbEI7QUFDQUQsZ0JBQVk3QixPQUFaLEdBQXNCNkIsWUFBWTdCLE9BQVosQ0FBb0JhLE1BQXBCLENBQTJCLGtCQUFVO0FBQ3pELFVBQU1rQixVQUFVLFNBQVZBLE9BQVU7QUFBQSxlQUNkcEUsUUFBUWtELE1BQVIsQ0FBZSxrQkFBVTtBQUN2QixjQUFJUSxPQUFPMUQsT0FBWCxFQUFvQjtBQUNsQixtQkFBT29FLFFBQVFWLE9BQU8xRCxPQUFmLENBQVA7QUFDRDtBQUNELGlCQUFPMEQsT0FBT1csT0FBZDtBQUNELFNBTEQsRUFLR0MsTUFOVztBQUFBLE9BQWhCO0FBT0EsVUFBSUMsT0FBT3ZFLE9BQVgsRUFBb0I7QUFDbEIsZUFBT29FLFFBQVFHLE9BQU92RSxPQUFmLENBQVA7QUFDRDtBQUNELGFBQU91RSxPQUFPRixPQUFkO0FBQ0QsS0FacUIsQ0FBdEI7O0FBY0E7QUFDQSxRQUFJSCxZQUFZN0IsT0FBWixDQUFvQmlDLE1BQXhCLEVBQWdDO0FBQzlCSixrQkFBWXZCLFdBQVosR0FBMEI7QUFBQSxZQUFDZCxLQUFELHVFQUFTLEVBQVQ7QUFBQSxlQUN4Qix1QkFDRTtBQUNFa0MsZUFBSyxZQUFVSSxDQUFWLEVBQWVGLElBQWYsQ0FBb0IsR0FBcEI7QUFEUCxTQURGLEVBSUUsMkJBQWVsQixJQUFJWixLQUFKLENBQVVTLGlCQUF6QixFQUE0Q3NCLFdBQTVDLEVBQXlEbkIsR0FBekQsQ0FKRixFQUtFbEIsS0FMRixDQUR3QjtBQUFBLE9BQTFCO0FBUUEsYUFBTyxJQUFQO0FBQ0Q7O0FBRUQsV0FBTyxLQUFQO0FBQ0QsR0EvQmdCLENBQW5CO0FBaUNBLE1BQUlGLEtBQUosRUFBV3FCLFFBQVFPLE9BQVIsQ0FBZ0Isb0JBQWhCOztBQUVYO0FBQ0EsTUFBSTVCLEtBQUosRUFBV3FCLFFBQVFDLElBQVIsQ0FBYSxZQUFiO0FBQ1hGLE1BQUlSLElBQUosR0FBVyx1QkFBV1EsSUFBSVosS0FBSixDQUFVSSxJQUFyQixFQUEyQlEsSUFBSVIsSUFBL0IsRUFBcUNRLEdBQXJDLENBQVg7QUFDQSxNQUFJcEIsS0FBSixFQUFXcUIsUUFBUU8sT0FBUixDQUFnQixZQUFoQjs7QUFFWDtBQUNBO0FBQ0FSLE1BQUl5QixVQUFKLEdBQWlCLGVBQU87QUFBQSxRQUNkQyxLQURjLEdBQ0pqQyxHQURJLENBQ2RpQyxLQURjOztBQUV0QmpDLFFBQUlHLFdBQUosR0FBa0I7QUFBQSxhQUNoQix1QkFDRSxFQUFFb0IsS0FBSyxDQUFDLEtBQUQsRUFBUVUsS0FBUixFQUFlUixJQUFmLENBQW9CLEdBQXBCLENBQVAsRUFERixFQUVFLHVCQUFXbEIsSUFBSVosS0FBSixDQUFVUSxXQUFyQixFQUFrQ0gsR0FBbEMsRUFBdUNPLEdBQXZDLENBRkYsRUFHRWxCLEtBSEYsQ0FEZ0I7QUFBQSxLQUFsQjs7QUFPQVcsUUFBSWtDLEtBQUosR0FBWWxDLElBQUlrQyxLQUFKLENBQVV4QixNQUFWLENBQWlCO0FBQUEsYUFBUXlCLEtBQUtqQixNQUFMLENBQVlXLE9BQXBCO0FBQUEsS0FBakIsQ0FBWjs7QUFFQTdCLFFBQUlrQyxLQUFKLENBQVVqQixPQUFWLENBQWtCLGdCQUFRO0FBQ3hCLFVBQUksQ0FBQ2tCLElBQUwsRUFBVztBQUNUO0FBQ0Q7O0FBSHVCLFVBS2hCakIsTUFMZ0IsR0FLTGlCLElBTEssQ0FLaEJqQixNQUxnQjs7O0FBT3hCaUIsV0FBSzdCLFlBQUwsR0FBb0IsaUJBQVM7QUFDM0IsWUFBTThCLGdCQUFnQixDQUFDSCxLQUFELEVBQVFmLE9BQU9NLEVBQWYsRUFBbUJDLElBQW5CLENBQXdCLEdBQXhCLENBQXRCO0FBQ0EsZUFBTyx1QkFDTDtBQUNFRixlQUFLLENBQUMsTUFBRCxFQUFTYSxhQUFULEVBQXdCWCxJQUF4QixDQUE2QixHQUE3QjtBQURQLFNBREssRUFJTCwyQkFBZWxCLElBQUlaLEtBQUosQ0FBVVcsWUFBekIsRUFBdUM2QixJQUF2QyxFQUE2QzVCLEdBQTdDLENBSkssRUFLTGxCLEtBTEssQ0FBUDtBQU9ELE9BVEQ7O0FBV0E4QyxXQUFLaEIsTUFBTCxHQUFjLFVBQUNDLElBQUQsRUFBMEI7QUFBQSxZQUFuQkMsU0FBbUIsdUVBQVAsRUFBTzs7QUFDdEMsWUFBSSxDQUFDRCxJQUFMLEVBQVc7QUFDVCxnQkFBTSxJQUFJRSxLQUFKLENBQ0oscUpBREksQ0FBTjtBQUdEO0FBQ0QsZUFBTyx1QkFBV0osT0FBT0UsSUFBUCxDQUFYLGVBQ0ZiLEdBREUsRUFFRjRCLElBRkUsRUFHRmQsU0FIRSxFQUFQO0FBS0QsT0FYRDtBQVlELEtBOUJEO0FBK0JELEdBMUNEOztBQTRDQWQsTUFBSUwsYUFBSixHQUFvQjtBQUFBLFdBQ2xCLHVCQUFXLDJCQUFlSyxJQUFJWixLQUFKLENBQVVPLGFBQXpCLEVBQXdDSyxHQUF4QyxDQUFYLEVBQXlEYyxTQUF6RCxDQURrQjtBQUFBLEdBQXBCOztBQUdBZCxNQUFJSixXQUFKLEdBQWtCO0FBQUEsV0FBYSx1QkFBVywyQkFBZUksSUFBSVosS0FBSixDQUFVUSxXQUF6QixFQUFzQ0ksR0FBdEMsQ0FBWCxFQUF1RGMsU0FBdkQsQ0FBYjtBQUFBLEdBQWxCOztBQUVBLFNBQU9kLEdBQVA7QUFDRCxDQTNLTSIsImZpbGUiOiJ1c2VUYWJsZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBQcm9wVHlwZXMgZnJvbSAncHJvcC10eXBlcydcbi8vXG5pbXBvcnQgeyBmbGV4UmVuZGVyLCBhcHBseUhvb2tzLCBhcHBseVByb3BIb29rcywgbWVyZ2VQcm9wcyB9IGZyb20gJy4uL3V0aWxzJ1xuXG5pbXBvcnQgeyB1c2VUYWJsZVN0YXRlIH0gZnJvbSAnLi91c2VUYWJsZVN0YXRlJ1xuXG5jb25zdCByZW5kZXJFcnIgPVxuICAnWW91IG11c3Qgc3BlY2lmeSBhIHJlbmRlciBcInR5cGVcIi4gVGhpcyBjb3VsZCBiZSBcIkhlYWRlclwiLCBcIkZpbHRlclwiLCBvciBhbnkgb3RoZXIgY3VzdG9tIHJlbmRlcmVycyB5b3UgaGF2ZSBzZXQgb24geW91ciBjb2x1bW4uJ1xuXG5jb25zdCBwcm9wVHlwZXMgPSB7XG4gIC8vIEdlbmVyYWxcbiAgZGF0YTogUHJvcFR5cGVzLmFueSxcbiAgY29sdW1uczogUHJvcFR5cGVzLmFycmF5T2YoXG4gICAgUHJvcFR5cGVzLnNoYXBlKHtcbiAgICAgIGFnZ3JlZ2F0ZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgICBmaWx0ZXJGbjogUHJvcFR5cGVzLmZ1bmMsXG4gICAgICBmaWx0ZXJBbGw6IFByb3BUeXBlcy5ib29sLFxuICAgICAgc29ydEJ5Rm46IFByb3BUeXBlcy5mdW5jLFxuICAgICAgcmVzb2x2ZWREZWZhdWx0U29ydERlc2M6IFByb3BUeXBlcy5ib29sLFxuICAgICAgY2FuU29ydEJ5OiBQcm9wVHlwZXMuYm9vbCxcbiAgICAgIGNhbkdyb3VwQnk6IFByb3BUeXBlcy5ib29sLFxuICAgICAgQ2VsbDogUHJvcFR5cGVzLmFueSxcbiAgICAgIEhlYWRlcjogUHJvcFR5cGVzLmFueSxcbiAgICAgIEZpbHRlcjogUHJvcFR5cGVzLmFueSxcbiAgICB9KVxuICApLFxuXG4gIGZpbHRlckZuOiBQcm9wVHlwZXMuZnVuYyxcbiAgc29ydEJ5Rm46IFByb3BUeXBlcy5mdW5jLFxuICBvcmRlckJ5Rm46IFByb3BUeXBlcy5mdW5jLFxuICBncm91cEJ5Rm46IFByb3BUeXBlcy5mdW5jLFxuXG4gIG1hbnVhbEdyb3VwaW5nOiBQcm9wVHlwZXMuYm9vbCxcbiAgbWFudWFsRmlsdGVyczogUHJvcFR5cGVzLmJvb2wsXG4gIG1hbnVhbFNvcnRpbmc6IFByb3BUeXBlcy5ib29sLFxuXG4gIGRlZmF1bHRTb3J0RGVzYzogUHJvcFR5cGVzLmJvb2wsXG4gIGRpc2FibGVNdWx0aVNvcnQ6IFByb3BUeXBlcy5ib29sLFxuICBzdWJSb3dzS2V5OiBQcm9wVHlwZXMuc3RyaW5nLFxuICBleHBhbmRlZEtleTogUHJvcFR5cGVzLnN0cmluZyxcbiAgdXNlckFnZ3JlZ2F0aW9uczogUHJvcFR5cGVzLm9iamVjdCxcblxuICBkZWJ1ZzogUHJvcFR5cGVzLmJvb2wsXG59XG5cbmV4cG9ydCBjb25zdCB1c2VUYWJsZSA9IChwcm9wcywgLi4ucGx1Z2lucykgPT4ge1xuICAvLyBWYWxpZGF0ZSBwcm9wc1xuICBQcm9wVHlwZXMuY2hlY2tQcm9wVHlwZXMocHJvcFR5cGVzLCBwcm9wcywgJ3Byb3BlcnR5JywgJ3VzZVRhYmxlJylcblxuICAvLyBEZXN0cnVjdHVyZSBwcm9wc1xuICBjb25zdCB7IGRhdGEgPSBbXSwgc3RhdGU6IHVzZXJTdGF0ZSwgZGVidWcgfSA9IHByb3BzXG5cbiAgLy8gQWx3YXlzIHByb3ZpZGUgYSBkZWZhdWx0IHN0YXRlXG4gIGNvbnN0IGRlZmF1bHRTdGF0ZSA9IHVzZVRhYmxlU3RhdGUoKVxuXG4gIC8vIEJ1dCB1c2UgdGhlIHVzZXJzIHN0YXRlIGlmIHByb3ZpZGVkXG4gIGNvbnN0IHN0YXRlID0gdXNlclN0YXRlIHx8IGRlZmF1bHRTdGF0ZVxuXG4gIC8vIFRoZXNlIGFyZSBob29rcyB0aGF0IHBsdWdpbnMgY2FuIHVzZSByaWdodCBiZWZvcmUgcmVuZGVyXG4gIGNvbnN0IGhvb2tzID0ge1xuICAgIGJlZm9yZVJlbmRlcjogW10sXG4gICAgY29sdW1uczogW10sXG4gICAgaGVhZGVyczogW10sXG4gICAgaGVhZGVyR3JvdXBzOiBbXSxcbiAgICByb3dzOiBbXSxcbiAgICByb3c6IFtdLFxuICAgIHJlbmRlcmFibGVSb3dzOiBbXSxcbiAgICBnZXRUYWJsZVByb3BzOiBbXSxcbiAgICBnZXRSb3dQcm9wczogW10sXG4gICAgZ2V0SGVhZGVyUm93UHJvcHM6IFtdLFxuICAgIGdldEhlYWRlclByb3BzOiBbXSxcbiAgICBnZXRDZWxsUHJvcHM6IFtdLFxuICB9XG5cbiAgLy8gVGhlIGluaXRpYWwgYXBpXG4gIGxldCBhcGkgPSB7XG4gICAgLi4ucHJvcHMsXG4gICAgZGF0YSxcbiAgICBzdGF0ZSxcbiAgICBob29rcyxcbiAgfVxuXG4gIGlmIChkZWJ1ZykgY29uc29sZS50aW1lKCdob29rcycpXG4gIC8vIExvb3AgdGhyb3VnaCBwbHVnaW5zIHRvIGJ1aWxkIHRoZSBhcGkgb3V0XG4gIGFwaSA9IHBsdWdpbnMuZmlsdGVyKEJvb2xlYW4pLnJlZHVjZSgocHJldiwgbmV4dCkgPT4gbmV4dChwcmV2KSwgYXBpKVxuICBpZiAoZGVidWcpIGNvbnNvbGUudGltZUVuZCgnaG9va3MnKVxuXG4gIC8vIFJ1biB0aGUgYmVmb3JlUmVuZGVyIGhvb2tcbiAgaWYgKGRlYnVnKSBjb25zb2xlLnRpbWUoJ2hvb2tzLmJlZm9yZVJlbmRlcicpXG4gIGFwcGx5SG9va3MoYXBpLmhvb2tzLmJlZm9yZVJlbmRlciwgdW5kZWZpbmVkLCBhcGkpXG4gIGlmIChkZWJ1ZykgY29uc29sZS50aW1lRW5kKCdob29rcy5iZWZvcmVSZW5kZXInKVxuXG4gIGlmIChkZWJ1ZykgY29uc29sZS50aW1lKCdob29rcy5jb2x1bW5zJylcbiAgYXBpLmNvbHVtbnMgPSBhcHBseUhvb2tzKGFwaS5ob29rcy5jb2x1bW5zLCBhcGkuY29sdW1ucywgYXBpKVxuICBpZiAoZGVidWcpIGNvbnNvbGUudGltZUVuZCgnaG9va3MuY29sdW1ucycpXG5cbiAgaWYgKGRlYnVnKSBjb25zb2xlLnRpbWUoJ2hvb2tzLmhlYWRlcnMnKVxuICBhcGkuaGVhZGVycyA9IGFwcGx5SG9va3MoYXBpLmhvb2tzLmhlYWRlcnMsIGFwaS5oZWFkZXJzLCBhcGkpXG4gIGlmIChkZWJ1ZykgY29uc29sZS50aW1lRW5kKCdob29rcy5oZWFkZXJzJyk7XG4gIFsuLi5hcGkuY29sdW1ucywgLi4uYXBpLmhlYWRlcnNdLmZvckVhY2goY29sdW1uID0+IHtcbiAgICAvLyBHaXZlIGNvbHVtbnMvaGVhZGVycyByZW5kZXJpbmcgcG93ZXJcbiAgICBjb2x1bW4ucmVuZGVyID0gKHR5cGUsIHVzZXJQcm9wcyA9IHt9KSA9PiB7XG4gICAgICBpZiAoIXR5cGUpIHtcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKHJlbmRlckVycilcbiAgICAgIH1cbiAgICAgIHJldHVybiBmbGV4UmVuZGVyKGNvbHVtblt0eXBlXSwge1xuICAgICAgICAuLi5hcGksXG4gICAgICAgIC4uLmNvbHVtbixcbiAgICAgICAgLi4udXNlclByb3BzLFxuICAgICAgfSlcbiAgICB9XG5cbiAgICAvLyBHaXZlIGNvbHVtbnMvaGVhZGVycyBnZXRIZWFkZXJQcm9wc1xuICAgIGNvbHVtbi5nZXRIZWFkZXJQcm9wcyA9IHByb3BzID0+XG4gICAgICBtZXJnZVByb3BzKFxuICAgICAgICB7XG4gICAgICAgICAga2V5OiBbJ2hlYWRlcicsIGNvbHVtbi5pZF0uam9pbignXycpLFxuICAgICAgICB9LFxuICAgICAgICBhcHBseVByb3BIb29rcyhhcGkuaG9va3MuZ2V0SGVhZGVyUHJvcHMsIGNvbHVtbiwgYXBpKSxcbiAgICAgICAgcHJvcHNcbiAgICAgIClcbiAgfSlcblxuICBpZiAoZGVidWcpIGNvbnNvbGUudGltZSgnaG9va3MuaGVhZGVyR3JvdXBzJylcbiAgYXBpLmhlYWRlckdyb3VwcyA9IGFwcGx5SG9va3MoYXBpLmhvb2tzLmhlYWRlckdyb3VwcywgYXBpLmhlYWRlckdyb3VwcywgYXBpKS5maWx0ZXIoXG4gICAgKGhlYWRlckdyb3VwLCBpKSA9PiB7XG4gICAgICAvLyBGaWx0ZXIgb3V0IGFueSBoZWFkZXJzIGFuZCBoZWFkZXJHcm91cHMgdGhhdCBkb24ndCBoYXZlIHZpc2libGUgY29sdW1uc1xuICAgICAgaGVhZGVyR3JvdXAuaGVhZGVycyA9IGhlYWRlckdyb3VwLmhlYWRlcnMuZmlsdGVyKGhlYWRlciA9PiB7XG4gICAgICAgIGNvbnN0IHJlY3Vyc2UgPSBjb2x1bW5zID0+XG4gICAgICAgICAgY29sdW1ucy5maWx0ZXIoY29sdW1uID0+IHtcbiAgICAgICAgICAgIGlmIChjb2x1bW4uY29sdW1ucykge1xuICAgICAgICAgICAgICByZXR1cm4gcmVjdXJzZShjb2x1bW4uY29sdW1ucylcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybiBjb2x1bW4udmlzaWJsZVxuICAgICAgICAgIH0pLmxlbmd0aFxuICAgICAgICBpZiAoaGVhZGVyLmNvbHVtbnMpIHtcbiAgICAgICAgICByZXR1cm4gcmVjdXJzZShoZWFkZXIuY29sdW1ucylcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gaGVhZGVyLnZpc2libGVcbiAgICAgIH0pXG5cbiAgICAgIC8vIEdpdmUgaGVhZGVyR3JvdXBzIGdldFJvd1Byb3BzXG4gICAgICBpZiAoaGVhZGVyR3JvdXAuaGVhZGVycy5sZW5ndGgpIHtcbiAgICAgICAgaGVhZGVyR3JvdXAuZ2V0Um93UHJvcHMgPSAocHJvcHMgPSB7fSkgPT5cbiAgICAgICAgICBtZXJnZVByb3BzKFxuICAgICAgICAgICAge1xuICAgICAgICAgICAgICBrZXk6IFtgaGVhZGVyJHtpfWBdLmpvaW4oJ18nKSxcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBhcHBseVByb3BIb29rcyhhcGkuaG9va3MuZ2V0SGVhZGVyUm93UHJvcHMsIGhlYWRlckdyb3VwLCBhcGkpLFxuICAgICAgICAgICAgcHJvcHNcbiAgICAgICAgICApXG4gICAgICAgIHJldHVybiB0cnVlXG4gICAgICB9XG5cbiAgICAgIHJldHVybiBmYWxzZVxuICAgIH1cbiAgKVxuICBpZiAoZGVidWcpIGNvbnNvbGUudGltZUVuZCgnaG9va3MuaGVhZGVyR3JvdXBzJylcblxuICAvLyBSdW4gdGhlIHJvd3MgKHRoaXMgY291bGQgYmUgYSBkYW5nZXJvdXMgaG9vayB3aXRoIGEgdG9uIG9mIGRhdGEpXG4gIGlmIChkZWJ1ZykgY29uc29sZS50aW1lKCdob29rcy5yb3dzJylcbiAgYXBpLnJvd3MgPSBhcHBseUhvb2tzKGFwaS5ob29rcy5yb3dzLCBhcGkucm93cywgYXBpKVxuICBpZiAoZGVidWcpIGNvbnNvbGUudGltZUVuZCgnaG9va3Mucm93cycpXG5cbiAgLy8gVGhpcyBmdW5jdGlvbiBpcyBhYnNvbHV0ZWx5IG5lY2Vzc2FyeSBhbmQgTVVTVCBiZSBjYWxsZWQgb25cbiAgLy8gYW55IHJvd3MgdGhlIHVzZXIgd2lzaGVzIHRvIGJlIGRpc3BsYXllZC5cbiAgYXBpLnByZXBhcmVSb3cgPSByb3cgPT4ge1xuICAgIGNvbnN0IHsgaW5kZXggfSA9IHJvd1xuICAgIHJvdy5nZXRSb3dQcm9wcyA9IHByb3BzID0+XG4gICAgICBtZXJnZVByb3BzKFxuICAgICAgICB7IGtleTogWydyb3cnLCBpbmRleF0uam9pbignXycpIH0sXG4gICAgICAgIGFwcGx5SG9va3MoYXBpLmhvb2tzLmdldFJvd1Byb3BzLCByb3csIGFwaSksXG4gICAgICAgIHByb3BzXG4gICAgICApXG5cbiAgICByb3cuY2VsbHMgPSByb3cuY2VsbHMuZmlsdGVyKGNlbGwgPT4gY2VsbC5jb2x1bW4udmlzaWJsZSlcblxuICAgIHJvdy5jZWxscy5mb3JFYWNoKGNlbGwgPT4ge1xuICAgICAgaWYgKCFjZWxsKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuXG4gICAgICBjb25zdCB7IGNvbHVtbiB9ID0gY2VsbFxuXG4gICAgICBjZWxsLmdldENlbGxQcm9wcyA9IHByb3BzID0+IHtcbiAgICAgICAgY29uc3QgY29sdW1uUGF0aFN0ciA9IFtpbmRleCwgY29sdW1uLmlkXS5qb2luKCdfJylcbiAgICAgICAgcmV0dXJuIG1lcmdlUHJvcHMoXG4gICAgICAgICAge1xuICAgICAgICAgICAga2V5OiBbJ2NlbGwnLCBjb2x1bW5QYXRoU3RyXS5qb2luKCdfJyksXG4gICAgICAgICAgfSxcbiAgICAgICAgICBhcHBseVByb3BIb29rcyhhcGkuaG9va3MuZ2V0Q2VsbFByb3BzLCBjZWxsLCBhcGkpLFxuICAgICAgICAgIHByb3BzXG4gICAgICAgIClcbiAgICAgIH1cblxuICAgICAgY2VsbC5yZW5kZXIgPSAodHlwZSwgdXNlclByb3BzID0ge30pID0+IHtcbiAgICAgICAgaWYgKCF0eXBlKSB7XG4gICAgICAgICAgdGhyb3cgbmV3IEVycm9yKFxuICAgICAgICAgICAgJ1lvdSBtdXN0IHNwZWNpZnkgYSByZW5kZXIgXCJ0eXBlXCIuIFRoaXMgY291bGQgYmUgXCJDZWxsXCIsIFwiSGVhZGVyXCIsIFwiRmlsdGVyXCIsIFwiQWdncmVnYXRlZFwiIG9yIGFueSBvdGhlciBjdXN0b20gcmVuZGVyZXJzIHlvdSBoYXZlIHNldCBvbiB5b3VyIGNvbHVtbi4nXG4gICAgICAgICAgKVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmbGV4UmVuZGVyKGNvbHVtblt0eXBlXSwge1xuICAgICAgICAgIC4uLmFwaSxcbiAgICAgICAgICAuLi5jZWxsLFxuICAgICAgICAgIC4uLnVzZXJQcm9wcyxcbiAgICAgICAgfSlcbiAgICAgIH1cbiAgICB9KVxuICB9XG5cbiAgYXBpLmdldFRhYmxlUHJvcHMgPSB1c2VyUHJvcHMgPT5cbiAgICBtZXJnZVByb3BzKGFwcGx5UHJvcEhvb2tzKGFwaS5ob29rcy5nZXRUYWJsZVByb3BzLCBhcGkpLCB1c2VyUHJvcHMpXG5cbiAgYXBpLmdldFJvd1Byb3BzID0gdXNlclByb3BzID0+IG1lcmdlUHJvcHMoYXBwbHlQcm9wSG9va3MoYXBpLmhvb2tzLmdldFJvd1Byb3BzLCBhcGkpLCB1c2VyUHJvcHMpXG5cbiAgcmV0dXJuIGFwaVxufVxuIl19