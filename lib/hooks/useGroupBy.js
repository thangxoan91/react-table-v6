"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGroupBy = undefined;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _react = require("react");

var _aggregations = require("../aggregations");

var aggregations = _interopRequireWildcard(_aggregations);

var _actions = require("../actions");

var _useTableState = require("./useTableState");

var _utils = require("../utils");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

_useTableState.defaultState.groupBy = [];

(0, _actions.addActions)({
  toggleGroupBy: "__toggleGroupBy__"
});

var useGroupBy = exports.useGroupBy = function useGroupBy(api) {
  var props = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var debug = api.debug,
      rows = api.rows,
      columns = api.columns,
      _api$groupByFn = api.groupByFn,
      groupByFn = _api$groupByFn === undefined ? _utils.defaultGroupByFn : _api$groupByFn,
      manualGroupBy = api.manualGroupBy,
      disableGrouping = api.disableGrouping,
      _api$aggregations = api.aggregations,
      userAggregations = _api$aggregations === undefined ? {} : _api$aggregations,
      hooks = api.hooks,
      _api$state = _slicedToArray(api.state, 2),
      groupBy = _api$state[0].groupBy,
      setState = _api$state[1];

  columns.forEach(function (column) {
    var id = column.id,
        accessor = column.accessor,
        canGroupBy = column.canGroupBy;

    column.grouped = groupBy.includes(id);

    column.canGroupBy = accessor ? (0, _utils.getFirstDefined)(canGroupBy, disableGrouping === true ? false : undefined, true) : false;

    column.Aggregated = column.Aggregated || column.Cell;
  });

  var toggleGroupBy = function toggleGroupBy(id, toggle) {
    return setState(function (old) {
      var resolvedToggle = typeof set !== "undefined" ? toggle : !groupBy.includes(id);
      if (resolvedToggle) {
        return _extends({}, old, {
          groupBy: [].concat(_toConsumableArray(groupBy), [id])
        });
      }
      return _extends({}, old, {
        groupBy: groupBy.filter(function (d) {
          return d !== id;
        })
      });
    }, _actions.actions.toggleGroupBy);
  };

  hooks.columns.push(function (columns) {
    columns.forEach(function (column) {
      if (column.canGroupBy) {
        column.toggleGroupBy = function () {
          return toggleGroupBy(column.id);
        };
      }
    });
    return columns;
  });

  hooks.getGroupByToggleProps = [];

  var addGroupByToggleProps = function addGroupByToggleProps(columns, api) {
    columns.forEach(function (column) {
      var canGroupBy = column.canGroupBy;

      column.getGroupByToggleProps = function (props) {
        return (0, _utils.mergeProps)({
          onClick: canGroupBy ? function (e) {
            e.persist();
            column.toggleGroupBy();
          } : undefined,
          style: {
            cursor: canGroupBy ? "pointer" : undefined
          },
          title: "Toggle GroupBy"
        }, (0, _utils.applyPropHooks)(api.hooks.getGroupByToggleProps, column, api), props);
      };
    });
    return columns;
  };

  hooks.columns.push(addGroupByToggleProps);
  hooks.headers.push(addGroupByToggleProps);

  var groupedRows = (0, _react.useMemo)(function () {
    if (manualGroupBy || !groupBy.length) {
      return rows;
    }
    if (debug) console.info("getGroupedRows");
    // Find the columns that can or are aggregating

    // Uses each column to aggregate rows into a single value
    var aggregateRowsToValues = function aggregateRowsToValues(rows) {
      var values = {};
      columns.forEach(function (column) {
        var columnValues = rows.map(function (d) {
          return d.values[column.id];
        });
        var aggregate = userAggregations[column.aggregate] || aggregations[column.aggregate] || column.aggregate;
        if (typeof aggregate === "function") {
          values[column.id] = aggregate(columnValues, rows);
        } else if (aggregate) {
          throw new Error("Invalid aggregate \"" + aggregate + "\" passed to column with ID: \"" + column.id + "\"");
        } else {
          values[column.id] = columnValues[0];
        }
      });
      return values;
    };

    // Recursively group the data
    var groupRecursively = function groupRecursively(rows, groupBy) {
      var depth = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;

      // This is the last level, just return the rows
      if (depth >= groupBy.length) {
        return rows;
      }

      // Group the rows together for this level
      var groupedRows = Object.entries(groupByFn(rows, groupBy[depth])).map(function (_ref, index) {
        var _ref2 = _slicedToArray(_ref, 2),
            groupByVal = _ref2[0],
            subRows = _ref2[1];

        // Recurse to sub rows before aggregation
        subRows = groupRecursively(subRows, groupBy, depth + 1);

        var values = aggregateRowsToValues(subRows);

        var row = {
          groupByID: groupBy[depth],
          groupByVal: groupByVal,
          values: values,
          subRows: subRows,
          depth: depth,
          index: index
        };
        return row;
      });

      return groupedRows;
    };

    // Assign the new data
    return groupRecursively(rows, groupBy);
  }, [rows, groupBy, columns, manualGroupBy]);

  return _extends({}, api, {
    rows: groupedRows
  });
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL3NyYy9ob29rcy91c2VHcm91cEJ5LmpzIl0sIm5hbWVzIjpbImFnZ3JlZ2F0aW9ucyIsImRlZmF1bHRTdGF0ZSIsImdyb3VwQnkiLCJ0b2dnbGVHcm91cEJ5IiwidXNlR3JvdXBCeSIsImFwaSIsInByb3BzIiwiZGVidWciLCJyb3dzIiwiY29sdW1ucyIsImdyb3VwQnlGbiIsImRlZmF1bHRHcm91cEJ5Rm4iLCJtYW51YWxHcm91cEJ5IiwiZGlzYWJsZUdyb3VwaW5nIiwidXNlckFnZ3JlZ2F0aW9ucyIsImhvb2tzIiwic3RhdGUiLCJzZXRTdGF0ZSIsImZvckVhY2giLCJpZCIsImNvbHVtbiIsImFjY2Vzc29yIiwiY2FuR3JvdXBCeSIsImdyb3VwZWQiLCJpbmNsdWRlcyIsInVuZGVmaW5lZCIsIkFnZ3JlZ2F0ZWQiLCJDZWxsIiwidG9nZ2xlIiwicmVzb2x2ZWRUb2dnbGUiLCJzZXQiLCJvbGQiLCJmaWx0ZXIiLCJkIiwiYWN0aW9ucyIsInB1c2giLCJnZXRHcm91cEJ5VG9nZ2xlUHJvcHMiLCJhZGRHcm91cEJ5VG9nZ2xlUHJvcHMiLCJvbkNsaWNrIiwiZSIsInBlcnNpc3QiLCJzdHlsZSIsImN1cnNvciIsInRpdGxlIiwiaGVhZGVycyIsImdyb3VwZWRSb3dzIiwibGVuZ3RoIiwiY29uc29sZSIsImluZm8iLCJhZ2dyZWdhdGVSb3dzVG9WYWx1ZXMiLCJ2YWx1ZXMiLCJjb2x1bW5WYWx1ZXMiLCJtYXAiLCJhZ2dyZWdhdGUiLCJFcnJvciIsImdyb3VwUmVjdXJzaXZlbHkiLCJkZXB0aCIsIk9iamVjdCIsImVudHJpZXMiLCJpbmRleCIsImdyb3VwQnlWYWwiLCJzdWJSb3dzIiwicm93IiwiZ3JvdXBCeUlEIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7OztBQUFBOztBQUVBOztJQUFZQSxZOztBQUNaOztBQUNBOztBQUNBOzs7Ozs7QUFPQUMsNEJBQWFDLE9BQWIsR0FBdUIsRUFBdkI7O0FBRUEseUJBQVc7QUFDVEMsaUJBQWU7QUFETixDQUFYOztBQUlPLElBQU1DLGtDQUFhLFNBQWJBLFVBQWEsQ0FBQ0MsR0FBRCxFQUFxQjtBQUFBLE1BQWZDLEtBQWUsdUVBQVAsRUFBTzs7QUFBQSxNQUUzQ0MsS0FGMkMsR0FXekNGLEdBWHlDLENBRTNDRSxLQUYyQztBQUFBLE1BRzNDQyxJQUgyQyxHQVd6Q0gsR0FYeUMsQ0FHM0NHLElBSDJDO0FBQUEsTUFJM0NDLE9BSjJDLEdBV3pDSixHQVh5QyxDQUkzQ0ksT0FKMkM7QUFBQSx1QkFXekNKLEdBWHlDLENBSzNDSyxTQUwyQztBQUFBLE1BSzNDQSxTQUwyQyxrQ0FLL0JDLHVCQUwrQjtBQUFBLE1BTTNDQyxhQU4yQyxHQVd6Q1AsR0FYeUMsQ0FNM0NPLGFBTjJDO0FBQUEsTUFPM0NDLGVBUDJDLEdBV3pDUixHQVh5QyxDQU8zQ1EsZUFQMkM7QUFBQSwwQkFXekNSLEdBWHlDLENBUTNDTCxZQVIyQztBQUFBLE1BUTdCYyxnQkFSNkIscUNBUVYsRUFSVTtBQUFBLE1BUzNDQyxLQVQyQyxHQVd6Q1YsR0FYeUMsQ0FTM0NVLEtBVDJDO0FBQUEsa0NBV3pDVixHQVh5QyxDQVUzQ1csS0FWMkM7QUFBQSxNQVVqQ2QsT0FWaUMsaUJBVWpDQSxPQVZpQztBQUFBLE1BVXRCZSxRQVZzQjs7QUFhN0NSLFVBQVFTLE9BQVIsQ0FBZ0Isa0JBQVU7QUFBQSxRQUNoQkMsRUFEZ0IsR0FDYUMsTUFEYixDQUNoQkQsRUFEZ0I7QUFBQSxRQUNaRSxRQURZLEdBQ2FELE1BRGIsQ0FDWkMsUUFEWTtBQUFBLFFBQ0ZDLFVBREUsR0FDYUYsTUFEYixDQUNGRSxVQURFOztBQUV4QkYsV0FBT0csT0FBUCxHQUFpQnJCLFFBQVFzQixRQUFSLENBQWlCTCxFQUFqQixDQUFqQjs7QUFFQUMsV0FBT0UsVUFBUCxHQUFvQkQsV0FDaEIsNEJBQ0VDLFVBREYsRUFFRVQsb0JBQW9CLElBQXBCLEdBQTJCLEtBQTNCLEdBQW1DWSxTQUZyQyxFQUdFLElBSEYsQ0FEZ0IsR0FNaEIsS0FOSjs7QUFRQUwsV0FBT00sVUFBUCxHQUFvQk4sT0FBT00sVUFBUCxJQUFxQk4sT0FBT08sSUFBaEQ7QUFDRCxHQWJEOztBQWVBLE1BQU14QixnQkFBZ0IsU0FBaEJBLGFBQWdCLENBQUNnQixFQUFELEVBQUtTLE1BQUwsRUFBZ0I7QUFDcEMsV0FBT1gsU0FBUyxlQUFPO0FBQ3JCLFVBQU1ZLGlCQUNKLE9BQU9DLEdBQVAsS0FBZSxXQUFmLEdBQTZCRixNQUE3QixHQUFzQyxDQUFDMUIsUUFBUXNCLFFBQVIsQ0FBaUJMLEVBQWpCLENBRHpDO0FBRUEsVUFBSVUsY0FBSixFQUFvQjtBQUNsQiw0QkFDS0UsR0FETDtBQUVFN0IsZ0RBQWFBLE9BQWIsSUFBc0JpQixFQUF0QjtBQUZGO0FBSUQ7QUFDRCwwQkFDS1ksR0FETDtBQUVFN0IsaUJBQVNBLFFBQVE4QixNQUFSLENBQWU7QUFBQSxpQkFBS0MsTUFBTWQsRUFBWDtBQUFBLFNBQWY7QUFGWDtBQUlELEtBYk0sRUFhSmUsaUJBQVEvQixhQWJKLENBQVA7QUFjRCxHQWZEOztBQWlCQVksUUFBTU4sT0FBTixDQUFjMEIsSUFBZCxDQUFtQixtQkFBVztBQUM1QjFCLFlBQVFTLE9BQVIsQ0FBZ0Isa0JBQVU7QUFDeEIsVUFBSUUsT0FBT0UsVUFBWCxFQUF1QjtBQUNyQkYsZUFBT2pCLGFBQVAsR0FBdUI7QUFBQSxpQkFBTUEsY0FBY2lCLE9BQU9ELEVBQXJCLENBQU47QUFBQSxTQUF2QjtBQUNEO0FBQ0YsS0FKRDtBQUtBLFdBQU9WLE9BQVA7QUFDRCxHQVBEOztBQVNBTSxRQUFNcUIscUJBQU4sR0FBOEIsRUFBOUI7O0FBRUEsTUFBTUMsd0JBQXdCLFNBQXhCQSxxQkFBd0IsQ0FBQzVCLE9BQUQsRUFBVUosR0FBVixFQUFrQjtBQUM5Q0ksWUFBUVMsT0FBUixDQUFnQixrQkFBVTtBQUFBLFVBQ2hCSSxVQURnQixHQUNERixNQURDLENBQ2hCRSxVQURnQjs7QUFFeEJGLGFBQU9nQixxQkFBUCxHQUErQixpQkFBUztBQUN0QyxlQUFPLHVCQUNMO0FBQ0VFLG1CQUFTaEIsYUFDTCxhQUFLO0FBQ0hpQixjQUFFQyxPQUFGO0FBQ0FwQixtQkFBT2pCLGFBQVA7QUFDRCxXQUpJLEdBS0xzQixTQU5OO0FBT0VnQixpQkFBTztBQUNMQyxvQkFBUXBCLGFBQWEsU0FBYixHQUF5Qkc7QUFENUIsV0FQVDtBQVVFa0IsaUJBQU87QUFWVCxTQURLLEVBYUwsMkJBQWV0QyxJQUFJVSxLQUFKLENBQVVxQixxQkFBekIsRUFBZ0RoQixNQUFoRCxFQUF3RGYsR0FBeEQsQ0FiSyxFQWNMQyxLQWRLLENBQVA7QUFnQkQsT0FqQkQ7QUFrQkQsS0FwQkQ7QUFxQkEsV0FBT0csT0FBUDtBQUNELEdBdkJEOztBQXlCQU0sUUFBTU4sT0FBTixDQUFjMEIsSUFBZCxDQUFtQkUscUJBQW5CO0FBQ0F0QixRQUFNNkIsT0FBTixDQUFjVCxJQUFkLENBQW1CRSxxQkFBbkI7O0FBRUEsTUFBTVEsY0FBYyxvQkFDbEIsWUFBTTtBQUNKLFFBQUlqQyxpQkFBaUIsQ0FBQ1YsUUFBUTRDLE1BQTlCLEVBQXNDO0FBQ3BDLGFBQU90QyxJQUFQO0FBQ0Q7QUFDRCxRQUFJRCxLQUFKLEVBQVd3QyxRQUFRQyxJQUFSLENBQWEsZ0JBQWI7QUFDWDs7QUFFQTtBQUNBLFFBQU1DLHdCQUF3QixTQUF4QkEscUJBQXdCLE9BQVE7QUFDcEMsVUFBTUMsU0FBUyxFQUFmO0FBQ0F6QyxjQUFRUyxPQUFSLENBQWdCLGtCQUFVO0FBQ3hCLFlBQU1pQyxlQUFlM0MsS0FBSzRDLEdBQUwsQ0FBUztBQUFBLGlCQUFLbkIsRUFBRWlCLE1BQUYsQ0FBUzlCLE9BQU9ELEVBQWhCLENBQUw7QUFBQSxTQUFULENBQXJCO0FBQ0EsWUFBSWtDLFlBQ0Z2QyxpQkFBaUJNLE9BQU9pQyxTQUF4QixLQUNBckQsYUFBYW9CLE9BQU9pQyxTQUFwQixDQURBLElBRUFqQyxPQUFPaUMsU0FIVDtBQUlBLFlBQUksT0FBT0EsU0FBUCxLQUFxQixVQUF6QixFQUFxQztBQUNuQ0gsaUJBQU85QixPQUFPRCxFQUFkLElBQW9Ca0MsVUFBVUYsWUFBVixFQUF3QjNDLElBQXhCLENBQXBCO0FBQ0QsU0FGRCxNQUVPLElBQUk2QyxTQUFKLEVBQWU7QUFDcEIsZ0JBQU0sSUFBSUMsS0FBSiwwQkFDa0JELFNBRGxCLHVDQUVGakMsT0FBT0QsRUFGTCxRQUFOO0FBS0QsU0FOTSxNQU1BO0FBQ0wrQixpQkFBTzlCLE9BQU9ELEVBQWQsSUFBb0JnQyxhQUFhLENBQWIsQ0FBcEI7QUFDRDtBQUNGLE9BakJEO0FBa0JBLGFBQU9ELE1BQVA7QUFDRCxLQXJCRDs7QUF1QkE7QUFDQSxRQUFNSyxtQkFBbUIsU0FBbkJBLGdCQUFtQixDQUFDL0MsSUFBRCxFQUFPTixPQUFQLEVBQThCO0FBQUEsVUFBZHNELEtBQWMsdUVBQU4sQ0FBTTs7QUFDckQ7QUFDQSxVQUFJQSxTQUFTdEQsUUFBUTRDLE1BQXJCLEVBQTZCO0FBQzNCLGVBQU90QyxJQUFQO0FBQ0Q7O0FBRUQ7QUFDQSxVQUFJcUMsY0FBY1ksT0FBT0MsT0FBUCxDQUFlaEQsVUFBVUYsSUFBVixFQUFnQk4sUUFBUXNELEtBQVIsQ0FBaEIsQ0FBZixFQUFnREosR0FBaEQsQ0FDaEIsZ0JBQXdCTyxLQUF4QixFQUFrQztBQUFBO0FBQUEsWUFBaENDLFVBQWdDO0FBQUEsWUFBcEJDLE9BQW9COztBQUNoQztBQUNBQSxrQkFBVU4saUJBQWlCTSxPQUFqQixFQUEwQjNELE9BQTFCLEVBQW1Dc0QsUUFBUSxDQUEzQyxDQUFWOztBQUVBLFlBQU1OLFNBQVNELHNCQUFzQlksT0FBdEIsQ0FBZjs7QUFFQSxZQUFNQyxNQUFNO0FBQ1ZDLHFCQUFXN0QsUUFBUXNELEtBQVIsQ0FERDtBQUVWSSxnQ0FGVTtBQUdWVix3QkFIVTtBQUlWVywwQkFKVTtBQUtWTCxzQkFMVTtBQU1WRztBQU5VLFNBQVo7QUFRQSxlQUFPRyxHQUFQO0FBQ0QsT0FoQmUsQ0FBbEI7O0FBbUJBLGFBQU9qQixXQUFQO0FBQ0QsS0EzQkQ7O0FBNkJBO0FBQ0EsV0FBT1UsaUJBQWlCL0MsSUFBakIsRUFBdUJOLE9BQXZCLENBQVA7QUFDRCxHQWhFaUIsRUFpRWxCLENBQUNNLElBQUQsRUFBT04sT0FBUCxFQUFnQk8sT0FBaEIsRUFBeUJHLGFBQXpCLENBakVrQixDQUFwQjs7QUFvRUEsc0JBQ0tQLEdBREw7QUFFRUcsVUFBTXFDO0FBRlI7QUFJRCxDQTVKTSIsImZpbGUiOiJ1c2VHcm91cEJ5LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgdXNlTWVtbyB9IGZyb20gXCJyZWFjdFwiO1xuXG5pbXBvcnQgKiBhcyBhZ2dyZWdhdGlvbnMgZnJvbSBcIi4uL2FnZ3JlZ2F0aW9uc1wiO1xuaW1wb3J0IHsgYWRkQWN0aW9ucywgYWN0aW9ucyB9IGZyb20gXCIuLi9hY3Rpb25zXCI7XG5pbXBvcnQgeyBkZWZhdWx0U3RhdGUgfSBmcm9tIFwiLi91c2VUYWJsZVN0YXRlXCI7XG5pbXBvcnQge1xuICBtZXJnZVByb3BzLFxuICBhcHBseVByb3BIb29rcyxcbiAgZGVmYXVsdEdyb3VwQnlGbixcbiAgZ2V0Rmlyc3REZWZpbmVkXG59IGZyb20gXCIuLi91dGlsc1wiO1xuXG5kZWZhdWx0U3RhdGUuZ3JvdXBCeSA9IFtdO1xuXG5hZGRBY3Rpb25zKHtcbiAgdG9nZ2xlR3JvdXBCeTogXCJfX3RvZ2dsZUdyb3VwQnlfX1wiXG59KTtcblxuZXhwb3J0IGNvbnN0IHVzZUdyb3VwQnkgPSAoYXBpLCBwcm9wcyA9IHt9KSA9PiB7XG4gIGNvbnN0IHtcbiAgICBkZWJ1ZyxcbiAgICByb3dzLFxuICAgIGNvbHVtbnMsXG4gICAgZ3JvdXBCeUZuID0gZGVmYXVsdEdyb3VwQnlGbixcbiAgICBtYW51YWxHcm91cEJ5LFxuICAgIGRpc2FibGVHcm91cGluZyxcbiAgICBhZ2dyZWdhdGlvbnM6IHVzZXJBZ2dyZWdhdGlvbnMgPSB7fSxcbiAgICBob29rcyxcbiAgICBzdGF0ZTogW3sgZ3JvdXBCeSB9LCBzZXRTdGF0ZV1cbiAgfSA9IGFwaTtcblxuICBjb2x1bW5zLmZvckVhY2goY29sdW1uID0+IHtcbiAgICBjb25zdCB7IGlkLCBhY2Nlc3NvciwgY2FuR3JvdXBCeSB9ID0gY29sdW1uO1xuICAgIGNvbHVtbi5ncm91cGVkID0gZ3JvdXBCeS5pbmNsdWRlcyhpZCk7XG5cbiAgICBjb2x1bW4uY2FuR3JvdXBCeSA9IGFjY2Vzc29yXG4gICAgICA/IGdldEZpcnN0RGVmaW5lZChcbiAgICAgICAgICBjYW5Hcm91cEJ5LFxuICAgICAgICAgIGRpc2FibGVHcm91cGluZyA9PT0gdHJ1ZSA/IGZhbHNlIDogdW5kZWZpbmVkLFxuICAgICAgICAgIHRydWVcbiAgICAgICAgKVxuICAgICAgOiBmYWxzZTtcblxuICAgIGNvbHVtbi5BZ2dyZWdhdGVkID0gY29sdW1uLkFnZ3JlZ2F0ZWQgfHwgY29sdW1uLkNlbGw7XG4gIH0pO1xuXG4gIGNvbnN0IHRvZ2dsZUdyb3VwQnkgPSAoaWQsIHRvZ2dsZSkgPT4ge1xuICAgIHJldHVybiBzZXRTdGF0ZShvbGQgPT4ge1xuICAgICAgY29uc3QgcmVzb2x2ZWRUb2dnbGUgPVxuICAgICAgICB0eXBlb2Ygc2V0ICE9PSBcInVuZGVmaW5lZFwiID8gdG9nZ2xlIDogIWdyb3VwQnkuaW5jbHVkZXMoaWQpO1xuICAgICAgaWYgKHJlc29sdmVkVG9nZ2xlKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgLi4ub2xkLFxuICAgICAgICAgIGdyb3VwQnk6IFsuLi5ncm91cEJ5LCBpZF1cbiAgICAgICAgfTtcbiAgICAgIH1cbiAgICAgIHJldHVybiB7XG4gICAgICAgIC4uLm9sZCxcbiAgICAgICAgZ3JvdXBCeTogZ3JvdXBCeS5maWx0ZXIoZCA9PiBkICE9PSBpZClcbiAgICAgIH07XG4gICAgfSwgYWN0aW9ucy50b2dnbGVHcm91cEJ5KTtcbiAgfTtcblxuICBob29rcy5jb2x1bW5zLnB1c2goY29sdW1ucyA9PiB7XG4gICAgY29sdW1ucy5mb3JFYWNoKGNvbHVtbiA9PiB7XG4gICAgICBpZiAoY29sdW1uLmNhbkdyb3VwQnkpIHtcbiAgICAgICAgY29sdW1uLnRvZ2dsZUdyb3VwQnkgPSAoKSA9PiB0b2dnbGVHcm91cEJ5KGNvbHVtbi5pZCk7XG4gICAgICB9XG4gICAgfSk7XG4gICAgcmV0dXJuIGNvbHVtbnM7XG4gIH0pO1xuXG4gIGhvb2tzLmdldEdyb3VwQnlUb2dnbGVQcm9wcyA9IFtdO1xuXG4gIGNvbnN0IGFkZEdyb3VwQnlUb2dnbGVQcm9wcyA9IChjb2x1bW5zLCBhcGkpID0+IHtcbiAgICBjb2x1bW5zLmZvckVhY2goY29sdW1uID0+IHtcbiAgICAgIGNvbnN0IHsgY2FuR3JvdXBCeSB9ID0gY29sdW1uO1xuICAgICAgY29sdW1uLmdldEdyb3VwQnlUb2dnbGVQcm9wcyA9IHByb3BzID0+IHtcbiAgICAgICAgcmV0dXJuIG1lcmdlUHJvcHMoXG4gICAgICAgICAge1xuICAgICAgICAgICAgb25DbGljazogY2FuR3JvdXBCeVxuICAgICAgICAgICAgICA/IGUgPT4ge1xuICAgICAgICAgICAgICAgICAgZS5wZXJzaXN0KCk7XG4gICAgICAgICAgICAgICAgICBjb2x1bW4udG9nZ2xlR3JvdXBCeSgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgOiB1bmRlZmluZWQsXG4gICAgICAgICAgICBzdHlsZToge1xuICAgICAgICAgICAgICBjdXJzb3I6IGNhbkdyb3VwQnkgPyBcInBvaW50ZXJcIiA6IHVuZGVmaW5lZFxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIHRpdGxlOiBcIlRvZ2dsZSBHcm91cEJ5XCJcbiAgICAgICAgICB9LFxuICAgICAgICAgIGFwcGx5UHJvcEhvb2tzKGFwaS5ob29rcy5nZXRHcm91cEJ5VG9nZ2xlUHJvcHMsIGNvbHVtbiwgYXBpKSxcbiAgICAgICAgICBwcm9wc1xuICAgICAgICApO1xuICAgICAgfTtcbiAgICB9KTtcbiAgICByZXR1cm4gY29sdW1ucztcbiAgfTtcblxuICBob29rcy5jb2x1bW5zLnB1c2goYWRkR3JvdXBCeVRvZ2dsZVByb3BzKTtcbiAgaG9va3MuaGVhZGVycy5wdXNoKGFkZEdyb3VwQnlUb2dnbGVQcm9wcyk7XG5cbiAgY29uc3QgZ3JvdXBlZFJvd3MgPSB1c2VNZW1vKFxuICAgICgpID0+IHtcbiAgICAgIGlmIChtYW51YWxHcm91cEJ5IHx8ICFncm91cEJ5Lmxlbmd0aCkge1xuICAgICAgICByZXR1cm4gcm93cztcbiAgICAgIH1cbiAgICAgIGlmIChkZWJ1ZykgY29uc29sZS5pbmZvKFwiZ2V0R3JvdXBlZFJvd3NcIik7XG4gICAgICAvLyBGaW5kIHRoZSBjb2x1bW5zIHRoYXQgY2FuIG9yIGFyZSBhZ2dyZWdhdGluZ1xuXG4gICAgICAvLyBVc2VzIGVhY2ggY29sdW1uIHRvIGFnZ3JlZ2F0ZSByb3dzIGludG8gYSBzaW5nbGUgdmFsdWVcbiAgICAgIGNvbnN0IGFnZ3JlZ2F0ZVJvd3NUb1ZhbHVlcyA9IHJvd3MgPT4ge1xuICAgICAgICBjb25zdCB2YWx1ZXMgPSB7fTtcbiAgICAgICAgY29sdW1ucy5mb3JFYWNoKGNvbHVtbiA9PiB7XG4gICAgICAgICAgY29uc3QgY29sdW1uVmFsdWVzID0gcm93cy5tYXAoZCA9PiBkLnZhbHVlc1tjb2x1bW4uaWRdKTtcbiAgICAgICAgICBsZXQgYWdncmVnYXRlID1cbiAgICAgICAgICAgIHVzZXJBZ2dyZWdhdGlvbnNbY29sdW1uLmFnZ3JlZ2F0ZV0gfHxcbiAgICAgICAgICAgIGFnZ3JlZ2F0aW9uc1tjb2x1bW4uYWdncmVnYXRlXSB8fFxuICAgICAgICAgICAgY29sdW1uLmFnZ3JlZ2F0ZTtcbiAgICAgICAgICBpZiAodHlwZW9mIGFnZ3JlZ2F0ZSA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICB2YWx1ZXNbY29sdW1uLmlkXSA9IGFnZ3JlZ2F0ZShjb2x1bW5WYWx1ZXMsIHJvd3MpO1xuICAgICAgICAgIH0gZWxzZSBpZiAoYWdncmVnYXRlKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXG4gICAgICAgICAgICAgIGBJbnZhbGlkIGFnZ3JlZ2F0ZSBcIiR7YWdncmVnYXRlfVwiIHBhc3NlZCB0byBjb2x1bW4gd2l0aCBJRDogXCIke1xuICAgICAgICAgICAgICAgIGNvbHVtbi5pZFxuICAgICAgICAgICAgICB9XCJgXG4gICAgICAgICAgICApO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB2YWx1ZXNbY29sdW1uLmlkXSA9IGNvbHVtblZhbHVlc1swXTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgICByZXR1cm4gdmFsdWVzO1xuICAgICAgfTtcblxuICAgICAgLy8gUmVjdXJzaXZlbHkgZ3JvdXAgdGhlIGRhdGFcbiAgICAgIGNvbnN0IGdyb3VwUmVjdXJzaXZlbHkgPSAocm93cywgZ3JvdXBCeSwgZGVwdGggPSAwKSA9PiB7XG4gICAgICAgIC8vIFRoaXMgaXMgdGhlIGxhc3QgbGV2ZWwsIGp1c3QgcmV0dXJuIHRoZSByb3dzXG4gICAgICAgIGlmIChkZXB0aCA+PSBncm91cEJ5Lmxlbmd0aCkge1xuICAgICAgICAgIHJldHVybiByb3dzO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gR3JvdXAgdGhlIHJvd3MgdG9nZXRoZXIgZm9yIHRoaXMgbGV2ZWxcbiAgICAgICAgbGV0IGdyb3VwZWRSb3dzID0gT2JqZWN0LmVudHJpZXMoZ3JvdXBCeUZuKHJvd3MsIGdyb3VwQnlbZGVwdGhdKSkubWFwKFxuICAgICAgICAgIChbZ3JvdXBCeVZhbCwgc3ViUm93c10sIGluZGV4KSA9PiB7XG4gICAgICAgICAgICAvLyBSZWN1cnNlIHRvIHN1YiByb3dzIGJlZm9yZSBhZ2dyZWdhdGlvblxuICAgICAgICAgICAgc3ViUm93cyA9IGdyb3VwUmVjdXJzaXZlbHkoc3ViUm93cywgZ3JvdXBCeSwgZGVwdGggKyAxKTtcblxuICAgICAgICAgICAgY29uc3QgdmFsdWVzID0gYWdncmVnYXRlUm93c1RvVmFsdWVzKHN1YlJvd3MpO1xuXG4gICAgICAgICAgICBjb25zdCByb3cgPSB7XG4gICAgICAgICAgICAgIGdyb3VwQnlJRDogZ3JvdXBCeVtkZXB0aF0sXG4gICAgICAgICAgICAgIGdyb3VwQnlWYWwsXG4gICAgICAgICAgICAgIHZhbHVlcyxcbiAgICAgICAgICAgICAgc3ViUm93cyxcbiAgICAgICAgICAgICAgZGVwdGgsXG4gICAgICAgICAgICAgIGluZGV4XG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgcmV0dXJuIHJvdztcbiAgICAgICAgICB9XG4gICAgICAgICk7XG5cbiAgICAgICAgcmV0dXJuIGdyb3VwZWRSb3dzO1xuICAgICAgfTtcblxuICAgICAgLy8gQXNzaWduIHRoZSBuZXcgZGF0YVxuICAgICAgcmV0dXJuIGdyb3VwUmVjdXJzaXZlbHkocm93cywgZ3JvdXBCeSk7XG4gICAgfSxcbiAgICBbcm93cywgZ3JvdXBCeSwgY29sdW1ucywgbWFudWFsR3JvdXBCeV1cbiAgKTtcblxuICByZXR1cm4ge1xuICAgIC4uLmFwaSxcbiAgICByb3dzOiBncm91cGVkUm93c1xuICB9O1xufTtcbiJdfQ==